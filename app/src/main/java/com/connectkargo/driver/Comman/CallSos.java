package com.connectkargo.driver.Comman;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.app.ActivityCompat;

import com.connectkargo.driver.Others.ErrorDialogClass;


public class CallSos
{
    Context context;
    String SosMobNo;
    ErrorDialogClass errorDialogClass;

    public CallSos(Context context)
    {
        this.context  = context;
        errorDialogClass = new ErrorDialogClass(context);
        SosMobNo = Constants.SosNumber;
        callSos(SosMobNo);
    }

    public void callSos(String SosMobNumber)
    {
//        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        int simState = telMgr.getSimState();

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + SosMobNumber));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 3);
        }
        else
        {
            context.startActivity(intent);
        }

//        switch (simState) {
//            case TelephonyManager.SIM_STATE_ABSENT:
//                errorDialogClass.showDialog("Sim card not available", context.getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
//                errorDialogClass.showDialog("Sim state network locked", context.getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
//                errorDialogClass.showDialog("Sim state pin required", context.getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
//                errorDialogClass.showDialog("Sim state puk required", context.getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_READY:
//
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:" + SosMobNumber));
//                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
//                {
//                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 3);
//                }
//                else
//                {
//                    context.startActivity(intent);
//                }
//                break;
//            case TelephonyManager.SIM_STATE_UNKNOWN:
//                errorDialogClass.showDialog("Sim state unknown", context.getString(R.string.info_message));
//                break;
//        }
    }
}
