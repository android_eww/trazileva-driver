package com.connectkargo.driver.Comman;



public class Constants {

    public static String newgpsLatitude = "0";//"50.605846";
    public static String newgpsLongitude = "0";//"7.206919";

    public static String oldgpsLatitude = "0";
    public static String oldgpsLongitude = "0";

    public static String oldTime = "";
    public static String newTime = "";

    public static double newgpsLatitude_pickUp = 0;//"7.206919";
    public static double newgpsLongitude_pickUp = 0;//"7.206919";
    public static String address_pickUp = "";

    public static String SosNumber = "0772506506";

    public static final String INTENT_BOOKING_TYPE = "bookingType";
    public static final String INTENT_BOOKING_ID = "bookingId";
    public static final String INTENT_RECEIVER_ID = "receiverId";
    public static final String INTENT_USER_NAME = "userName";
    public static final String INTENT_RECEIVER_NAME = "receiverName";

}
