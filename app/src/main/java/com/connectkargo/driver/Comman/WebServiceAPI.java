package com.connectkargo.driver.Comman;


public interface WebServiceAPI {

  //Developing Base Url
  /*String BASE_URL = "http://3.6.225.107/Drvier_Api/";
  String BASE_URL_IMAGE  = "http://3.6.225.107/";*/
  String BASE_URL = "https://connectkargo.com/Drvier_Api/";
  String BASE_URL_IMAGE  = "https://connectkargo.com/";
  public static String HEADER_KEY = "key";
  public static String HEADER_VALUE = "Tranzileva#951";

  //Update Driver Location SOKET
  String SERVER_SOCKET_UPDATE_DRIVER_LOACATION_KEY = "UpdateDriverLatLong";
  String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_ID = "DriverId";
  String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LAT = "Lat";
  String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LNG = "Long";
  String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_APP_VERSION = "Version";
  String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_TOKEN = "Token";

  //Update Driver Location SOKET pickup
  String SERVER_SOCKET_EMIT_TRACK_TRIP_PICK_UP = "TrackTripPickup";
  String SERVER_SOCKET_OBJ_TRACK_TRIP_PICK_UP_PASSENGER_ID = "PassengerId";
  String SERVER_SOCKET_OBJ_TRACK_TRIP_PICK_UP_LAT = "PickupLat";
  String SERVER_SOCKET_OBJ_TRACK_TRIP_PICK_UP_LONG = "PickupLng";

  //Update Driver Location SOKET DropOff
  String SERVER_SOCKET_EMIT_TRACK_TRIP_DROP_OFF = "TrackTripDropoff";
  String SERVER_SOCKET_OBJ_TRACK_TRIP_DROP_OFF_PASSENGER_ID = "PassengerId";
  String SERVER_SOCKET_OBJ_TRACK_TRIP_DROP_OFF_LAT = "DropoffLat";
  String SERVER_SOCKET_OBJ_TRACK_TRIP_DROP_OFF_LONG = "DropoffLng";

  ////// AriveBookingRequest
  String SERVER_SOCKET_DRIVER_ARRIVE_BOOKING_REQUEST = "AriveBookingRequest";

  ////// Reject Booking request //BookingId,DriverId
  String SERVER_SOCKET_DRIVER_REJECT_BOOKING_REQUEST = "ForwardBookingRequestToAnother";
  String SOCKET_DRIVER_REJECT_BOOKING_REQUEST_BOOKING_ID = "BookingId";
  String SOCKET_DRIVER_REJECT_BOOKING_REQUEST_DRIVER_ID = "DriverId";

  ////// Accept booking request //BookingId,DriverId
  String SERVER_SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST = "AcceptBookingRequest";
  String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_BOOKING_ID = "BookingId";
  String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_DRIVER_ID = "DriverId";
  String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT = "Lat";
  String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG = "Long";


  ////Get booking details after booking request accepted
  String SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST = "BookingInfo";

  ////Pickup passenger by driver // BookingId,DriverId
  String SERVER_SOCKET_PICKUP_PASSENGER = "PickupPassenger";
  String SOCKET_PICKUP_PASSENGER_BOOKING_ID = "BookingId";
  String SOCKET_PICKUP_PASSENGER_DRIVER_ID = "DriverId";

  /////Start hold trip //BookingId
  String SERVER_SOCKET_HOLD_PASSENGER = "StartHoldTrip";
  String SOCKET_HOLD_PASSENGER_BOOKING_ID = "BookingId";

  /////End hold trip //BookingId
  String SERVER_SOCKET_END_HOLD_PASSENGER = "EndHoldTrip";
  String END_SOCKET_HOLD_PASSENGER_BOOKING_ID = "BookingId";


  /////Received cancel trip notification to driver
  String SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION= "DriverCancelTripNotification";

  ///////////Receive book later booking request
  String SERVER_SOCKET_ARIVE_ADVANCE_BOOKING_REQUEST = "AriveAdvancedBookingRequest";


  ////// Accept book later booking request
  String SERVER_SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST = "AcceptAdvancedBookingRequest";
  String SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_BOOKING_ID = "BookingId";
  String SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_DRIVER_ID = "DriverId";


  //////Reject book later booking request
  String SERVER_SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST = "ForwardAdvancedBookingRequestToAnother";
  String SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_BOOKING_ID = "BookingId";
  String SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_DRIVER_ID = "DriverId";

  ////Pickup passenger by driver in book later request
  String SERVER_SOCKET_PICKUP_PASSENGER_BOOK_LATER = "AdvancedBookingPickupPassenger";
  String SOCKET_PICKUP_PASSENGER_BOOK_LATER_BOOKING_ID = "BookingId";
  String SOCKET_PICKUP_PASSENGER_BOOK_LATER_DRIVER_ID = "DriverId";

  /////Received cancel trip notification to driver
  String SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION_LATER = "AdvancedBookingDriverCancelTripNotification";


  /////Start Start hold trip later
  String SERVER_SOCKET_HOLD_PASSENGER_lATER = "AdvancedBookingStartHoldTrip";
  String SOCKET_HOLD_PASSENGER_lATER_BOOKING_ID = "BookingId";

  /////End hold trip later
  String SERVER_SOCKET_END_HOLD_PASSENGER_LATER = "AdvancedBookingEndHoldTrip";
  String END_SOCKET_HOLD_PASSENGER_LATER_BOOKING_ID = "BookingId";

  ////Get booking details after booking request accepted later
  String SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST_LATER = "AdvancedBookingInfo";

  ///Notify passenger befor start trip
  String SERVER_SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP = "NotifyPassengerForAdvancedTrip";
  String SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_BOOKING_ID = "BookingId";
  String SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_DRIVER_ID = "DriverId";


  /// New book later request arrived notification
  String SERVER_SOCKET_BOOK_LATER_DRIVER_NOTIFY = "BookLaterDriverNotify";

  /// New book later request arrived notification
  String SOCKET_SESSION_ERROR = "SessionError";

  /////
  String SOCKET_START_TRIP_ERROR = "StartTripTimeError";






  ///lOGIN :: Username,Password,Lat,Lng
  String WEB_SERVICE_LOGIN =  BASE_URL + "Login";
  String LOGIN_PARAM_DEVICE_TYPE = "DeviceType";
  String LOGIN_PARAM_TOKEN = "Token";
  String LOGIN_PARAM_USERNAME = "Username";
  String LOGIN_PARAM_PASSWORD = "Password";
  String LOGIN_PARAM_LATITUDE = "Lat";
  String LOGIN_PARAM_LONGITUDE = "Lng";
  String LOGIN_PARAM_DEVICE_ID = "Token";


  ////FORGOT PASSWORD : Email
  String WEB_SERVICE_FORGOT_PASSWORD =  BASE_URL + "ForgotPassword";
  String FORGOT_PASSWORD_PARAM_MOB_NO = "MobileNo";


  ///////OtpForRegister
  String WEB_SERVICE_OTP_FOR_REGISTER =  BASE_URL + "OtpForRegister";
  String OTP_FOR_REGISTER_PARAM_EMAIL = "Email";
  String REGISTER_PARAM_DEVICE_ID = "Token";


  ////// Vehical Model list
  String WEB_SERVICE_FOR_VEHICLE_MODEL_LIST = BASE_URL + "TaxiModel/";


  ///Driver register ::
//  String WEB_SERVICE_OTP_FOR_DRIVER_REGISTER = BASE_URL + "Register";
  String WEB_SERVICE_OTP_FOR_DRIVER_REGISTER = BASE_URL + "New_Register";
  String DRIVER_REGISTER_PARAM_DEVICE_TYPE = "DeviceType";
  String DRIVER_REGISTER_PARAM_TOKEN = "Token";
  String DRIVER_REGISTER_PARAM_EMAIL = "Email";
  String DRIVER_REGISTER_PARAM_MOBILE_NUMBER = "MobileNo";
  String DRIVER_REGISTER_PARAM_FULL_NAME = "Fullname";
  String DRIVER_REGISTER_PARAM_AGENT_CODE = "AgentCode";
  String DRIVER_REGISTER_PARAM_DOB = "DOB";
  String DRIVER_REGISTER_PARAM_GENDER = "Gender";
  String DRIVER_REGISTER_PARAM_PASSWORD = "Password";
  String DRIVER_REGISTER_PARAM_ADDRESS = "Address";
  String DRIVER_REGISTER_PARAM_REFERRAL_CODE = "ReferralCode";
  String DRIVER_REGISTER_PARAM_LATITUDE = "Lat";
  String DRIVER_REGISTER_PARAM_LONGITUDE = "Lng";
  String DRIVER_REGISTER_PARAM_VEHICLE_CLASS = "VehicleClass";
  String DRIVER_REGISTER_PARAM_DRIVER_LICENCE_FRONT = "DriverLicenseFront";
  String DRIVER_REGISTER_PARAM_DRIVER_LICENCE_BACK = "DriverLicenseBack";
  String DRIVER_REGISTER_PARAM_CAR_REGISTRATION = "CarRegistrationCertificate";
  String DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY = "AccreditationCertificate";
  String DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE = "VehicleInsuranceCertificate";
  String DRIVER_REGISTER_PARAM_GOVERNMENT_ID_EXPIRY = "GovIdExpire";
  String DRIVER_REGISTER_PARAM_GOVERNMENT_ID_FRONT = "FrontGovIdCard";
  String DRIVER_REGISTER_PARAM_GOVERNMENT_ID_BACK = "BackGovIdCard";
  String DRIVER_REGISTER_PARAM_CRIMINAL_CERTI = "CriminalCertificate";
  String DRIVER_REGISTER_PARAM_ADDRESS_PROOF = "AddressProof";
  String DRIVER_REGISTER_PARAM_DAA_MOBILE = "DriverDadMobile";
  String DRIVER_REGISTER_PARAM_WIFE_MOBILE = "DriverWifeMobile";
  String DRIVER_REGISTER_PARAM_PDF1 = "DocPage1";
  String DRIVER_REGISTER_PARAM_PDF2 = "DocPage2";
  String DRIVER_REGISTER_PARAM_PDF3 = "DocPage3";

  String DRIVER_REGISTER_PARAM_COMPANY_ID = "CompanyId";
  String DRIVER_REGISTER_PARAM_SUB_URB = "Suburb";
  String DRIVER_REGISTER_PARAM_CITY = "City";
  String DRIVER_REGISTER_PARAM_COUNTRY = "Country";
  String DRIVER_REGISTER_PARAM_STATE = "State";
  String DRIVER_REGISTER_PARAM_ZIPCODE = "Zipcode";
  String DRIVER_REGISTER_PARAM_DRIVER_IMAGE = "DriverImage";
  String DRIVER_REGISTER_PARAM_DRIVER_LICENCE_EXPIRY = "DriverLicenseExpire";

  String DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY_EXPIRY = "AccreditationCertificateExpire";
  String DRIVER_REGISTER_PARAM_BANK_ACCOUNT_HOLDER_NALE = "AccountHolderName";
  String DRIVER_REGISTER_PARAM_BANK_NAME = "BankName";
  String DRIVER_REGISTER_PARAM_BANK_BRANCH = "BSB";
  String DRIVER_REGISTER_PARAM_BANK_ACCOUNT_NO = "BankAcNo";
  String DRIVER_REGISTER_PARAM_BSB = "BSB";
  String DRIVER_REGISTER_PARAM_ABN = "ABN";
  String DRIVER_REGISTER_PARAM_SERVICE_DESCRIPTION = "Description";
  String DRIVER_REGISTER_PARAM_VEHICLE_COLOR = "VehicleColor";
  String DRIVER_REGISTER_PARAM_VEHICLE_RIGISTRATION_NO = "VehicleRegistrationNo";
  String DRIVER_REGISTER_PARAM_REGISTRATION_CERTY_EXPIRY = "RegistrationCertificateExpire";
  String DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE_CERTY_EXPIRE = "VehicleInsuranceCertificateExpire";
  String DRIVER_REGISTER_PARAM_VEHICLE_IMAGE = "VehicleImage";
  String DRIVER_REGISTER_PARAM_VEHICLE_IMAGE_LEFT = "VehicleLeftImage";
  String DRIVER_REGISTER_PARAM_VEHICLE_IMAGE_RIGHT = "VehicleRightImage";
  String DRIVER_REGISTER_PARAM_COMPANY_MODEL = "CompanyModel";


  ////////Change driver Status //DriverId, Lat, Lng
  String WEB_SERVICE_CHANGE_DRIVER_SHIFT_STATUS = BASE_URL +"ChangeDriverShiftStatus/";//DriverId
  String CHANGE_DRIVER_SHIFT_STATUS_PARAM_USER_ID = "DriverId";
  String CHANGE_DRIVER_SHIFT_STATUS_PARAM_LAT = "Lat";
  String CHANGE_DRIVER_SHIFT_STATUS_PARAM_LNG = "Lng";


  ///// http://54.206.55.185/web/Drvier_Api/GetDriverProfile/driver_id
  String WEB_SERVICE_GET_DRIVER_PROFILE = BASE_URL +"GetDriverProfile/";//driver_id

  ///// Company list
  String WEB_SERVICE_GET_COMPANY_ID_LIST = BASE_URL +"Company";



  /////Update Prfile///UpdateDriverBasicInfo
    /*DriverId,CompanyId,Fullname,Gender,Address,Suburb,Zipcode,City,State,Country,DriverImage*/
  String WEB_SERVICE_UPDATE_DRIVER_BASIC_INFO = BASE_URL +"UpdateDriverBasicInfo";//driver_id
  String UPDATE_DRIVER_BASIC_INFO_PARAM_DIVER_ID = "DriverId";
  String UPDATE_DRIVER_BASIC_INFO_PARAM_FULL_NAME = "Fullname";
  String UPDATE_DRIVER_BASIC_INFO_PARAM_MOB_NO = "MobileNo";
  String UPDATE_DRIVER_BASIC_INFO_PARAM_DOB = "DOB";
  String UPDATE_DRIVER_BASIC_INFO_PARAM_GENDER = "Gender";
  String UPDATE_DRIVER_BASIC_INFO_PARAM_ADDRESS = "Address";
  String UPDATE_DRIVER_BASIC_INFO_PARAM_ZIPCODE = "Zipcode";
  String UPDATE_DRIVER_BASIC_INFO_PARAM_DRIVER_IMAGE = "DriverImage";



  //Update All driver documents
  String WEB_SERVICE_UPDATE_DOCUMENT = BASE_URL +"UpdateDocument";//driver_id
  String WEB_SERVICE_UPDATE_DOCS= BASE_URL +"UpdateDocs";
  String UPDATE_DOCUMENT_PARAM_DIVER_ID = "DriverId";
  String UPDATE_DOCUMENT_PARAM_DIVER_LICENCE_BACK = "DriverLicenseBack";
  String UPDATE_DOCUMENT_PARAM_DIVER_LICENCE_FRONT = "DriverLicenseFront";
  String UPDATE_DOCUMENT_PARAM_DIVER_VEHICLE_INSURANCE_CERY = "VehicleInsuranceCertificate";
  String UPDATE_DOCUMENT_PARAM_DIVER_ACCREDITATION_CERTY = "AccreditationCertificate";
  String UPDATE_DOCUMENT_PARAM_DIVER_REGISTRATION_CERTY = "CarRegistrationCertificate";
  String UPDATE_DOCUMENT_PARAM_VEHICLE_IMAGE = "VehicleImage";
  String UPDATE_DOCUMENT_PARAM_LICENSE_EXPIRY = "DriverLicenseExpire";
  String UPDATE_DOCUMENT_PARAM_INSURANCE_CERTY_EXPIRY = "VehicleInsuranceCertificateExpire";
  String UPDATE_DOCUMENT_PARAM_ACCREDITATION_EXPIRY = "AccreditationCertificateExpire";
  String UPDATE_DOCUMENT_PARAM_REGISTERATION_CERTY_EXPIRY = "RegistrationCertificateExpire";


  ////Update Driver Car Info /*DriverId,VehicleClass,VehicleColor,CompanyModel,VehicleRegistrationNo*/
  String WEB_SERVICE_UPDATE_DRIVER_CAR_INFO = BASE_URL +"UpdateVehicleInfo";//driver_id
  String UPDATE_DRIVER_CAR_INFO_PARAM_DIVER_ID = "DriverId";
  String UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_REGISTRATION_NO = "VehicleRegistrationNo";
  String UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_CLASS = "VehicleClass";
  String UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_MODEL = "CompanyModel";




  //Update Driver bank info // DriverId,AccountHolderName,BankName,BankAcNo,BSB,ABN
  String WEB_SERVICE_UPDATE_BANK_INFO = BASE_URL +"UpdateBankInfo";//driver_id
  String UPDATE_BANK_INFO_PARAM_DRIVER_ID = "DriverId";
  String UPDATE_BANK_INFO_PARAM_ACCOUNT_HOLDER_NAME = "AccountHolderName";
  String UPDATE_BANK_INFO_PARAM_BANK_NAME = "BankName";
  String UPDATE_BANK_INFO_PARAM_BANK_AC_NO = "BankAcNo";
  String UPDATE_BANK_INFO_PARAM_BSB = "BSB";


  /////Completed trip successfully//BookingId,TripDistance,NightFareApplicable,PromoCode,PaymentType,PaymentStatus,TransactionId,TollFee
  String WEB_SERVICE_COMPLETE_TRIP = BASE_URL +"SubmitCompleteBooking";
  String COMPLETE_TRIP_PARAM_BOOKING_ID = "BookingId";
  String COMPLETE_TRIP_PARAM_TRIP_DISTANCE = "TripDistance";
  String COMPLETE_TRIP_PARAM_NIGHT_FARE_APPLICABLE = "NightFareApplicable";///"0"
  String COMPLETE_TRIP_PARAM_PROMO_CODE = "PromoCode"; //" "
  String COMPLETE_TRIP_PARAM_PAYMENT_TYPE = "PaymentType";// cash , wallet, online
  String COMPLETE_TRIP_PARAM_PAYMENT_STATUS = "PaymentStatus"; //" "
  String COMPLETE_TRIP_PARAM_TRANSACTION_ID = "TransactionId"; // " "
  String COMPLETE_TRIP_PARAM_TOLL_FEE = "TollFee"; //"0"
  String COMPLETE_TRIP_PARAM_DROP_OFF_LOCATION = "DropoffLocation"; //"0"
  String COMPLETE_TRIP_PARAM_LAT = "Lat"; //"0"
  String COMPLETE_TRIP_PARAM_LONG = "Long"; //"0"
  String COMPLETE_TRIP_PARAM_WAITING_TIME = "WaitingTime"; //"0"
  String COMPLETE_TRIP_PARAM_PENDING = "Pending"; //"0"


  /////COMPLETE TRIP OF BOOK LATER
  String WEB_SERVICE_COMPLETE_TRIP_LATER = BASE_URL + "SubmitCompleteAdvancedBooking";

  /////COMPLETE TRIP CODE
  String WEB_SERVICE_COMPLETE_TRIP_CODE = BASE_URL + "SendVerificationCode";
  String PARAM_BOOKING_TYPE = "BookingType";

  ///change password
  String WEB_SERVICE_CHANGE_PASSWORD = BASE_URL + "ChangePassword";
  String CHANGE_PASSWORD_PARAM_DRIVER_ID = "DriverId";
  String CHANGE_PASSWORD_PARAM_PASSWORD = "Password";
  String CHANGE_PASSWORD_PARAM_OLD_PASSWORD = "OldPassword";

  //Send verification code for complete trip
  String WEB_SERVICE_SEND_VERIFICATION_CODE = BASE_URL + "SendVerificationCode";
  String SEND_VERIFICATION_CODE_PARAM_BOOKING_ID = "BookingId";
  String SEND_VERIFICATION_CODE_PARAM_BOOKING_TYPE = "BookingType";

  ///////get list of Distpatch jb
//    String WEB_SERVICE_DISPATCH_JOB_LIST = BASE_URL + "DispatchJob/";

  /////List of my dispatch job (For dispatch job menu)
  String WEB_SERVICE_MY_DISPATCH_JOB_LIST = BASE_URL + "MyDispatchJob/";

  ////List of nearby upcoming dipatch job(for future booking)
  String WEB_SERVICE_FUTURE_BOOKING_LIST = BASE_URL + "FutureBooking/";

  ////Accept dispatch job request
  String WEB_SERVICE_ACCEPT_FUTURE_JOB_REQUEST = BASE_URL + "AcceptDispatchJobRequest/";

  ///////Booking history
  String WEB_SERVICE_BOOKING_HISTORY_LIST = BASE_URL + "BookingHistory/";

  String WEB_SERVICE_PENDING_BOOKING_HISTORY_LIST = BASE_URL + "PendingJobs/";

  String WEB_SERVICE_PAST_BOOKING_HISTORY_LIST = BASE_URL + "PastJobs/";

  /*Lat:23.545454
  Lng:72.564652
  DriverId:5*/
  String WEB_SERVICE_SOS = BASE_URL + "SOS";



  /////Driver Logout : /DriverId
  String WEB_SERVICE_DRIVER_LOGOUT = BASE_URL + "Logout/";


  ////Submit book now for dispatch job
  /// ,,,,,,PaymentType(collect,account)
  String WEB_SERVICE_DISPATCH_JOB_BOOK_NOW = BASE_URL + "SubmitBookNowByDispatchJob";
  String DISPATCH_JOB_BOOK_NOW_PARAM_DRIVER_ID = "DriverId";
  String DISPATCH_JOB_BOOK_NOW_PARAM_MODEL_ID = "ModelId";
  String DISPATCH_JOB_BOOK_NOW_PARAM_PICK_UP_LOC = "PickupLocation";
  String DISPATCH_JOB_BOOK_NOW_PARAM_DROP_OFF_LOC = "DropoffLocation";
  String DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_NAME = "PassengerName";
  String DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_CONTACT_NO = "PassengerContact";
  String DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_EMAIL = "PassengerEmail";
  String DISPATCH_JOB_BOOK_NOW_PARAM_FARE_AMOUNT = "FareAmount";
  String DISPATCH_JOB_BOOK_NOW_PARAM_PAYMENT_TYPE = "PaymentType";

  ///Submit book later for dispatch job
  String WEB_SERVICE_DISPATCH_JOB_BOOK_LATER = BASE_URL + "SubmitBookLaterByDispatchJob";
  String DISPATCH_JOB_BOOK_LATER_PARAM_PICK_UP_DATE_TIME = "PickupDateTime";


  //////Current trip FLOW
  String WEB_SERVICE_CURRENT_TRIP_FLOW = BASE_URL + "CurrentBooking/";

  ////Add new card ///DriverId,CardNo,Cvv,Expiry,Alias (CarNo : 4444555511115555,Expiry:09/20)
  String WEB_SERVICE_ADD_NEW_CARD = BASE_URL + "AddNewCard";
  String ADD_NEW_CARD_PARAM_DRIVER_ID = "DriverId";
  String ADD_NEW_CARD_PARAM_CARD_NO = "CardNo";
  String ADD_NEW_CARD_PARAM_CVV = "Cvv";
  String ADD_NEW_CARD_PARAM_EXPIRY = "Expiry";
  String ADD_NEW_CARD_PARAM_ALIAS = "Alias";


  /////Card listing
  String WEB_SERVICE_DABITCARD_LIST = BASE_URL + "Cards/";//DriverId


  /////Add Money ////DriverId,Amount,CardId
  String WEB_SERVICE_ADD_MONEY = BASE_URL + "AddMoney";
  String ADD_MONEY_PARAM_DRIVER_ID = "DriverId";
  String ADD_MONEY_PARAM_AMOUNT = "Amount";
  String ADD_MONEY_PARAM_CARD_ID = "CardId";


  /////Transaction History
  String WEB_SERVICE_TRANSACTION_HISTORY = BASE_URL + "TransactionHistory/";///DriverId


  //////Tickpay ///DriverId,Name,Amount,CardNo,CVV,Expiry
  String WEB_SERVICE_TICKPAY = BASE_URL + "Tickpay";
  String TICKPAY_PARAM_DRIVER_ID = "DriverId";
  String TICKPAY_PARAM_DRIVER_NAME = "Name";
  String TICKPAY_PARAM_AMOUNT = "Amount";
  String TICKPAY_PARAM_CARD_NO = "CardNo";
  String TICKPAY_PARAM_CVV = "CVV";
  String TICKPAY_PARAM_CARD_EXPIRY = "Expiry";
  String TICKPAY_PARAM_ABN = "ABN";


  // get qr code detials //QRCode
  String WEB_SERVICE_QR_CODE_DETAILS = BASE_URL + "QRCodeDetails/";
  String PARAM_QR_CODE = "QRCode";

  // Send Money   //QRCode,SenderId,Amount
  String WEB_SERVICE_SEND_MONEY = BASE_URL + "SendMoney";
  String SEND_MONEY_PARAM_QRCODE = "QRCode";
  String SEND_MONEY_PARAM_SENDER_ID = "SenderId";
  String SEND_MONEY_PARAM_AMOUNT = "Amount";



  ////Remove Card
  String WEB_SERVICE_REMOVE_CARD = BASE_URL + "RemoveCard/";///DriverId/CardId

  //Get Tickpay Rate
  String WEB_SERVICE_GET_TICKPAY_RATE = BASE_URL + "GetTickpayRate";

  //send invoice
  String WEB_SERVICE_TICK_PAY_INVOICE = BASE_URL + "TickpayInvoice";
  String PARAM_TICKPAY_ID = "TickpayId";
  String PARAM_INVOICE_TYPE = "InvoiceType";
  String PARAM_INVOICE_TYPE_EMAIL  = "Email";
  String PARAM_INVOICE_TYPE_SMS = "SMS";
  String PARAM_CUSTOMER_NAME = "CustomerName";
  String PARAM_NOTES = "Notes";
  String PARAM_AMOUNT = "Amount";
  String PARAM_EMAIL = "Email";
  String PARAM_MOBILE_NO= "MobileNo";

  ////// App settings
  String WEB_SERVICE_APP_SETTING = BASE_URL + "Init/";///version/app_type
  String APP_SETTING_APP_TYPE = "/AndroidDriver";///version/app_type


  //////Weekly Earning /DriverId
  String WEB_SERVICE_WEEKLY_EARNING = BASE_URL + "WeeklyEaring/";

  /////Rating and comment   ////BookingId,Rating,Comment,BookingType(BookNow,BookLater)
  String WEB_SERVICE_RATING_AND_COMMENT = BASE_URL + "ReviewRating";
  String PARAM_RATING_AND_COMMENT_BOOKING_ID = "BookingId";
  String PARAM_RATING_AND_COMMENT_RATING = "Rating";
  String PARAM_RATING_AND_COMMENT_DRESCRIPTION = "Comment";
  String PARAM_RATING_AND_COMMENT_BOOKING_TYPE = "BookingType";

  ////Transfer money wallet to bank ///DriverId,Amount,HolderName,ABN,BankName,BSB,AccountNo
  String WEB_SERVICE_TRANSFERT_TO_BANK = BASE_URL + "TransferToBank";
  String PARAM_TRANSFERT_TO_BANK_DRIVER_ID = "DriverId";
  String PARAM_TRANSFERT_TO_BANK_AMOUNT = "Amount";
  String PARAM_TRANSFERT_TO_BANK_HOLDER_NAME = "HolderName";
  String PARAM_TRANSFERT_TO_BANK_ABN = "ABN";
  String PARAM_TRANSFERT_TO_BANK_BANK_NAME = "BankName";
  String PARAM_TRANSFERT_TO_BANK_BSB = "BSB";
  String PARAM_TRANSFERT_TO_BANK_ACCOUNT_NO = "AccountNo";

  ///GET ASTIMATE FARE//PickupLocation,DropoffLocation,ModelId
  String WEB_SERVICE_GET_ESTIMATE_FARE = BASE_URL + "GetEstimateFare";
  String PARAM_GET_ESTIMATE_FARE_PICKUP_LOCATION = "PickupLocation";
  String PARAM_GET_ESTIMATE_FARE_DROPOFF_LOCATION = "DropoffLocation";
  String PARAM_GET_ESTIMATE_FARE_MODEL_ID = "ModelId";

  //////get Meter screen estimate fare on taking trip ModelId,EstimateKM
  String WEB_SERVICE_GET_FARE_FOR_METER = BASE_URL + "GetEstimateFareWithKM";
  String PARAM_GET_FARE_FOR_METER_MODEL_ID = "ModelId";
  String PARAM_GET_FARE_FOR_METER_ESTIMATE_KM = "EstimateKM";
  String PARAM_GET_FARE_FOR_WAITING_MIN_COST = "WaitingCost";


  ////get model detail
  String WEB_SERVICE_GET_MODEL_DETAIL = BASE_URL + "TaxiModelForPricing";


  /////  CancelTrip//=== DriverId,BookingId,BookingType
  String WEB_SERVICE_CANCEL_TRIP = BASE_URL + "CancelTrip";
  String PARAM_CANCEL_TRIP_DRIVER_ID = "DriverId";
  String PARAM_CANCEL_TRIP_BOOKING_ID = "BookingId";
  String PARAM_CANCEL_TRIP_BOOKING_TYPE = "BookingType";

  String WEB_SERVICE_TRIP_TP_DESTINATION = BASE_URL + "ManageTripToDestination";
  String PARAM_TRIP_TO_DESTINATION_DRIVER_ID = "DriverId";
  String PARAM_TRIP_TO_DESTINATION_STATUS = "Status";
  String PARAM_TRIP_TO_DESTINATION_LOCATION = "Location";
  String PARAM_TRIP_TO_DESTINATION_LAT = "Lat";
  String PARAM_TRIP_TO_DESTINATION_LONG = "Lng";

  //// DriverId , PickupLocation , DropoffLocation , ModelId,ModelName , GrandTotal , KM , BaseFare, WaitingTime, WaitingCost, NightCharge
  String WEB_SERVICE_SEND_METER_TRIP_DATA = BASE_URL + "PrivateMeterBooking";
  String PARAM_SEND_METER_TRIP_DATA_DRIVER_ID = "DriverId";
  String PARAM_SEND_METER_TRIP_DATA_PICK_UP_LOCATION = "PickupLocation";
  String PARAM_SEND_METER_TRIP_DATA_DROP_OFF_LOACTION = "DropoffLocation";
  String PARAM_SEND_METER_TRIP_DATA_MODEL_ID = "ModelId";
  String PARAM_SEND_METER_TRIP_DATA_MODEL_NAME = "ModelName";
  String PARAM_SEND_METER_TRIP_DATA_GRAND_TOTAL = "GrandTotal";
  String PARAM_SEND_METER_TRIP_DATA_KM = "KM";
  String PARAM_SEND_METER_TRIP_DATA_BASE_FARE = "BaseFare";
  String PARAM_SEND_METER_TRIP_DATA_WAITING_TIME = "WaitingTime";
  String PARAM_SEND_METER_TRIP_DATA_WAITING_COST = "WaitingCost";
  String PARAM_SEND_METER_TRIP_DATA_NIGHT_CHARGE = "NightCharge";
  String PARAM_SEND_METER_TRIP_DATA_MOBILE_NO = "MobileNo";

  ////  Change_ShareRide_Status   //DriverId
  String WEB_SERVICE_CHANGE_SHARE_RIDE_STATUS = BASE_URL + "ManageShareRideFlag/";

  String WEB_SERVICE_CHAT_LIST = BASE_URL + "chat_history/";
  String WEB_SERVICE_CHAT = BASE_URL + "chat";
  String WEB_SERVICE_CHAT_PARAM_BOOKING_ID = "BookingId";
  String WEB_SERVICE_CHAT_PARAM_BOOKING_TYPE = "BookingType";
  String WEB_SERVICE_CHAT_PARAM_SENDER_ID = "SenderId";
  String WEB_SERVICE_CHAT_PARAM_RECIEVER_ID = "ReceiverId";
  String WEB_SERVICE_CHAT_PARAM_MESSAGE = "Message";


  String WEB_HEATMAP_DATA = BASE_URL+"HighDemandArea";

  String WEB_SERVICE_MY_BID = BASE_URL+"BidList/";

  String WEB_SERVICE_OPEN_BID = BASE_URL + "DriverBid/";

  String WEB_SERVICE_BID_ACCEPT = BASE_URL+"BidAccept";
  String WEB_SERVICE_BID_ACCEPT_PASSENGERID = "PassengerId";
  String WEB_SERVICE_BID_ACCEPT_DRIVERID = "DriverId";
  String WEB_SERVICE_BID_ACCEPT_BIDID = "BidId";
  String WEB_SERVICE_BID_ACCEPT_BUDGET = "Budget";
  String WEB_SERVICE_BID_ACCEPT_STATUS = "Status";
  String WEB_SERVICE_BID_ACCEPT_NOTES = "Notes";

  String WEB_SERVICE_CHAT_HISTORY = BASE_URL + "ChatHistory";
  String WEB_BID_CHAT = BASE_URL + "BidChat";
  String WEB_BID_CHAT_HISTORY = BASE_URL + "BidChatHistory/";
  String PARAM_RECEIVERID = "ReceiverId";

  String PARAM_SENDER = "Sender";
  String PARAM_RECEIVER = "Receiver";
}
