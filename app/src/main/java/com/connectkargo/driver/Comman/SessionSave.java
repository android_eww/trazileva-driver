package com.connectkargo.driver.Comman;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * This common class to store the require data by using SharedPreferences.
 */
public class SessionSave
{
    public static void saveToken(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFRENCE_TOKEN, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }
    public static String getToken(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Comman.PREFRENCE_TOKEN, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }


    //Store data's into sharedPreference
    public static void saveUserSession(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFERENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }
    // Get Data's from SharedPreferences
    public static String getUserSession(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Comman.PREFERENCE_USER, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }
    //clear data's into sharedPreference
    public static void clearUserSession(Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFERENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }


    //Store data's into sharedPreference
    public static void saveMeterData(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFERENCE_METER, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }
    // Get Data's from SharedPreferences
    public static String getMeterData(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Comman.PREFERENCE_METER, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }
    //clear data's into sharedPreference
    public static void clearMeterData(Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFERENCE_METER, Activity.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }

}
