package com.connectkargo.driver.Comman;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Fragment.Map_Fragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public class LocationService extends Service implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public String TAG = "LocationService";
    private static final long INTERVAL = 1000 * 2;
    private static final long FASTEST_INTERVAL = 1000;
    private static final String NOTIFICATION_CHANNEL_ID = "Foreground service";
    private final IBinder mBinder = new LocalBinder();
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private double distance = 0;

    Location mCurrentLocation, lStart, lEnd;
    double speed;

    Map_Fragment map_fragment;

    public LocationService() {
    }

    @Override
    public void onCreate()
    {
        Log.e(TAG,"onCreate()");
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            startForeground(12345678, getNotification());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Notification getNotification()
    {
        Log.e(TAG,"getNotification()");

        NotificationChannel channel = new NotificationChannel("channel_01", "My Channel", NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
        Notification.Builder builder = new Notification.Builder(getApplicationContext(), "channel_01").setAutoCancel(true);
        return builder.build();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        Log.e(TAG,"onBind()");

        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        Log.e(TAG,"yyyyyyyyyyy SelectedPos=0 ");
        return mBinder;
    }

    protected void createLocationRequest()
    {
        Log.e(TAG,"createLocationRequest()");

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG,"onStartCommand()");
        return START_STICKY;
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        Log.e(TAG,"onConnected()");
        try
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
        catch (SecurityException e)
        {
            e.printStackTrace();
        }
    }

    protected void stopLocationUpdates()
    {
        Log.e(TAG,"stopLocationUpdates()");

        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
        distance = 0;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location)
    {
//        MainActivity.locate.dismiss();
        Log.e("LocationService","avooooooooooooooooooooo");
        mCurrentLocation = location;

        if (lStart == null)
        {
            lStart = mCurrentLocation;
            lEnd = mCurrentLocation;
        }
        else
        {
            lEnd = mCurrentLocation;
        }


        //Calling the method below updates the  live values of distance and speed to the TextViews.
        updateUI();
        //calculating the speed with getSpeed method it returns speed in m/s so we are converting it into kmph
        speed = location.getSpeed() * 18 / 5;
        if (Drawer_Activity.tv_speedTrip!=null)
        {
            Log.e("LocationService","intI : 111111 speedKmPerHour: "+speed);

            if (Double.isNaN(speed) || Double.isInfinite(speed))
            {
                speed = 0.0000;
                Drawer_Activity.tv_speedTrip.setText(speed+" km/hr");
            }
            else
            {
                String speed1 = String.format( "%.4f", speed);
                Drawer_Activity.tv_speedTrip.setText(speed1+" km/hr");
            }
        }
        else
        {
            Log.e("LocationService","intI : 111111 Drawer_Activity.tv_speedTrip null");
        }

        if(map_fragment != null)
        {
            map_fragment  = ((Map_Fragment) Drawer_Activity.activity.getSupportFragmentManager().findFragmentByTag("mapFragment"));
        }
        else
        {
            map_fragment = new Map_Fragment();
        }

//        Map_Fragment map_fragment = new Map_Fragment();
        map_fragment.GetLocationGps(lEnd, speed);
    }

    //The live feed of Distance and Speed are being set in the method below .
    private void updateUI()
    {
        Constants.newgpsLatitude = lEnd.getLatitude()+"";
        Constants.newgpsLongitude = lEnd.getLongitude()+"";

        if (SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, this)!=null && !SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, this).equalsIgnoreCase(""))
        {
            distance = Float.parseFloat(SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, this));
        }
        else
        {
            distance = 0;
            SessionSave.saveUserSession(Comman.SUM_FOR_LAT_LONG_METER, "", this);
        }

        String meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, this);
        if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
        {
            if (Drawer_Activity.ll_Meter!=null)
            {
                Drawer_Activity.ll_Meter.setVisibility(View.GONE);
            }
            if (Drawer_Activity.spinner!=null)
            {
                Drawer_Activity.spinner.setEnabled(false);
            }

            double getNewDistance = (lStart.distanceTo(lEnd) / 1000.00);
            Log.e("LocationService ","11111111111111111 : "+ getNewDistance +" : getNewDistance");

            if (SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, this)!=null && SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, this).equals("1")) {
            }
            else {
                distance = distance + getNewDistance;
            }

            String distanceMain = String.format( "%.2f", distance);
            if (Drawer_Activity.tv_meter_digit!=null)
            {
                Drawer_Activity.tv_meter_digit.setText(distanceMain+"");
            }
            Log.e("LocationService ","11111111111111111 : "+ distanceMain +" : distanceMain");

//            Drawer_Activity.GetBaseFareOffline(distanceMain);
        }
        else
        {
            distance=0;
            if (Drawer_Activity.ll_Meter!=null)
            {
                Drawer_Activity.ll_Meter.setVisibility(View.GONE);
            }
            if (Drawer_Activity.spinner!=null)
            {
                Drawer_Activity.spinner.setEnabled(true);
            }
        }
        SessionSave.saveUserSession(Comman.SUM_FOR_LAT_LONG_METER, distance+"", this);
        lStart = lEnd;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {

    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        Log.e(TAG,"onUnbind()");

        stopLocationUpdates();
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        distance = 0;
        stopForeground(true);
        stopSelf();
        return super.onUnbind(intent);
    }

    public class LocalBinder extends Binder
    {
        public LocationService getService()
        {
            return LocationService.this;
        }
    }
}