package com.connectkargo.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.R;
import com.squareup.picasso.Picasso;


public class Wallet_Transfer_ReceiveMoney_Fragment extends Fragment {

    private EditText et_setAmount;
    private ImageView iv_qr_code;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_receive_transfer_wallet, container, false);

        iv_qr_code = (ImageView) rootView.findViewById(R.id.iv_qr_code);
        et_setAmount= (EditText) rootView.findViewById(R.id.et_setAmount);

        et_setAmount.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId== EditorInfo.IME_ACTION_DONE){

                }
                return false;
            }
        });

        Log.e("basQrCode","USER_QR_CODE : "+SessionSave.getUserSession(Comman.USER_QR_CODE,getActivity()));
        if (SessionSave.getUserSession(Comman.USER_QR_CODE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_QR_CODE,getActivity()).equalsIgnoreCase(""))
        {
            iv_qr_code.setVisibility(View.VISIBLE);
//            if (SessionSave.getUserSession(Comman.USER_QR_CODE,getActivity()).contains(WebServiceAPI.BASE_URL_IMAGE))
//            {
                Picasso.get().load(SessionSave.getUserSession(Comman.USER_QR_CODE,getActivity())).into(iv_qr_code);
//            }
//            else
//            {
//                Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE+"/"+SessionSave.getUserSession(Comman.USER_QR_CODE,getActivity())).into(iv_qr_code);
//            }
        }
        else
        {
            iv_qr_code.setVisibility(View.INVISIBLE);
        }


        /*String QRCode = SessionSave.getUserSession(Comman.USER_QR_CODE, getActivity());

        if(QRCode!=null && !QRCode.equalsIgnoreCase(""))
        {
            Log.e("GET QRCODE","QRCode:- " + QRCode);
            Picasso.get().load(QRCode).into(iv_qr_code);
        }*/
        return rootView;
    }
}