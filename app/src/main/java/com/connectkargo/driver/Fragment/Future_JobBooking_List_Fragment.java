package com.connectkargo.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connectkargo.driver.Adapter.Future_Job_List_Adapter;
import com.connectkargo.driver.Been.Future_BookingList_Been;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Future_JobBooking_List_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ExpandableRecyclerView recyclerView;
    private ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    public static List<Future_BookingList_Been> list = new ArrayList<Future_BookingList_Been>();

    String TAG = "CallDispatched_List";
    private AQuery aQuery;
    DialogClass dialogClass;
    public SwipeRefreshLayout swipeRefreshLayout;
    int RunAct_FirstTime;
    TextView tv_NoDataFound;

    ErrorDialogClass errorDialogClass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_future_job_booking_list, container, false);

        aQuery = new AQuery(getActivity());

        errorDialogClass = new ErrorDialogClass(getActivity());

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        tv_NoDataFound = (TextView) rootView.findViewById(R.id.tv_NoDataFound);
        tv_NoDataFound.setVisibility(View.GONE);

        recyclerView = (ExpandableRecyclerView) rootView.findViewById(R.id.rv_future_job);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new Future_Job_List_Adapter((LinearLayoutManager) layoutManager);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "firstTime call");
            RunAct_FirstTime=0;
            if (ConnectivityReceiver.isConnected())
            {
                CallPendingJob_List(userId);
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
        return rootView;
    }

    @Override
    public void onRefresh()
    {
        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "On refresh");
            if (ConnectivityReceiver.isConnected())
            {
                CallPendingJob_List(userId);
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
    }

    private void CallPendingJob_List(String userId)
    {
        dialogClass = new DialogClass(Drawer_Activity.activity, 1);
        if (RunAct_FirstTime==0)
        {
            dialogClass.showDialog();
            RunAct_FirstTime=1;
        }


        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_FUTURE_BOOKING_LIST + userId;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.d(TAG,"status:true");
                                list.clear();

                                if (json.has("dispath_job"))
                                {
                                    String dispath_job = json.getString("dispath_job");
                                    if (dispath_job !=null && !dispath_job.equalsIgnoreCase(""))
                                    {
                                        JSONArray dispatchJob_Array = json.getJSONArray("dispath_job");

                                        for (int i = 0; i < dispatchJob_Array.length(); i++)
                                        {
                                            JSONObject dispatchJobObj = dispatchJob_Array.getJSONObject(i);

                                            if (dispatchJobObj != null)
                                            {
                                                String TripId = "", PickupDateTime = "", PickupLocation = "", DropoffLocation = "", CompanyId = "", PassengerId = "", TripDistance = ""
                                                        , ModelId = "", PassengerName = "", PassengerContact = "", Model = "", FlightNumber="", Notes="", PaymentType="", DispatcherDriverInfo=""
                                                        , dispatherEmail="", dispatherFullname="", dispatherMobileNo="", RequestFor="",
                                                        Labour="",ParcelName="", Height="", Length="", Breadth="", Weigth="", Quantity="",
                                                        BookingType = "", ReceiverName = "", ReceiverContactNo = "", ReceiverEmail = "",ParcelImage="", EstimateFare = "";

                                                if (dispatchJobObj.has("Id")) {
                                                    TripId = dispatchJobObj.getString("Id");
                                                }
                                                if (dispatchJobObj.has("PassengerId")) {
                                                    PassengerId = dispatchJobObj.getString("PassengerId");
                                                }
                                                if (dispatchJobObj.has("ModelId")) {
                                                    ModelId = dispatchJobObj.getString("ModelId");
                                                }
                                                if (dispatchJobObj.has("TripDistance")) {
                                                    TripDistance = dispatchJobObj.getString("TripDistance");
                                                }
                                                if (dispatchJobObj.has("PickupLocation")) {
                                                    PickupLocation = dispatchJobObj.getString("PickupLocation");
                                                }
                                                if (dispatchJobObj.has("DropoffLocation")) {
                                                    DropoffLocation = dispatchJobObj.getString("DropoffLocation");
                                                }
                                                if (dispatchJobObj.has("Model")) {
                                                    Model = dispatchJobObj.getString("Model");
                                                }
                                                if (dispatchJobObj.has("PassengerName")) {
                                                    PassengerName = dispatchJobObj.getString("PassengerName");
                                                }
                                                if (dispatchJobObj.has("PassengerContact")) {
                                                    PassengerContact = dispatchJobObj.getString("PassengerContact");
                                                }
                                                if (dispatchJobObj.has("PickupDateTime")) {
                                                    PickupDateTime = dispatchJobObj.getString("PickupDateTime");
                                                }
                                                if (dispatchJobObj.has("FlightNumber")) {
                                                    FlightNumber = dispatchJobObj.getString("FlightNumber");
                                                }
                                                if (dispatchJobObj.has("Notes")) {
                                                    Notes = dispatchJobObj.getString("Notes");
                                                }
                                                if (dispatchJobObj.has("PaymentType")) {
                                                    PaymentType = dispatchJobObj.getString("PaymentType");
                                                }

                                                if (dispatchJobObj.has("RequestFor")) {
                                                    RequestFor = dispatchJobObj.getString("RequestFor");
                                                }

                                                if (dispatchJobObj.has("Labour"))
                                                {
                                                    Labour = dispatchJobObj.getString("Labour");
                                                }

                                                if (dispatchJobObj.has("Parcel") &&
                                                        dispatchJobObj.getString("Parcel") != null &&
                                                        !dispatchJobObj.getString("Parcel").equalsIgnoreCase("") &&
                                                        !dispatchJobObj.getString("Parcel").equalsIgnoreCase("null"))
                                                {
                                                    JSONObject jsonObjectParcel = dispatchJobObj.getJSONObject("Parcel");

                                                    if (jsonObjectParcel != null)
                                                    {
                                                        if (jsonObjectParcel.has("Name"))
                                                        {
                                                            ParcelName = jsonObjectParcel.getString("Name");
                                                        }
                                                    }
                                                }

                                                if (dispatchJobObj.has("Height"))
                                                {
                                                    Height = dispatchJobObj.getString("Height");
                                                }

                                                if (dispatchJobObj.has("Length"))
                                                {
                                                    Length = dispatchJobObj.getString("Length");
                                                }

                                                if (dispatchJobObj.has("Breadth"))
                                                {
                                                    Breadth = dispatchJobObj.getString("Breadth");
                                                }

                                                if (dispatchJobObj.has("Weight"))
                                                {
                                                    Weigth = dispatchJobObj.getString("Weight");
                                                }

                                                if (dispatchJobObj.has("Quantity"))
                                                {
                                                    Quantity = dispatchJobObj.getString("Quantity");
                                                }

                                                if (dispatchJobObj.has("BookingType"))
                                                {
                                                    BookingType = dispatchJobObj.getString("BookingType");
                                                }

                                                if (dispatchJobObj.has("ReceiverName"))
                                                {
                                                    ReceiverName = dispatchJobObj.getString("ReceiverName");
                                                }

                                                if (dispatchJobObj.has("ReceiverContactNo"))
                                                {
                                                    ReceiverContactNo = dispatchJobObj.getString("ReceiverContactNo");
                                                }

                                                if (dispatchJobObj.has("ReceiverEmail"))
                                                {
                                                    ReceiverEmail = dispatchJobObj.getString("ReceiverEmail");
                                                }
                                                if (dispatchJobObj.has("ParcelImage"))
                                                {
                                                    ParcelImage = dispatchJobObj.getString("ParcelImage");
                                                }

                                                if(dispatchJobObj.has("EstimateFare")) {
                                                    EstimateFare = dispatchJobObj.getString("EstimateFare");
                                                }

                                                if (dispatchJobObj.has("DispatcherDriverInfo"))
                                                {
                                                    DispatcherDriverInfo = dispatchJobObj.getString("DispatcherDriverInfo");
                                                    if (DispatcherDriverInfo!=null && !DispatcherDriverInfo.equalsIgnoreCase(""))
                                                    {
                                                        JSONObject DispatcherDriverInfoObj = dispatchJobObj.getJSONObject("DispatcherDriverInfo");
                                                        if (DispatcherDriverInfoObj!=null)
                                                        {
                                                            if (DispatcherDriverInfoObj.has("Email"))
                                                            {
                                                                dispatherEmail = DispatcherDriverInfoObj.getString("Email");
                                                            }
                                                            if (DispatcherDriverInfoObj.has("Fullname"))
                                                            {
                                                                dispatherFullname = DispatcherDriverInfoObj.getString("Fullname");
                                                            }
                                                            if (DispatcherDriverInfoObj.has("MobileNo"))
                                                            {
                                                                dispatherMobileNo = DispatcherDriverInfoObj.getString("MobileNo");
                                                            }
                                                        }
                                                    }
                                                }
                                                Log.e("ccccccccc","TripId : "+TripId+"\nPassengerId : "+PassengerId+"\nModelId : "+ModelId+"\nTripDistance : "+TripDistance+"\nPickupLocation : "+PickupLocation
                                                        +"\nDropoffLocation : "+DropoffLocation+"\nModel : "+Model+"\nPassengerName : "+PassengerName +"\nFlightNumber : "+FlightNumber+"\nNotes : "+Notes
                                                        +"\nPaymentType : "+PaymentType);

                                                String driverID = "" , tripType = "0";

                                                driverID = SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,getActivity());

//                                                if(RequestFor != null &&
//                                                        !RequestFor.equalsIgnoreCase("") &&
//                                                        !RequestFor.equalsIgnoreCase("null") &&
//                                                        RequestFor.equalsIgnoreCase("delivery"))
//                                                {
//                                                    tripType = "2";
//                                                }
//                                                else
//                                                {
//                                                    tripType = "1";
//                                                }
//                                                if (tripType != null &&
//                                                        !tripType.equalsIgnoreCase("0") &&
//                                                        driverID != null &&
//                                                        driverID.trim().equalsIgnoreCase(tripType.trim()))
//                                                {
//                                                    list.add(new Future_BookingList_Been(TripId, PickupDateTime, PickupLocation, DropoffLocation, CompanyId, PassengerId, TripDistance, ModelId,
//                                                            PassengerName, PassengerContact, Model, FlightNumber, Notes, PaymentType,
//                                                            dispatherEmail, dispatherFullname, dispatherMobileNo, RequestFor, ParcelName,
//                                                            Labour, Height, Length, Breadth, Weigth, Quantity, BookingType));
//                                                }

                                                list.add(new Future_BookingList_Been(TripId, PickupDateTime, PickupLocation, DropoffLocation, CompanyId, PassengerId, TripDistance, ModelId,
                                                        PassengerName, PassengerContact, Model, FlightNumber, Notes, PaymentType,
                                                        dispatherEmail, dispatherFullname, dispatherMobileNo, RequestFor, ParcelName,
                                                        Labour, Height, Length, Breadth, Weigth, Quantity, BookingType, ReceiverName,
                                                        ReceiverContactNo, ReceiverEmail,ParcelImage, EstimateFare));

                                            }
                                            else
                                                {
                                                Log.d(TAG, "bookingHistoryObj:NULL");
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                    else
                                    {
                                        Log.d(TAG,"history:NULL");
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                        else
                                        {
                                            errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.d(TAG,"history:NULL");
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
                finally
                {
                    Log.e(TAG,"");

                    if (list.size()>0)
                    {
                        tv_NoDataFound.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        recyclerView.setVisibility(View.GONE);
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                    }
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}
