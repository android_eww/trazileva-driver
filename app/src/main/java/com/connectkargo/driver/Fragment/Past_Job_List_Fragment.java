package com.connectkargo.driver.Fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Adapter.Past_Job_List_Adapter;
import com.connectkargo.driver.Been.Pending_JobList_Been;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.connectkargo.driver.listner.OnLoadMoreListener;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Past_Job_List_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ExpandableRecyclerView recyclerView;
    private Past_Job_List_Adapter mAdapter;
    private LinearLayoutManager layoutManager;
    public static List<Pending_JobList_Been> list = new ArrayList<Pending_JobList_Been>();

    String TAG = "CallDispatched_List";
    private AQuery aQuery;
    DialogClass dialogClass;
    public SwipeRefreshLayout swipeRefreshLayout;
    int RunAct_FirstTime;
    public String userId;
    public TextView tv_NoDataFound;
    public int pageIndex = 1;
    public int priviousListSize = 0;

    ErrorDialogClass errorDialogClass ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_past_job, container, false);

        pageIndex = 1;
        priviousListSize = 0;

        aQuery = new AQuery(getActivity());
        errorDialogClass = new ErrorDialogClass(getActivity());

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        tv_NoDataFound = (TextView) rootView.findViewById(R.id.tv_NoDataFound);
        tv_NoDataFound.setVisibility(View.GONE);
        recyclerView = (ExpandableRecyclerView) rootView.findViewById(R.id.rv_past_job);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new Past_Job_List_Adapter(layoutManager, recyclerView, getActivity());
        recyclerView.setAdapter(mAdapter);


        userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener()
        {
            @Override
            public void onLoadMore()
            {
                Log.e(TAG,"priviousListSize1111 = "+priviousListSize);
                Log.e(TAG,"storeBeensList.size()1111 = "+ list.size());

                if (Global.isNetworkconn(getActivity()))
                {
                    if (priviousListSize < list.size())
                    {
                        {
                            Log.e(TAG,"Past_Job_List_Fragment list.add(null); ");
                            list.add(null);
                            mAdapter.notifyItemInserted(list.size());

                            new Handler().postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    //add items one by one
                                    Log.e(TAG,"priviousListSize2222 = "+priviousListSize);
                                    Log.e(TAG,"list.size()2222 = "+list.size());

                                    if (priviousListSize < list.size())
                                    {
                                        priviousListSize = list.size();
                                        pageIndex = pageIndex + 1;
//                                    isReffreshing =true;
                                        Log.e(TAG,"call_store_list() MyBookingActivity.pastPageIndex " + pageIndex);

                                        CallPendingJob_List();
                                    }
                                    else
                                    {
                                        if (list != null && list.size() > 0)
                                        {
                                            list.remove(list.size() - 1);
                                            mAdapter.notifyItemRemoved(list.size());
                                            Log.e(TAG,"call loadmore = in else");
                                            mAdapter.setLoaded();
                                        }
                                    }
                                }
                            }, 0);
                        }
                    }
                    else
                    {
                        Log.e(TAG, "StoreFragment Load More Loading data completed");
                    }
                }
            }
        });

        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "firstTime call");
            RunAct_FirstTime=0;

            if (ConnectivityReceiver.isConnected())
            {
                CallPendingJob_List();
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
        return rootView;
    }

    @Override
    public void onRefresh()
    {
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "On refresh");
            swipeRefreshLayout.setRefreshing(true);
            if (ConnectivityReceiver.isConnected())
            {
                pageIndex = 1;
                priviousListSize = 0;
                CallPendingJob_List();
            }
            else
            {
                swipeRefreshLayout.setRefreshing(false);
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
    }

    private void CallPendingJob_List()
    {
        dialogClass = new DialogClass(Drawer_Activity.activity, 1);
        if (RunAct_FirstTime==0)
        {
            dialogClass.showDialog();
            RunAct_FirstTime=1;
        }

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_PAST_BOOKING_HISTORY_LIST + userId + "/" + pageIndex;

        Log.e(TAG, "CallPendingJob_List() URL = " + url);
        Log.e(TAG, "CallPendingJob_List() PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "CallPendingJob_List() responseCode = " + responseCode);
                    Log.e(TAG, "CallPendingJob_List() Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e(TAG,"status:true");
                                if (pageIndex== 1)
                                {
                                    if (list != null)
                                    {
                                        list.clear();
                                    }
                                }
                                else
                                {
                                    if (list != null && list.size() >0 )
                                    {
                                        list.remove(list.size() - 1);
                                        mAdapter.notifyItemRemoved(list.size() - 1);
                                    }
                                }

                                if (json.has("history"))
                                {
                                    JSONArray bookingHistoryArray = json.getJSONArray("history");

                                    if (bookingHistoryArray != null && bookingHistoryArray.length() >0)
                                    {
                                        Log.e(TAG,"CallPendingJob_List() bookingHistory:\n" + bookingHistoryArray);
                                        Log.e(TAG,"CallPendingJob_List() bookingHistory Length:\n" + bookingHistoryArray.length());
                                        for (int i = 0; i < bookingHistoryArray.length() ; i++)
                                        {
                                            JSONObject bookingHistoryObj = bookingHistoryArray.getJSONObject(i);
                                            Log.e(TAG,"CallPendingJob_List() bookingHistoryObj:\n" + bookingHistoryObj);

                                            if (bookingHistoryObj != null)
                                            {
                                                String Id="", PassengerId="", ModelId="", DriverId="", CreatedDate="", TransactionId="", PaymentStatus="",
                                                        PickupDateTime="", DropTime="",TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="",
                                                        NightFareApplicable="",NightFare="",TripFare="",WaitingTime="",WaitingTimeCost="",TollFee="",BookingCharge="",
                                                        Tax="",PromoCode="",Discount="",SubTotal="",GrandTotal="",Status="",OnTheWay="",Reason="",PaymentType="",AdminAmount="",
                                                        CompanyAmount="",PickupLat="",PickupLng="",DropOffLat="",DropOffLon="",Model="",PassengerName="",PassengerEmail="",
                                                        PassengerMobileNo="",HistoryType="", FlightNumber="", Notes="", DispatcherDriverInfo="", dispatherEmail=""
                                                        , dispatherFullname="", dispatherMobileNo="", RequestFor="", Labour="", ParcelName="",
                                                        Height="", Length="", Breadth="", Weigth="", Quantity="", BookingType="",
                                                        ReceiverName ="", ReceiverEmail= "", ReceiverContactNo= "",ParcelImage="";

                                                if(bookingHistoryObj.has("Id"))
                                                {
                                                    Id = bookingHistoryObj.getString("Id");
                                                    Log.e(TAG,"Id:\n" + Id);
                                                }
                                                if (bookingHistoryObj.has("PassengerId"))
                                                {
                                                    PassengerId = bookingHistoryObj.getString("PassengerId");
                                                    Log.e(TAG,"PassengerId:\n" + PassengerId);
                                                }
                                                if (bookingHistoryObj.has("ModelId"))
                                                {
                                                    ModelId = bookingHistoryObj.getString("ModelId");
                                                    Log.e(TAG,"ModelId:\n" + ModelId);
                                                }
                                                if (bookingHistoryObj.has("DriverId"))
                                                {
                                                    DriverId = bookingHistoryObj.getString("DriverId");
                                                    Log.e(TAG,"DriverId:\n" + DriverId);
                                                }
                                                if (bookingHistoryObj.has("CreatedDate"))
                                                {
                                                    CreatedDate = bookingHistoryObj.getString("CreatedDate");
                                                    Log.e(TAG,"CreatedDate:\n" + CreatedDate);
                                                }
                                                if (bookingHistoryObj.has("TransactionId"))
                                                {
                                                    TransactionId = bookingHistoryObj.getString("TransactionId");
                                                }
                                                if (bookingHistoryObj.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = bookingHistoryObj.getString("PaymentStatus");
                                                }
                                                if (bookingHistoryObj.has("PickupDateTime"))
                                                {
                                                    PickupDateTime = bookingHistoryObj.getString("PickupDateTime");
                                                }
                                                if (bookingHistoryObj.has("DropTime"))
                                                {
                                                    DropTime = bookingHistoryObj.getString("DropTime");
                                                }
                                                if (bookingHistoryObj.has("TripDuration"))
                                                {
                                                    TripDuration = bookingHistoryObj.getString("TripDuration");
                                                }
                                                if (bookingHistoryObj.has("TripDistance"))
                                                {
                                                    TripDistance = bookingHistoryObj.getString("TripDistance");
                                                }
                                                if (bookingHistoryObj.has("PickupLocation"))
                                                {
                                                    PickupLocation = bookingHistoryObj.getString("PickupLocation");
                                                }
                                                if (bookingHistoryObj.has("DropoffLocation"))
                                                {
                                                    DropoffLocation = bookingHistoryObj.getString("DropoffLocation");
                                                }
                                                if (bookingHistoryObj.has("NightFareApplicable"))
                                                {
                                                    NightFareApplicable = bookingHistoryObj.getString("NightFareApplicable");
                                                }
                                                if (bookingHistoryObj.has("NightFare"))
                                                {
                                                    NightFare = bookingHistoryObj.getString("NightFare");
                                                }
                                                if (bookingHistoryObj.has("TripFare"))
                                                {
                                                    TripFare = bookingHistoryObj.getString("TripFare");
                                                }
                                                if (bookingHistoryObj.has("WaitingTime"))
                                                {
                                                    WaitingTime = bookingHistoryObj.getString("WaitingTime");
                                                }
                                                if (bookingHistoryObj.has("WaitingTimeCost"))
                                                {
                                                    WaitingTimeCost = bookingHistoryObj.getString("WaitingTimeCost");
                                                }
                                                if (bookingHistoryObj.has("TollFee"))
                                                {
                                                    TollFee = bookingHistoryObj.getString("TollFee");
                                                }
                                                if (bookingHistoryObj.has("BookingCharge"))
                                                {
                                                    BookingCharge = bookingHistoryObj.getString("BookingCharge");
                                                }
                                                if (bookingHistoryObj.has("Tax"))
                                                {
                                                    Tax = bookingHistoryObj.getString("Tax");
                                                }
                                                if (bookingHistoryObj.has("PromoCode"))
                                                {
                                                    PromoCode = bookingHistoryObj.getString("PromoCode");
                                                }
                                                if (bookingHistoryObj.has("Discount"))
                                                {
                                                    Discount = bookingHistoryObj.getString("Discount");
                                                }
                                                if (bookingHistoryObj.has("SubTotal"))
                                                {
                                                    SubTotal = bookingHistoryObj.getString("SubTotal");
                                                }
                                                if (bookingHistoryObj.has("GrandTotal"))
                                                {
                                                    GrandTotal = bookingHistoryObj.getString("GrandTotal");
                                                }
                                                if (bookingHistoryObj.has("Status"))
                                                {
                                                    Status = bookingHistoryObj.getString("Status");
                                                }
                                                if (bookingHistoryObj.has("OnTheWay"))
                                                {
                                                    OnTheWay = bookingHistoryObj.getString("OnTheWay");
                                                }
                                                if (bookingHistoryObj.has("Reason"))
                                                {
                                                    Reason = bookingHistoryObj.getString("Reason");
                                                }
                                                if (bookingHistoryObj.has("PaymentType"))
                                                {
                                                    PaymentType = bookingHistoryObj.getString("PaymentType");
                                                }
                                                if (bookingHistoryObj.has("AdminAmount"))
                                                {
                                                    AdminAmount = bookingHistoryObj.getString("AdminAmount");
                                                }
                                                if (bookingHistoryObj.has("CompanyAmount"))
                                                {
                                                    CompanyAmount = bookingHistoryObj.getString("CompanyAmount");
                                                }
                                                if (bookingHistoryObj.has("PickupLat"))
                                                {
                                                    PickupLat = bookingHistoryObj.getString("PickupLat");
                                                }
                                                if (bookingHistoryObj.has("PickupLng"))
                                                {
                                                    PickupLng = bookingHistoryObj.getString("PickupLng");
                                                }
                                                if (bookingHistoryObj.has("DropOffLat"))
                                                {
                                                    DropOffLat = bookingHistoryObj.getString("DropOffLat");
                                                }
                                                if (bookingHistoryObj.has("DropOffLon"))
                                                {
                                                    DropOffLon = bookingHistoryObj.getString("DropOffLon");
                                                }
                                                if (bookingHistoryObj.has("Model"))
                                                {
                                                    Model = bookingHistoryObj.getString("Model");
                                                }
                                                if (bookingHistoryObj.has("PassengerName"))
                                                {
                                                    PassengerName = bookingHistoryObj.getString("PassengerName");
                                                }
                                                if (bookingHistoryObj.has("PassengerEmail"))
                                                {
                                                    PassengerEmail = bookingHistoryObj.getString("PassengerEmail");
                                                }
                                                if (bookingHistoryObj.has("PassengerMobileNo"))
                                                {
                                                    PassengerMobileNo = bookingHistoryObj.getString("PassengerMobileNo");
                                                }
                                                if (bookingHistoryObj.has("HistoryType"))
                                                {
                                                    HistoryType = bookingHistoryObj.getString("HistoryType");
                                                }
                                                if (bookingHistoryObj.has("FlightNumber")) {
                                                    FlightNumber = bookingHistoryObj.getString("FlightNumber");
                                                }
                                                if (bookingHistoryObj.has("Notes")) {
                                                    Notes = bookingHistoryObj.getString("Notes");
                                                }

                                                if (bookingHistoryObj.has("RequestFor"))
                                                {
                                                    RequestFor = bookingHistoryObj.getString("RequestFor");
                                                }

                                                if (bookingHistoryObj.has("Labour"))
                                                {
                                                    Labour = bookingHistoryObj.getString("Labour");
                                                }

                                                if (bookingHistoryObj.has("Parcel") &&
                                                        bookingHistoryObj.getString("Parcel") != null &&
                                                        !bookingHistoryObj.getString("Parcel").equalsIgnoreCase("") &&
                                                        !bookingHistoryObj.getString("Parcel").equalsIgnoreCase("null"))
                                                {
                                                    JSONObject jsonObjectParcel = bookingHistoryObj.getJSONObject("Parcel");

                                                    if (jsonObjectParcel!=null)
                                                    {
                                                        if (jsonObjectParcel.has("Name"))
                                                        {
                                                            ParcelName = jsonObjectParcel.getString("Name");
                                                        }
                                                    }
                                                }

                                                if (bookingHistoryObj.has("Height"))
                                                {
                                                    Height = bookingHistoryObj.getString("Height");
                                                }

                                                if (bookingHistoryObj.has("Length"))
                                                {
                                                    Length = bookingHistoryObj.getString("Length");
                                                }

                                                if (bookingHistoryObj.has("Breadth"))
                                                {
                                                    Breadth = bookingHistoryObj.getString("Breadth");
                                                }

                                                if (bookingHistoryObj.has("Weight"))
                                                {

                                                    Weigth = bookingHistoryObj.getString("Weight");
                                                }

                                                if (bookingHistoryObj.has("Quantity"))
                                                {
                                                    Quantity = bookingHistoryObj.getString("Quantity");
                                                }

                                                if (bookingHistoryObj.has("BookingType"))
                                                {
                                                    BookingType = bookingHistoryObj.getString("BookingType");
                                                }

                                                if (bookingHistoryObj.has("ReceiverName"))
                                                {
                                                    ReceiverName = bookingHistoryObj.getString("ReceiverName");
                                                }

                                                if (bookingHistoryObj.has("ReceiverEmail"))
                                                {
                                                    ReceiverEmail = bookingHistoryObj.getString("ReceiverEmail");
                                                }

                                                if (bookingHistoryObj.has("ReceiverContactNo"))
                                                {
                                                    ReceiverContactNo = bookingHistoryObj.getString("ReceiverContactNo");
                                                }
                                                if (bookingHistoryObj.has("ParcelImage"))
                                                {
                                                    ParcelImage = bookingHistoryObj.getString("ParcelImage");
                                                }

                                                if (HistoryType!=null && !HistoryType.equalsIgnoreCase("") &&
                                                        HistoryType.equalsIgnoreCase("Past"))
                                                {
                                                    String driverID = "" , tripType = "0";

                                                    driverID = SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,getActivity());

//                                                    if(RequestFor != null &&
//                                                            !RequestFor.equalsIgnoreCase("") &&
//                                                            !RequestFor.equalsIgnoreCase("null") &&
//                                                            RequestFor.equalsIgnoreCase("delivery"))
//                                                    {
//                                                        tripType = "2";
//                                                    }
//                                                    else
//                                                    {
//                                                        tripType = "1";
//                                                    }
//                                                    if (tripType != null &&
//                                                            !tripType.equalsIgnoreCase("0") &&
//                                                            driverID != null &&
//                                                            driverID.trim().equalsIgnoreCase(tripType.trim()))
//                                                    {
//                                                        list.add(new Pending_JobList_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,PickupDateTime, DropTime,TripDuration,TripDistance,
//                                                                PickupLocation,DropoffLocation,NightFareApplicable,NightFare,TripFare,WaitingTime,WaitingTimeCost,TollFee,BookingCharge,Tax,
//                                                                PromoCode,Discount,SubTotal,GrandTotal,Status,OnTheWay,Reason,PaymentType,AdminAmount,CompanyAmount,PickupLat,PickupLng,DropOffLat,
//                                                                DropOffLon,Model,PassengerName,PassengerEmail,PassengerMobileNo,HistoryType, FlightNumber, Notes, dispatherEmail, dispatherFullname
//                                                                , dispatherMobileNo, RequestFor, ParcelName, Labour, Height, Length,
//                                                                Breadth, Weigth, Quantity));
//                                                    }

                                                    list.add(new Pending_JobList_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,PickupDateTime, DropTime,TripDuration,TripDistance,
                                                            PickupLocation,DropoffLocation,NightFareApplicable,NightFare,TripFare,WaitingTime,WaitingTimeCost,TollFee,BookingCharge,Tax,
                                                            PromoCode,Discount,SubTotal,GrandTotal,Status,OnTheWay,Reason,PaymentType,AdminAmount,CompanyAmount,PickupLat,PickupLng,DropOffLat,
                                                            DropOffLon,Model,PassengerName,PassengerEmail,PassengerMobileNo,HistoryType, FlightNumber, Notes, dispatherEmail, dispatherFullname
                                                            , dispatherMobileNo, RequestFor, ParcelName, Labour, Height, Length, Breadth,
                                                            Weigth, Quantity, BookingType, ReceiverName, ReceiverContactNo, ReceiverEmail,ParcelImage));
                                                }
                                            }
                                            else
                                            {
                                                Log.e(TAG,"CallPendingJob_List() bookingHistoryObj:NULL");
                                            }
                                        }

                                        Log.e(TAG,"CallPendingJob_List() bookingHistoryObj list.size():-" + list.size());

                                        mAdapter.notifyDataSetChanged();
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        if (json.has("message"))
                                        {
                                            Log.e(TAG, "CallPendingJob_List() message:" + json.getString("message"));
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }
                                    else
                                    {
                                        Log.e(TAG,"CallPendingJob_List() history:NULL");
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        if (json.has("message"))
                                        {
                                            Log.e(TAG, "message:" + json.getString("message"));
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }

                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    if (json.has("message"))
                                    {
                                        Log.e(TAG, "message:" + json.getString("message"));
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e(TAG, "CallPendingJob_List() status false");
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                if (json.has("message"))
                                {
                                    Log.e(TAG, "message:" + json.getString("message"));
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e(TAG, "CallPendingJob_List() no status");
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        Log.e(TAG, "CallPendingJob_List() getMessage = " + "null");
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "CallPendingJob_List() Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
                finally
                {
                    Log.e(TAG,"CallPendingJob_List()  finally");
                    if (list.size()>0)
                    {
                        tv_NoDataFound.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        if (pageIndex == 1)
                        {
                            recyclerView.setVisibility(View.GONE);
                            tv_NoDataFound.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            recyclerView.setVisibility(View.VISIBLE);
                            tv_NoDataFound.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}
