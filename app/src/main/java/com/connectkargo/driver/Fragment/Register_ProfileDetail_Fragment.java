package com.connectkargo.driver.Fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Activity.Login_Activity;
import com.connectkargo.driver.Activity.Registration_Activity;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class Register_ProfileDetail_Fragment extends Fragment implements View.OnClickListener{


    AppCompatRadioButton checkBox_male, checkBox_Female;
    TextView tv_register_profileNext, tv_backToLogin, tv_register_Dob;
    LinearLayout main_layout;
    RelativeLayout rl_register_userProfile;

    EditText et_register_fullname, etAgentCode, et_register_ResidentialAdd, et_register_PostCode, et_register_InvitedCode,et_driver_dad_mobileNo,et_driver_wife_mobileNo;
    String MALE="male", FEMALE ="female", chackBox_Selected ="";

    public CircleImageView iv_profile_register;
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private static final int MY_REQUEST_CODE_CAMERA = 101;
    private static final int MY_REQUEST_CODE_STORAGE = 100;
    private int GALLERY = 1, CAMERA = 0;
    byte[] Profile_ByteImage=null;
    Transformation mTransformation;
    String encodedImage;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;

    private Animation animBounce;
    Intent intent;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    public static Bitmap bitmapSumsung;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_profile, container, false);

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);
        rl_register_userProfile = (RelativeLayout) rootView.findViewById(R.id.rl_register_userProfile);

        iv_profile_register= (CircleImageView) rootView.findViewById(R.id.iv_profile_register);
        iv_profile_register.setImageResource(R.drawable.cab_dummy_profile);


        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(getActivity(), R.color.appColor))
                .borderWidthDp(0)
                .oval(true)
                .build();


        checkBox_male = (AppCompatRadioButton) rootView.findViewById(R.id.checkBox_male);
        checkBox_male.setChecked(true);
        chackBox_Selected = MALE;
        checkBox_Female = (AppCompatRadioButton) rootView.findViewById(R.id.checkBox_Female);

        et_register_fullname = (EditText) rootView.findViewById(R.id.et_register_fullname);
        etAgentCode = (EditText) rootView.findViewById(R.id.etAgentCode);
        et_register_ResidentialAdd = (EditText) rootView.findViewById(R.id.et_register_ResidentialAdd);
        et_register_PostCode = (EditText) rootView.findViewById(R.id.et_register_PostCode);
        et_register_InvitedCode = (EditText) rootView.findViewById(R.id.et_register_InvitedCode);
        et_driver_wife_mobileNo = rootView.findViewById(R.id.et_driver_wife_mobileNo);
        et_driver_dad_mobileNo = rootView.findViewById(R.id.et_driver_dad_mobileNo);

        tv_register_profileNext = (TextView) rootView.findViewById(R.id.tv_register_profileNext);
        tv_backToLogin = (TextView) rootView.findViewById(R.id.tv_backToLogin);
        tv_register_Dob = (TextView) rootView.findViewById(R.id.tv_register_Dob);

        checkBox_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMale();
            }
        });
        checkBox_Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFemale();
            }
        });

        tv_register_profileNext.setOnClickListener(this);
        tv_backToLogin.setOnClickListener(this);
        rl_register_userProfile.setOnClickListener(this);
        tv_register_Dob.setOnClickListener(this);

        setUserDetial();

        return rootView;
    }

    private void setUserDetial()
    {

        if (SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, Registration_Activity.activity).equalsIgnoreCase(""))
        {
//            if (SessionSave.getUserSession(Comman.REGISTER_PROFILE_ACTIVITY,Registration_Activity.activity) != null &&
//                    !SessionSave.getUserSession(Comman.REGISTER_PROFILE_ACTIVITY,Registration_Activity.activity).equalsIgnoreCase("1"))
//            {
//                iv_profile_register.setImageResource(R.drawable.cab_dummy_profile);
//            }
//            else
//            {
//                byte[] ByteImage  = Base64.decode(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, Registration_Activity.activity).toString().trim(),Base64.DEFAULT);
//                Bitmap bitmap = BitmapFactory.decodeByteArray(ByteImage, 0, ByteImage.length);
//                iv_profile_register.setImageBitmap(bitmap);
//
//                Profile_ByteImage = ConvertToByteArray(bitmap);
//                encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
//                SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
//            }

            byte[] ByteImage  = Base64.decode(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, Registration_Activity.activity).toString().trim(),Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(ByteImage, 0, ByteImage.length);
            iv_profile_register.setImageBitmap(bitmap);

            Profile_ByteImage = ConvertToByteArray(bitmap);
            encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
        }
        if (SessionSave.getUserSession(Comman.USER_FULL_NAME, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_FULL_NAME, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_fullname.setText(SessionSave.getUserSession(Comman.USER_FULL_NAME, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_AGENT_CODE, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_AGENT_CODE, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            etAgentCode.setText(SessionSave.getUserSession(Comman.USER_AGENT_CODE, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_DATE_OF_BIRTH, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_DATE_OF_BIRTH, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            tv_register_Dob.setText(SessionSave.getUserSession(Comman.USER_DATE_OF_BIRTH, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_GENDER, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_GENDER, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.USER_GENDER, Registration_Activity.activity).toString().equalsIgnoreCase("mail"))
            {
                checkBox_male.setChecked(true);
                checkBox_Female.setChecked(false);
            }
            else if (SessionSave.getUserSession(Comman.USER_GENDER, Registration_Activity.activity).toString().equalsIgnoreCase("female"))
            {
                checkBox_male.setChecked(false);
                checkBox_Female.setChecked(true);
            }

        }
        if (SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_ResidentialAdd.setText(SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_POST_CODE, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_POST_CODE, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_PostCode.setText(SessionSave.getUserSession(Comman.USER_POST_CODE, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_InvitedCode.setText(SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, Registration_Activity.activity).toString().trim());
        }

        if(SessionSave.getUserSession(Comman.USER_DAD_NUMBER, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_DAD_NUMBER, Registration_Activity.activity).equalsIgnoreCase("")){
            et_driver_dad_mobileNo.setText(SessionSave.getUserSession(Comman.USER_DAD_NUMBER, Registration_Activity.activity));
        }

        if(SessionSave.getUserSession(Comman.USER_WIFE_NUMBER, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_WIFE_NUMBER, Registration_Activity.activity).equalsIgnoreCase("")){
            et_driver_wife_mobileNo.setText(SessionSave.getUserSession(Comman.USER_WIFE_NUMBER, Registration_Activity.activity));
        }


    }

    public void selectMale()
    {
        chackBox_Selected = MALE;
        checkBox_male.setChecked(true);
        checkBox_Female.setChecked(false);
    }

    public void selectFemale()
    {
        chackBox_Selected = FEMALE;
        checkBox_male.setChecked(false);
        checkBox_Female.setChecked(true);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tv_register_Dob:
                OpenPopopFor_datePicker(tv_register_Dob);
                break;

            case R.id.rl_register_userProfile:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                rl_register_userProfile.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        takePermission();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation)
                    {

                    }
                });

                break;

            case R.id.tv_register_profileNext:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_register_profileNext.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        CheckUserData();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                break;

            case R.id.tv_backToLogin:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_backToLogin.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        SessionSave.clearUserSession(Registration_Activity.activity);
                        intent = new Intent(Registration_Activity.activity, Login_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        if (Registration_Activity.activity!=null)
                        {
                            Registration_Activity.activity.finish();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
        }
    }

    public void OpenPopopFor_datePicker(final TextView textView)
    {
        Calendar newCalendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        fromDatePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogThemeReal, new DatePickerDialog.OnDateSetListener()
        {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        fromDatePickerDialog.setSpinnersShown

        fromDatePickerDialog.setTitle(getContext().getResources().getString(R.string.select_date_of_birth));
        fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        fromDatePickerDialog.show();
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialog)
            {
                Log.e("yyyyyy","yyyyyyyyyyy: ");
                textView.setText("");
            }
        });
    }

    private void CheckUserData()
    {
        if (TextUtils.isEmpty(et_register_fullname.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_full_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(tv_register_Dob.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_dob),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_register_ResidentialAdd.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_residential_address),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        /*else  if (TextUtils.isEmpty(et_register_PostCode.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_post_code),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }*/
        else
        {
            SessionSave.saveUserSession(Comman.USER_FULL_NAME, et_register_fullname.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_AGENT_CODE, etAgentCode.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_DATE_OF_BIRTH, tv_register_Dob.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, et_register_ResidentialAdd.getText().toString(),getActivity());

            SessionSave.saveUserSession(Comman.USER_DAD_NUMBER, et_driver_dad_mobileNo.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_WIFE_NUMBER, et_driver_wife_mobileNo.getText().toString(),getActivity());

            if (Profile_ByteImage != null)
            {
//            new SnackbarUtils(main_layout, getString(R.string.please_select_profile_pic),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();

                encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
            }
            else
            {
                encodedImage = null;
                SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());
            }

            if (!TextUtils.isEmpty(et_register_PostCode.getText().toString()))
            {
                SessionSave.saveUserSession(Comman.USER_POST_CODE, et_register_PostCode.getText().toString(),getActivity());
            }
            else
            {
                SessionSave.saveUserSession(Comman.USER_POST_CODE,"",getActivity());
            }

            SessionSave.saveUserSession(Comman.USER_GENDER, chackBox_Selected,getActivity());

            if (et_register_InvitedCode.getText().toString()!=null && !et_register_InvitedCode.getText().toString().equalsIgnoreCase(""))
            {
                SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, et_register_InvitedCode.getText().toString(),getActivity());
            }
            else
            {
                SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, "",getActivity());
            }

            SessionSave.saveUserSession(Comman.REGISTER_PROFILE_ACTIVITY, "1",getActivity());

//            ((Registration_Activity)getActivity()).setTabBank();
            ((Registration_Activity)getActivity()).setTabCarInfo1();
        }
    }

    private void takePermission()
    {

        int a = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        int b = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        Log.e("TAG", "a = " + a);
        Log.e("TAG", "b = " + b);
        if (a != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("TAG", "a not granted");
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
            Registration_Activity.cameraPermissionLength = listPermissionsNeeded.size();
        }
        if (b != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("TAG", "b not granted");
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            Registration_Activity.cameraPermissionLength = listPermissionsNeeded.size();
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            Log.e("TAG", "ask permission");
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), 5/*Common_varible.REQUEST_ID_MULTIPLE_PERMISSIONS*/);
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            ShowPictureDialog();
        }
    }

    public void ShowPictureDialog()
    {
        final Dialog dialog = new Dialog(getActivity(),R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_photo_choose);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
        TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
            }
        });

        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        choosePhotoFromGallary();
                    }
                },800);
            }
        });

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        takePhotoFromCamera();
                    }
                },800);

            }
        });

        dialog.show();
    }

//    private void ShowPictureDialog()
//    {
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
//        {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE_CAMERA);
//        }
//        else if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
//        {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE_STORAGE);
//        }
//        else
//        {
//            final Dialog dialog = new Dialog(getActivity(),R.style.PauseDialog);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setCancelable(true);
//            dialog.setContentView(R.layout.dialog_photo_choose);
//
//            TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
//            TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
//            TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
//            LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
//            ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);
//
//            ll_Ok.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//
//            tv_Ok.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//
//            iv_close.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view)
//                {
//                    dialog.dismiss();
//                }
//            });
//
//            tv_gallery.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v)
//                {
//                    dialog.dismiss();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run()
//                        {
//                            choosePhotoFromGallary();
//                        }
//                    },800);
//                }
//            });
//
//            tv_camera.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v)
//                {
//                    dialog.dismiss();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run()
//                        {
//                            takePhotoFromCamera();
//                        }
//                    },800);
//
//                }
//            });
//
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            lp.copyFrom(dialog.getWindow().getAttributes());
//            //lp.width = Comman.DEVICE_WIDTH;
//            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
//            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//            lp.gravity = Gravity.CENTER;
//
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            dialog.getWindow().setAttributes(lp);
//
//            dialog.show();
//        }
//    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }
        else
        {
            userChoosenTask = chooseCamera;
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
            startActivityForResult(intent, CAMERA);
        }
    }

    public String getDeviceName()
    {
        String manufacturer = Build.MANUFACTURER;
        return manufacturer;
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
//        switch (requestCode) {
//            case MY_REQUEST_CODE_CAMERA:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                {
//                    Log.e("showPictureDialog","checkSelfPermission CAMERA permission granted");
//                    ShowPictureDialog();
//                }
//                break;
//
//            case MY_REQUEST_CODE_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                {
//                    Log.e("showPictureDialog","checkSelfPermission GALLERY permission granted");
//                    ShowPictureDialog();
//                }
//                break;
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                bitmapSumsung=null;
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                bitmapSumsung=null;
                onCaptureImageResult(data);
            }
            else
            {
                Profile_ByteImage=null;
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try
        {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            bitmapSumsung = bitmapImage;
        }
        if (bitmapImage!=null)
        {
            Picasso.get()
                    .load(data.getData())
                    .fit()
                    .transform(mTransformation)
                    .into(iv_profile_register);
            Profile_ByteImage = ConvertToByteArray(bitmapImage);

//            encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
//            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
        }
        else
        {
//            Profile_ByteImage=null;
//            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());

            new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
    }

    private void onCaptureImageResult(Intent data)
    {
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Log.e("hahahahahahahaha", "getDeviceName() : "+getDeviceName());
            bitmapSumsung = (Bitmap) data.getExtras().get("data");
            Uri imageUri1 = getImageUri(Registration_Activity.activity, bitmapSumsung);
            Picasso.get()
                    .load(imageUri1)
                    .fit()
                    .transform(mTransformation)
                    .into(iv_profile_register);
            Profile_ByteImage = ConvertToByteArray(bitmapSumsung);
//            encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
//            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
        }
        else
        {
            try
            {
                Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
                Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                Log.e("#########","bmp height = "+thumbnail.getHeight());
                Log.e("#########","bmp width = "+thumbnail.getWidth());
                thumbnail = getResizedBitmap(thumbnail,400);
                bitmapImage=thumbnail;
//                bitmapSumsung =thumbnail;
                if (thumbnail!=null)
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .transform(mTransformation)
                            .into(iv_profile_register);
                    Profile_ByteImage = ConvertToByteArray(thumbnail);

//                    encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
//                    SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
                }
                else
                {
                    iv_profile_register.setImageResource(R.drawable.cab_dummy_profile);
//                    SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());
//                    Profile_ByteImage=null;
                    new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
                }
            }
            catch (Exception e)
            {
                iv_profile_register.setImageResource(R.drawable.cab_dummy_profile);
//                SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());
//                Profile_ByteImage=null;
                Log.e("call","exception = "+e.getMessage());
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.e("OnResume","resuuuummmmmeeeeeeeeeeeeeeeeeee");
        if (bitmapSumsung != null)
        {
            Log.e("Call OnResume:- ", "Camera Click True");
            if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
            {
                Log.e("Call OnResume:- ", "Company Name" + getDeviceName() );
                Uri imageUri1 = getImageUri(Registration_Activity.activity, bitmapSumsung);
                Picasso.get()
                        .load(imageUri1)
                        .fit()
                        .transform(mTransformation)
                        .into(iv_profile_register);
                Profile_ByteImage = ConvertToByteArray(bitmapSumsung);
//                encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
//                SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
            }
        }
        else
        {
//            iv_profile_register.setImageResource(R.drawable.cab_dummy_profile);
//            Profile_ByteImage=null;
//            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "PickNGo", null);
        return Uri.parse(path);
    }
}
