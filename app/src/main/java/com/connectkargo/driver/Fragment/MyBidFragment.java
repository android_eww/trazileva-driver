package com.connectkargo.driver.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.connectkargo.driver.Activity.BidDetailActivity;
import com.connectkargo.driver.Activity.BidListActivity;
import com.connectkargo.driver.Adapter.MyBidAdapter;
import com.connectkargo.driver.Been.myBid.Datum;
import com.connectkargo.driver.Been.myBid.MyBid;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.google.gson.Gson;

import org.json.JSONObject;

public class MyBidFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    MyBidAdapter bidAdapter;
    RecyclerView rv_bid;
    LinearLayoutManager layoutManager;
    TextView tv_NoDataFound;
    SwipeRefreshLayout swipe_refresh_layout;

    private DialogClass dialogClass;
    private AQuery aQuery;
    MyBid myBid;
    ErrorDialogClass errorDialogClass;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_bid,container,false);
        init(view);
        return view;
    }

    private void init(View view)
    {
        errorDialogClass = new ErrorDialogClass(getActivity());
        aQuery = new AQuery(getActivity());
        dialogClass = new DialogClass(getActivity(), 1);

        Log.e("TAG","MyBid");
        rv_bid = view.findViewById(R.id.rv_bid);
        tv_NoDataFound = view.findViewById(R.id.tv_NoDataFound);
        layoutManager = new LinearLayoutManager(getActivity());
        rv_bid.setLayoutManager(layoutManager);
        swipe_refresh_layout = view.findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

        callMyBidApi();
    }

    private void callMyBidApi()
    {
        if (!Global.isNetworkconn(getActivity()))
        {
            errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
            return;
        }

        if(!swipe_refresh_layout.isRefreshing())
        {
            dialogClass.showDialog();
        }

        String UserId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        String url = WebServiceAPI.WEB_SERVICE_MY_BID+UserId;
        Log.e("TAG", "url:- " + url);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {
                int responseCode = status.getCode();

                if(!swipe_refresh_layout.isRefreshing())
                {
                    dialogClass.hideDialog();
                }

                Log.e("TAG", "myBid responseCode:- " + responseCode);
                Log.e("TAG", "myBid Response:- " + json);
                if(responseCode == 200)
                {
                    myBid = new Gson().fromJson(json.toString(), MyBid.class);
                    if(myBid.getStatus() && myBid.getData() != null && myBid.getData().size() > 0)
                    {
                        tv_NoDataFound.setVisibility(View.GONE);
                        bidAdapter = new MyBidAdapter(getActivity(),myBid.getData(), BidListActivity.MY_BID, new MyBidAdapter.onBidClick() {
                            @Override
                            public void onBidView(com.connectkargo.driver.Been.openBid.Datum openBib) {

                            }

                            @Override
                            public void onMyBidView(com.connectkargo.driver.Been.myBid.Datum myBib) {
                                Intent intent = new Intent(getActivity(), BidDetailActivity.class);
                                intent.putExtra("bidId",myBib);
                                intent.putExtra("from","mybid");
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            }
                        });
                        rv_bid.setAdapter(bidAdapter);
                    }
                    else
                    {
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                }
                if(swipe_refresh_layout.isRefreshing())
                {
                    swipe_refresh_layout.setRefreshing(false);
                }

            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onRefresh()
    {
        swipe_refresh_layout.setRefreshing(true);
        if(bidAdapter != null)
        {
            myBid.getData().clear();
            bidAdapter.notifyDataSetChanged();
        }
        callMyBidApi();
    }
}

