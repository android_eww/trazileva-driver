package com.connectkargo.driver.Fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connectkargo.driver.Activity.WebViewActivity;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.Util.DownloadTask;
import com.connectkargo.driver.Util.Utility;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Activity.Login_Activity;
import com.connectkargo.driver.Activity.Registration_Activity;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Register_Attachment_Fragment extends Fragment implements View.OnClickListener{

    public String TAG = "Attachment_Fragment";

    TextView tv_next, tv_driver_licence_expiry, /*tv_accreditation_expiry,*/ tv_carRegistration_expiry, /*tv_vehicle_Insurance_expiry,*/
            tv_backToLogin, tvGovernmentIdExpiry,tvPrivacyPolicy;
    LinearLayout ll_driver_licence, /*ll_acrreditation_certy,*/ ll_car_registration_certy, /*ll_vehicle_Insurance,*/ llGovernmentId;
            //,llCriminalCertificate, llAddressProof;
    ImageView iv_driverLicence, /*iv_accreditation_certy,*/ iv_car_Registration, /*iv_vehicle_insurance,*/
                    ivGovernmentIdFront, ivGovernmentIdBack,iv_driverLicence_front;
            //ivCriminalCertificate, ivAddressProof,;
    CheckBox checkBoxPrivacy;
    public static String ImageViewClicked = "", DRIVER_LICENCE="driverLicence", /*ACRREDITATION= "accreditation_certy",*/
            CAR_REGISTRATION="car_Registration", /*VEHICLE_INSURANCE="vehicle_insurance",*/ GOVERNMENT_ID_FORNT = "government_id_front",
            GOVERNMENT_ID_BACK = "government_id_back", /*CRIMINAL_CERTI = "criminal_certi", ADDRESS_PROOF = "address_proof",*/
            DRIVER_LICENCE_FRONT="driverLicenceFront"/*,PDF1 ="pfd1",PDF2 ="pfd2",PDF3 ="pfd3"*/;
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";

    private int GALLERY = 1, CAMERA = 0;
    private static final int MY_REQUEST_CODE_CAMERA = 101;
    private static final int MY_REQUEST_CODE_STORAGE = 100;

    Bitmap finalImageBitmap, oldBitmap;
    Transformation mTransformation;


    byte[] DriverLicence_ByteImage=null, /*Accreditation_certy_ByteImage=null,*/ Car_Registration_ByteImage=null,
            /*Vehicle_insurance_ByteImage=null,*/ Government_id_front_ByteImage=null, Government_id_back_ByteImage=null,
            /*Pdf1_ByteImage =null,Pdf2_ByteImage=null,Pdf3_ByteImage=null,*/DriverLicence_Front_ByteImage=null;
    //Criminal_Certi_ByteImage=null, Address_Proof_ByteImage=null,

    DialogClass dialogClass;
    AQuery aQuery;
    ErrorDialogClass errorDialogClass;

    LinearLayout main_layout;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;

    private Animation animBounce;
    Intent intent;

    public static Bitmap bitmapSumsung_DriverLicence, /*bitmapSumsung_Accreditation,*/ bitmapSumsung_CarRegi, /*bitmapSumsung_VehiInsurance,*/
            bitmapSumsung_Government_Id_Front, bitmapSumsung_Government_Id_Back, /*bitmapSumsung_Criminal_Certi,*/
            /*bitmapSumsung_Address_Proof,*/bitmapSumsung_DriverLicence_front/*,Pdf1_bitmapSumsung,Pdf2_bitmapSumsung,Pdf3_bitmapSumsung*/;
    public static String date_DriverLicence, date_Accredi, date_VehiIn, date_Government_Id;
    public static int datepickerDialogFlag;
    public static int ClickCameraFlagResumeCount;

//    private TextView tvDownload;
//    private ImageView ivPdf1,ivPdf2,ivPdf3;

    public EditText et_register_fullname, et_vehiRegistrationNo, etAgentCode;
    public ImageView ivDriverProfile, ivCarFront, ivCarLeft, ivCarRight;

    public static String DRIVER_PROFILE="driverProfile", DRIVER_CAR_FRONT="carFront", DRIVER_CAR_LEFT="carLeft", DRIVER_CAR_RIGHT="carRight";
    public byte[] DriverProfile_ByteImage=null, Car_front_ByteImage=null, Car_left_ByteImage=null, Car_Right_ByteImage=null;
    public static Bitmap bitmapSumsung_DriverProfile, bitmapSumsung_CarFront, bitmapSumsung_CarLeft, bitmapSumsung_CarRight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_register_attachment, container, false);


        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        aQuery = new AQuery(getActivity());
        errorDialogClass = new ErrorDialogClass(getActivity());

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        tv_next = (TextView) rootView.findViewById(R.id.tv_next);

        ll_driver_licence = (LinearLayout) rootView.findViewById(R.id.ll_driver_licence);
//        ll_acrreditation_certy = (LinearLayout) rootView.findViewById(R.id.ll_acrreditation_certy);
        ll_car_registration_certy = (LinearLayout) rootView.findViewById(R.id.ll_car_registration_certy);
//        ll_vehicle_Insurance = rootView.findViewById(R.id.ll_vehicle_Insurance);
        llGovernmentId = rootView.findViewById(R.id.llGovernmentId);
//        llCriminalCertificate = rootView.findViewById(R.id.llCriminalCertificate);
//        llAddressProof = rootView.findViewById(R.id.llAddressProof);

        tv_driver_licence_expiry = (TextView) rootView.findViewById(R.id.tv_driver_licence_expiry);
//        tv_accreditation_expiry = (TextView) rootView.findViewById(R.id.tv_accreditation_expiry);
        tv_carRegistration_expiry = (TextView) rootView.findViewById(R.id.tv_carRegistration_expiry);
//        tv_vehicle_Insurance_expiry = (TextView) rootView.findViewById(R.id.tv_vehicle_Insurance_expiry);
        tv_backToLogin = (TextView) rootView.findViewById(R.id.tv_backToLogin);
        tvPrivacyPolicy =  rootView.findViewById(R.id.tvPrivacyPolicy);

        iv_driverLicence = (ImageView) rootView.findViewById(R.id.iv_driverLicence);
        iv_driverLicence_front = rootView.findViewById(R.id.iv_driverLicence_front);
//        iv_accreditation_certy = (ImageView) rootView.findViewById(R.id.iv_accreditation_certy);
        iv_car_Registration = (ImageView) rootView.findViewById(R.id.iv_car_Registration);
//        iv_vehicle_insurance = rootView.findViewById(R.id.iv_vehicle_insurance);
        ivGovernmentIdFront = rootView.findViewById(R.id.ivGovernmentIdFront);
        ivGovernmentIdBack = rootView.findViewById(R.id.ivGovernmentIdBack);
//        ivCriminalCertificate = rootView.findViewById(R.id.ivCriminalCertificate);
//        ivAddressProof = rootView.findViewById(R.id.ivAddressProof);
        tvGovernmentIdExpiry = rootView.findViewById(R.id.tvGovernmentIdExpiry);
//        tvDownload = rootView.findViewById(R.id.tvDownload);
//        ivPdf1 = rootView.findViewById(R.id.ivPdf1);
//        ivPdf2 = rootView.findViewById(R.id.ivPdf2);
//        ivPdf3 = rootView.findViewById(R.id.ivPdf3);
        et_register_fullname = rootView.findViewById(R.id.et_register_fullname);
        et_vehiRegistrationNo = rootView.findViewById(R.id.et_vehiRegistrationNo);
        etAgentCode = rootView.findViewById(R.id.etAgentCode);
        ivDriverProfile = rootView.findViewById(R.id.ivDriverProfile);
        ivCarFront = rootView.findViewById(R.id.ivCarFront);
        ivCarLeft = rootView.findViewById(R.id.ivCarLeft);
        ivCarRight = rootView.findViewById(R.id.ivCarRight);

        checkBoxPrivacy = rootView.findViewById(R.id.checkBoxPrivacy);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(getActivity(), R.color.colorGray))
                .borderWidthDp(2)
                .oval(true)
                .build();


        tv_next.setOnClickListener(this);
        ll_driver_licence.setOnClickListener(this);
        iv_driverLicence.setOnClickListener(this);
        iv_driverLicence_front.setOnClickListener(this);
//        ll_acrreditation_certy.setOnClickListener(this);
        ll_car_registration_certy.setOnClickListener(this);
//        ll_vehicle_Insurance.setOnClickListener(this);
        ivGovernmentIdFront.setOnClickListener(this);
        ivGovernmentIdBack.setOnClickListener(this);
//        llCriminalCertificate.setOnClickListener(this);
//        llAddressProof.setOnClickListener(this);
        tv_backToLogin.setOnClickListener(this);
        ivDriverProfile.setOnClickListener(this);
        ivCarFront.setOnClickListener(this);
        ivCarLeft.setOnClickListener(this);
        ivCarRight.setOnClickListener(this);
//        ivPdf1.setOnClickListener(this);
//        ivPdf2.setOnClickListener(this);
//        ivPdf3.setOnClickListener(this);

        setPrivacyPolicy();

        /*final DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        tvDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadTask(getActivity(), tvDownload, "http:\\/\\/connectkargo.com\\/assets\\/pdf\\/REQUISITOS_PARA_SER_MOTORISTA_CONNECTKARGO.pdf");

                *//*Uri Download_Uri = Uri.parse("http:\\\\/\\\\/connectkargo.com\\\\/assets\\\\/pdf\\\\/REQUISITOS_PARA_SER_MOTORISTA_CONNECTKARGO.pdf");

                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                request.setAllowedOverRoaming(false);
                request.setTitle("REQUISITOS_PARA_SER_MOTORISTA_CONNECTKARGO.pdf");
                request.setDescription("REQUISITOS_PARA_SER_MOTORISTA_CONNECTKARGO.pdf");
                request.setVisibleInDownloadsUi(true);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/"+getActivity().getString(R.string.app_name)+"/"  + "/" + "Sample" + ".pdf");


                long refid = downloadManager.enqueue(request);*//*
            }
        });*/
        return rootView;
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {

            // get the refid from the download manager
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            openDownloadedFolder();
            /*NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getContext())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("GadgetSaint")
                            .setContentText("All Download completed");


            NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(455, mBuilder.build());*/

        }
    };

    private void openDownloadedFolder() {
        //First check if SD Card is present or not
        if (Utility.isSDCardPresent()) {

            //Get Download Directory File
            File apkStorage = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + getActivity().getString(R.string.app_name));

            //If file is not present then display Toast
            if (!apkStorage.exists())
                Toast.makeText(getActivity(), "Right now there is no directory. Please download some file first.", Toast.LENGTH_SHORT).show();

            else {

                //If directory is present Open Folder

                /** Note: Directory will open only if there is a app to open directory like File Manager, etc.  **/

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                        + "/" + getActivity().getString(R.string.app_name));
                intent.setDataAndType(uri, "file/*");
                getActivity().startActivity(Intent.createChooser(intent, "Open Download Folder"));
            }

        } else
            Toast.makeText(getActivity(), "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //getActivity().unregisterReceiver(onComplete);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //getActivity().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    private void setPrivacyPolicy()
    {
        SpannableString spanString = new SpannableString(getResources().getString(R.string.privacy_policy));
        Matcher matcher = Pattern.compile(getContext().getString(R.string.terms_condition)).matcher(spanString);
        Matcher matcher1 = Pattern.compile(getContext().getString(R.string.privacy_policy1)).matcher(spanString);

        while (matcher.find()) {
            ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
            spanString.setSpan(foregroundSpan, matcher.start(), matcher.end(), 0); //to highlight word havgin '@'
            final String tag = matcher.group(0);

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    Log.e("TAG","acvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
                    Intent i = new Intent(getActivity(), WebViewActivity.class);
                    i.putExtra("title",getContext().getString(R.string.terms_condition));
                    i.putExtra("url","http://connectkargo.com/terms-of-use");
                    startActivity(i);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorAccent));

                }
            };
            spanString.setSpan(clickableSpan, matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        while (matcher1.find()) {
            ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
            spanString.setSpan(foregroundSpan, matcher1.start(), matcher1.end(), 0); //to highlight word havgin '@'
            final String tag = matcher1.group(0);

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    //fffffffffff
                    Log.e("TAG","ddddddddddddddddddddddddd");
                    Intent i = new Intent(getActivity(),WebViewActivity.class);
                    i.putExtra("title",getContext().getString(R.string.privacy_policy1));
                    i.putExtra("url","http://connectkargo.com/privacy-policy");
                    startActivity(i);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorAccent));

                }
            };
            spanString.setSpan(clickableSpan, matcher1.start(), matcher1.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        tvPrivacyPolicy.setText(spanString);
        tvPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_next:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_next.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        String Token = SessionSave.getToken(Comman.DEVICE_TOKEN, getActivity());
                        if (Token!=null && !Token.equalsIgnoreCase(""))
                        {
                            checkData();
                        }
                        else
                        {

                            errorDialogClass.showDialog(getString(R.string.please_restart_app), getString(R.string.error_message));

                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.iv_driverLicence:
                ImageViewClicked = DRIVER_LICENCE;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                iv_driverLicence.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.iv_driverLicence_front:
                ImageViewClicked = DRIVER_LICENCE_FRONT;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                iv_driverLicence.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            /*case R.id.ll_acrreditation_certy:
                ImageViewClicked = ACRREDITATION;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                iv_accreditation_certy.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;*/

            case R.id.ll_car_registration_certy:
                ImageViewClicked = CAR_REGISTRATION;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                iv_car_Registration.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            /*case R.id.ll_vehicle_Insurance:
                ImageViewClicked = VEHICLE_INSURANCE;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                iv_vehicle_insurance.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;*/

            case R.id.ivGovernmentIdBack:
                ImageViewClicked = GOVERNMENT_ID_BACK;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                ivGovernmentIdBack.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.ivGovernmentIdFront:
                ImageViewClicked = GOVERNMENT_ID_FORNT;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                ivGovernmentIdFront.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            /*case R.id.llCriminalCertificate:
                ImageViewClicked = CRIMINAL_CERTI;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                iv_vehicle_insurance.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.llAddressProof:
                ImageViewClicked = ADDRESS_PROOF;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                iv_vehicle_insurance.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;*/

            /*case R.id.ivPdf1:
                ImageViewClicked = PDF1;
                showPictureDialog();
                break;

            case R.id.ivPdf2:
                ImageViewClicked = PDF2;
                showPictureDialog();
                break;

            case R.id.ivPdf3:
                ImageViewClicked = PDF3;
                showPictureDialog();
                break;*/

            case R.id.ivDriverProfile:
                ImageViewClicked = DRIVER_PROFILE;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                ivDriverProfile.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.ivCarFront:
                ImageViewClicked = DRIVER_CAR_FRONT;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                ivCarFront.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.ivCarLeft:
                ImageViewClicked = DRIVER_CAR_LEFT;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                ivCarLeft.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.ivCarRight:
                ImageViewClicked = DRIVER_CAR_RIGHT;
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                ivCarRight.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showPictureDialog();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.tv_backToLogin:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_backToLogin.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        SessionSave.clearUserSession(Registration_Activity.activity);
                        intent = new Intent(Registration_Activity.activity, Login_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        if (Registration_Activity.activity!=null)
                        {
                            Registration_Activity.activity.finish();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
        }
    }

    private void checkData()
    {
        if (TextUtils.isEmpty(et_register_fullname.getText().toString().trim()))
        {
            Log.e(TAG, "checkData() FullName");
            new SnackbarUtils(main_layout, getString(R.string.please_enter_full_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_vehiRegistrationNo.getText().toString().trim()))
        {
            Log.e(TAG, "checkData() Registration Number");
            new SnackbarUtils(main_layout, getString(R.string.please_enter_vehicle_registration_no),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else if (DriverProfile_ByteImage==null)
        {
            Log.e(TAG, "checkData() Driver Image");
            new SnackbarUtils(main_layout, getString(R.string.please_enter_select_driver_image),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else if (Car_front_ByteImage==null)
        {
            Log.e(TAG, "checkData() Car Front Image");
            new SnackbarUtils(main_layout, getString(R.string.please_select_car_front_image),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else if (Car_left_ByteImage==null)
        {
            Log.e(TAG, "checkData() Car Left Image");

            new SnackbarUtils(main_layout, getString(R.string.please_select_car_left_image),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else if (Car_Right_ByteImage ==null)
        {
            Log.e(TAG, "checkData() Car Right Image");

            new SnackbarUtils(main_layout, getString(R.string.please_select_car_right_image),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }

//        if(tv_driver_licence_expiry.getText().toString().trim().equalsIgnoreCase(""))
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
//        /*else if (tv_accreditation_expiry.getText().toString().trim().equalsIgnoreCase(""))
//        {
//            new SnackbarUtils(main_layout,getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }*/
//        else if (Car_Registration_ByteImage==null)
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_add_vehi_regi_doc),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
//        /*else if (tv_carRegistration_expiry.getText().toString().equalsIgnoreCase(""))
//        {
//            new SnackbarUtils(main_layout,getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }*/
//        /*else if (tv_vehicle_Insurance_expiry.getText().toString().trim().equalsIgnoreCase(""))
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }*/
//        else if (Government_id_front_ByteImage==null)
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_add_government_id_front_image),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
//        else if (Government_id_back_ByteImage==null)
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_add_government_id_back_image),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
//        else if (tvGovernmentIdExpiry.getText().toString().trim().equalsIgnoreCase(""))
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_select_government_id_expiry),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
//        /*else if (Criminal_Certi_ByteImage ==null)
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_add_criminal_ceritificate_image),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
//        else if (Address_Proof_ByteImage ==null)
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_add_address_proof_image),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }*/
//        else if (DriverLicence_Front_ByteImage ==null)
//        {
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_add_address_proof_image),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
//        /*else if(Pdf1_ByteImage == null || Pdf2_ByteImage == null || Pdf3_ByteImage == null){
//
//            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_upload_signed_doc),
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }*/
        else if(!checkBoxPrivacy.isChecked())
        {
            Log.e(TAG, "checkData() terms and condition check box");

            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_accept_terms_conition),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else
        {
            if (Global.isNetworkconn(getActivity()))
            {
                UserRegistration();
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
    }

    private void UserRegistration()
    {

        dialogClass = new DialogClass(getActivity(), 1);
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_OTP_FOR_DRIVER_REGISTER;

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DEVICE_TYPE, Comman.DEVICE_TYPE);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_TOKEN, SessionSave.getToken(Comman.DEVICE_TOKEN, getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_LATITUDE, Constants.newgpsLatitude);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_LONGITUDE, Constants.newgpsLatitude);


        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_EMAIL, SessionSave.getUserSession(Comman.USER_EMAIL,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_MOBILE_NUMBER, SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_PASSWORD,SessionSave.getUserSession(Comman.USER_PASSWORD,getActivity()));

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_FULL_NAME, et_register_fullname.getText().toString().trim());
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_RIGISTRATION_NO, et_vehiRegistrationNo.getText().toString().trim());
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_AGENT_CODE, etAgentCode.getText().toString().trim());

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_IMAGE, DriverProfile_ByteImage); //Driver image
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE, Car_front_ByteImage); // Front
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE_LEFT, Car_left_ByteImage); // Left
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE_RIGHT, Car_Right_ByteImage); // Right

//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DEVICE_TYPE, Comman.DEVICE_TYPE);
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_TOKEN, SessionSave.getToken(Comman.DEVICE_TOKEN, getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_LATITUDE, Constants.newgpsLatitude);
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_LONGITUDE, Constants.newgpsLatitude);
//
//
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_EMAIL, SessionSave.getUserSession(Comman.USER_EMAIL,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_MOBILE_NUMBER, SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_PASSWORD,SessionSave.getUserSession(Comman.USER_PASSWORD,getActivity()));
//
//        if (SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,getActivity()).equalsIgnoreCase(""))
//        {
//            byte[] driverImage_ByteImage = Base64.decode(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,getActivity()), Base64.DEFAULT);
//            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_IMAGE, driverImage_ByteImage);
//        }
//
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_FULL_NAME, SessionSave.getUserSession(Comman.USER_FULL_NAME,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_AGENT_CODE, SessionSave.getUserSession(Comman.USER_AGENT_CODE,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DOB, SessionSave.getUserSession(Comman.USER_DATE_OF_BIRTH,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GENDER, SessionSave.getUserSession(Comman.USER_GENDER,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ADDRESS,SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,getActivity()));
//        if (SessionSave.getUserSession(Comman.USER_POST_CODE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_POST_CODE,getActivity()).equalsIgnoreCase(""))
//        {
//            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ZIPCODE, SessionSave.getUserSession(Comman.USER_POST_CODE,getActivity()));
//        }
//        if (SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity()).equalsIgnoreCase(""))
//        {
//            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_REFERRAL_CODE,SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity()));
//        }
//
//
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BANK_ACCOUNT_HOLDER_NALE, SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BANK_NAME, SessionSave.getUserSession(Comman.USER_BANK_NAME,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BANK_BRANCH, SessionSave.getUserSession(Comman.USER_BANK_BRANCH,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BANK_ACCOUNT_NO, SessionSave.getUserSession(Comman.USER_BANK_AC_NO,getActivity()));
//
//
//        if (SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,getActivity()).equalsIgnoreCase(""))
//        {
//            byte[] vehicleImage_ByteImage = Base64.decode(SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,getActivity()), Base64.DEFAULT);
//            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE, vehicleImage_ByteImage);
//        }
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_RIGISTRATION_NO, SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_COMPANY_MODEL, SessionSave.getUserSession(Comman.USER_CAR_COMPANY,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_CLASS, SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,getActivity()));
//
//
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_LICENCE_BACK, DriverLicence_ByteImage);
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_LICENCE_FRONT, DriverLicence_Front_ByteImage);
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_LICENCE_EXPIRY, tv_driver_licence_expiry.getText().toString());
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE,Vehicle_insurance_ByteImage);
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE_CERTY_EXPIRE, tv_vehicle_Insurance_expiry.getText().toString());
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY, Accreditation_certy_ByteImage);
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY_EXPIRY, tv_accreditation_expiry.getText().toString());
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CAR_REGISTRATION, Car_Registration_ByteImage);
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GOVERNMENT_ID_EXPIRY, tvGovernmentIdExpiry.getText().toString().trim());
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GOVERNMENT_ID_FRONT, Government_id_front_ByteImage);
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GOVERNMENT_ID_BACK, Government_id_back_ByteImage);
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CRIMINAL_CERTI, Criminal_Certi_ByteImage);
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ADDRESS_PROOF, Address_Proof_ByteImage);
//
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_PDF1, Pdf1_ByteImage);
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_PDF2, Pdf2_ByteImage);
////        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_PDF3, Pdf3_ByteImage);
//
//        if (SessionSave.getUserSession(Comman.USER_DAD_NUMBER,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_DAD_NUMBER,getActivity()).equalsIgnoreCase("")){
//            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DAA_MOBILE,SessionSave.getUserSession(Comman.USER_DAD_NUMBER, Registration_Activity.activity) );
//        }
//        if (SessionSave.getUserSession(Comman.USER_WIFE_NUMBER,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_WIFE_NUMBER,getActivity()).equalsIgnoreCase("")){
//            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_WIFE_MOBILE, SessionSave.getUserSession(Comman.USER_WIFE_NUMBER, Registration_Activity.activity));
//        }
//
//
//        /*params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_COMPANY_ID, SessionSave.getUserSession(Comman.USER_COMPANY_ID,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_SUB_URB, SessionSave.getUserSession(Comman.USER_SUB_URB,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CITY, SessionSave.getUserSession(Comman.USER_CITY,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_COUNTRY, SessionSave.getUserSession(Comman.USER_COUNTRY,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_STATE, SessionSave.getUserSession(Comman.USER_STATE,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BSB, SessionSave.getUserSession(Comman.USER_BSB,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ABN, SessionSave.getUserSession(Comman.USER_ABN,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_SERVICE_DESCRIPTION, SessionSave.getUserSession(Comman.USER_SERVICE_DESCRIPTION,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_COLOR, SessionSave.getUserSession(Comman.USER_CAR_COLOR,getActivity()));
//        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_REGISTRATION_CERTY_EXPIRY, tv_carRegistration_expiry.getText().toString());*/


        Log.e(TAG,"url UserRegistration = " + url);
        Log.e(TAG,"param UserRegistration = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
        {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG,"responseCode UserRegistration = " + responseCode);
                    Log.e(TAG,"Response UserRegistration = " + json);

                    /// {"status":true,"otp":378048,"message":"OTP send your email address"}

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {

                                SessionSave.saveUserSession(Comman.LOGIN_FLAG_USER,"1",getActivity());
                                SessionSave.saveUserSession(Comman.REGISTER_ATTACHMENT_ACTIVITY, "1",getActivity());
                                SessionSave.saveUserSession(Comman.CREATED_PASSCODE,"",getActivity());

                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", QRCode="", Password="", Address="", SubUrb = ""
                                                , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status="", DOB=""
                                                , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="", ProfileComplete="",
                                                CategoryId="", Balance="", ReferralAmount="", GovIdExpire = "", FrontGovIdCard="",
                                                BackGovIdCard  ="", CriminalCertificate = "", AddressProof  = "";

                                        if (driverProfile.has("Id"))
                                        {
                                            DriverId = driverProfile.getString("Id");
                                            SessionSave.saveUserSession(Comman.USER_ID, DriverId, getActivity());
                                        }
                                        if (driverProfile.has("CompanyId"))
                                        {
                                            CompanyId = driverProfile.getString("CompanyId");
                                            SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, getActivity());
                                        }
                                        if (driverProfile.has("DispatcherId"))
                                        {
                                            DispatcherId = driverProfile.getString("DispatcherId");
                                            SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, getActivity());
                                        }
                                        if (driverProfile.has("Email"))
                                        {
                                            Email = driverProfile.getString("Email");
                                            SessionSave.saveUserSession(Comman.USER_EMAIL, Email, getActivity());
                                        }
                                        if (driverProfile.has("Fullname"))
                                        {
                                            FullName = driverProfile.getString("Fullname");
                                            SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, getActivity());
                                        }

                                        if (driverProfile.has("DOB"))
                                        {
                                            DOB = driverProfile.getString("DOB");
                                            SessionSave.saveUserSession(Comman.USER_DATE_OF_BIRTH, DOB, getActivity());
                                        }

                                        if (driverProfile.has("MobileNo"))
                                        {
                                            MobileNo = driverProfile.getString("MobileNo");
                                            SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, getActivity());
                                        }
                                        if (driverProfile.has("Gender"))
                                        {
                                            Gender = driverProfile.getString("Gender");
                                            SessionSave.saveUserSession(Comman.USER_GENDER, Gender, getActivity());
                                        }
                                        if (driverProfile.has("Image"))
                                        {
                                            Image = driverProfile.getString("Image");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, getActivity());
                                        }
                                        if (driverProfile.has("QRCode"))
                                        {
                                            QRCode = driverProfile.getString("QRCode");
                                            SessionSave.saveUserSession(Comman.USER_QR_CODE, QRCode, getActivity());
                                        }
                                        if (driverProfile.has("Password"))
                                        {
                                            Password = driverProfile.getString("Password");
                                            SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, getActivity());
                                        }
                                        if (driverProfile.has("Address"))
                                        {
                                            Address = driverProfile.getString("Address");
                                            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, getActivity());
                                        }
                                        if (driverProfile.has("SubUrb"))
                                        {
                                            SubUrb = driverProfile.getString("SubUrb");
                                            SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, getActivity());
                                        }


                                        if (driverProfile.has("City"))
                                        {
                                            City = driverProfile.getString("City");
                                            SessionSave.saveUserSession(Comman.USER_CITY, City, getActivity());
                                        }
                                        if (driverProfile.has("State"))
                                        {
                                            State = driverProfile.getString("State");
                                            SessionSave.saveUserSession(Comman.USER_STATE, State, getActivity());
                                        }
                                        if (driverProfile.has("Country"))
                                        {
                                            Country = driverProfile.getString("Country");
                                            SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, getActivity());
                                        }
                                        if (driverProfile.has("ZipCode"))
                                        {
                                            Zipcode = driverProfile.getString("ZipCode");
                                            SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, getActivity());
                                        }
                                        if (driverProfile.has("ReferralCode"))
                                        {
                                            ReferralCode = driverProfile.getString("ReferralCode");
                                            SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, getActivity());
                                        }

                                        if (driverProfile.has("DriverLicenseFront"))
                                        {
                                            DriverLicense = driverProfile.getString("DriverLicenseFront");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE_FRONT, DriverLicense, getActivity());
                                        }
                                        if (driverProfile.has("DriverLicenseBack"))
                                        {
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE_BACK, driverProfile.getString("DriverLicenseBack"), getActivity());
                                        }
                                        if (driverProfile.has("AccreditationCertificate"))
                                        {
                                            AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, getActivity());
                                        }
                                        if (driverProfile.has("DriverLicenseExpire"))
                                        {
                                            DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, getActivity());
                                        }
                                        if (driverProfile.has("AccreditationCertificateExpire"))
                                        {
                                            AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, getActivity());
                                        }
                                        if (driverProfile.has("BankHolderName"))
                                        {
                                            BankHolderName = driverProfile.getString("BankHolderName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, getActivity());
                                        }
                                        if (driverProfile.has("BankName"))
                                        {
                                            BankName = driverProfile.getString("BankName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, getActivity());
                                        }

                                        if (driverProfile.has("BankAcNo"))
                                        {
                                            BankAcNo = driverProfile.getString("BankAcNo");
                                            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, getActivity());
                                        }
                                        if (driverProfile.has("BSB"))
                                        {
                                            BSB = driverProfile.getString("BSB");
                                            SessionSave.saveUserSession(Comman.USER_BSB, BSB, getActivity());
                                        }
                                        if (driverProfile.has("Lat"))
                                        {
                                            Lat = driverProfile.getString("Lat");
                                            SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, getActivity());
                                        }
                                        if (driverProfile.has("Lng"))
                                        {
                                            Lng = driverProfile.getString("Lng");
                                            SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, getActivity());
                                        }
                                        if (driverProfile.has("Status"))
                                        {
                                            Status = driverProfile.getString("Status");
                                            SessionSave.saveUserSession(Comman.USER_STATE, Status, getActivity());
                                        }
                                        if (driverProfile.has("Availability"))
                                        {
                                            Availability = driverProfile.getString("Availability");
                                            SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, getActivity());
                                        }

                                        if (driverProfile.has("DriverDuty"))
                                        {
                                            DriverDuty = driverProfile.getString("DriverDuty");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, getActivity());
                                        }
                                        if (driverProfile.has("ABN"))
                                        {
                                            ABN = driverProfile.getString("ABN");
                                            SessionSave.saveUserSession(Comman.USER_ABN, ABN, getActivity());
                                        }
                                        if (driverProfile.has("Description"))
                                        {
                                            serviceDescription = driverProfile.getString("Description");
                                            SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, getActivity());
                                        }
                                        if (driverProfile.has("DCNumber"))
                                        {
                                            DCNumber = driverProfile.getString("DCNumber");
                                            SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, getActivity());
                                        }
                                        if (driverProfile.has("ProfileComplete"))
                                        {
                                            ProfileComplete = driverProfile.getString("ProfileComplete");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, getActivity());
                                        }
                                        if (driverProfile.has("CategoryId"))
                                        {
                                            CategoryId = driverProfile.getString("CategoryId");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, getActivity());
                                        }
                                        if (driverProfile.has("Balance"))
                                        {
                                            Balance = driverProfile.getString("Balance");
                                            SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, Balance,  getActivity());
                                        }
                                        if (driverProfile.has("ReferralAmount"))
                                        {
                                            ReferralAmount = driverProfile.getString("ReferralAmount");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, ReferralAmount,  getActivity());
                                        }
                                        if (driverProfile.has("GovIdExpire"))
                                        {
                                            GovIdExpire = driverProfile.getString("GovIdExpire");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_EXPIRY, GovIdExpire,  getActivity());
                                        }
                                        if (driverProfile.has("FrontGovIdCard"))
                                        {
                                            FrontGovIdCard = driverProfile.getString("FrontGovIdCard");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_FRONT_ID, FrontGovIdCard,  getActivity());
                                        }
                                        if (driverProfile.has("BackGovIdCard"))
                                        {
                                            BackGovIdCard = driverProfile.getString("BackGovIdCard");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_BACK_ID, BackGovIdCard,  getActivity());
                                        }
                                        if (driverProfile.has("CriminalCertificate"))
                                        {
                                            CriminalCertificate = driverProfile.getString("CriminalCertificate");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_CRIMINAL_CERTI, CriminalCertificate,  getActivity());
                                        }
                                        if (driverProfile.has("AddressProof"))
                                        {
                                            AddressProof = driverProfile.getString("AddressProof");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_ADDRESS_PROOF, AddressProof,  getActivity());
                                        }


                                        if (driverProfile.has("DriverDadMobile"))
                                        {
                                            SessionSave.saveUserSession(Comman.USER_DAD_NUMBER, driverProfile.getString("DriverDadMobile"),  getActivity());
                                        }
                                        if (driverProfile.has("DriverWifeMobile"))
                                        {
                                            SessionSave.saveUserSession(Comman.USER_WIFE_NUMBER, driverProfile.getString("DriverWifeMobile"),  getActivity());
                                        }


                                        String Rate = "";

                                        if (driverProfile.has("rating"))
                                        {
                                            JSONObject driverRating = driverProfile.getJSONObject("rating");

                                            if (driverRating!=null)
                                            {
                                                if (driverRating.has("Rate"))
                                                {
                                                    Rate = driverRating.getString("Rate");
                                                }
                                            }
                                        }
                                        SessionSave.saveUserSession(Comman.USER_DRIVER_RATE, Rate, getActivity());

                                        if (driverProfile.has("Vehicle"))
                                        {
                                            JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                            if (driverVehicle!=null)
                                            {
                                                String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                        , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage,
                                                        VehicleLeftImage, VehicleRightImage, Description, VehicleModelName;

                                                if (driverVehicle.has("Id"))
                                                {
                                                    VehicleId = driverVehicle.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleModel"))
                                                {
                                                    VehicleModel = driverVehicle.getString("VehicleModel");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, getActivity());
                                                }
                                                if (driverVehicle.has("Company"))
                                                {
                                                    CarCompany = driverVehicle.getString("Company");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, getActivity());
                                                }
                                                if (driverVehicle.has("Color"))
                                                {
                                                    CarColor = driverVehicle.getString("Color");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleRegistrationNo"))
                                                {
                                                    VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, getActivity());
                                                }

                                                if (driverVehicle.has("RegistrationCertificate"))
                                                {
                                                    RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                {
                                                    VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, getActivity());
                                                }
                                                if (driverVehicle.has("RegistrationCertificateExpire"))
                                                {
                                                    RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                {
                                                    VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleImage"))
                                                {
                                                    VehicleImage = driverVehicle.getString("VehicleImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleLeftImage"))
                                                {
                                                    VehicleLeftImage = driverVehicle.getString("VehicleLeftImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, VehicleLeftImage, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleRightImage"))
                                                {
                                                    VehicleRightImage = driverVehicle.getString("VehicleRightImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, VehicleRightImage, getActivity());
                                                }

                                                if (driverVehicle.has("Description"))
                                                {
                                                    Description = driverVehicle.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleClass"))
                                                {
                                                    VehicleModelName = driverVehicle.getString("VehicleClass");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, getActivity());

                                                    Log.e(TAG,"VehicleModelName VehicleModelName : "+VehicleModelName);
                                                    if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Business Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Economy"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Taxi"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("LUX-VAN"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Disability"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", getActivity());
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", getActivity());
                                                    }
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", getActivity());
                                                }
                                                dialogClass.hideDialog();
                                            }
                                            else
                                            {
                                                //////// else
                                                dialogClass.hideDialog();
                                            }
                                            dialogClass.hideDialog();

                                            Log.e(TAG,"GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, getActivity()));
                                        }

                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                        }
                                        else
                                        {
                                            errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                                        }
                                    }
                                }


                                dialogClass.hideDialog();
//                                SessionSave.clearUserSession(getActivity());
                                intent = new Intent(getActivity(), Drawer_Activity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                Registration_Activity.activity.finish();

                            }
                            else
                            {
                                Log.e(TAG,"status false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e(TAG,"json no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e(TAG,"json null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));

                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"UserRegistration Exception : " + e.toString());
                    dialogClass.hideDialog();

                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void showPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE_CAMERA);
        }
        else if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE_STORAGE);
        }
        else
        {
            final Dialog dialog = new Dialog(getActivity(),R.style.PauseDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_photo_choose);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            //lp.width = Comman.DEVICE_WIDTH;
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);

            TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
            TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
            TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
            LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
            ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

            ll_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            tv_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            iv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    dialog.dismiss();
                }
            });

            tv_gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            choosePhotoFromGallary();
                        }
                    },800);
                }
            });

            tv_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            takePhotoFromCamera();
                        }
                    },800);

                }
            });

            dialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        ClickCameraFlagResumeCount=0;
        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if(ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT)){
                bitmapSumsung_DriverLicence_front = null;
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
            {
                bitmapSumsung_DriverLicence = null;
                date_DriverLicence="";
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
            {
                bitmapSumsung_Accreditation = null;
                date_Accredi="";
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
            {
                bitmapSumsung_CarRegi = null;
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
            {
                bitmapSumsung_VehiInsurance = null;
                date_VehiIn="";
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
            {
                bitmapSumsung_Government_Id_Front = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
            {
                bitmapSumsung_Government_Id_Back = null;
                date_Government_Id="";
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
            {
                bitmapSumsung_Criminal_Certi = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
            {
                bitmapSumsung_Address_Proof = null;
            }*/
            /*else if(ImageViewClicked.equalsIgnoreCase(PDF1))
            {
                Pdf1_bitmapSumsung = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(PDF2))
            {
                Pdf2_bitmapSumsung = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(PDF3))
            {
                Pdf3_bitmapSumsung = null;
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_PROFILE))
            {
                DriverProfile_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
            {
                Car_front_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
            {
                Car_left_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
            {
                Car_Right_ByteImage = null;
            }
        }
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        ClickCameraFlagResumeCount=0;
        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if(ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT)){
                bitmapSumsung_DriverLicence_front = null;
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
            {
                bitmapSumsung_DriverLicence = null;
                date_DriverLicence="";
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
            {
                bitmapSumsung_Accreditation = null;
                date_Accredi="";
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
            {
                bitmapSumsung_CarRegi = null;
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
            {
                bitmapSumsung_VehiInsurance = null;
                date_VehiIn="";
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
            {
                bitmapSumsung_Government_Id_Front = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
            {
                bitmapSumsung_Government_Id_Back = null;
                date_Government_Id="";
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
            {
                bitmapSumsung_Criminal_Certi = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
            {
                bitmapSumsung_Address_Proof = null;
            }*/
            /*else if(ImageViewClicked.equalsIgnoreCase(PDF1))
            {
                Pdf1_bitmapSumsung = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(PDF2))
            {
                Pdf2_bitmapSumsung = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(PDF3))
            {
                Pdf3_bitmapSumsung = null;
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_PROFILE))
            {
                DriverProfile_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
            {
                Car_front_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
            {
                Car_left_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
            {
                Car_Right_ByteImage = null;
            }
        }
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }
        else
        {
            userChoosenTask = chooseCamera;
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            Log.e(TAG, "imageUri11111111111111 : "+imageUri);
            startActivityForResult(intent, CAMERA);
        }
    }

    public String getDeviceName(){
        String manufacturer = Build.MANUFACTURER;
        return manufacturer;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e(TAG,"checkSelfPermission CAMERA permission granted");
                    showPictureDialog();
                }
                break;

            case MY_REQUEST_CODE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e(TAG,"checkSelfPermission GALLERY permission granted");
                    showPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG, "onActivityResult requestCode "+requestCode);
        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                onCaptureImageResult(data);
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG,"MediaStore");
        }
        datepickerDialogFlag=0;
        bitmapImage = getResizedBitmap(bitmap,400);

        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT))
            {
                bitmapSumsung_DriverLicence_front = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_driverLicence_front);
                DriverLicence_Front_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
            {
                bitmapSumsung_DriverLicence = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_driverLicence);
                DriverLicence_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
            {
                bitmapSumsung_Accreditation = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_accreditation_certy);
                Accreditation_certy_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
            {
                bitmapSumsung_CarRegi = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_car_Registration);
                Car_Registration_ByteImage = ConvertToByteArray(bitmapImage);
//                OpenPopopFor_datePicker(tv_carRegistration_expiry, iv_car_Registration);
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
            {
                bitmapSumsung_VehiInsurance = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_vehicle_insurance);
                Vehicle_insurance_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
            }*/
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
            {
                bitmapSumsung_Government_Id_Front = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivGovernmentIdFront);
                Government_id_front_ByteImage = ConvertToByteArray(bitmapImage);
//                OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdFront);
            }
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
            {
                bitmapSumsung_Government_Id_Back = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivGovernmentIdBack);
                Government_id_back_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdBack);
            }
            /*else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
            {
                bitmapSumsung_Criminal_Certi = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCriminalCertificate);
                Criminal_Certi_ByteImage = ConvertToByteArray(bitmapImage);
//                OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdFront);
            }
            else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
            {
                bitmapSumsung_Address_Proof = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivAddressProof);
                Address_Proof_ByteImage = ConvertToByteArray(bitmapImage);
//                OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdFront);
            }*/
            /*else if(ImageViewClicked.equalsIgnoreCase(PDF1))
            {
                Pdf1_bitmapSumsung = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivPdf1);
                Pdf1_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if(ImageViewClicked.equalsIgnoreCase(PDF2))
            {
                Pdf2_bitmapSumsung = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivPdf2);
                Pdf2_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if(ImageViewClicked.equalsIgnoreCase(PDF3))
            {
                Pdf3_bitmapSumsung = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivPdf3);
                Pdf3_ByteImage = ConvertToByteArray(bitmapImage);
            }*/
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_PROFILE))
            {
                bitmapSumsung_DriverProfile = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivDriverProfile);
                DriverProfile_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
            {
                bitmapSumsung_CarFront = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCarFront);
                Car_front_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
            {
                bitmapSumsung_CarLeft = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCarLeft);
                Car_left_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
            {
                bitmapSumsung_CarRight = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCarRight);
                Car_Right_ByteImage = ConvertToByteArray(bitmapImage);
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "PickNGo", null);
        return Uri.parse(path);
    }

    private void onCaptureImageResult(Intent data)
    {
        datepickerDialogFlag=0;
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT))
                {
                    bitmapSumsung_DriverLicence_front = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_DriverLicence_front);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                {
                    bitmapSumsung_DriverLicence = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_DriverLicence);
                }
                /*else if (ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                {
                    bitmapSumsung_Accreditation = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Accreditation);
                }*/
                else if (ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                {
                    bitmapSumsung_CarRegi = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarRegi);
                }
                /*else if (ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                {
                    bitmapSumsung_VehiInsurance = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_VehiInsurance);
                }*/
                else if (ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
                {
                    bitmapSumsung_Government_Id_Front = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Government_Id_Front);
                }
                else if (ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
                {
                    bitmapSumsung_Government_Id_Back = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Government_Id_Back);
                }
                /*else if (ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
                {
                    bitmapSumsung_Criminal_Certi = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Criminal_Certi);
                }
                else if (ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
                {
                    bitmapSumsung_Address_Proof = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Address_Proof);
                }*/
                /*else if(ImageViewClicked.equalsIgnoreCase(PDF1))
                {
                    Pdf1_bitmapSumsung = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, Pdf1_bitmapSumsung);
                }
                else if(ImageViewClicked.equalsIgnoreCase(PDF2))
                {
                    Pdf2_bitmapSumsung = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, Pdf2_bitmapSumsung);
                }
                else if(ImageViewClicked.equalsIgnoreCase(PDF3))
                {
                    Pdf3_bitmapSumsung = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, Pdf3_bitmapSumsung);
                }*/
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_PROFILE))
                {
                    bitmapSumsung_DriverProfile = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_DriverProfile);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
                {
                    bitmapSumsung_CarFront = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarFront);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
                {
                    bitmapSumsung_CarLeft = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarLeft);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
                {
                    bitmapSumsung_CarRight = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarRight);
                }
            }
        }

        try
        {
            Log.e(TAG, "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
            Log.e(TAG,"######### bmp height = "+thumbnail.getHeight());
            Log.e(TAG,"######### bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_driverLicence_front);
                    DriverLicence_Front_ByteImage = ConvertToByteArray(thumbnail);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_driverLicence);
                    DriverLicence_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
                }
                /*else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_accreditation_certy);
                    Accreditation_certy_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
                }*/
                else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_car_Registration);
                    Car_Registration_ByteImage = ConvertToByteArray(thumbnail);
//                    OpenPopopFor_datePicker(tv_carRegistration_expiry, iv_car_Registration);
                }
                /*else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_vehicle_insurance);
                    Vehicle_insurance_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
                }*/
                else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivGovernmentIdFront);
                    Government_id_front_ByteImage = ConvertToByteArray(thumbnail);
//                    OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdFront);
                }
                else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivGovernmentIdBack);
                    Government_id_back_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdBack);
                }
                /*else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCriminalCertificate);
                    Criminal_Certi_ByteImage = ConvertToByteArray(thumbnail);
//                    OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdFront);
                }
                else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivAddressProof);
                    Address_Proof_ByteImage = ConvertToByteArray(thumbnail);
//                    OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdFront);
                }*/
                /*else if(ImageViewClicked.equalsIgnoreCase(PDF1))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivPdf1);
                    Pdf1_ByteImage = ConvertToByteArray(bitmapImage);
                }
                else if(ImageViewClicked.equalsIgnoreCase(PDF2))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivPdf2);
                    Pdf2_ByteImage = ConvertToByteArray(bitmapImage);
                }
                else if(ImageViewClicked.equalsIgnoreCase(PDF3))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivPdf3);
                    Pdf3_ByteImage = ConvertToByteArray(bitmapImage);
                }*/
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_PROFILE))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivDriverProfile);
                    DriverProfile_ByteImage = ConvertToByteArray(thumbnail);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCarFront);
                    Car_front_ByteImage = ConvertToByteArray(thumbnail);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCarLeft);
                    Car_left_ByteImage = ConvertToByteArray(thumbnail);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCarRight);
                    Car_Right_ByteImage = ConvertToByteArray(thumbnail);
                }
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"call exception = "+e.getMessage());
        }
    }

    public void OpenPopopFor_datePicker(final TextView textView, final ImageView imageView)
    {
        /*if (datepickerDialogFlag==0)
        {
            Log.e("lllllllll","1111111111111111");
            datepickerDialogFlag=1;


        }*/
        Log.e("lllllllll","1111111111111111");
        if (fromDatePickerDialog!=null)
        {
            Log.e(TAG,"22222222222222222222");
            fromDatePickerDialog.dismiss();
        }
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogThemeReal, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(newDate.getTime()));

                if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
                {
                    if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                    {
                        date_DriverLicence = dateFormatter.format(newDate.getTime());
                    }
                    /*else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                    {
                        date_Accredi = dateFormatter.format(newDate.getTime());
                    }
                    else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                    {
                        date_VehiIn = dateFormatter.format(newDate.getTime());
                    }*/
                    else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
                    {
                        date_Government_Id = dateFormatter.format(newDate.getTime());
                    }
                }
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        fromDatePickerDialog.setSpinnersShown

        if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
        {
            fromDatePickerDialog.setTitle(getActivity().getResources().getString(R.string.setlect_driver_licence_expiry_date));
        }
        /*else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
        {
            fromDatePickerDialog.setTitle(getActivity().getResources().getString(R.string.select_revenue_licence_expiry_date));
        }
        else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
        {
            fromDatePickerDialog.setTitle(getActivity().getResources().getString(R.string.select_vehicle_insurance_policy_expiry_date));
        }*/
        else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
        {
            fromDatePickerDialog.setTitle(getActivity().getResources().getString(R.string.select_government_expiry_date));
        }

        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
        fromDatePickerDialog.show();
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.e(TAG,"yyyyyyyyyyy: ");
                textView.setText("");
                imageView.setImageResource(R.drawable.icon_camera_yellow);
            }
        });
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (fromDatePickerDialog!=null)
        {
            fromDatePickerDialog.dismiss();
        }

        if (dialogClass!=null)
        {
            dialogClass.hideDialog();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            if (bitmapSumsung_DriverLicence_front!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_DriverLicence_front);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(iv_driverLicence_front);
                DriverLicence_Front_ByteImage = ConvertToByteArray(bitmapSumsung_DriverLicence_front);
            }
            else
            {
                iv_driverLicence_front.setImageBitmap(null);
            }


            if (bitmapSumsung_DriverLicence!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_DriverLicence);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(iv_driverLicence);
                DriverLicence_ByteImage = ConvertToByteArray(bitmapSumsung_DriverLicence);
            }
            else
            {
                iv_driverLicence.setImageBitmap(null);
                tv_driver_licence_expiry.setText(date_DriverLicence);
            }

            /*if (bitmapSumsung_Accreditation!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Accreditation);Picasso.get()
                    .load(imageUri)
                    .fit()
                    .into(iv_accreditation_certy);
                Accreditation_certy_ByteImage = ConvertToByteArray(bitmapSumsung_Accreditation);

            }
            else
            {
                iv_accreditation_certy.setImageBitmap(null);
                tv_accreditation_expiry.setText(date_DriverLicence);
            }*/

            if (bitmapSumsung_CarRegi!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarRegi);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(iv_car_Registration);
                Car_Registration_ByteImage = ConvertToByteArray(bitmapSumsung_CarRegi);
            }
            else
            {
                iv_car_Registration.setImageBitmap(null);
            }

            /*if (bitmapSumsung_VehiInsurance!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_VehiInsurance);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(iv_vehicle_insurance);
                Vehicle_insurance_ByteImage = ConvertToByteArray(bitmapSumsung_VehiInsurance);
            }
            else
            {
                iv_vehicle_insurance.setImageBitmap(null);
                tv_vehicle_Insurance_expiry.setText(date_DriverLicence);
            }*/

            if (bitmapSumsung_Government_Id_Front!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Government_Id_Front);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivGovernmentIdFront);
                Government_id_front_ByteImage = ConvertToByteArray(bitmapSumsung_Government_Id_Front);
            }
            else
            {
                ivGovernmentIdFront.setImageBitmap(null);
            }

            if (bitmapSumsung_Government_Id_Back!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Government_Id_Back);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivGovernmentIdBack);
                Government_id_back_ByteImage = ConvertToByteArray(bitmapSumsung_Government_Id_Back);
            }
            else
            {
                ivGovernmentIdBack.setImageBitmap(null);
                tvGovernmentIdExpiry.setText(date_Government_Id);
            }

            if (bitmapSumsung_DriverProfile!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_DriverProfile);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivDriverProfile);
                DriverProfile_ByteImage = ConvertToByteArray(bitmapSumsung_DriverProfile);
            }
            else
            {
                ivDriverProfile.setImageBitmap(null);
            }

            if (bitmapSumsung_CarFront!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarFront);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivCarFront);
                Car_front_ByteImage = ConvertToByteArray(bitmapSumsung_CarFront);
            }
            else
            {
                ivCarFront.setImageBitmap(null);
            }

            if (bitmapSumsung_CarLeft!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarLeft);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivCarLeft);
                Car_left_ByteImage = ConvertToByteArray(bitmapSumsung_CarLeft);
            }
            else
            {
                ivCarLeft.setImageBitmap(null);
            }

            if (bitmapSumsung_CarRight!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarRight);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivCarRight);
                Car_Right_ByteImage = ConvertToByteArray(bitmapSumsung_CarRight);
            }
            else
            {
                ivCarRight.setImageBitmap(null);
            }

            /*if (bitmapSumsung_Criminal_Certi!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Criminal_Certi);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivCriminalCertificate);
                Criminal_Certi_ByteImage = ConvertToByteArray(bitmapSumsung_Criminal_Certi);
            }
            else
            {
                ivCriminalCertificate.setImageBitmap(null);
            }*/

            /*if (bitmapSumsung_Address_Proof!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_Address_Proof);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivAddressProof);
                Address_Proof_ByteImage = ConvertToByteArray(bitmapSumsung_Address_Proof);
            }
            else
            {
                ivAddressProof.setImageBitmap(null);
            }*/

            /*if (Pdf1_bitmapSumsung != null) {
                imageUri = getImageUri(Registration_Activity.activity, Pdf1_bitmapSumsung);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivPdf1);
                Pdf1_ByteImage = ConvertToByteArray(Pdf1_bitmapSumsung);
            }else{
                ivPdf1.setImageBitmap(null);
            }

            if (Pdf2_bitmapSumsung != null) {
                imageUri = getImageUri(Registration_Activity.activity, Pdf2_bitmapSumsung);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivPdf2);
                Pdf2_ByteImage = ConvertToByteArray(Pdf2_bitmapSumsung);
            }else{
                ivPdf2.setImageBitmap(null);
            }

            if (Pdf3_bitmapSumsung != null) {
                imageUri = getImageUri(Registration_Activity.activity, Pdf3_bitmapSumsung);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivPdf3);
                Pdf3_ByteImage = ConvertToByteArray(Pdf3_bitmapSumsung);
            }else{
                ivPdf3.setImageBitmap(null);
            }*/

            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    /*if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }*/
                    /*if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }*/
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    /*if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }*/
                    /*if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }*/
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                    OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
                }
                /*else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }
                    *//*if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }*//*
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                    OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
                }*/
                else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    /*if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }*/
                    /*if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }*/
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                }
                /*else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }
                    if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                    OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
                }*/
                else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    /*if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }*/
                    /*if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }*/
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                }
                /*else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }
                    if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                    OpenPopopFor_datePicker(tvGovernmentIdExpiry, iv_vehicle_insurance);
                }*/
                /*else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    *//*if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }*//*
                    *//*if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }*//*
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                }*/
                /*else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
                {
                    if (date_DriverLicence!=null && !date_DriverLicence.equalsIgnoreCase(""))
                    {
                        tv_driver_licence_expiry.setText(date_DriverLicence);
                    }
                    *//*if (date_Accredi!=null && !date_Accredi.equalsIgnoreCase(""))
                    {
                        tv_accreditation_expiry.setText(date_Accredi);
                    }*//*
                    *//*if (date_VehiIn!=null && !date_VehiIn.equalsIgnoreCase(""))
                    {
                        tv_vehicle_Insurance_expiry.setText(date_VehiIn);
                    }*//*
                    if (date_Government_Id!=null && !date_Government_Id.equalsIgnoreCase(""))
                    {
                        tvGovernmentIdExpiry.setText(date_Government_Id);
                    }
                }*/
            }
        }
    }
}
