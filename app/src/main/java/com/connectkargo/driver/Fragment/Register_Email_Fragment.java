package com.connectkargo.driver.Fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Activity.Login_Activity;
import com.connectkargo.driver.Activity.Registration_Activity;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Register_Email_Fragment extends Fragment implements View.OnClickListener{

    private String TAG = "Register_Email_Fragment";

    private TextView tv_sendOtp, tv_register_verifyOtp, tv_backToLoginEmail, tv_backToLoginOtp;
    private EditText et_register_email, et_register_mobileNo, et_register_password, et_register_RePassword, et_register_Otp;
    private LinearLayout main_layout, ll_forOtp;
    private ScrollView Rl_forEmail;

    private DialogClass dialogClass;
    private AQuery aQuery;
    private Animation animBounce;

    private ErrorDialogClass errorDialogClass;
    private Intent intent;


    private JSONArray jsonArray_CompanyId = new JSONArray();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_register_email, container, false);

        aQuery = new AQuery(getActivity());
        errorDialogClass =  new ErrorDialogClass(getActivity());
        dialogClass = new DialogClass(Registration_Activity.activity, 1);

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);
        ll_forOtp = (LinearLayout) rootView.findViewById(R.id.ll_forOtp);
        Rl_forEmail = (ScrollView) rootView.findViewById(R.id.Rl_forEmail);

        tv_sendOtp = (TextView) rootView.findViewById(R.id.tv_sendOtp);
        tv_backToLoginEmail = (TextView) rootView.findViewById(R.id.tv_backToLoginEmail);
        tv_register_verifyOtp = (TextView) rootView.findViewById(R.id.tv_register_verifyOtp);
        tv_backToLoginOtp = (TextView) rootView.findViewById(R.id.tv_backToLoginOtp);

        et_register_email = (EditText) rootView.findViewById(R.id.et_register_email);
        et_register_mobileNo = (EditText) rootView.findViewById(R.id.et_register_mobileNo);
        et_register_password = (EditText) rootView.findViewById(R.id.et_register_password);
        et_register_RePassword = (EditText) rootView.findViewById(R.id.et_register_RePassword);
        et_register_Otp = (EditText) rootView.findViewById(R.id.et_register_Otp);

        tv_backToLoginEmail.setOnClickListener(this);
        tv_backToLoginOtp.setOnClickListener(this);
        tv_sendOtp.setOnClickListener(this);
        tv_register_verifyOtp.setOnClickListener(this);

        String CheckUserLogin = SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY, Registration_Activity.activity);
        String CheckUserOTP = SessionSave.getUserSession(Comman.REGISTER_OTP_ACTIVITY, Registration_Activity.activity);

        if (CheckUserLogin!=null && !CheckUserLogin.equalsIgnoreCase("") && CheckUserLogin.equalsIgnoreCase("1"))
        {
            if (CheckUserOTP != null && !CheckUserOTP.equalsIgnoreCase("") && CheckUserOTP.equalsIgnoreCase("1"))
            {
                Rl_forEmail.setVisibility(View.GONE);
                ll_forOtp.setVisibility(View.VISIBLE);
            }
            else
            {
                setUserDetial();

                Rl_forEmail.setVisibility(View.VISIBLE);
                ll_forOtp.setVisibility(View.GONE);
            }
        }
        else
        {
            Rl_forEmail.setVisibility(View.VISIBLE);
            ll_forOtp.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tv_backToLoginEmail:

                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_backToLoginEmail.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {

                        if (Registration_Activity.activity!=null)
                        {
                            Registration_Activity.activity.finish();
                        }

                        SessionSave.clearUserSession(Registration_Activity.activity);
                        intent = new Intent(Registration_Activity.activity, Login_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.tv_backToLoginOtp:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_backToLoginOtp.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {

                        if (Registration_Activity.activity!=null)
                        {
                            Registration_Activity.activity.finish();
                        }
                        SessionSave.clearUserSession(Registration_Activity.activity);
                        intent = new Intent(Registration_Activity.activity, Login_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.tv_sendOtp:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_sendOtp.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        SendDataForOtp();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.tv_register_verifyOtp:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_register_verifyOtp.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (TextUtils.isEmpty(et_register_Otp.getText().toString().trim()))
                        {
                            new SnackbarUtils(main_layout, getString(R.string.please_enter_otp_number),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
                        }
                        else
                        {
                            OTPVerification();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
        }
    }

    private void setUserDetial()
    {
        if (SessionSave.getUserSession(Comman.USER_EMAIL, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_EMAIL, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_email.setText(SessionSave.getUserSession(Comman.USER_EMAIL, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_mobileNo.setText(SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_password.setText(SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity).toString().trim());
        }
        if (SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_RePassword.setText(SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity).toString().trim());
        }
    }

    private void SendDataForOtp()
    {
        String Email = et_register_email.getText().toString().trim();
        String MobNo = et_register_mobileNo.getText().toString().trim();
        String Password = et_register_password.getText().toString().trim();
        String RePassword = et_register_RePassword.getText().toString().trim();

        if (TextUtils.isEmpty(Email))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_email),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else if(!isEmailValid(Email))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_email),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else if (TextUtils.isEmpty(MobNo))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_mobile_number),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else if(MobNo.length()!=9)
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_mobile_no),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else if (TextUtils.isEmpty(Password))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_password),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else if (Password.length() < 8 )
        {
            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.password_must_contain_8_character),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else if (TextUtils.isEmpty(RePassword))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_repassword),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
//        else if (RePassword.length() < 8 )
//        {
//            new SnackbarUtils(main_layout, "Confirm password must contain atleast 8 characters",
//                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
//        }
        else if (!Password.equalsIgnoreCase(RePassword))
        {
            new SnackbarUtils(main_layout, getString(R.string.password_not_match),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else
        {
//            if (SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY,getActivity()) != null &&
//                    !SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY,getActivity()).equalsIgnoreCase("") &&
//                    SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY,getActivity()).equalsIgnoreCase("1"))
//            {
//                Log.e(TAG,"SendDataForOtp() REGISTER_EMAIL_ACTIVITY == 1");
//
//                if (checkSessionField())
//                {
//                    Log.e(TAG,"SendDataForOtp() checkSessionField == true");
//
//                    if (Global.isNetworkconn(getActivity()))
//                    {
//                        Log.e(TAG,"SendDataForOtp()  internet");
//
//                        EmailVerification();
//                    }
//                    else
//                    {
//                        errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
//                    }
//                }
//                else
//                {
//                    Log.e(TAG,"SendDataForOtp() checkSessionField == false");
//
//                    ((Registration_Activity)getActivity()).setTabAddProfileDetails();
//                }
//            }
//            else
//            {
//                Log.e(TAG,"SendDataForOtp() REGISTER_EMAIL_ACTIVITY != 1");
//
//                if (Global.isNetworkconn(getActivity()))
//                {
//                    EmailVerification();
//                }
//                else
//                {
//                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
//                }
//            }

            if (Global.isNetworkconn(getActivity()))
            {
                EmailVerification();
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
    }

    public boolean checkSessionField()
    {
        boolean flage = false;
        String email ="", phone="", pass="", confirmPass="";

        email = SessionSave.getUserSession(Comman.USER_EMAIL, Registration_Activity.activity).trim();
        phone = SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER, Registration_Activity.activity).trim();
        pass = SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity).trim();
        confirmPass = SessionSave.getUserSession(Comman.USER_PASSWORD, Registration_Activity.activity).trim();

        if (email.equals(et_register_email.getText().toString().trim()))
        {
            Log.e(TAG,"checkSessionField() email == " + email);

            if (phone.equals(et_register_mobileNo.getText().toString().trim()))
            {
                Log.e(TAG,"checkSessionField() phone == " + phone);

                if (pass.equals(et_register_password.getText().toString().trim()))
                {
                    Log.e(TAG,"checkSessionField() pass == " + pass);

                    if (confirmPass.equals(et_register_RePassword.getText().toString().trim()))
                    {
                        Log.e(TAG,"checkSessionField() confirmPass == " + confirmPass);

                        flage = false;
                    }
                    else
                    {
                        Log.e(TAG,"checkSessionField() confirmPass != " + confirmPass);
                        Log.e(TAG,"checkSessionField() et_register_RePassword != " + et_register_RePassword.getText().toString());

                        flage = true;
                    }
                }
                else
                {
                    Log.e(TAG,"checkSessionField() pass != " + pass);
                    Log.e(TAG,"checkSessionField() et_register_password != " + et_register_password.getText().toString());

                    flage = true;
                }
            }
            else
            {
                Log.e(TAG,"checkSessionField() phone != " + phone);
                Log.e(TAG,"checkSessionField() et_register_mobileNo != " + et_register_mobileNo.getText().toString());
                flage = true;
            }
        }
        else
        {
            Log.e(TAG,"checkSessionField() email != " + email);
            Log.e(TAG,"checkSessionField() et_register_email != " + et_register_email.getText().toString());

            flage = true;
        }

        return flage;
    }

    private void EmailVerification()
    {
        Map<String, Object> params = new HashMap<String, Object>();

        dialogClass.showDialog();

        String url = WebServiceAPI.WEB_SERVICE_OTP_FOR_REGISTER;
        params.put(WebServiceAPI.OTP_FOR_REGISTER_PARAM_EMAIL, et_register_email.getText().toString().trim());
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_MOBILE_NUMBER, et_register_mobileNo.getText().toString().trim());

        Log.e("EmailVerification", "URL = " + url);
        Log.e("EmailVerification", "Param = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("EmailVerification", "responseCode = " + responseCode);
                    Log.e("EmailVerification", "Response = " + json);

                    /// {"status":true,"otp":378048,"message":"OTP send your email address"}

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("company"))
                                {
                                    String  CompanyId="", CompanyName="", Address="", City="", State="", Country="";
                                    jsonArray_CompanyId = new JSONArray();

                                    JSONArray arrayCompany = json.getJSONArray("company");

                                    for (int i=0; i<arrayCompany.length() ; i++)
                                    {
                                        JSONObject objectCompany = arrayCompany.getJSONObject(i);
                                        if (objectCompany.has("Id"))
                                        {
                                            CompanyId = objectCompany.getString("Id");
                                        }
                                        if (objectCompany.has("CompanyName"))
                                        {
                                            CompanyName = objectCompany.getString("CompanyName");
                                        }
                                        if (objectCompany.has("City"))
                                        {
                                            City = objectCompany.getString("City");
                                        }
                                        if (objectCompany.has("State"))
                                        {
                                            State = objectCompany.getString("State");
                                        }
                                        if (objectCompany.has("Country"))
                                        {
                                            Country = objectCompany.getString("Country");
                                        }
                                        JSONObject companyIdObject = new JSONObject();
                                        companyIdObject.put(Comman.USER_COMPANY_ID_FOR_STORE, CompanyId);
                                        companyIdObject.put(Comman.USER_COMPANY_NAME_FOR_STORE, CompanyName);
                                        companyIdObject.put(Comman.USER_COMPANY_CITY_FOR_STORE, City);
                                        companyIdObject.put(Comman.USER_COMPANY_STATE_FOR_STORE, State);
                                        companyIdObject.put(Comman.USER_COMPANY_COUNTRY_FOR_STORE, Country);
                                        jsonArray_CompanyId.put(companyIdObject);
                                    }

                                    JSONObject studentsObj = new JSONObject();
                                    studentsObj.put(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE, jsonArray_CompanyId);

                                    String jsonCompanyIdStr = studentsObj.toString();
                                    SessionSave.saveUserSession(Comman.USER_COMPANY_ID_ARRAY, jsonCompanyIdStr, Registration_Activity.activity);

                                    Log.e("jsonArray_CompanyId", "jsonArray_CompanyIdj222222222222222 :"+SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY, Registration_Activity.activity));
                                }

                                SessionSave.saveUserSession(Comman.REGISTER_EMAIL_ACTIVITY,"1", Registration_Activity.activity);
//                                if (Login_Activity.activity!=null)
//                                {
//                                    Login_Activity.activity.finish();
//                                }

                                Log.e("status", "true");
                                if (json.has("otp"))
                                {
                                    SessionSave.saveUserSession(Comman.USER_EMAIL, et_register_email.getText().toString(), Registration_Activity.activity);
                                    SessionSave.saveUserSession(Comman.USER_ONE_TIME_OTP,json.getString("otp"), Registration_Activity.activity);
                                    SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, et_register_mobileNo.getText().toString(),getActivity());
                                    SessionSave.saveUserSession(Comman.USER_PASSWORD, et_register_password.getText().toString(),getActivity());

                                    Log.e("SessionSave.","et_register_email ; "+SessionSave.getUserSession(Comman.USER_EMAIL, Registration_Activity.activity));
                                    Log.e("SessionSave.","otp ; "+SessionSave.getUserSession(Comman.USER_ONE_TIME_OTP, Registration_Activity.activity));

                                    Rl_forEmail.setVisibility(View.GONE);
                                    ll_forOtp.setVisibility(View.VISIBLE);

                                    dialogClass.hideDialog();
                                }
                                else
                                {
                                    Rl_forEmail.setVisibility(View.VISIBLE);
                                    ll_forOtp.setVisibility(View.GONE);

                                    Log.e("otp", "not Find");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog( getString(R.string.something_is_wrong), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Rl_forEmail.setVisibility(View.VISIBLE);
                                ll_forOtp.setVisibility(View.GONE);

                                Log.e("EmailVerificat status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), "" );
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Rl_forEmail.setVisibility(View.VISIBLE);
                            ll_forOtp.setVisibility(View.GONE);

                            Log.e("EmailVerification json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), "" );
                            }
                            else
                            {
                                errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Rl_forEmail.setVisibility(View.VISIBLE);
                        ll_forOtp.setVisibility(View.GONE);

                        Log.e("EmailVerification json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog( getString(R.string.something_is_wrong), getString(R.string.error_message));

                    }
                }
                catch (Exception e)
                {
                    Rl_forEmail.setVisibility(View.VISIBLE);
                    ll_forOtp.setVisibility(View.GONE);

                    Log.e("EmailVerification", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog( getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void OTPVerification()
    {
        String currentOtp = SessionSave.getUserSession(Comman.USER_ONE_TIME_OTP,Registration_Activity.activity);
        if (currentOtp!= null && !currentOtp.equalsIgnoreCase(""))
        {
            if (currentOtp.equalsIgnoreCase(et_register_Otp.getText().toString().trim()))
            {
                SessionSave.saveUserSession(Comman.REGISTER_OTP_ACTIVITY, "1", Registration_Activity.activity);
//                ((Registration_Activity)getActivity()).setTabAddProfileDetails();
                ((Registration_Activity)getActivity()).setTabAttachment();
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.otp_is_not_same), getString(R.string.error_message));
            }
        }
        else
        {
            errorDialogClass.showDialog(getString(R.string.please_try_to_login_again), getString(R.string.error_message));
        }
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
