package com.connectkargo.driver.Fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Activity.Login_Activity;
import com.connectkargo.driver.Activity.Registration_Activity;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.R;


public class Register_Bank_Fragment extends Fragment implements View.OnClickListener{

    TextView tv_nextBank, tv_backToLogin;
    EditText et_register_AccountHolderName, et_register_BankName, et_register_bankBranch, et_register_AccountNo;

    LinearLayout main_layout;

    private Animation animBounce;
    Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_register_bank, container, false);

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);

        tv_nextBank = (TextView) rootView.findViewById(R.id.tv_nextBank);
        tv_backToLogin = (TextView) rootView.findViewById(R.id.tv_backToLogin);

        et_register_AccountHolderName =(EditText) rootView.findViewById(R.id.et_register_AccountHolderName);
        et_register_BankName =(EditText) rootView.findViewById(R.id.et_register_BankName);
        et_register_bankBranch =(EditText) rootView.findViewById(R.id.et_register_bankBranch);
        et_register_AccountNo =(EditText) rootView.findViewById(R.id.et_register_AccountNo);

        tv_nextBank.setOnClickListener(this);
        tv_backToLogin.setOnClickListener(this);

        setUserDetial();

        return rootView;
    }

    private void setUserDetial()
    {
        if(SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_AccountHolderName.setText(SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,Registration_Activity.activity).toString().trim());
        }
        if(SessionSave.getUserSession(Comman.USER_BANK_NAME,Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_BANK_NAME,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_BankName.setText(SessionSave.getUserSession(Comman.USER_BANK_NAME,Registration_Activity.activity).toString().trim());
        }
        if(SessionSave.getUserSession(Comman.USER_BANK_BRANCH,Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_BANK_BRANCH,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_bankBranch.setText(SessionSave.getUserSession(Comman.USER_BANK_BRANCH,Registration_Activity.activity).toString().trim());
        }
        if(SessionSave.getUserSession(Comman.USER_BANK_AC_NO,Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_BANK_AC_NO,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_AccountNo.setText(SessionSave.getUserSession(Comman.USER_BANK_AC_NO,Registration_Activity.activity).toString().trim());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_nextBank:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_nextBank.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        CheckUserData();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                break;

            case R.id.tv_backToLogin:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_backToLogin.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        SessionSave.clearUserSession(Registration_Activity.activity);
                        intent = new Intent(Registration_Activity.activity, Login_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        if (Registration_Activity.activity!=null)
                        {
                            Registration_Activity.activity.finish();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
        }

    }

    private void CheckUserData()
    {
        if (TextUtils.isEmpty(et_register_AccountHolderName.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_company_holder_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_register_BankName.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_register_bankBranch.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_branch),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_register_AccountNo.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_no),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else  if (et_register_AccountNo.getText().toString().trim().length() < 9 )
        {
            new SnackbarUtils(main_layout, getContext().getResources().getString(R.string.please_enter_valid_account_number),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
        }
        else
        {
            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, et_register_AccountHolderName.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_BANK_NAME, et_register_BankName.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_BANK_BRANCH, et_register_bankBranch.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, et_register_AccountNo.getText().toString(),getActivity());

            SessionSave.saveUserSession(Comman.REGISTER_BANK_ACTIVITY, "1",getActivity());

            ((Registration_Activity)getActivity()).setTabCarInfo1();
        }
    }
}