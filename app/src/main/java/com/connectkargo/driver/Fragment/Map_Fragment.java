package com.connectkargo.driver.Fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Been.HeatmapData.HeatmapData;
import com.connectkargo.driver.Been.distance.DataRoute;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.DataParser;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.GPSTracker;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Activity.Login_Activity;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class Map_Fragment extends Fragment implements OnMapReadyCallback {

    private static String TAG = "Map_Fragment";
    public static GoogleMap googleMap;
    public static Marker marker, marker_pickUpLocation, marker_dropOffLocation;

    public static double latitude, longitude;
    public static double pickUpLat, pickUpLng;
    public static double DropOffLat, DropOffLng;

    public static TextView tv_location;

    public static ImageView reset_current_location, iv_TripToDestination;

    public static GPSTracker gpsTracker;

    public static LinearLayout ll_start_trip_option, ll_complete_hold;
    public static RelativeLayout main_layout;
    public static TextView tv_start_trip, tv_passengerInfo, tv_directionAcceptReq, tv_directionStartTrip, tv_complete_trip, tv_cancel_trip;////, tv_start_waiting, tv_stop_waiting

    public static AQuery aQuery;
    public static DialogClass dialogClass;

    public static String dropOffLocationApi="";
    public static int intMarkerAnim;
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static int ClickRelocateButton=0;
    public static int readBearing =0;
    public LinearLayout reset_sos;

    public static LatLng sydney=null;

    public static Float bearingF = 0f;

    public static int flagCameraAnim=0;
    public static Ringtone ringTone;
    public static Dialog dialogTollFee;
    public static String TollFeeApi="";

    public static ErrorDialogClass errorDialogClass;

    public static Handler handler;
    public static Calendar calendar;
    public static SimpleDateFormat dateFormat;
    String meterActiveFlag;
    public static int AnimateCameraTime =0;
    SupportMapFragment mapFragment;
    public static int flagMapReady=0;
    public static ArrayList<LatLng> latLngArrayList= new ArrayList<>();

    public static Polyline polyline;
    public static int FlagPolyline=0;

    private static TileOverlay mOverlay;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        intMarkerAnim=0;
        flagCameraAnim=0;
        ClickRelocateButton=0;
        AnimateCameraTime =0;
        flagMapReady=0;
        latLngArrayList.clear();
        Log.e("GetLocationGps","11111111111");

        aQuery = new AQuery(Drawer_Activity.activity);
        dialogClass = new DialogClass(Drawer_Activity.activity, 1);
        errorDialogClass = new ErrorDialogClass(getActivity());
        handler = new Handler();
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

        try
        {
            Uri path =  Uri.parse("android.resource://"+ getActivity().getPackageName() + "/raw/ring_pick_n_go");
            ringTone = RingtoneManager.getRingtone(Drawer_Activity.activity, path);
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        reset_sos = (LinearLayout) rootView.findViewById(R.id.reset_sos);
        main_layout = (RelativeLayout) rootView.findViewById(R.id.main_layout);
        ll_start_trip_option = (LinearLayout) rootView.findViewById(R.id.ll_start_trip_option);
        ll_complete_hold = (LinearLayout) rootView.findViewById(R.id.ll_complete_hold);
        ll_start_trip_option.setVisibility(View.GONE);
        ll_complete_hold.setVisibility(View.GONE);

        tv_start_trip = (TextView) rootView.findViewById(R.id.tv_start_trip);
        tv_cancel_trip = (TextView) rootView.findViewById(R.id.tv_cancel_trip);
        tv_passengerInfo = (TextView) rootView.findViewById(R.id.tv_passengerInfo);
        tv_directionAcceptReq = (TextView) rootView.findViewById(R.id.tv_directionAcceptReq);
        tv_directionStartTrip = (TextView) rootView.findViewById(R.id.tv_directionStartTrip);
        tv_complete_trip = (TextView) rootView.findViewById(R.id.tv_complete_trip);
//        tv_start_waiting = (TextView) rootView.findViewById(R.id.tv_start_waiting);
//        tv_stop_waiting = (TextView) rootView.findViewById(R.id.tv_stop_waiting);

        reset_current_location = (ImageView) rootView.findViewById(R.id.reset_current_location);
        iv_TripToDestination = (ImageView) rootView.findViewById(R.id.iv_TripToDestination);
        iv_TripToDestination.setVisibility(View.GONE);
        tv_location = (TextView) rootView.findViewById(R.id.tv_location);

        tv_location.post(new Runnable() {
            @Override
            public void run() {
                tv_location.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                tv_location.setSelected(true);
            }
        });


        try {
            MapsInitializer.initialize(Drawer_Activity.activity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map123);
        mapFragment.getMapAsync(this);

        reset_sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(), "Sos click", Toast.LENGTH_SHORT).show();
                callSOSConfirem();

            }
        });


        tv_start_trip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tv_start_trip.setEnabled(false);
                String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, Drawer_Activity.activity);

                if (!ConnectivityReceiver.isConnected())
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
                else if (DutyStatus==null || DutyStatus.trim().equalsIgnoreCase("") || DutyStatus.equalsIgnoreCase("0"))
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.get_online_first), getString(R.string.info_message));
                }
                else if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)==null ||
                        SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).trim().
                                equalsIgnoreCase("")||
                        SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).trim().
                                equalsIgnoreCase("0"))
                {
                    Drawer_Activity.activity.StartTrip_BookNow(SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity)
                            , SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity));
                    ((Drawer_Activity)getActivity()).startTripMeter();

                    SetDriverTo_DropOffLocation(0, 0);
                }
                else
                {
                    StartTripClicked();
                }
                tv_start_trip.setEnabled(true);
            }
        });

        tv_cancel_trip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                tv_cancel_trip.setEnabled(false);
                String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, Drawer_Activity.activity);
                if (!ConnectivityReceiver.isConnected())
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
                else if (DutyStatus==null || DutyStatus.trim().equalsIgnoreCase("") || DutyStatus.equalsIgnoreCase("0"))
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.get_online_first), getString(R.string.info_message));
                }
                else
                {
                    String userId = SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity);
                    if (userId!=null && !userId.equalsIgnoreCase(""))
                    {
                        OpenDialogForNotice(userId);
                    }
                }
                tv_cancel_trip.setEnabled(true);
            }
        });

        tv_directionAcceptReq.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (ConnectivityReceiver.isConnected())
                {
                    if(Build.VERSION.SDK_INT >= 23)
                    {
                        if (Settings.canDrawOverlays(Drawer_Activity.activity))
                        {
//                            ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                            tv_directionAcceptReq.setEnabled(false);
                            Uri.Builder directionsBuilder = new Uri.Builder()
                                    .scheme("https")
                                    .authority("www.google.com")
                                    .appendPath("maps")
                                    .appendPath("dir")
                                    .appendPath("")
                                    .appendQueryParameter("api", "1")
                                    .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity) +","+ SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity));

                            startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                            tv_directionAcceptReq.setEnabled(true);
                            //((Drawer_Activity)getActivity()).finish();
                        }
                        else
                        {
                            ((Drawer_Activity)getActivity()).askForSystemOverlayPermission();
                        }
                    }
                    else
                    {
//                        ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                        tv_directionAcceptReq.setEnabled(false);
                        Uri.Builder directionsBuilder = new Uri.Builder()
                                .scheme("https")
                                .authority("www.google.com")
                                .appendPath("maps")
                                .appendPath("dir")
                                .appendPath("")
                                .appendQueryParameter("api", "1")
                                .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity) +","+ SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity));

                        startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                        tv_directionAcceptReq.setEnabled(true);
                        //((Drawer_Activity)getActivity()).finish();
                    }
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
            }
        });

        tv_directionStartTrip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected())
                {
                    if(Build.VERSION.SDK_INT >= 23)
                    {
                        if (Settings.canDrawOverlays(Drawer_Activity.activity)) {
//                            ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                            tv_directionStartTrip.setEnabled(false);
                            Uri.Builder directionsBuilder = new Uri.Builder()
                                    .scheme("https")
                                    .authority("www.google.com")
                                    .appendPath("maps")
                                    .appendPath("dir")
                                    .appendPath("")
                                    .appendQueryParameter("api", "1")
                                    .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity) +","+ SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity));

                            startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                            tv_directionStartTrip.setEnabled(true);
                            //((Drawer_Activity)getActivity()).finish();
                        } else {
                            ((Drawer_Activity)getActivity()).askForSystemOverlayPermission();
                        }
                    }else{
//                        ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                        tv_directionStartTrip.setEnabled(false);
                        Uri.Builder directionsBuilder = new Uri.Builder()
                                .scheme("https")
                                .authority("www.google.com")
                                .appendPath("maps")
                                .appendPath("dir")
                                .appendPath("")
                                .appendQueryParameter("api", "1")
                                .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity) +","+ SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity));

                        startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                        tv_directionStartTrip.setEnabled(true);
                        //((Drawer_Activity)getActivity()).finish();
                    }
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
            }
        });

        tv_passengerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawer_Activity.ExpandBottomShit();
            }
        });

        tv_complete_trip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tv_complete_trip.setEnabled(false);
                String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, Drawer_Activity.activity);
                if (!ConnectivityReceiver.isConnected())
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet),
                            getString(R.string.internet_error_message));
                }
                else if (DutyStatus==null || DutyStatus.trim().equalsIgnoreCase("") ||
                        DutyStatus.equalsIgnoreCase("0"))
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.get_online_first),
                            getString(R.string.info_message));
                }
                else
                {

                    //PASSENGER_BOOKING_TYPE
                    if(SessionSave.getUserSession(Comman.PASSENGER_BOOKING_TYPE, getActivity()).equalsIgnoreCase("dispatch")) {
                        Call_CompleteTrip();
                    } else {
                        callForCompleteCodeApi();
                    }



                }
                tv_complete_trip.setEnabled(true);
            }
        });

        reset_current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickRelocateButton=1;
//                RefreshLocation();
                setCameraOnMarker();
            }
        });

        flagMapReady=1;
        Log.e("GetLocationGps","222222222222");

        return rootView;
    }

    private void OpenDialogForNotice(final String userId)
    {
        final Dialog dialogNotice = new Dialog(getActivity(), R.style.PauseDialog);
        dialogNotice.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNotice.setCancelable(false);
        dialogNotice.setContentView(R.layout.dialog_app_notice_cancel_request);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogNotice.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogNotice.getWindow().setAttributes(lp);

        dialogNotice.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogNotice.getWindow().setAttributes(lp);

        final TextView  tv_message, tv_update, tv_no_update;
        final ImageView dialog_close;
        final LinearLayout dialog_ok_layout, dialog_no_layout;

        tv_message = (TextView) dialogNotice.findViewById(R.id.tv_message);
        tv_no_update = (TextView) dialogNotice.findViewById(R.id.tv_no_update);
        tv_update = (TextView) dialogNotice.findViewById(R.id.tv_update);
        dialog_close = (ImageView) dialogNotice.findViewById(R.id.dialog_close);
        dialog_ok_layout = (LinearLayout) dialogNotice.findViewById(R.id.dialog_ok_layout);
        dialog_no_layout = (LinearLayout) dialogNotice.findViewById(R.id.dialog_no_layout);

        String CancellationFee = SessionSave.getUserSession(Comman.DRIVER_CANCELATION_FEE,Drawer_Activity.activity);
        Log.e(TAG,"OpenDialogForNotice() CancellationFee:- " + CancellationFee);

        if (!CancellationFee.equalsIgnoreCase("") &&
                !CancellationFee.equalsIgnoreCase("null"))
        {
            tv_message.setText(getResources().getString(R.string.cancel_request_message2));
        }
        else
        {
            tv_message.setText(getResources().getString(R.string.cancel_request_message1));
        }
        tv_update.setText("Yes");

        tv_update.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                dialogNotice.dismiss();
                callCancelTrip(userId);
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
                callCancelTrip(userId);
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
            }
        });

        tv_no_update.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
            }
        });

        dialog_no_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
            }
        });
        dialogNotice.show();
    }

    public static void callForCompleteCodeApi()
    {
        Log.e(TAG,"callForCompleteCodeApi()");

        dialogClass.showDialog();

        if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)!=null &&
                !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            String url = WebServiceAPI.WEB_SERVICE_COMPLETE_TRIP_CODE;
            Map<String, Object> params = new HashMap<String, Object>();

            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_BOOKING_ID,
                    SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));

            if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR,
                    Drawer_Activity.activity).equalsIgnoreCase("0"))
            {
                params.put(WebServiceAPI.PARAM_BOOKING_TYPE, "BookNow");
            }
            else
            {
                params.put(WebServiceAPI.PARAM_BOOKING_TYPE, "BookLater");
            }

            Log.e(TAG, "callForCompleteCodeApi() Url = " + url);
            Log.e(TAG, "callForCompleteCodeApi()  params = " + params);


            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
            {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status)
                {

                    try
                    {
                        int responseCode = status.getCode();
                        Log.e(TAG, "callForCompleteCodeApi()  responseCode= " + responseCode);
                        Log.e(TAG, "callForCompleteCodeApi()  json = " + json);

                        if (json != null)
                        {
                            if (json.has("status") &&
                                    json.getBoolean("status"))
                            {
                                String otp = "";
                                if (json.has("otp"))
                                {
                                    otp = json.getString("otp");
                                }
                                dialogClass.hideDialog();
                                final String finalOtp = otp;
                                new Handler().postDelayed(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        callCompl1(finalOtp);
                                    }
                                },300);
                            }
                            else
                            {
                                Log.e(TAG, "callForCompleteCodeApi()  no status found");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),
                                            Drawer_Activity.activity.getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e(TAG, "callForCompleteCodeApi() json null");
                            dialogClass.hideDialog();
                            errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong),
                                    Drawer_Activity.activity.getString(R.string.error_message));
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e(TAG, "callForCompleteCodeApi()  Exception : " + e.toString());
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong),
                                Drawer_Activity.activity.getString(R.string.error_message));
                    }
                    finally
                    {
                        Log.e(TAG, "callForCompleteCodeApi()  finally");
                        dialogClass.hideDialog();
                    }
                }
            }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
    }

    public static void callCompl1(final String OTP)
    {
        Log.e(TAG,"callCompl1() OTP:- " + OTP);

        final Dialog dialog = new Dialog(Drawer_Activity.activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_complete_code);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        ImageView ivClose = dialog.findViewById(R.id.ivClose);
        final EditText etCode = dialog.findViewById(R.id.etCode);
        LinearLayout llSubmit = dialog.findViewById(R.id.llSubmit);

        ivClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        llSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (etCode.getText() !=null &&
                        !etCode.getText().toString().trim().equalsIgnoreCase(""))
                {
                    if (etCode.getText().toString().trim().equalsIgnoreCase(OTP))
                    {

                        if (Global.isNetworkconn(Drawer_Activity.activity))
                        {
                            dialog.dismiss();
                            Call_CompleteTrip();
                        }
                        else
                        {
                            errorDialogClass.showDialog(Drawer_Activity.activity.getResources().getString(R.string.not_connected_to_internet),
                                    Drawer_Activity.activity.getResources().getString(R.string.internet_error_message));
                        }
                    }
                    else
                    {
                        etCode.setError(Drawer_Activity.activity.getResources().getString(R.string.please_enter_valid_complete_trip_code));
                    }
                }
                else
                {
                    etCode.setError(Drawer_Activity.activity.getResources().getString(R.string.please_enter_complete_trip_code));
                }
            }
        });

        dialog.show();
    }

    public static void Call_CompleteTrip()
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        String url;
        if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)!=null &&
                !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR,
                    Drawer_Activity.activity).equalsIgnoreCase("0"))
            {
                url = WebServiceAPI.WEB_SERVICE_COMPLETE_TRIP;
            }
            else
            {
                url = WebServiceAPI.WEB_SERVICE_COMPLETE_TRIP_LATER;
            }

            String PaymentType = SessionSave.getUserSession(Comman.PASSENGER_PAYMENT_TYPE, Drawer_Activity.activity);
            if (PaymentType!=null && !PaymentType.equalsIgnoreCase(""))
            {
                if (PaymentType.equalsIgnoreCase("collect"))
                {
                    PaymentType = "cash";
                }
            }
            else
            {
                PaymentType="cash";
            }

            String tripDistance = SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, Drawer_Activity.activity);
            if (tripDistance!=null && !tripDistance.equalsIgnoreCase(""))
            {
                double distance = Double.parseDouble(tripDistance);
                String abc = String.format( "%.2f", distance);
                if(abc.contains(",")){
                    abc = abc.replace(",",".");
                }
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TRIP_DISTANCE, abc);

            }
            else
            {
                if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase(""))
                {
                    params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_LAT, Double.parseDouble(Constants.newgpsLatitude));
                    params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_LONG, Double.parseDouble(Constants.newgpsLongitude));
                }
            }
            String totalMinInSecond = SessionSave.getMeterData(Comman.METER_TOTAL_MIN_CALCULATE, Drawer_Activity.activity);
            if (totalMinInSecond!=null && !totalMinInSecond.equalsIgnoreCase(""))
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_WAITING_TIME, totalMinInSecond);
            }
            else
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_WAITING_TIME, "0");
            }

            if (SessionSave.getUserSession(Comman.BOOKING_PENDING, Drawer_Activity.activity)!=null && SessionSave.getUserSession(Comman.BOOKING_PENDING, Drawer_Activity.activity).trim().equalsIgnoreCase("1"))
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PENDING, "1");
            }
            else
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PENDING, "0");
            }

            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_BOOKING_ID,
                    SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_NIGHT_FARE_APPLICABLE, "0");
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PROMO_CODE, "");
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PAYMENT_TYPE, PaymentType);
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PAYMENT_STATUS, "");
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TRANSACTION_ID, "");

            if (TollFeeApi!=null && !TollFeeApi.equalsIgnoreCase(""))
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TOLL_FEE, TollFeeApi);
            }
            else
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TOLL_FEE, "0");
            }

            if (dropOffLocationApi!=null && !dropOffLocationApi.equalsIgnoreCase(""))
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_DROP_OFF_LOCATION, dropOffLocationApi);
            }

            Log.e("url", "Call_CompleteTrip = " + url);
            Log.e("param", "Call_CompleteTrip = " + params);


            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("responseCode", "Call_CompleteTrip = " + responseCode);
                        Log.e("Response", "Call_CompleteTrip = " + json);

                        if (json != null)
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");
                                    if (marker != null)
                                    {
                                        marker.remove();
                                        marker=null;
                                    }
                                    if (marker_pickUpLocation != null)
                                    {
                                        marker_pickUpLocation.remove();
                                        marker_pickUpLocation=null;
                                    }
                                    if (marker_dropOffLocation != null)
                                    {
                                        marker_dropOffLocation.remove();
                                        marker_dropOffLocation=null;
                                    }
                                    Drawer_Activity.flagCallDriverOnline=0;
                                    googleMap.clear();
                                    intMarkerAnim=0;

                                    ll_start_trip_option.setVisibility(View.GONE);
                                    ll_complete_hold.setVisibility(View.GONE);
                                    dialogClass.hideDialog();

                                    SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
                                    Log.e("avooooooooooooo","avoooooooooooooooooooo444444444444444444444444");
                                    SessionSave.saveUserSession(Comman.BOOKING_PENDING, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.USER_TRIP_FLAG, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.PARCEL_IMG, "", Drawer_Activity.activity);

                                    String PaymentType = SessionSave.getUserSession(Comman.PASSENGER_PAYMENT_TYPE, Drawer_Activity.activity);
                                    final String PassType = SessionSave.getUserSession(Comman.PASSENGER_TYPE, Drawer_Activity.activity);
                                    Log.e("PassType","PassType : "+PassType);
                                    Log.e("PaymentType","PaymentType : "+PaymentType);

                                    SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FLAG, "0", Drawer_Activity.activity);

                                    final Dialog dialogTy = new Dialog(Drawer_Activity.activity, R.style.DialogTheme);
                                    dialogTy.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    lp.copyFrom(dialogTy.getWindow().getAttributes());
                                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                                    lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                                    dialogTy.getWindow().setAttributes(lp);
                                    dialogTy.setCancelable(false);
                                    dialogTy.setContentView(R.layout.dialog_receive_cancel_trip);

                                    TextView tv_dialog_ok, tv_title, tv_message;
                                    ImageView dialog_close;

                                    tv_title = (TextView) dialogTy.findViewById(R.id.tv_title);
                                    tv_dialog_ok = (TextView) dialogTy.findViewById(R.id.tv_dialog_ok);
                                    tv_message = (TextView) dialogTy.findViewById(R.id.tv_message);
                                    dialog_close = (ImageView) dialogTy.findViewById(R.id.dialog_close);



                                    tv_title.setText(Drawer_Activity.activity.getResources().getString(R.string.alert_job_mesage));
                                    tv_message.setText(Drawer_Activity.activity.getResources().getString(R.string.collect_money));
                                    tv_title.setTypeface(tv_title.getTypeface(), Typeface.NORMAL);
                                    tv_title.setTextColor(Color.parseColor("#000000"));
                                    tv_message.setTextColor(Color.parseColor("#000000"));

                                    if (json.has("payment_status") && json.has("payment_message") && json.getString("payment_status")!=null
                                            && json.getString("payment_status").equalsIgnoreCase("1"))
                                    {
                                        tv_title.setText("Warning");
                                        tv_message.setText(json.getString("payment_message"));
                                        tv_title.setTypeface(tv_title.getTypeface(), Typeface.BOLD);
                                        tv_title.setTextColor(Color.parseColor("#ff0000"));
                                        tv_message.setTextColor(Color.parseColor("#ff0000"));
                                    }

                                    final Dialog dialog = new Dialog(Drawer_Activity.activity, R.style.DialogTheme);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setCancelable(false);
                                    dialog.setContentView(R.layout.dialog_show_complete_trip_details);


                                    WindowManager.LayoutParams lp1 = new WindowManager.LayoutParams();
                                    lp1.copyFrom(dialog.getWindow().getAttributes());
                                    lp1.width = LinearLayout.LayoutParams.MATCH_PARENT;
                                    lp1.height = LinearLayout.LayoutParams.MATCH_PARENT;
                                    dialog.getWindow().setAttributes(lp1);

                                    TextView tv_drop_off_location, tv_pick_up_location, tv_promocode, tv_dialog_ok1, tv_trip_fare, tv_WaitingTime, tv_SpecialCost,
                                            tv_waiting_time_cost, tv_toll_fee, tv_booking_charge, tv_tax, tv_discount, tv_grand_total,
                                            tv_distanceFare, tv_nightFare, tv_TripDistance, tvSize, tvWeight, tvQuantity;
                                    LinearLayout dialog_ok_layout, ll_SpecialCost;
                                    ImageView dialog_close_trip;
                                    LinearLayout llEarning,llCommission;
                                    TextView tv_earning,tv_commission;

                                    dialog_close_trip = dialog.findViewById(R.id.dialog_close);
                                    tv_drop_off_location = dialog.findViewById(R.id.tv_drop_off_location);
                                    tv_pick_up_location = dialog.findViewById(R.id.tv_pick_up_location);
                                    tvSize = dialog.findViewById(R.id.tvSize);
                                    tvWeight = dialog.findViewById(R.id.tvWeight);
                                    tvQuantity = dialog.findViewById(R.id.tvQuantity);
                                    tv_promocode = dialog.findViewById(R.id.tv_promocode);
                                    tv_trip_fare = dialog.findViewById(R.id.tv_trip_fare);
                                    tv_waiting_time_cost = dialog.findViewById(R.id.tv_waiting_time_cost);
                                    tv_toll_fee = dialog.findViewById(R.id.tv_toll_fee);
                                    tv_tax = dialog.findViewById(R.id.tv_tax);
                                    tv_booking_charge = dialog.findViewById(R.id.tv_booking_charge);
                                    tv_discount = dialog.findViewById(R.id.tv_discount);
                                    tv_grand_total = dialog.findViewById(R.id.tv_grand_total);
                                    tv_dialog_ok1 = dialog.findViewById(R.id.tv_dialog_ok);
                                    dialog_ok_layout = dialog.findViewById(R.id.dialog_ok_layout);
                                    tv_distanceFare = dialog.findViewById(R.id.tv_distanceFare);
                                    tv_nightFare = dialog.findViewById(R.id.tv_nightFare);
                                    tv_WaitingTime = dialog.findViewById(R.id.tv_WaitingTime);
                                    tv_TripDistance = dialog.findViewById(R.id.tv_TripDistance);
                                    tv_SpecialCost = dialog.findViewById(R.id.tv_SpecialCost);
                                    ll_SpecialCost = dialog.findViewById(R.id.ll_SpecialCost);
                                    ImageView ivParcel = dialog.findViewById(R.id.ivParcel);
                                    LinearLayout llImg = dialog.findViewById(R.id.llImg);

                                    llEarning = dialog.findViewById(R.id.llEarning);
                                    llCommission = dialog.findViewById(R.id.llCommission);
                                    tv_earning = dialog.findViewById(R.id.tv_earning);
                                    tv_commission = dialog.findViewById(R.id.tv_commission);

                                    SessionSave.clearMeterData(Drawer_Activity.activity);
                                    Drawer_Activity.stopTripMainTrip();

                                    Drawer_Activity.flagCallDriverOnline=0;
//                                    Drawer_Activity.activity.startTimer();

                                    tv_dialog_ok1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            ClickRelocateButton = 0;
                                            dialog.dismiss();
                                            if (PassType != null && !PassType.equalsIgnoreCase("") && (PassType.equalsIgnoreCase("others") || PassType.equalsIgnoreCase("other"))) {
                                                ClearTripSession();

                                                Location location = new Location(LocationManager.GPS_PROVIDER);
                                                location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                                                location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                                                LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

                                                if (marker != null) {
                                                    Log.e("marker", "marker not null");
                                                    animateMarkerNew(location, marker);
                                                } else {
                                                    Log.e("marker", "marker nulllllllllllllllllll");
                                                    if (googleMap!=null)
                                                    {
                                                        marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                                                        animateMarkerNew(location, marker);
                                                    }
                                                }
                                            } else {
                                                showRatingDialog();
                                            }
                                        }
                                    });

                                    dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            ClickRelocateButton = 0;
                                            dialog.dismiss();
                                            if (PassType != null && !PassType.equalsIgnoreCase("") && (PassType.equalsIgnoreCase("others") || PassType.equalsIgnoreCase("other"))) {
                                                ClearTripSession();

                                                Location location = new Location(LocationManager.GPS_PROVIDER);
                                                location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                                                location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                                                LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

                                                if (marker != null) {
                                                    Log.e("marker", "marker not null");
                                                    animateMarkerNew(location, marker);
                                                } else {
                                                    Log.e("marker", "marker nulllllllllllllllllll");
                                                    if (googleMap!=null)
                                                    {
                                                        marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                                                        animateMarkerNew(location, marker);
                                                    }
                                                }
                                            } else {
                                                showRatingDialog();
                                            }
                                        }
                                    });

                                    dialog_close_trip.setOnClickListener(new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {
                                            ClickRelocateButton = 0;
                                            dialog.dismiss();
                                            if (PassType != null && !PassType.equalsIgnoreCase("") && (PassType.equalsIgnoreCase("others") || PassType.equalsIgnoreCase("other"))) {
                                                ClearTripSession();

                                                Location location = new Location(LocationManager.GPS_PROVIDER);
                                                location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                                                location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                                                LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

                                                if (marker != null) {
                                                    Log.e("marker", "marker not null");
                                                    animateMarkerNew(location, marker);
                                                } else {
                                                    Log.e("marker", "marker nulllllllllllllllllll");
                                                    if (googleMap!=null)
                                                    {
                                                        marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                                                        animateMarkerNew(location, marker);
                                                    }
                                                }
                                            } else {
                                                showRatingDialog();
                                            }
                                        }
                                    });

                                    tv_complete_trip.setEnabled(true);

                                    if (json.has("details"))
                                    {
                                        JSONObject detailsObject = json.getJSONObject("details");
                                        if (detailsObject != null)
                                        {
                                            String PickupLocation = "", DropoffLocation = "", PromoCode="", TripFare = "", WaitingTimeCost = "", WaitingTime="", TollFee = "", BookingCharge = "", Tax = "", Discount = "", SubTotal = ""
                                                    , GrandTotal = "", DistanceFare = "", NightFare = "", TripDistance="",
                                                    Height="", Length="", Breadth="", Weigth="", Quantity= "";

                                            if (detailsObject.has("PickupLocation"))
                                            {
                                                PickupLocation = detailsObject.getString("PickupLocation");

                                                if (!PickupLocation.equalsIgnoreCase("") &&
                                                        !PickupLocation.equalsIgnoreCase("null"))
                                                {
                                                    tv_pick_up_location.setText(PickupLocation);
                                                }
                                                else
                                                {
                                                    tv_pick_up_location.setText("-");
                                                }

                                            }
                                            else
                                            {
                                                tv_pick_up_location.setText("-");
                                            }

                                            if (detailsObject.has("DropoffLocation"))
                                            {
                                                DropoffLocation = detailsObject.getString("DropoffLocation");

                                                if (!DropoffLocation.equalsIgnoreCase("") &&
                                                        !DropoffLocation.equalsIgnoreCase("null"))
                                                {
                                                    tv_drop_off_location.setText(DropoffLocation);
                                                }
                                                else
                                                {
                                                    tv_drop_off_location.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_drop_off_location.setText("-");
                                            }

                                            if (detailsObject.has("Height"))
                                            {
                                                Height = detailsObject.getString("Height");
                                            }

                                            if (detailsObject.has("Length"))
                                            {
                                                Length = detailsObject.getString("Length");
                                            }

                                            if (detailsObject.has("Breadth"))
                                            {
                                                Breadth = detailsObject.getString("Breadth");
                                            }
                                            if (detailsObject.has("ParcelImage"))
                                            {
                                                String  parcelImg = detailsObject.getString("ParcelImage");
                                                if(parcelImg != null && !parcelImg.equalsIgnoreCase("")){
                                                    Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE + parcelImg).into(ivParcel);
                                                }else {
                                                    llImg.setVisibility(View.GONE);
                                                }
                                            }

                                            if (!Height.equalsIgnoreCase("") &&
                                                    !Height.equalsIgnoreCase("null") &&
                                                    !Height.equalsIgnoreCase("0") &&
                                                    !Length.equalsIgnoreCase("") &&
                                                    !Length.equalsIgnoreCase("null") &&
                                                    !Length.equalsIgnoreCase("0") &&
                                                    !Breadth.equalsIgnoreCase("") &&
                                                    !Breadth.equalsIgnoreCase("null") &&
                                                    !Breadth.equalsIgnoreCase("0"))
                                            {
                                                String Size = "(" + Height + " * "  + Length + " * " + Breadth + ")";
                                                tvSize.setText(Size);
                                            }
                                            else
                                            {
                                                tvSize.setText("-");
                                            }

                                            if (detailsObject.has("Weight"))
                                            {
                                                Weigth = detailsObject.getString("Weight");

                                                if (!Weigth.equalsIgnoreCase("") &&
                                                        !Weigth.equalsIgnoreCase("null") &&
                                                        !Weigth.equalsIgnoreCase("0"))
                                                {
                                                    tvWeight.setText(Weigth+"Kg");
                                                }
                                                else
                                                {
                                                    tvWeight.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tvWeight.setText("-");
                                            }

                                            if (detailsObject.has("Quantity"))
                                            {
                                                Quantity = detailsObject.getString("Quantity");

                                                if (!Quantity.equalsIgnoreCase("") &&
                                                        !Quantity.equalsIgnoreCase("null") &&
                                                        !Quantity.equalsIgnoreCase("0"))
                                                {
                                                    tvQuantity.setText(Quantity);
                                                }
                                                else
                                                {
                                                    tvQuantity.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tvQuantity.setText("-");
                                            }

                                            if (detailsObject.has("PromoCode"))
                                            {
                                                PromoCode = detailsObject.getString("PromoCode");

                                                if (!PromoCode.equalsIgnoreCase("") &&
                                                        !PromoCode.equalsIgnoreCase("null"))
                                                {
                                                    tv_promocode.setText(PromoCode);
                                                }
                                                else
                                                {
                                                    tv_promocode.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_promocode.setText("-");
                                            }

                                            if (detailsObject.has("TripFare"))
                                            {
                                                TripFare = detailsObject.getString("TripFare");
                                                if (!TripFare.equalsIgnoreCase("") &&
                                                        !TripFare.equalsIgnoreCase("null"))
                                                {
                                                    Log.e(TAG, "Dialog()( : TripFare : "+TripFare);
                                                    tv_trip_fare.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) + " " +
                                                            Global.commaFormat(TripFare));
                                                }
                                                else
                                                {
                                                    tv_trip_fare.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_trip_fare.setText("-");
                                            }

                                            if (detailsObject.has("DistanceFare"))
                                            {
                                                DistanceFare = detailsObject.getString("DistanceFare");
                                                if (!DistanceFare.equalsIgnoreCase("") &&
                                                        !DistanceFare.equalsIgnoreCase("null"))
                                                {
                                                    tv_distanceFare.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                                            Global.commaFormat(DistanceFare));
                                                }
                                                else
                                                {
                                                    tv_distanceFare.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_distanceFare.setText("-");
                                            }

                                            if(detailsObject.has("CompanyAmount")){
                                                String a = detailsObject.getString("CompanyAmount");
                                                if(!a.equalsIgnoreCase("")
                                                        && !a.equalsIgnoreCase("null")){
                                                    tv_earning.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                                            Global.commaFormat(a));
                                                }else {
                                                    llEarning.setVisibility(View.GONE);
                                                }
                                            }else {
                                                llEarning.setVisibility(View.GONE);
                                            }

                                            if(detailsObject.has("AdminAmount")){
                                                String a = detailsObject.getString("AdminAmount");
                                                if(!a.equalsIgnoreCase("")
                                                        && !a.equalsIgnoreCase("null")){
                                                    tv_commission.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                                            Global.commaFormat(a));
                                                }else {
                                                    llCommission.setVisibility(View.GONE);
                                                }
                                            }else {
                                                llCommission.setVisibility(View.GONE);
                                            }

                                            if (detailsObject.has("WaitingTimeCost"))
                                            {
                                                WaitingTimeCost = detailsObject.getString("WaitingTimeCost");
                                                if (!WaitingTimeCost.equalsIgnoreCase("") &&
                                                        !WaitingTimeCost.equalsIgnoreCase("null"))
                                                {
                                                    tv_waiting_time_cost.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +
                                                            " " +Global.commaFormat(WaitingTimeCost));
                                                }
                                                else
                                                {
                                                    tv_waiting_time_cost.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_waiting_time_cost.setText("-");
                                            }


                                            if (detailsObject.has("WaitingTime"))
                                            {
                                                WaitingTime = detailsObject.getString("WaitingTime");
                                                if (WaitingTime!=null &&
                                                        !WaitingTime.equalsIgnoreCase("") &&
                                                        !WaitingTime.equalsIgnoreCase("null"))
                                                {
                                                    long abc = Long.parseLong(WaitingTime);
                                                    long hour = (abc / 60)/60;
                                                    long minutes = abc / 60;
                                                    long seconds = abc % 60;
                                                    tv_WaitingTime.setText(hour+":"+minutes+":"+seconds);
                                                }
                                                else
                                                {
                                                    tv_WaitingTime.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_WaitingTime.setText("-");
                                            }

                                            if (detailsObject.has("NightFare"))
                                            {
                                                NightFare = detailsObject.getString("NightFare");
                                                if (!NightFare.equalsIgnoreCase("") &&
                                                        !NightFare.equalsIgnoreCase("null"))
                                                {
                                                    tv_nightFare.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) + " " +
                                                            Global.commaFormat(NightFare));
                                                }
                                                else
                                                {
                                                    tv_nightFare.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_nightFare.setText("-");
                                            }

                                            if (detailsObject.has("TollFee"))
                                            {
                                                TollFee = detailsObject.getString("TollFee");
                                                if (!TollFee.equalsIgnoreCase("") &&
                                                        !TollFee.equalsIgnoreCase("null"))
                                                {
                                                    tv_toll_fee.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) + " " +
                                                            Global.commaFormat(TollFee));
                                                }
                                                else
                                                {
                                                    tv_toll_fee.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_toll_fee.setText("-");
                                            }

                                            if (detailsObject.has("BookingCharge"))
                                            {
                                                BookingCharge = detailsObject.getString("BookingCharge");
                                                if (!BookingCharge.equalsIgnoreCase("") &&
                                                        !BookingCharge.equalsIgnoreCase("null"))
                                                {
                                                    tv_booking_charge.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +
                                                            " " +Global.commaFormat(BookingCharge));
                                                }
                                                else
                                                {
                                                    tv_booking_charge.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_booking_charge.setText("-");
                                            }

                                            if (detailsObject.has("Tax")) {
                                                Tax = detailsObject.getString("Tax");
                                                if (!Tax.equalsIgnoreCase("") &&
                                                        !Tax.equalsIgnoreCase("null"))
                                                {
                                                    tv_tax.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) + " " +
                                                            Global.commaFormat(Tax));
                                                }
                                                else
                                                {
                                                    tv_tax.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_tax.setText("-");
                                            }

                                            if (detailsObject.has("Discount")) {
                                                Discount = detailsObject.getString("Discount");

                                                if (!Discount.equalsIgnoreCase("") &&
                                                        !Discount.equalsIgnoreCase("null"))
                                                {
                                                    tv_discount.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +
                                                            " " +String.format("%.2f",Double.parseDouble(Discount)));
                                                }
                                                else
                                                {
                                                    tv_discount.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_discount.setText("-");
                                            }

                                            if (detailsObject.has("SubTotal")) {
                                                SubTotal = detailsObject.getString("SubTotal");

                                            }

                                            if (detailsObject.has("GrandTotal"))
                                            {
                                                GrandTotal = detailsObject.getString("GrandTotal");
                                                if (!GrandTotal.equalsIgnoreCase("") &&
                                                        !GrandTotal.equalsIgnoreCase("null"))
                                                {
                                                    tv_grand_total.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +
                                                            " " + Global.commaFormat(GrandTotal));
                                                }
                                                else
                                                {
                                                    tv_grand_total.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_grand_total.setText("-");
                                            }

                                            if (detailsObject.has("TripDistance")) {
                                                TripDistance = detailsObject.getString("TripDistance");
                                                if (!TripDistance.equalsIgnoreCase("") &&
                                                        !TripDistance.equalsIgnoreCase("null"))
                                                {
                                                    tv_TripDistance.setText(TripDistance + " " + Drawer_Activity.activity.getResources().getString(R.string.km));
                                                }
                                                else
                                                {
                                                    tv_TripDistance.setText("-");
                                                }
                                            }
                                            else
                                            {
                                                tv_TripDistance.setText("-");
                                            }

                                            if (detailsObject.has("Special") &&
                                                    detailsObject.getString("Special")!=null &&
                                                    detailsObject.getString("Special").trim().equalsIgnoreCase("1"))
                                            {
                                                if (detailsObject.has("SpecialExtraCharge") &&
                                                        detailsObject.getString("SpecialExtraCharge")!=null )
                                                {
                                                    ll_SpecialCost.setVisibility(View.GONE);
                                                    String SpecialExtraCharge = detailsObject.getString("SpecialExtraCharge");
                                                    if (!SpecialExtraCharge.equalsIgnoreCase("") &&
                                                            !SpecialExtraCharge.equalsIgnoreCase("null"))
                                                    {
                                                        tv_SpecialCost.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +
                                                                " " +Global.commaFormat(SpecialExtraCharge));
                                                    }
                                                    else
                                                    {
                                                        tv_SpecialCost.setText("-");
                                                    }
                                                }
                                                else
                                                {
                                                    ll_SpecialCost.setVisibility(View.GONE);
                                                }
                                            }
                                            else
                                            {
                                                ll_SpecialCost.setVisibility(View.GONE);
                                            }
                                        }

                                        dialog_close.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ringTone.stop();
                                                dialogTy.dismiss();
                                                dialog.show();
                                                Drawer_Activity.AssignAllDialog = dialog;
                                            }
                                        });
                                        tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ringTone.stop();
                                                dialogTy.dismiss();
                                                dialog.show();
                                                Drawer_Activity.AssignAllDialog = dialog;
                                            }
                                        });


                                        if (json.has("payment_status") && json.has("payment_message") && json.getString("payment_status")!=null
                                                && json.getString("payment_status").equalsIgnoreCase("1"))
                                        {
                                            dialogTy.show();
                                            Drawer_Activity.AssignAllDialog = dialogTy;
                                        }
                                        else
                                        {
                                            if (PaymentType!=null && !PaymentType.equalsIgnoreCase("") && (PaymentType.equalsIgnoreCase("cash") || PaymentType.equalsIgnoreCase("collect")))
                                            {
                                                if(ringTone.isPlaying())
                                                {
                                                    ringTone.stop();
                                                }
                                                ringTone.play();
                                                dialogTy.show();
                                                Drawer_Activity.AssignAllDialog = dialogTy;
                                            }
                                            else
                                            {
                                                dialog.show();
                                                Drawer_Activity.AssignAllDialog = dialog;
                                            }
                                        }

                                        dialogClass.hideDialog();
                                        tv_complete_trip.setEnabled(true);
                                    }
                                    dialogClass.hideDialog();
                                    tv_complete_trip.setEnabled(true);
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    ll_start_trip_option.setVisibility(View.GONE);
                                    ll_complete_hold.setVisibility(View.VISIBLE);
                                    tv_complete_trip.setEnabled(true);
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), Drawer_Activity.activity.getString(R.string.error_message));
                                    }
                                }
                            } else {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                ll_start_trip_option.setVisibility(View.GONE);
                                ll_complete_hold.setVisibility(View.VISIBLE);
                                tv_complete_trip.setEnabled(true);
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), Drawer_Activity.activity.getString(R.string.error_message));
                                }
                            }
                        } else {
                            Log.e("json", "null");
                            dialogClass.hideDialog();
                            ll_start_trip_option.setVisibility(View.GONE);
                            ll_complete_hold.setVisibility(View.VISIBLE);
                            tv_complete_trip.setEnabled(true);
                            errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong), Drawer_Activity.activity.getString(R.string.error_message));
                        }
                    } catch (Exception e) {
                        Log.e("Call_CompleteTrip", "Exception : " + e.toString());
                        dialogClass.hideDialog();
                        ll_start_trip_option.setVisibility(View.GONE);
                        ll_complete_hold.setVisibility(View.VISIBLE);
                        tv_complete_trip.setEnabled(true);

                        errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong), Drawer_Activity.activity.getString(R.string.error_message));
                    }
                }
            }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
    }

    public static void showRatingDialog()
    {
        final Dialog dialogRating = new Dialog(Drawer_Activity.activity, R.style.PauseDialogAnimation);
        Drawer_Activity.AssignAllDialog = dialogRating;
        dialogRating.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpRating = new WindowManager.LayoutParams();
        lpRating.copyFrom(dialogRating.getWindow().getAttributes());
        lpRating.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lpRating.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogRating.getWindow().setAttributes(lpRating);
        dialogRating.setCancelable(false);
        dialogRating.setContentView(R.layout.dialog_rating);

        TextView tv_submit, tv_message_Rating, dialog_title;

        final EditText et_description;
        final MaterialRatingBar ratingBar;
        ImageView dialog_close;

        dialog_close = (ImageView) dialogRating.findViewById(R.id.dialog_close);
        et_description = (EditText) dialogRating.findViewById(R.id.et_description);
        tv_submit = (TextView) dialogRating.findViewById(R.id.tv_submit);
        dialog_title = (TextView) dialogRating.findViewById(R.id.dialog_title);

        dialog_title.setText("");
        tv_message_Rating = (TextView) dialogRating.findViewById(R.id.tv_message_Rating);
        ratingBar = (MaterialRatingBar) dialogRating.findViewById(R.id.ratingBar);


        String passengerName = SessionSave.getUserSession(Comman.PASSENGER_NAME, Drawer_Activity.activity);
        if (passengerName!=null && !passengerName.equalsIgnoreCase(""))
        {
            tv_message_Rating.setText(Drawer_Activity.activity.getResources().getString(R.string.how_was_your_expirenece_with)+" "+passengerName+"?");
        }
        else
        {
            tv_message_Rating.setText(Drawer_Activity.activity.getResources().getString(R.string.how_was_your_expirenece_with_passenger));
        }

        tv_submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ratingBar.getRating();

                if (Global.isNetworkconn(Drawer_Activity.activity))
                {
                    if (TextUtils.isEmpty(et_description.getText().toString().trim()))
                    {
                        callRateMethod(dialogRating, ratingBar.getRating(), "");
                    }
                    else
                    {
                        callRateMethod(dialogRating, ratingBar.getRating(), et_description.getText().toString());
                    }
                }
                else
                {
                    errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.not_connected_to_internet),
                            Drawer_Activity.activity.getString(R.string.internet_error_message));
                }
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callRateMethod(dialogRating, 0, "");
            }
        });


        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener()
        {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
            {
                Log.e("RatingBar","RatingBar rating : "+rating);
            }
        });

        dialogRating.show();
    }

    public static void callRateMethod(final Dialog dialogRating, float rating, String description)
    {
        Log.e("callRateMethod","callRateMethod description : "+ description);

        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        String url = WebServiceAPI.WEB_SERVICE_RATING_AND_COMMENT;

        if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase("0"))
            {
                params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_BOOKING_TYPE, "BookNow");
            }
            else
            {
                params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_BOOKING_TYPE, "BookLater");
            }

            if (description!=null && !description.equalsIgnoreCase(""))
            {
                params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_DRESCRIPTION, description);
            }

            params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_BOOKING_ID, SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));
            params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_RATING, rating+"");

            Log.e("url", "callRateMethod = " + url);
            Log.e("param", "callRateMethod = " + params);


            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("responseCode", "callRateMethod = " + responseCode);
                        Log.e("Response", "callRateMethod = " + json);

                        if (json != null)
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");
                                    SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                    dialogClass.hideDialog();
                                    dialogRating.hide();
//                                    dialog.show();


                                    Location location = new Location(LocationManager.GPS_PROVIDER);
                                    location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                                    location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                                    LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

                                    if (marker != null)
                                    {
                                        Log.e("marker","marker not null");
                                        animateMarkerNew(location, marker);
                                    }
                                    else
                                    {
                                        Log.e("marker","marker nulllllllllllllllllll");
                                        if (googleMap!=null)
                                        {
                                            marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                                            animateMarkerNew(location, marker);
                                        }
                                    }

                                    final String DriverId = SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity);
                                    if (DriverId!=null && !DriverId.equalsIgnoreCase(""))
                                    {
                                        CallApiFor_FindStatus(DriverId);
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), Drawer_Activity.activity.getString(R.string.error_message));
                                    }
                                }
                            } else {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), Drawer_Activity.activity.getString(R.string.error_message));
                                }
                            }
                        } else {
                            Log.e("json", "null");
                            dialogClass.hideDialog();
                            errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong), Drawer_Activity.activity.getString(R.string.error_message));
                        }
                    } catch (Exception e) {
                        Log.e("callRateMethod", "Exception : " + e.toString());
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong), Drawer_Activity.activity.getString(R.string.error_message));
                    }
                }
            }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
    }

    private void callCancelTrip(String userId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CANCEL_TRIP;

        params.put(WebServiceAPI.PARAM_CANCEL_TRIP_DRIVER_ID, userId);
        params.put(WebServiceAPI.PARAM_CANCEL_TRIP_BOOKING_ID, SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));
        if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)!=null && SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase("1"))
        {
            params.put(WebServiceAPI.PARAM_CANCEL_TRIP_BOOKING_TYPE, "BookLater");
        }
        else
        {
            params.put(WebServiceAPI.PARAM_CANCEL_TRIP_BOOKING_TYPE, "BookNow");
        }

        Log.e("url", "callCancelTrip = " + url);
        Log.e("param", "callCancelTrip = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "callCancelTrip = " + responseCode);
                    Log.e("Response", "callCancelTrip = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                ((Drawer_Activity)getActivity()).stopTripMainTrip();
                                Log.e("status", "true");

                                if (marker != null)
                                {
                                    marker.remove();
                                    marker=null;
                                }
                                if (marker_pickUpLocation != null)
                                {
                                    marker_pickUpLocation.remove();
                                    marker_pickUpLocation=null;
                                }
                                if (marker_dropOffLocation != null)
                                {
                                    marker_dropOffLocation.remove();
                                    marker_dropOffLocation=null;
                                }

                                googleMap.clear();
                                intMarkerAnim=0;

                                ll_start_trip_option.setVisibility(View.GONE);
                                ll_complete_hold.setVisibility(View.GONE);

                                SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", Drawer_Activity.activity);
                                SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", Drawer_Activity.activity);
                                SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
                                SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
                                SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                ClearTripSession();

                                dialogClass.hideDialog();

                                /*if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.info_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog("Trip is canceled",getResources().getString(R.string.info_message));
                                }*/
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));

                    }
                } catch (Exception e) {
                    Log.e("callCancelTrip", "Exception : " + e.toString());
                    dialogClass.hideDialog();

                    errorDialogClass.showDialog(getString(R.string.something_is_wrong),"Error Message");
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onMapReady(GoogleMap googleMap1)
    {
        googleMap = googleMap1;
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
//                googleMap.setLocationSource(mLocationSource);

        // For showing a move to my location button
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        googleMap.setMyLocationEnabled(false);
        ll_start_trip_option.setVisibility(View.GONE);

        flagMapReady=1;
        Log.e("GetLocationGps","3333333333333333");

        // For dropping a marker at a point on the Map
        ClickRelocateButton=0;
        ClearTripSession();
    }

    public static void setCameraOnMarker()
    {
        intMarkerAnim=0;
        gpsTracker = new GPSTracker(Drawer_Activity.activity);
        if (ContextCompat.checkSelfPermission(Drawer_Activity.activity, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Drawer_Activity.activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
        else
        {
            try {
                int locationMode = Settings.Secure.getInt(Drawer_Activity.activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
                int LOCATION_MODE_HIGH_ACCURACY = 3;
                if(locationMode == LOCATION_MODE_HIGH_ACCURACY)
                {
                    if (!gpsTracker.canGetLocation())
                    {
                        gpsTracker.showSettingsAlert(Drawer_Activity.activity);
                    }
                    else
                    {
                        gpsTracker.getLocation();
                        if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase(""))
                        {
                            if (Double.parseDouble(Constants.newgpsLatitude)!=0)
                            {

                            }
                            else
                            {
                                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                            }
                        }
                        else
                        {
                            Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                            Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                        }

                        latitude = Double.parseDouble(Constants.newgpsLatitude);
                        longitude = Double.parseDouble(Constants.newgpsLongitude);
                        sydney=null;
                        sydney = new LatLng(latitude, longitude);

                        Location location = new Location(LocationManager.GPS_PROVIDER);
                        location.setLatitude(latitude);
                        location.setLongitude(longitude);

                        if (marker != null)
                        {
                            Log.e("marker","marker not null");
                            animateMarkerNew(location, marker);
                        }
                        else
                        {
                            Log.e("marker","marker nulllllllllllllllllll");
                            if (googleMap!=null)
                            {
                                marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                                animateMarkerNew(location, marker);
                            }
                        }
                        getAddress(latitude, longitude);
                    }
                }
                else
                {
                    //redirect user to settings page
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Drawer_Activity.activity);
                    alertDialog.setTitle("GPS is settings");

                    alertDialog.setMessage("Please set your Location Mode to High accuracy.");

                    // On pressing Settings button
                    alertDialog.setPositiveButton("Settings",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Drawer_Activity.activity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });

                    AlertDialog alertDialog1 = alertDialog.create();
                    Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
                    Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

                    if (Pbutton != null)
                    {
                        Pbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                    }
                    else
                    {
                        Log.e("Map Fragment","takePermision():- " + "Pbutton == null");
                    }

                    if (Pbutton != null)
                    {
                        Nbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                    }
                    else
                    {
                        Log.e("Map Fragment","takePermision():- " + "Nbutton == null");

                    }

                    alertDialog.show();

                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void clearMap()
    {
        ll_start_trip_option.setVisibility(View.GONE);
        ll_complete_hold.setVisibility(View.GONE);
        sydney=null;
        sydney = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));
        if (marker != null)
        {
            marker.remove();
            marker=null;
        }
        if (marker_pickUpLocation != null)
        {
            marker_pickUpLocation.remove();
            marker_pickUpLocation=null;
        }
        if (marker_dropOffLocation != null)
        {
            marker_dropOffLocation.remove();
            marker_dropOffLocation=null;
        }
        googleMap.clear();

        if (googleMap!=null)
        {
            marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(17.5f).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public static void RefreshLocation()
    {
        intMarkerAnim=0;
        gpsTracker = new GPSTracker(Drawer_Activity.activity);
        if (ContextCompat.checkSelfPermission(Drawer_Activity.activity, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Drawer_Activity.activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
        else
        {
            try {
                int locationMode = Settings.Secure.getInt(Drawer_Activity.activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
                int LOCATION_MODE_HIGH_ACCURACY = 3;
                if(locationMode == LOCATION_MODE_HIGH_ACCURACY)
                {
                    if (!gpsTracker.canGetLocation())
                    {
                        gpsTracker.showSettingsAlert(Drawer_Activity.activity);
                    }
                    else
                    {
                        final String DriverId = SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity);
                        if (DriverId!=null && !DriverId.equalsIgnoreCase(""))
                        {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    CallApiFor_FindStatus(DriverId);
                                }
                            }, 100);
                        }
                    }
                }
                else
                {
                    //redirect user to settings page
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Drawer_Activity.activity);
                    alertDialog.setTitle("GPS is settings");

                    alertDialog.setMessage("Please set your Location Mode to High accuracy.");

                    // On pressing Settings button
                    alertDialog.setPositiveButton("Settings",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Drawer_Activity.activity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });

                    AlertDialog alertDialog1 = alertDialog.create();
                    Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
                    Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

                    if (Pbutton != null)
                    {
                        Pbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                    }
                    else
                    {
                        Log.e("Map Fragment","takePermision():- " + "Pbutton == null");
                    }

                    if (Pbutton != null)
                    {
                        Nbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                    }
                    else
                    {
                        Log.e("Map Fragment","takePermision():- " + "Nbutton == null");

                    }

                    alertDialog.show();

                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void CallApiFor_FindStatus(String driverId)
    {
        Log.e(TAG,"CallApiFor_FindStatus()");

        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CURRENT_TRIP_FLOW + driverId+"/"+ SessionSave.getToken(Comman.DEVICE_TOKEN, Drawer_Activity.activity);

        Log.e(TAG, "CallApiFor_FindStatus() url = " + url);
        Log.e(TAG, "CallApiFor_FindStatus() param = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "CallApiFor_FindStatus() responseCode = " + responseCode);
                    Log.e(TAG,"CallApiFor_FindStatus() Response = " + json);

                    if (json != null)
                    {
                        if (json.has("trip_to_destin") && json.getString("trip_to_destin")!=null && json.getBoolean("trip_to_destin"))
                        {
                            if (json.has("location") && json.getString("location")!=null && !json.getString("location").equalsIgnoreCase(""))
                            {
                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FLAG, "1", Drawer_Activity.activity);
                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, json.getString("location"), Drawer_Activity.activity);
                                iv_TripToDestination.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FLAG, "0", Drawer_Activity.activity);
                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, "", Drawer_Activity.activity);
                                iv_TripToDestination.setVisibility(View.GONE);
                            }
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FLAG, "0", Drawer_Activity.activity);
                            SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, "", Drawer_Activity.activity);
                            iv_TripToDestination.setVisibility(View.GONE);
                        }

                        if (json.has("share_ride") && json.getString("share_ride")!=null && json.getString("share_ride").equalsIgnoreCase("1"))
                        {
                            SessionSave.saveUserSession(Comman.SHARE_RIDE_STATUS, "1", Drawer_Activity.activity);
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.SHARE_RIDE_STATUS, "0", Drawer_Activity.activity);
                        }

                        if (json.has("rating") && json.getString("rating")!=null &&
                                !json.getString("rating").equalsIgnoreCase(""))
                        {
                            SessionSave.saveUserSession(Comman.USER_DRIVER_RATE, json.getString("rating"), Drawer_Activity.activity);
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.USER_DRIVER_RATE,"0", Drawer_Activity.activity);
                        }

                        if (json.has("refferal_reward") &&
                                json.getString("refferal_reward")!= null &&
                                !json.getString("refferal_reward").equalsIgnoreCase("") &&
                                !json.getString("refferal_reward").equalsIgnoreCase(""))
                        {
                            SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, json.getString("refferal_reward"), Drawer_Activity.activity);
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT,"0.00", Drawer_Activity.activity);
                        }

                        if (json.has("login") &&
                                json.getString("login")!=null &&
                                !json.getString("login").equalsIgnoreCase("false"))
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");

                                    if (json.has("BookingType"))
                                    {
                                        String BookingType = json.getString("BookingType");
                                        if (BookingType!=null &&
                                                !BookingType.equalsIgnoreCase(""))
                                        {
                                            if (BookingType.equalsIgnoreCase("BookLater"))
                                            {
                                                SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", Drawer_Activity.activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                            }
                                        }
                                        else
                                        {
                                            SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                        }
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                    }
                                    if (json.has("BookingInfo"))
                                    {
                                        String BookingInfo = json.getString("BookingInfo");
                                        if (BookingInfo!=null && !BookingInfo.equalsIgnoreCase(""))
                                        {
                                            String TripId="", PassengerId="", DropOffLat="", DropOffLon="", DropoffLocation="", PickupLat="", PickupLng="", PickupLocation="", Notes=""
                                                    , PaymentType="", FlightNumber="", PassengerType="", BookingType="";
                                            JSONObject BookingInfoObj = json.getJSONObject("BookingInfo");
                                            if (BookingInfoObj.has("Id"))
                                            {
                                                TripId = BookingInfoObj.getString("Id");
                                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, TripId, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PassengerId"))
                                            {
                                                PassengerId = BookingInfoObj.getString("PassengerId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_ID, PassengerId, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("DropOffLat"))
                                            {
                                                DropOffLat = BookingInfoObj.getString("DropOffLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, DropOffLat, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("DropOffLon"))
                                            {
                                                DropOffLon = BookingInfoObj.getString("DropOffLon");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, DropOffLon, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("DropoffLocation"))
                                            {
                                                DropoffLocation = BookingInfoObj.getString("DropoffLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, DropoffLocation, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PickupLat"))
                                            {
                                                PickupLat = BookingInfoObj.getString("PickupLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, PickupLat, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PickupLng"))
                                            {
                                                PickupLng = BookingInfoObj.getString("PickupLng");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, PickupLng, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PickupLocation"))
                                            {
                                                PickupLocation = BookingInfoObj.getString("PickupLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, PickupLocation, Drawer_Activity.activity);
                                            }

                                            if (BookingInfoObj.has("BookingType")) {
                                                BookingType = BookingInfoObj.getString("BookingType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, BookingType, Drawer_Activity.activity);
                                            }

                                            if (BookingInfoObj.has("Status"))
                                            {
                                                String Status = BookingInfoObj.getString("Status");
                                                if (Status!=null && !Status.equalsIgnoreCase(""))
                                                {
                                                    if (Status.equalsIgnoreCase("accepted"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", Drawer_Activity.activity);
                                                        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", Drawer_Activity.activity);
                                                        SessionSave.saveUserSession(Comman.USER_TRIP_FLAG, "1", Drawer_Activity.activity);
                                                    }
                                                    else if (Status.equalsIgnoreCase("traveling"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", Drawer_Activity.activity);
                                                        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "1", Drawer_Activity.activity);
                                                        SessionSave.saveUserSession(Comman.USER_TRIP_FLAG, "2", Drawer_Activity.activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_TRIP_FLAG, "0", Drawer_Activity.activity);
                                                    }
                                                }
                                            }
                                            if (BookingInfoObj.has("Notes"))
                                            {
                                                Notes = BookingInfoObj.getString("Notes");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, Notes, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PaymentType"))
                                            {
                                                PaymentType = BookingInfoObj.getString("PaymentType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, PaymentType, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("FlightNumber"))
                                            {
                                                FlightNumber = BookingInfoObj.getString("FlightNumber");
                                                SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, FlightNumber, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PassengerType"))
                                            {
                                                PassengerType = BookingInfoObj.getString("PassengerType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_TYPE, PassengerType, Drawer_Activity.activity);
                                            }
                                            if (PassengerType!=null && !PassengerType.equalsIgnoreCase("") && (PassengerType.equalsIgnoreCase("others") || PassengerType.equalsIgnoreCase("other")))
                                            {
                                                if (BookingInfoObj.has("PassengerName"))
                                                {
                                                    String Fullname = BookingInfoObj.getString("PassengerName");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, Drawer_Activity.activity);
                                                }
                                                if (BookingInfoObj.has("PassengerContact"))
                                                {
                                                    String MobileNo = BookingInfoObj.getString("PassengerContact");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_MOB_NO_OTHER, MobileNo, Drawer_Activity.activity);
                                                }
                                            }
                                            else
                                            {
                                                if (json.has("PassengerInfo"))
                                                {
                                                    String PassengerInfo = json.getString("PassengerInfo");
                                                    if (PassengerInfo!=null && !PassengerInfo.equalsIgnoreCase(""))
                                                    {
                                                        JSONObject PassengerInfoObj = json.getJSONObject("PassengerInfo");
                                                        if (PassengerInfoObj.has("Fullname"))
                                                        {
                                                            String Fullname = PassengerInfoObj.getString("Fullname");
                                                            SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, Drawer_Activity.activity);
                                                        }
                                                        if (PassengerInfoObj.has("MobileNo"))
                                                        {
                                                            String MobileNo = PassengerInfoObj.getString("MobileNo");
                                                            SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, MobileNo, Drawer_Activity.activity);
                                                        }
                                                        if (PassengerInfoObj.has("Image"))
                                                        {
                                                            String image = PassengerInfoObj.getString("Image");
                                                            SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, image, Drawer_Activity.activity);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        else
                                        {
                                            Log.e("status", "false");
                                            dialogClass.hideDialog();
                                            ClearDataOnWeserviceCall();
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        ClearDataOnWeserviceCall();
                                    }
                                    dialogClass.hideDialog();
                                    if (ClickRelocateButton==0)
                                    {
                                        if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                                && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                        {
                                            SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "1",Drawer_Activity.activity);
                                            if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                                    && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                            {
                                                SetDriverTo_DropOffLocation(1, 0);
                                            }
                                            else
                                            {
                                                SetDriverToPickUpLocation(0);
                                            }
                                        }
                                    }
                                    dialogClass.hideDialog();
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    ClearDataOnWeserviceCall();
                                    //call heat map api
                                    findHeatMapArea();
                                }
                            }
                            else
                            {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), Drawer_Activity.activity.getString(R.string.error_message));
                                }
                                ClearDataOnWeserviceCall();
                            }
                        }
                        else
                        {
                            SessionSave.clearUserSession(Drawer_Activity.activity);

                            final Dialog dialog;
                            if (CabRideDriverApplication.currentActivity()!=null)
                            {
                                dialog = new Dialog(CabRideDriverApplication.currentActivity(), R.style.DialogTheme);
                            }
                            else
                            {
                                dialog = new Dialog(Drawer_Activity.activity, R.style.DialogTheme);
                            }
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(dialog.getWindow().getAttributes());
                            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                            dialog.getWindow().setAttributes(lp);
                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                            TextView tv_dialog_ok, tv_title, tv_message;
                            ImageView dialog_close;

                            tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                            tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                            tv_message = (TextView) dialog.findViewById(R.id.tv_message);
                            dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

                            tv_title.setText("Session Expire");
                            tv_message.setText(Drawer_Activity.activity.getResources().getString(R.string.your_session_is_expire));

                            dialog_close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Login_Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Drawer_Activity.activity.startActivity(intent);
                                    dialog.dismiss();
                                }
                            });
                            tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Login_Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Drawer_Activity.activity.startActivity(intent);
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }

                        if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase("") && Constants.newgpsLongitude!=null && !Constants.newgpsLongitude.equalsIgnoreCase(""))
                        {
                            Location location = new Location(LocationManager.GPS_PROVIDER);
                            location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                            location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                            getAddress(location.getLatitude(), location.getLongitude());
                        }

//                        if (json.has("balance") && json.getString("balance")!=null && !json.getString("balance").equalsIgnoreCase(""))
//                        {
//                            double strBalance = Double.parseDouble(json.getString("balance"));
//                            if (json.has("require_balance") && json.getString("require_balance")!=null && !json.getString("require_balance").equalsIgnoreCase(""))
//                            {
//                                double strRequire_balance = Double.parseDouble(json.getString("require_balance"));
//                                if (strBalance<strRequire_balance)
//                                {
//                                    if (Drawer_Activity.activity!=null)
//                                    {
//                                        Drawer_Activity.activity.OnSwitchChange();
//                                    }
//                                }
//                            }
//                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong), Drawer_Activity.activity.getString(R.string.error_message));
                        iv_TripToDestination.setVisibility(View.GONE);
                        ClearDataOnWeserviceCall();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG,"CallApiFor_FindStatus Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong), Drawer_Activity.activity.getString(R.string.error_message));
                    iv_TripToDestination.setVisibility(View.GONE);
                    ClearDataOnWeserviceCall();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private static void findHeatMapArea() {

        String url = WebServiceAPI.WEB_HEATMAP_DATA;
        dialogClass.showDialog();
        Log.e("url", "HeatMapDataApi = " + url);
        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "HeatMapDataApi = " + responseCode);
                    Log.e("Response", "HeatMapDataApi = " + json);
                    dialogClass.hideDialog();
                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                HeatmapData heatmapData = new Gson().fromJson(json.toString(),HeatmapData.class);
                                if(heatmapData.getTrips() != null && heatmapData.getTrips().size()>0){
                                    List<LatLng> list = new ArrayList<>();
                                    for(int i = 0;i<heatmapData.getTrips().size();i++){
                                        list.add(new LatLng(Double.parseDouble(heatmapData.getTrips().get(i).getPickupLat()),
                                                Double.parseDouble(heatmapData.getTrips().get(i).getPickupLng())));
                                    }

                                    HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                                            .data(list)
                                            .build();

                                    mOverlay = googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                                    //mOverlay.remove();
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),Drawer_Activity.activity.getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(Drawer_Activity.activity.getResources().getString(R.string.something_is_wrong),Drawer_Activity.activity.getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),Drawer_Activity.activity.getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(Drawer_Activity.activity.getResources().getString(R.string.something_is_wrong),Drawer_Activity.activity.getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong),Drawer_Activity.activity.getResources().getString(R.string.error_message));

                    }
                } catch (Exception e) {
                    Log.e("callCancelTrip", "Exception : " + e.toString());
                    dialogClass.hideDialog();

                    errorDialogClass.showDialog(Drawer_Activity.activity.getString(R.string.something_is_wrong),"Error Message");
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    public void removeHeatMap(){
        if(mOverlay != null){
            mOverlay.remove();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    gpsTracker = new GPSTracker(getActivity());
                    Log.e(TAG,"onRequestPermissionsResult() 22 = ");
                    if (gpsTracker.canGetLocation())
                    {
                        Log.e(TAG,"onRequestPermissionsResult() 33 = ");
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    }
                    else
                    {
                        Log.e(TAG,"onRequestPermissionsResult() 44 = ");
                    }
                }
                else
                {
                    Log.e(TAG,"onRequestPermissionsResult() 55 = ");
                    errorDialogClass.showDialog(getResources().getString(R.string.permission_denied_for_location),"Location!!");
                }
                break;
        }
    }


    public static void getAddress(double lat, double lng)
    {
        Geocoder geocoder = new Geocoder(Drawer_Activity.activity, Locale.getDefault());
        try
        {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.size()>0)
            {
                if (addresses.get(0) != null)
                {
                    tv_location.setVisibility(View.VISIBLE);
                    Address obj = addresses.get(0);
                    String add="";
                    if (obj.getAddressLine(0)!= null && !obj.getAddressLine(0).equalsIgnoreCase(""))
                    {
                        add = obj.getAddressLine(0);
                        Log.e(TAG, "getAddress() Address1 " + add);
                    }
                    Log.e(TAG, "getAddress() Address" + add);
                    tv_location.setText(add);
                    dropOffLocationApi = add;
                }
                else
                {
                    tv_location.setVisibility(View.INVISIBLE);
                }
            }
            else
            {
                tv_location.setVisibility(View.INVISIBLE);
            }

        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e(TAG,"getAddress() IOException :"+e.getMessage());
        }
    }

    public static void SetDriverToPickUpLocation(int flagForPolyLine)
    {
        if (googleMap!=null)
        {
            dialogClass.showDialog();
            intMarkerAnim=1;
            if (marker != null)
            {
                marker.remove();
                marker=null;
            }
            if (marker_pickUpLocation != null)
            {
                marker_pickUpLocation.remove();
                marker_pickUpLocation=null;
            }
            if (marker_dropOffLocation != null)
            {
                marker_dropOffLocation.remove();
                marker_dropOffLocation=null;
            }

            if (flagForPolyLine==0)
            {
                polyline=null;
                googleMap.clear();
//                flagCameraAnim=0;
                flagCameraAnim=1;
            }
            TollFeeApi="";

            Log.e(TAG," SetDriverToPickUpLocation() ");
            ll_start_trip_option.setVisibility(View.VISIBLE);
            ll_complete_hold.setVisibility(View.GONE);
            if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity)!=null
                    && !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity).equalsIgnoreCase(""))
            {
                pickUpLat = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity));
            }
            else
            {
                pickUpLat = 0;
            }

            if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity)!=null
                    && !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity).equalsIgnoreCase(""))
            {
                pickUpLng = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity));
            }
            else
            {
                pickUpLng = 0;
            }

            LatLng pickUpLoc = new LatLng(pickUpLat, pickUpLng);
            if (marker_pickUpLocation != null)
            {
                marker_pickUpLocation.remove();
            }
            marker_pickUpLocation = googleMap.addMarker(new MarkerOptions().position(pickUpLoc).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_drop_off_pin)).anchor(0.5f, 1).title("Passenger").snippet(""));
            ll_start_trip_option.setVisibility(View.VISIBLE);

            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
            location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
            LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

            if (marker != null)
            {
                Log.e(TAG,"SetDriverToPickUpLocation() marker not null");
                animateMarkerNew(location, marker);
            }
            else
            {
                Log.e(TAG,"SetDriverToPickUpLocation() marker nulllllllllllllllllll");
                marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                animateMarkerNew(location, marker);
            }

            LatLng origin = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));
            LatLng dest = new LatLng(pickUpLat, pickUpLng);

            // Getting URL to the Google Directions API
            String url = getUrl(origin, dest);
            Log.e(TAG,"SetDriverToPickUpLocation()  onMapClick" +  url.toString());
            FetchUrl FetchUrl = new FetchUrl();
            FetchUrl.execute(url);
            dialogClass.hideDialog();
        }
    }

    public static int GetIconMarker()
    {
        Log.e(TAG ,"GetIconMarker()");

        String driverLogin = ""; // 1 for taxi , 2 for truck

        if (SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,Drawer_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            driverLogin = SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,Drawer_Activity.activity);
            Log.e(TAG ,"GetIconMarker() driverLogin:- " + driverLogin);
        }

        /*String modelName = SessionSave.getUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, Drawer_Activity.activity);
        Log.e("modelName ","modelName : "+modelName);

        if (modelName!=null && modelName.equalsIgnoreCase("First Class"))
        {
            return R.drawable.icon_car_firstclass;
        }
        else if (modelName.equalsIgnoreCase("Business Class"))
        {
            return R.drawable.icon_car_business;
        }
        else if (modelName.equalsIgnoreCase("Economy"))
        {
            return R.drawable.icon_car_economy;
        }
        else if (modelName.equalsIgnoreCase("Taxi"))
        {
            return R.drawable.icon_car_taxi;
        }
        else if (modelName.equalsIgnoreCase("LUX-VAN"))
        {
            return R.drawable.icon_car_lux_van;
        }
        else if (modelName.equalsIgnoreCase("Disability"))
        {
            return R.drawable.icon_car_disability;
        }
        else
        {
            return R.drawable.icon_car_business;
        }*/

        if (driverLogin.equalsIgnoreCase("2"))
        {
            Log.e(TAG ,"GetIconMarker() driverLogin:- Truck");
            return R.drawable.cab_dummy_truck;
        }
        else
        {
            Log.e(TAG ,"GetIconMarker() driverLogin:- Car");
            return R.drawable.cab_dummy_truck;
        }
    }

    public static void StartTripClicked()
    {
        Log.e(TAG,"StartTripClicked()");
        /*Drawer_Activity.activity.StartTrip(SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity)
                , SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity));
        if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)!=null
                && SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase("0"))
        {
            SetDriverTo_DropOffLocation(0, 0);
        }*/
        Drawer_Activity.activity.StartTrip_BookLater(SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity)
                , SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity));
    }

    public static void SetDriverTo_DropOffLocation(int flagForStartTrip, int flagForPolyLine)
    {
        if (googleMap!=null)
        {
            dialogClass.showDialog();
            tv_start_trip.setEnabled(false);
            SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "1", Drawer_Activity.activity);
            intMarkerAnim=1;

            TollFeeApi="";
            if (marker != null)
            {
                marker.remove();
                marker=null;
            }
            if (marker_pickUpLocation != null)
            {
                marker_pickUpLocation.remove();
                marker_pickUpLocation=null;
            }
            if (marker_dropOffLocation != null)
            {
                marker_dropOffLocation.remove();
                marker_dropOffLocation=null;
            }

            ll_start_trip_option.setVisibility(View.GONE);
            ll_complete_hold.setVisibility(View.VISIBLE);
            tv_start_trip.setEnabled(true);
            SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LAT, Constants.newgpsLatitude, Drawer_Activity.activity);
            SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LNG, Constants.newgpsLongitude, Drawer_Activity.activity);

            if (flagForStartTrip==0)
            {
                SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "1", Drawer_Activity.activity);
            }
            if (flagForPolyLine==0)
            {
                polyline=null;
                googleMap.clear();
                flagCameraAnim=1;
            }

            if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity).equalsIgnoreCase(""))
            {
                DropOffLat = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity));
            }
            else
            {
                DropOffLat = 0;
            }

            if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity).equalsIgnoreCase(""))
            {
                DropOffLng = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity));
            }
            else
            {
                DropOffLng = 0;
            }

            LatLng DropUpLoc = new LatLng(DropOffLat, DropOffLng);
            LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

            marker_dropOffLocation = googleMap.addMarker(new MarkerOptions().position(DropUpLoc).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_drop_off_pin)).anchor(0.5f, 1).title("Destination").snippet(""));

            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
            location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));

            if (marker != null)
            {
                Log.e(TAG,"SetDriverTo_DropOffLocation() marker not null");
                animateMarkerNew(location, marker);
            }
            else
            {
                Log.e(TAG,"SetDriverTo_DropOffLocation() marker nulllllllllllllllllll");
                marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                animateMarkerNew(location, marker);
            }


            // Checks, whether start and end locations are captured
            LatLng origin = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));
            LatLng dest = new LatLng(DropOffLat, DropOffLng);

            // Getting URL to the Google Directions API
            String url = getUrl(origin, dest);
            dialogClass.hideDialog();
            Log.e(TAG,"SetDriverTo_DropOffLocation()  onMapClick" + url.toString());
            FetchUrl FetchUrl = new FetchUrl();
            FetchUrl.execute(url);
            ll_start_trip_option.setVisibility(View.GONE);
            dialogClass.hideDialog();
        }
    }

    public static String getUrl(LatLng origin, LatLng dest){

        Log.e(TAG,"showRoute() 11 11");
        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=true&key="+Drawer_Activity.activity.getResources().getString(R.string.api_key);

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
        Log.e(TAG,"showRoute() 12 12 = url = "+url);
        return url;
    }

    // Fetches data from url passed
    public static class FetchUrl extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.e(TAG,"FetchUrl() Background Task data" + data);

            } catch (Exception e) {
                Log.e(TAG,"FetchUrl() Background Task" + e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            if (result!=null)
            {

                try
                {
                    JSONObject resultOb = new JSONObject(result);
                    if (resultOb.has("status"))
                    {
                        String status = resultOb.getString("status");
                        if (status!=null)
                        {
                            if (!status.equalsIgnoreCase("OVER_QUERY_LIMIT"))
                            {
                                Gson gson=new Gson();
                                DataRoute route = gson.fromJson(result,DataRoute.class);
                                if (route!=null)
                                {
                                    String Distance = "0";
                                    try
                                    {
                                        if (route.getRoutes().size()>0 && route.getRoutes().get(0).getLegs().size()>0)
                                        {
                                            Distance =  route.getRoutes().get(0).getLegs().get(0).getDistance().getText();
                                        }
                                    }
                                    catch (NullPointerException| ArrayIndexOutOfBoundsException e) {
                                    }
                                    if (marker_pickUpLocation!=null)
                                    {
                                        if (Distance!=null && !Distance.equalsIgnoreCase(""))
                                        {
                                            marker_pickUpLocation.setSnippet("Distance : " +Distance);
                                        }
                                        else
                                        {
                                            marker_pickUpLocation.setSnippet("Distance : " + 0);
                                        }
                                    }
                                }
                                parserTask.execute(result);
                            }
                            else
                            {
                               /* if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                        && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                {
                                    if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                            && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                    {
                                        SetDriverTo_DropOffLocation(1, 0);
                                    }
                                    else
                                    {
                                        SetDriverToPickUpLocation(0);
                                    }
                                }*/
                            }
                        }
                        else
                        {
                           /* if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                    && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                            {
                                if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                        && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                {
                                    SetDriverTo_DropOffLocation(1, 0);
                                }
                                else
                                {
                                    SetDriverToPickUpLocation(0);
                                }
                            }*/
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("wwwwwwww","wwwwwwwwwwwwwwwwww JSONException 7777777777777"+e.getMessage());
                }
            }
        }
    }

    public static String downloadUrl(String strUrl) throws IOException
    {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.e("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.e("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public static class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>>
    {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.e("ParserTask",jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.e("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.e("ParserTask","Executing routes");
                Log.e("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.e("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;

            PolylineOptions lineOptions = null;

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                latLngArrayList = new ArrayList<>();
                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
//                    latLngArrayList.add(position);
                    builder.include(position);
                    points.add(position);
                    latLngArrayList.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorPolyline));

                Log.e("onPostExecute","onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                if (polyline!=null)
                {
                    Log.e("poluline","is there");
                    List<LatLng> points1 = latLngArrayList;
                    polyline.setPoints(points1);
                }
                else
                {
                    Log.e("poluline","no polyline");
                    polyline = googleMap.addPolyline(lineOptions);
                }

                if (flagCameraAnim==1)
                {
                    Log.e("flagCameraAnim","11111111111111111");
                    LatLngBounds bounds = builder.build();
                    int padding = 200; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    googleMap.animateCamera(cu);
                    flagCameraAnim=0;
                }
            }
            else {
                Log.e("onPostExecute","without Polylines drawn");
            }
        }
    }

    ////// https://stackoverflow.com/questions/35554796/rotate-marker-and-move-animation-on-map-like-uber-android
    public static void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null)
        {
            readBearing=0;
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(4000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        bearingF = getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude()));
                        if (!String.valueOf(bearingF).equalsIgnoreCase("NaN"))
                        {
                            if (readBearing==0)
                            {
//                                marker.setRotation(bearingF);
                                rotateMarker(marker, bearingF);
                                marker.setIcon(BitmapDescriptorFactory.fromResource(GetIconMarker()));
                                readBearing=1;
                            }
                        }
                    } catch (Exception ex) {}
                }
            });

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                    .target(endPosition)
                    .zoom(17.5f)
                    .build()));

            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            valueAnimator.start();
        }
    }

    static public void rotateMarker(final Marker marker, final float toRotation) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 -t) * startRotation;

                marker.setRotation(-rot > 180 ? rot/2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    //Method for finding bearing between two points
    public static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) ((Math.toDegrees(Math.atan(lng / lat))));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) - 90 /*+ 270*/);
        return -1;
    }

    public void GetLocationGps(Location location, double speedKmPerHour)
    {
        if (flagMapReady==1)
        {
            changeLocationOfMarker(location, speedKmPerHour);
        }
    }

    private void changeLocationOfMarker(Location location, double speedOfUser)
    {
        if (location!=null)
        {
            Log.e("GetLocationGps","11111111111111111 : " +location.getProvider()+"\nChange_DriverMarkerPosition() location.getLatitude(): "+location.getLatitude()+"\n location.getLongitude() : "+location.getLongitude());

            if (location.getLatitude()!=0)
            {
                calendar = Calendar.getInstance();
                dateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

                Constants.newgpsLatitude = location.getLatitude() + "";
                Constants.newgpsLongitude = location.getLongitude() + "";
                Constants.newTime = dateFormat.format(calendar.getTime());

                meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, Drawer_Activity.activity);
                if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
                {
                    if (speedOfUser<5.0)
                    {
                        Log.e("GetLocationGps ","11111111111111111 : pause trip111");
                        meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, Drawer_Activity.activity);
                        if (SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, Drawer_Activity.activity)!=null
                                && SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, Drawer_Activity.activity).equals("1"))
                        {

                        }
                        else
                        {
                            if (meterActiveFlag.equalsIgnoreCase("2"))
                            {

                            }
                            else
                            {
                                Drawer_Activity.startChronometer();
                                SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "2", Drawer_Activity.activity);
                            }
                        }
                    }
                    else
                    {
                        Log.e("GetLocationGps ","11111111111111111 : again start trip111");
                        meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, Drawer_Activity.activity);
                        if (meterActiveFlag.equalsIgnoreCase("3"))
                        {

                        }
                        else
                        {
                            Drawer_Activity.PauseChronometer();
                            SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "3", Drawer_Activity.activity);
                        }
                    }
                }
                else
                {
                    Drawer_Activity.stopChronometer();
                }

                Constants.oldgpsLatitude = location.getLatitude() + "";
                Constants.oldgpsLongitude = location.getLongitude() + "";
                Constants.oldTime = dateFormat.format(calendar.getTime());

                sydney=null;
                sydney = new LatLng(location.getLatitude(), location.getLongitude());

                if (marker != null)
                {
                    Log.e("GetLocationGps","marker" + "marker not null");
                    animateMarkerNew(location, marker);
                }
                else
                {
                    Log.e("GetLocationGps", "marker" + "marker nulllllllllllllllllll");
                    if (googleMap!=null)
                    {
                        marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet(""));
                        animateMarkerNew(location, marker);
                    }
                }
                if (tv_location!=null)
                {
                    getAddress(location.getLatitude(), location.getLongitude());
                }

                if (Drawer_Activity.activity!=null)
                {
                    if (SessionSave.getUserSession(Comman.USER_TRIP_FLAG,Drawer_Activity.activity) != null &&
                            SessionSave.getUserSession(Comman.USER_TRIP_FLAG,Drawer_Activity.activity).equalsIgnoreCase("2"))
                    {
                        Drawer_Activity.activity.startTimerDropOff();
                    }
                    else if (SessionSave.getUserSession(Comman.USER_TRIP_FLAG,Drawer_Activity.activity) != null &&
                            SessionSave.getUserSession(Comman.USER_TRIP_FLAG,Drawer_Activity.activity).equalsIgnoreCase("1"))
                    {
                        Drawer_Activity.activity.startTimerPickUp();
                    }
                    else
                    {
                        Drawer_Activity.activity.startTimer();
                    }
                }

                LatLng coordinate = new LatLng(location.getLatitude(), location.getLongitude());
                if (latLngArrayList!=null && latLngArrayList.size()>0)
                {
                    Log.e("PolyUtil","PolyUtil : " + PolyUtil.isLocationOnEdge(coordinate,latLngArrayList,true,100));
                    if (!PolyUtil.isLocationOnEdge(coordinate,latLngArrayList,true,100))
                    {
                        if (FlagPolyline>3)
                        {
                            Log.e("Change_DriverMarkerPosi","avoooo");
                            FlagPolyline=0;
                            if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                    && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                            {
                                if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                        && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                {
                                    SetDriverTo_DropOffLocation(1, 1);
                                }
                                else
                                {
                                    SetDriverToPickUpLocation(1);
                                }
                            }
                        }
                        FlagPolyline++;
                    }
                }
            }
        }
    }

    @Override
    public void onDestroy()
    {
        if (marker!=null)
        {
            marker.remove();
            marker=null;
        }
        if (marker_pickUpLocation!=null)
        {
            marker_pickUpLocation.remove();
            marker_pickUpLocation=null;
        }
        if (marker_dropOffLocation!=null)
        {
            marker_dropOffLocation.remove();
            marker_dropOffLocation=null;
        }

        super.onDestroy();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        Log.e("onDestroyView", "onDestroyView onDestroyView onDestroyView");
    }

    public static void ClearDataOnWeserviceCall()
    {
        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0",  Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
        Log.e("avooooooooooooo","avoooooooooooooooooooo555555555555555");
        SessionSave.saveUserSession(Comman.BOOKING_PENDING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.USER_TRIP_FLAG, "0", Drawer_Activity.activity);
        setCameraOnMarker();
    }

    public static void ClearTripSession()
    {
        SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_ID, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_END_TRIP_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_END_TRIP_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_NAME, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_NOTE, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_TYPE, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_MOB_NO_OTHER, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0",  Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, "", Drawer_Activity.activity);
        Log.e("avooooooooooooo","avoooooooooooooooooooo6666666666666666");
        SessionSave.saveUserSession(Comman.BOOKING_PENDING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.USER_TRIP_FLAG, "0", Drawer_Activity.activity);

        RefreshLocation();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        String CheckMark = SessionSave.getUserSession(Comman.DESTINATION_ADDRESS_FLAG, Drawer_Activity.activity);
        if (CheckMark != null && !CheckMark.equalsIgnoreCase("") && CheckMark.equalsIgnoreCase("1"))
        {
            iv_TripToDestination.setVisibility(View.VISIBLE);
        }
        else
        {
            iv_TripToDestination.setVisibility(View.GONE);
        }
    }

    private void callSOSConfirem() {
        final Dialog dialogNotice = new Dialog(getActivity(), R.style.PauseDialog);
        dialogNotice.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNotice.setCancelable(false);
        dialogNotice.setContentView(R.layout.dialog_app_notice_cancel_request);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogNotice.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogNotice.getWindow().setAttributes(lp);

        dialogNotice.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogNotice.getWindow().setAttributes(lp);

        final TextView  tv_message, tv_update, tv_no_update;
        final ImageView dialog_close;
        final LinearLayout dialog_ok_layout, dialog_no_layout;

        tv_message = (TextView) dialogNotice.findViewById(R.id.tv_message);
        tv_no_update = (TextView) dialogNotice.findViewById(R.id.tv_no_update);
        tv_update = (TextView) dialogNotice.findViewById(R.id.tv_update);
        dialog_close = (ImageView) dialogNotice.findViewById(R.id.dialog_close);
        dialog_ok_layout = (LinearLayout) dialogNotice.findViewById(R.id.dialog_ok_layout);
        dialog_no_layout = (LinearLayout) dialogNotice.findViewById(R.id.dialog_no_layout);

        String CancellationFee = SessionSave.getUserSession(Comman.DRIVER_CANCELATION_FEE,Drawer_Activity.activity);
        Log.e(TAG,"OpenDialogForNotice() CancellationFee:- " + CancellationFee);

        if (!CancellationFee.equalsIgnoreCase("") &&
                !CancellationFee.equalsIgnoreCase("null"))
        {
            tv_message.setText(getResources().getString(R.string.are_you_sure_you_have_emergency));
        }
        else
        {
            tv_message.setText(getResources().getString(R.string.are_you_sure_you_have_emergency));
        }
        //tv_update.setText("Yes");

        tv_update.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                dialogNotice.dismiss();
                if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase("")) {
                    callSos(SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity), Constants.newgpsLatitude, Constants.newgpsLongitude);
                }

            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
                if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase("")) {
                    callSos(SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity), Constants.newgpsLatitude, Constants.newgpsLongitude);
                }
            }
        });


        dialog_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
            }
        });

        tv_no_update.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
            }
        });

        dialog_no_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
            }
        });
        dialogNotice.show();
    }

    private void callSos(String DriverId, String lat, String lng) {

        dialogClass.showDialog();

        String Url = WebServiceAPI.WEB_SERVICE_SOS;

        Log.e(TAG, "callSOS() : URL : "+Url);

        HashMap<String, String> param = new HashMap<>();
        param.put(WebServiceAPI.CHANGE_PASSWORD_PARAM_DRIVER_ID, DriverId);
        param.put(WebServiceAPI.COMPLETE_TRIP_PARAM_LAT, lat);
        param.put(WebServiceAPI.CHANGE_DRIVER_SHIFT_STATUS_PARAM_LNG, lng);

        Log.e(TAG, "callSOS() : param : "+param);

        aQuery.ajax(Url, param, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);

                try {

                    if(object != null) {

                        if(object.has("status")) {

                            Log.e(TAG, "callSOS() : RESPONSE : "+object.toString());

                            if(object.getBoolean("status")) {

                                String message =
                                        "SOS gerado com sucesso, entraremos em contacto consigo, em breve.";

                                if(object.has("message")) {
                                    errorDialogClass.showDialog("SOS gerado com sucesso, entraremos em contacto consigo, em breve.",
                                            "");
                                }

                            } else {
                                Log.e(TAG, "callSOS() : status not found");
                            }

                        } else {
                            Log.e(TAG, "callSOS() : status not found");
                        }

                    } else {
                        Log.e(TAG, "callSOS() : object null");

                    }

                } catch (Exception e) {
                    Log.e(TAG, "callSOS() : "+e.getMessage());
                } finally {
                    dialogClass.hideDialog();
                }

            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}
