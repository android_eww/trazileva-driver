package com.connectkargo.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.R;


public class MyJob_Fragment  extends Fragment implements View.OnClickListener{

    public static LinearLayout ll_future, ll_pending, ll_past, main_layout;
    public static View view_future, view_pending, view_past;
    public static TextView tv_future, tv_pending, tv_pastjob;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_myjob, container, false);

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);

        ll_future = (LinearLayout) rootView.findViewById(R.id.ll_future);
        ll_pending = (LinearLayout) rootView.findViewById(R.id.ll_pending);
        ll_past = (LinearLayout) rootView.findViewById(R.id.ll_past);

        tv_future = (TextView) rootView.findViewById(R.id.tv_future);
        tv_pending = (TextView) rootView.findViewById(R.id.tv_pending);
        tv_pastjob = (TextView) rootView.findViewById(R.id.tv_pastjob);

        view_future = (View) rootView.findViewById(R.id.view_future);
        view_pending = (View) rootView.findViewById(R.id.view_pending);
        view_past = (View) rootView.findViewById(R.id.view_past);

        ll_future.setOnClickListener(this);
        ll_pending.setOnClickListener(this);
        ll_past.setOnClickListener(this);

        call_FutureList();

        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_future:
                call_FutureList();
                break;

            case R.id.ll_pending:
                call_PendingList();
                break;

            case R.id.ll_past:
                call_PastList();
                break;
        }
    }

    public static void call_FutureList()
    {
        view_future.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorThemeYellow));
        view_pending.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorWhite));
        view_past.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorWhite));

        tv_future.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTextThemeYello));
        tv_pending.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorHintGray));
        tv_pastjob.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorHintGray));

        Future_JobBooking_List_Fragment homeFragment = new Future_JobBooking_List_Fragment();
        FragmentTransaction fragmentTransaction = Drawer_Activity.activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public static void call_PendingList()
    {
        view_future.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorWhite));
        view_pending.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorThemeYellow));
        view_past.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorWhite));

        tv_future.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorHintGray));
        tv_pending.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTextThemeYello));
        tv_pastjob.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorHintGray));

        Pending_Job_List_Fragment homeFragment = new Pending_Job_List_Fragment();
        FragmentTransaction fragmentTransaction = Drawer_Activity.activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public static void call_PastList()
    {
        view_future.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorWhite));
        view_pending.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorWhite));
        view_past.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorThemeYellow));

        tv_future.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorHintGray));
        tv_pending.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorHintGray));
        tv_pastjob.setTextColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTextThemeYello));

        Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
        FragmentTransaction fragmentTransaction = Drawer_Activity.activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden)
        {
            if (ConnectivityReceiver.isConnected())
            {
                call_FutureList();
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color), getActivity()).snackieBar().show();
            }
        }
    }
}
