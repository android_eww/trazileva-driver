package com.connectkargo.driver.Fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.connectkargo.driver.Adapter.CarType_Multi_Selection_Adapter;
import com.connectkargo.driver.Activity.Login_Activity;
import com.connectkargo.driver.Activity.Registration_Activity;

import com.connectkargo.driver.Adapter.TruckType_Multi_Selection_Adapter;
import com.connectkargo.driver.Been.TruckType_Been;
import com.connectkargo.driver.Been.VehicleType_Been;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Register_VehicleDetail_Fragment extends Fragment implements View.OnClickListener {

    private String TAG = "Register_VehicleDetail_Fragment";
    LinearLayout  main_layout, main_layoutDialog;
    ListView dialog_ListView, dialog_ListView_truck;
    TextView tv_register_CarType, tv_register_VehicleNext, tv_backToLogin;
    EditText et_vehiRegistrationNo, et_register_CarModel;
    ImageView iv_carImage;

    AppCompatRadioButton cb_car_service, cb_truck_service;
    String CAR="car", TRUCK="truck", checkBox_Selected ="";

    ArrayList<VehicleType_Been> list_vehicleItem = new ArrayList<>();
    CarType_Multi_Selection_Adapter adapter;

    ArrayList<TruckType_Been> list_vehicleItem_turck = new ArrayList<>();
    TruckType_Multi_Selection_Adapter adapterTruck;


    String selectedModelId = "";
    List<String> list_ModelName = new ArrayList<String>();

    DialogClass dialogClass;
    AQuery aQuery;

    public static String clickedType="";
    private static final int MY_REQUEST_CODE_CAMERA = 101;
    private static final int MY_REQUEST_CODE_STORAGE = 100;
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private int GALLERY = 1, CAMERA = 0;
    byte[] DriverCar_ByteImage=null;
    String encodedImage;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;

    private Animation animBounce;
    Intent intent;
    ErrorDialogClass errorDialogClass;

    public static Bitmap bitmapSumsung;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_vehicle_detail, container, false);

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        aQuery = new AQuery(getActivity());
        errorDialogClass = new ErrorDialogClass(getActivity());

        main_layout= (LinearLayout) rootView.findViewById(R.id.main_layout);

        et_vehiRegistrationNo = (EditText) rootView.findViewById(R.id.et_vehiRegistrationNo);
        et_register_CarModel = (EditText) rootView.findViewById(R.id.et_register_CarModel);

        tv_register_VehicleNext = (TextView) rootView.findViewById(R.id.tv_register_VehicleNext);
        tv_register_CarType = (TextView) rootView.findViewById(R.id.tv_register_CarType);
        tv_backToLogin = (TextView) rootView.findViewById(R.id.tv_backToLogin);

        iv_carImage = (ImageView) rootView.findViewById(R.id.iv_carImage);

        cb_car_service = (AppCompatRadioButton) rootView.findViewById(R.id.cb_car_service);
        cb_truck_service = (AppCompatRadioButton) rootView.findViewById(R.id.cb_truck_service);
        cb_truck_service.setChecked(true);
        checkBox_Selected = TRUCK;

        iv_carImage.setOnClickListener(this);
        tv_register_VehicleNext.setOnClickListener(this);
        tv_register_CarType.setOnClickListener(this);
        tv_backToLogin.setOnClickListener(this);

        cb_car_service.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tv_register_CarType.setText("");
                SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,"",getActivity());
                selectedModelId = "";

                selectCar();
            }
        });
        cb_truck_service.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tv_register_CarType.setText("");
                SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,"",getActivity());
                selectedModelId = "";

                selectTruck();
            }
        });

        setUserDetails();

        if (Global.isNetworkconn(getActivity()))
        {
            FindWhichTypeIs_Visible(true);
        }
        else
        {
            errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
        }

        return rootView;
    }

    private void setUserDetails()
    {
        Log.e(TAG,"setUserDetails()");

        if(SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            Log.e(TAG, "Call OnResume  byte[] ByteImage");
            byte[] ByteImage = Base64.decode(SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,Registration_Activity.activity).toString().trim(),Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(ByteImage, 0, ByteImage.length);
            iv_carImage.setImageBitmap(bitmap);

            DriverCar_ByteImage = ConvertToByteArray(bitmap);
            encodedImage = Base64.encodeToString(DriverCar_ByteImage, Base64.DEFAULT);
            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, encodedImage,getActivity());
        }
        if(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_vehiRegistrationNo.setText(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,Registration_Activity.activity).toString().trim());
        }

        if (SessionSave.getUserSession(Comman.USER_SERVICE,Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_SERVICE,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            checkBox_Selected = SessionSave.getUserSession(Comman.USER_SERVICE,Registration_Activity.activity);

            if (checkBox_Selected.equalsIgnoreCase(CAR))
            {
                selectCar();
            }
            else
            {
                selectTruck();
            }
        }

        if(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,Registration_Activity.activity) != null &&
                !SessionSave.getUserSession(Comman.USER_CAR_COMPANY,Registration_Activity.activity).equalsIgnoreCase(""))
        {
            et_register_CarModel.setText(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,Registration_Activity.activity).toString().trim());
        }
    }

    public void selectCar()
    {
        checkBox_Selected = CAR;
        cb_car_service.setChecked(true);
        cb_truck_service.setChecked(false);
    }

    public void selectTruck()
    {
        checkBox_Selected = TRUCK;
        cb_car_service.setChecked(false);
        cb_truck_service.setChecked(true);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.rl_carImage:
                takePermission();
//                ShowPictureDialog();
                break;

            case R.id.iv_carImage:
                takePermission();
//                ShowPictureDialog();
                break;

            case R.id.tv_register_CarType:

                if (Global.isNetworkconn(getActivity()))
                {
                    SelectCarOrTruckType();
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
                break;

            case R.id.tv_register_VehicleNext:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_register_VehicleNext.startAnimation(animBounce);

                animBounce.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)

                    {
                        CheckData();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation)
                    {

                    }
                });
                break;

            case R.id.tv_backToLogin:
                animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                tv_backToLogin.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        SessionSave.clearUserSession(Registration_Activity.activity);
                        intent = new Intent(Registration_Activity.activity, Login_Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        if (Registration_Activity.activity!=null)
                        {
                            Registration_Activity.activity.finish();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
        }
    }

    private void SelectCarOrTruckType()
    {
        final Dialog dialog = new Dialog(getActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_vehicle_type);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        main_layoutDialog = (LinearLayout) dialog.findViewById(R.id.main_layoutDialog);

        dialog_ListView = (ListView) dialog.findViewById(R.id.lv_vehicle_type);
        dialog_ListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        dialog_ListView_truck = (ListView) dialog.findViewById(R.id.lv_truck_type);
        dialog_ListView_truck.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        adapter = new CarType_Multi_Selection_Adapter(getActivity(), list_vehicleItem, main_layoutDialog);
        dialog_ListView.setAdapter(adapter);

        adapterTruck = new TruckType_Multi_Selection_Adapter(getActivity(), list_vehicleItem_turck ,  main_layoutDialog);
        dialog_ListView_truck.setAdapter(adapterTruck);

        final LinearLayout dialog_ok_layout, ll_select_cars, ll_select_truck;
        ImageView dialogClose;

        dialogClose = (ImageView) dialog.findViewById(R.id.dialog_close);
        dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ll_select_cars = (LinearLayout) dialog.findViewById(R.id.ll_select_cars);
        ll_select_truck = (LinearLayout) dialog.findViewById(R.id.ll_select_truck);

        dialogClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                String SelectedVehicle = "";

                if (checkBox_Selected.equalsIgnoreCase(CAR))
                {
                    for (int i=0; i<list_vehicleItem.size() ; i++)
                    {
                        if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
                        {
                            if (SelectedVehicle !=null && !SelectedVehicle.equalsIgnoreCase(""))
                            {
                                SelectedVehicle = SelectedVehicle + "," + list_vehicleItem.get(i).getName();
                                selectedModelId = selectedModelId + "," + list_vehicleItem.get(i).getId();
                            }
                            else
                            {
                                SelectedVehicle = list_vehicleItem.get(i).getName();
                                selectedModelId =  list_vehicleItem.get(i).getId();
                            }
                        }
                    }
                }
                else
                {
                    for (int i=0; i<list_vehicleItem_turck.size() ; i++)
                    {
                        if (list_vehicleItem_turck.get(i).getStatus().equalsIgnoreCase("1"))
                        {
                            if (SelectedVehicle !=null && !SelectedVehicle.equalsIgnoreCase(""))
                            {
                                SelectedVehicle = SelectedVehicle + "," + list_vehicleItem_turck.get(i).getName();
                                selectedModelId = selectedModelId + "," + list_vehicleItem_turck.get(i).getId();
                            }
                            else
                            {
                                SelectedVehicle = list_vehicleItem_turck.get(i).getName();
                                selectedModelId =  list_vehicleItem_turck.get(i).getId();
                            }
                        }
                    }
                }

                if (SelectedVehicle != null && !SelectedVehicle.equalsIgnoreCase(""))
                {
                    tv_register_CarType.setText(SelectedVehicle);
                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, selectedModelId, getActivity());
                    Log.e(TAG, "SelectCarOrTruckType() carIds:- " + selectedModelId);
                }
                else
                {
                    tv_register_CarType.setText(getResources().getString(R.string.car_type));
                    selectedModelId = "";

                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, selectedModelId, getActivity());
                    Log.e(TAG, "SelectCarOrTruckType() carIds:- " + selectedModelId);
                }
            }
        });

//        ll_select_cars.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                ll_select_cars.setBackgroundColor(getResources().getColor(R.color.colorRed));
//                ll_select_truck.setBackgroundColor(getResources().getColor(R.color.colorWhiteLow));
//                dialog_ListView.setVisibility(View.VISIBLE);
//                dialog_ListView_truck.setVisibility(View.GONE);
//
//            }
//        });
//
//        ll_select_truck.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                ll_select_truck.setBackgroundColor(getResources().getColor(R.color.colorRed));
//                ll_select_cars.setBackgroundColor(getResources().getColor(R.color.colorWhiteLow));
//                dialog_ListView.setVisibility(View.GONE);
//                dialog_ListView_truck.setVisibility(View.VISIBLE);
//
//            }
//        });

        if (checkBox_Selected.equalsIgnoreCase(CAR))
        {
            dialog_ListView.setVisibility(View.VISIBLE);
            dialog_ListView_truck.setVisibility(View.GONE);
        }
        else
        {
            dialog_ListView.setVisibility(View.GONE);
            dialog_ListView_truck.setVisibility(View.VISIBLE);
        }

//        if (list_vehicleItem != null && list_vehicleItem.size() < 1 &&
//                list_vehicleItem_turck != null && list_vehicleItem_turck.size() < 1)
//        {
//
//        }

        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                FindWhichTypeIs_Visible(false);
            }
        },800);

        dialog.show();
    }

    private void takePermission()
    {

        int a = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        int b = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        Log.e("TAG", "a = " + a);
        Log.e("TAG", "b = " + b);
        if (a != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("TAG", "a not granted");
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
            Registration_Activity.cameraPermissionLength = listPermissionsNeeded.size();
        }
        if (b != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("TAG", "b not granted");
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            Registration_Activity.cameraPermissionLength = listPermissionsNeeded.size();
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            Log.e("TAG", "ask permission");
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), 5/*Common_varible.REQUEST_ID_MULTIPLE_PERMISSIONS*/);
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            ShowPictureDialog();
        }
    }

    public void ShowPictureDialog()
    {
        final Dialog dialog = new Dialog(getActivity(),R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_photo_choose);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
        TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
            }
        });

        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        choosePhotoFromGallary();
                    }
                },800);
            }
        });

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        takePhotoFromCamera();
                    }
                },800);

            }
        });

        dialog.show();
    }

//    private void ShowPictureDialog()
//    {
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
//        {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE_CAMERA);
//        }
//        else if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
//        {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE_STORAGE);
//        }
//        else
//        {
//            final Dialog dialog = new Dialog(getActivity(),R.style.PauseDialog);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setCancelable(true);
//            dialog.setContentView(R.layout.dialog_photo_choose);
//
//            TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
//            TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
//            TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
//            LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
//            ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);
//
//            ll_Ok.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//
//            tv_Ok.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//
//            iv_close.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view)
//                {
//                    dialog.dismiss();
//                }
//            });
//
//            tv_gallery.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v)
//                {
//                    dialog.dismiss();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run()
//                        {
//                            choosePhotoFromGallary();
//                        }
//                    },800);
//                }
//            });
//
//            tv_camera.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v)
//                {
//                    dialog.dismiss();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run()
//                        {
//                            takePhotoFromCamera();
//                        }
//                    },800);
//
//                }
//            });
//
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            lp.copyFrom(dialog.getWindow().getAttributes());
//            //lp.width = Comman.DEVICE_WIDTH;
//            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
//            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//            lp.gravity = Gravity.CENTER;
//
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            dialog.getWindow().setAttributes(lp);
//
//            dialog.show();
//        }
//    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }
        else
        {
            userChoosenTask = chooseCamera;
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
            startActivityForResult(intent, CAMERA);
        }
    }

    public String getDeviceName()
    {
        String manufacturer = Build.MANUFACTURER;
        return manufacturer;
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
//        switch (requestCode) {
//            case MY_REQUEST_CODE_CAMERA:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                {
//                    Log.e("showPictureDialog","checkSelfPermission CAMERA permission granted");
//                    ShowPictureDialog();
//                }
//                break;
//
//            case MY_REQUEST_CODE_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                {
//                    Log.e("showPictureDialog","checkSelfPermission GALLERY permission granted");
//                    ShowPictureDialog();
//                }
//                break;
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                bitmapSumsung=null;
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                bitmapSumsung=null;
                onCaptureImageResult(data);
            }
            else
            {
                DriverCar_ByteImage=null;
                bitmapSumsung=null;
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try
        {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }
        if (bitmap!=null)
        {
            bitmapImage = getResizedBitmap(bitmap,400);

            if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
            {
                bitmapSumsung = bitmapImage;
            }
            if (bitmapImage!=null)
            {
                Picasso.get()
                        .load(data.getData())
//                        .fit()
                        .into(iv_carImage);
                DriverCar_ByteImage = ConvertToByteArray(bitmapImage);

                encodedImage = Base64.encodeToString(DriverCar_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, encodedImage,getActivity());
            }
            else
            {
                DriverCar_ByteImage=null;
                SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, "",getActivity());

                new SnackbarUtils(main_layout, getString(R.string.please_select_vehicle_image),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "PickNGo", null);
        return Uri.parse(path);
    }

    private void onCaptureImageResult(Intent data)
    {
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Log.e("hahahahahahahaha", "getDeviceName() : "+getDeviceName());
            bitmapSumsung = (Bitmap) data.getExtras().get("data");
            Uri imageUri1 = getImageUri(Registration_Activity.activity, bitmapSumsung);
            Picasso.get()
                    .load(imageUri1)
//                    .fit()
                    .into(iv_carImage);
            DriverCar_ByteImage = ConvertToByteArray(bitmapSumsung);
            encodedImage = Base64.encodeToString(DriverCar_ByteImage, Base64.DEFAULT);
            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, encodedImage,getActivity());
        }
        else {
            try
            {
                Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
                Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                Log.e("#########","bmp height = "+thumbnail.getHeight());
                Log.e("#########","bmp width = "+thumbnail.getWidth());
                thumbnail = getResizedBitmap(thumbnail,400);
                bitmapImage=thumbnail;
//                bitmapSumsung = thumbnail;
                if (thumbnail!=null)
                {
                    Picasso.get()
                            .load(imageUri)
//                            .fit()
                            .into(iv_carImage);
                    DriverCar_ByteImage = ConvertToByteArray(thumbnail);

                    encodedImage = Base64.encodeToString(DriverCar_ByteImage, Base64.DEFAULT);
                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, encodedImage,getActivity());
                }
                else
                {
                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, "",getActivity());
                    DriverCar_ByteImage=null;
                    new SnackbarUtils(main_layout, getString(R.string.please_select_vehicle_image),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
                }
            }
            catch (Exception e)
            {
                DriverCar_ByteImage=null;
                Log.e("call","exception = "+e.getMessage());
            }

        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    private void CheckData()
    {
        Log.e(TAG,"CheckData()");

        if (DriverCar_ByteImage==null)
        {
            new SnackbarUtils(main_layout, getString(R.string.please_select_vehicle_image),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else
        if (TextUtils.isEmpty(et_vehiRegistrationNo.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_vehicle_registration_no),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_register_CarModel.getText().toString().trim()))
        {
            Log.e(TAG,"CheckData() et_register_CarModel empty");

            new SnackbarUtils(main_layout, getString(R.string.please_enter_car_model),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(selectedModelId))
        {
            Log.e(TAG,"CheckData() selectedModelId empty");

            new SnackbarUtils(main_layout, getString(R.string.please_select_car_type),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
        else
        {
            SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, et_vehiRegistrationNo.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, et_register_CarModel.getText().toString(),getActivity());

            GotoNextPage();
        }
    }

    private void GotoNextPage()
    {
        Log.e(TAG, "GotoNextPage() AllSelectedVehicle:- ");

        String AllSelectedVehicle="", VehicleId="";

        if (checkBox_Selected.equalsIgnoreCase(CAR))
        {
            for (int i=0; i<list_vehicleItem.size() ; i++)
            {
                if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
                {
                    if (AllSelectedVehicle !=null && !AllSelectedVehicle.equalsIgnoreCase(""))
                    {
                        AllSelectedVehicle = AllSelectedVehicle + "," + list_vehicleItem.get(i).getName();
                        VehicleId = VehicleId + "," + list_vehicleItem.get(i).getId();
                    }
                    else
                    {
                        AllSelectedVehicle = list_vehicleItem.get(i).getName();
                        VehicleId =  list_vehicleItem.get(i).getId();
                    }
                }
            }
        }
        else
        {
            for (int i=0; i<list_vehicleItem_turck.size() ; i++)
            {
                if (list_vehicleItem_turck.get(i).getStatus().equalsIgnoreCase("1"))
                {
                    if (AllSelectedVehicle !=null && !AllSelectedVehicle.equalsIgnoreCase(""))
                    {
                        AllSelectedVehicle = AllSelectedVehicle + "," + list_vehicleItem_turck.get(i).getName();
                        VehicleId = VehicleId + "," + list_vehicleItem_turck.get(i).getId();
                    }
                    else
                    {
                        AllSelectedVehicle = list_vehicleItem_turck.get(i).getName();
                        VehicleId =  list_vehicleItem_turck.get(i).getId();
                    }
                }
            }
        }

        if (VehicleId!= null && !VehicleId.equalsIgnoreCase(""))
        {
            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleId, getActivity());
            SessionSave.saveUserSession(Comman.REGISTER_VEHICLE_ACTIVITY, "1",getActivity());
            SessionSave.saveUserSession(Comman.USER_SERVICE, checkBox_Selected, getActivity());

            Log.e(TAG, "GotoNextPage() AllSelectedVehicle:- " + AllSelectedVehicle);
            Log.e(TAG, "GotoNextPage() VehicleId:- " + VehicleId);
            Log.e(TAG, "GotoNextPage() checkBox_Selected:- " + checkBox_Selected);


            ((Registration_Activity) getActivity()).setTabAttachment();
        }
        else
        {
            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_select_car_type),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),getActivity()).snackieBar().show();
        }
    }

    private void FindWhichTypeIs_Visible(boolean isVisible)
    {
        list_vehicleItem.clear();
        list_vehicleItem_turck.clear();

        dialogClass = new DialogClass(getActivity(), 1);

        if (isVisible)
        {
            dialogClass.showDialog();
        }
        else
        {
            dialogClass.hideDialog();
        }

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_FOR_VEHICLE_MODEL_LIST;

        Log.e("url", "FindWhichTypeIs_Visible = " + url);
        Log.e("param", "FindWhichTypeIs_Visible = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "FindWhichTypeIs_Visible = " + responseCode);
                    Log.e("Response", "FindWhichTypeIs_Visible = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                list_vehicleItem.clear();
                                list_vehicleItem_turck.clear();

                                String Id ="", CategoryId = "", Name="", Sort="", BaseFare="", MinKm="", PerKmCharge="", CancellationFee="", NightCharge="",NightTimeFrom=""
                                        , NightTimeTo="", SpecialEventSurcharge="", SpecialEventTimeFrom="", SpecialEventTimeTo="", WaitingTimeCost="", MinuteFare="", BookingFee=""
                                        , Description="", Status="";
                                String Image ="", ModelSizeImage = "", Height="", Width="", Capacity="";
                                Log.e("status", "true");

                                String carNames="";

                                if (json.has("cars_and_taxi"))
                                {
                                    String cars_and_taxi = json.getString("cars_and_taxi");
                                    if (cars_and_taxi!=null && !cars_and_taxi.equalsIgnoreCase(""))
                                    {
                                        JSONArray arrayCarTaxi = json.getJSONArray("cars_and_taxi");
                                        for (int i= 0; i<arrayCarTaxi.length(); i++)
                                        {
                                            JSONObject objectCarTexi = arrayCarTaxi.getJSONObject(i);
                                            if (objectCarTexi.has("Id"))
                                            {
                                                Id = objectCarTexi.getString("Id");
                                            }

                                            if (objectCarTexi.has("Name"))
                                            {
                                                Name = objectCarTexi.getString("Name");
                                            }

                                            if (objectCarTexi.has("Description"))
                                            {
                                                Description = objectCarTexi.getString("Description");
                                            }

                                            String VehicleModelId = SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL, getActivity());
                                            if (VehicleModelId!=null && !VehicleModelId.equalsIgnoreCase(""))
                                            {
                                                if (VehicleModelId.contains(Id))
                                                {
                                                    list_vehicleItem.add(new VehicleType_Been(Id, Name, Description, "1"));

                                                    if (carNames !=null && !carNames.equalsIgnoreCase(""))
                                                    {
                                                        carNames = carNames + "," + Name;
                                                        selectedModelId = selectedModelId +"," + Id;
                                                    }
                                                    else
                                                    {
                                                        carNames = Name;
                                                        selectedModelId = Id;
                                                    }
                                                }
                                                else
                                                {
                                                    list_vehicleItem.add(new VehicleType_Been(Id, Name, Description, "0"));
                                                }
                                            }
                                            else
                                            {
                                                list_vehicleItem.add(new VehicleType_Been(Id, Name, Description, "0"));
                                            }

                                        }
                                        if (adapter != null)
                                        {
                                            adapter.notifyDataSetChanged();
                                        }
                                        Log.e(TAG, "FindWhichTypeIs_Visible() list_vehicleItem.size():- " + list_vehicleItem.size());
                                        Log.e(TAG, "FindWhichTypeIs_Visible() carIds:- " + selectedModelId);
                                    }
                                    else
                                    {
                                        Log.e("cars_and_taxi", "null");
                                    }
                                    tv_register_CarType.setText(carNames);

                                }

                                if (json.has("delivery_services"))
                                {
                                    String delivery_services = json.getString("delivery_services");

                                    if (delivery_services!=null && !delivery_services.equalsIgnoreCase(""))
                                    {
                                        JSONArray arrayDelevryServices = json.getJSONArray("delivery_services");

                                        for (int i= 0; i<arrayDelevryServices.length(); i++)
                                        {
                                            JSONObject objectdeleveryServices = arrayDelevryServices.getJSONObject(i);

                                            if (objectdeleveryServices.has("Id"))
                                            {
                                                Id = objectdeleveryServices.getString("Id");
                                            }

                                            if (objectdeleveryServices.has("Name"))
                                            {
                                                Name = objectdeleveryServices.getString("Name");
                                            }

                                            if (objectdeleveryServices.has("Description"))
                                            {
                                                Description = objectdeleveryServices.getString("Description");
                                            }

                                            if (objectdeleveryServices.has("Image"))
                                            {
                                                if (objectdeleveryServices.getString("Image") != null &&
                                                        !objectdeleveryServices.getString("Image").equalsIgnoreCase("") &&
                                                        !objectdeleveryServices.getString("Image").equalsIgnoreCase("null"))
                                                {
                                                    Image = WebServiceAPI.BASE_URL_IMAGE + objectdeleveryServices.getString("Image");
                                                }

                                            }

                                            if (objectdeleveryServices.has("ModelSizeImage"))
                                            {
                                                if (objectdeleveryServices.getString("ModelSizeImage") != null &&
                                                        !objectdeleveryServices.getString("ModelSizeImage").equalsIgnoreCase("") &&
                                                        !objectdeleveryServices.getString("ModelSizeImage").equalsIgnoreCase("null"))
                                                {
                                                    ModelSizeImage = WebServiceAPI.BASE_URL_IMAGE + objectdeleveryServices.getString("ModelSizeImage");
                                                }
                                            }

                                            if (objectdeleveryServices.has("Height"))
                                            {
                                                Height = objectdeleveryServices.getString("Height");
                                            }

                                            if (objectdeleveryServices.has("Width"))
                                            {
                                                Width = objectdeleveryServices.getString("Width");
                                            }

                                            if (objectdeleveryServices.has("Capacity"))
                                            {
                                                Capacity = objectdeleveryServices.getString("Capacity");
                                            }

                                            String VehicleModelId = SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL, getActivity());
                                            if (VehicleModelId!=null && !VehicleModelId.equalsIgnoreCase(""))
                                            {
                                                if (VehicleModelId.contains(Id))
                                                {
                                                    list_vehicleItem_turck.add(new TruckType_Been(Id, Name, Description, Image, ModelSizeImage, Height, Width, Capacity, "1"));

                                                    if (carNames !=null && !carNames.equalsIgnoreCase(""))
                                                    {
                                                        carNames = carNames + "," + Name;
                                                        selectedModelId = selectedModelId +"," + Id;
                                                    }
                                                    else
                                                    {
                                                        carNames = Name;
                                                        selectedModelId = Id;
                                                    }
                                                }
                                                else
                                                {
                                                    list_vehicleItem_turck.add(new TruckType_Been(Id, Name, Description, Image, ModelSizeImage, Height, Width, Capacity, "0"));
                                                }
                                            }
                                            else
                                            {
                                                list_vehicleItem_turck.add(new TruckType_Been(Id, Name, Description, Image, ModelSizeImage, Height, Width, Capacity, "0"));
                                            }
                                        }
                                        if (adapterTruck != null)
                                        {
                                            adapterTruck.notifyDataSetChanged();
                                        }

                                        Log.e(TAG, "FindWhichTypeIs_Visible() list_vehicleItem_turck.size():- " + list_vehicleItem_turck.size());
                                        Log.e(TAG, "FindWhichTypeIs_Visible() delivery_services:- " + selectedModelId);
                                    }
                                    else
                                    {
                                        Log.e("delivery_services", "null");
                                    }
                                    tv_register_CarType.setText(carNames);
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e("FindWhichTypeIs_Visible", "Exception : " + e.toString());
                    dialogClass.hideDialog();

                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.e(TAG,"OnResume()");

        if (bitmapSumsung != null)
        {
            Log.e(TAG, "Call OnResume  Camera Click True");
            if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
            {
                Log.e(TAG, "Call OnResume Company Name:- " + getDeviceName() );
                Uri imageUri1 = getImageUri(Registration_Activity.activity, bitmapSumsung);
                Picasso.get()
                        .load(imageUri1)
//                        .fit()
                        .into(iv_carImage);
                DriverCar_ByteImage = ConvertToByteArray(bitmapSumsung);
                encodedImage = Base64.encodeToString(DriverCar_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, encodedImage,getActivity());
            }
        }
//        else
//        {
////            iv_carImage.setImageBitmap(null);
////            DriverCar_ByteImage=null;
////            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, "",getActivity());
//        }
    }

}
