package com.connectkargo.driver.Others;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.widget.Button;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GPSTracker extends Service implements LocationListener {

	private String TAG ="GPSTracker";
	private Context mContext;
	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	boolean canGetLocation = false;

	public Location location; // location
	double latitude; // latitude
	double longitude; // longitude
	double precision, new_latitude, new_longitude;

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 2000; // 1 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;

	private float distance;
	protected Location mCurrentLocation;
	int ACCURACY_THRESHOLD = 500;
	private Location oldLocation;
	private Location newLocation;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
	Calendar calendar;
	public static String oldTime, latestTime, currentTime;

//	ArrayList<Double> listLati;
//	ArrayList<Double> listLong;
//	public static int intI = 0;

	public GPSTracker(Context mContext) {
		this.mContext = mContext;

		oldLocation = new Location("Point A");
		newLocation = new Location("Point B");

		/*listLati = new ArrayList<Double>();
		listLong = new ArrayList<Double>();
		listLati.clear();
		listLong.clear();
		listLati.add(23.072837);
		listLong.add(72.516455);

		listLati.add(23.072302);
		listLong.add(72.516258);

		listLati.add(23.072218);
		listLong.add(72.516438);

		listLati.add(23.071586);
		listLong.add(72.517854);

		listLati.add(23.070937);
		listLong.add(72.519848);

		listLati.add(23.069668);
		listLong.add(72.522538);

		listLati.add(23.069668);
		listLong.add(72.522538);

		listLati.add(23.069668);
		listLong.add(72.522538);

		listLati.add(23.069668);
		listLong.add(72.522538);

		listLati.add(23.069668);
		listLong.add(72.522538);


		listLati.add(23.066982);
		listLong.add(72.522452);

		listLati.add(23.066073);
		listLong.add(72.522366);

		listLati.add(23.063901);
		listLong.add(72.521636);*/

		getLocation();
	}

	public Location getLocation() {
		try
		{
			locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled)
			{
				// no network provider is enabled
				location = null;
			}
			else
			{
				this.canGetLocation = true;
				// First get location from Network Provider
				if (isNetworkEnabled)
				{
					if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
					{
						Constants.newgpsLatitude = "0";
						Constants.newgpsLongitude = "0";
						location = null;
						return null;
					}
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					if (locationManager != null)
					{
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null)
						{
							latitude = location.getLatitude();
							longitude = location.getLongitude();

							Constants.newgpsLatitude = location.getLatitude() + "";
							Constants.newgpsLongitude = location.getLongitude() + "";

							precision = Math.pow(10, 6);
							new_latitude = (double) ((int) (precision * latitude)) / precision;
							new_longitude = (double) ((int) (precision * longitude)) / precision;

							Log.e("latitude##########", "" + latitude);
							Log.e("longitude##########", "" + longitude);
						}
					}

				}

				if (isGPSEnabled)
				{
					if (location == null)
					{
						if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
						{
							Constants.newgpsLatitude = "0";
							Constants.newgpsLongitude = "0";
							location = null;
							return null;
						}
						locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						if (locationManager != null)
						{
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null)
							{
								latitude = location.getLatitude();
								longitude = location.getLongitude();

								precision =  Math.pow(10, 6);
								new_latitude = (double)((int)(precision * latitude))/precision;
								new_longitude = (double)((int)(precision * longitude))/precision;
							}
						}
					}
				}

				if (location!=null)
				{
					Constants.newgpsLatitude = location.getLatitude()+"";
					Constants.newgpsLongitude = location.getLongitude()+"";
				}
				else
				{
					Constants.newgpsLatitude = "0";
					Constants.newgpsLongitude = "0";
				}
			}

		}
		catch (Exception e)
		{
			Log.e("Exception"," = "+e.getMessage());
			e.printStackTrace();
		}
		return location;
	}

	@Override
	public void onLocationChanged(Location location)
	{
		this.canGetLocation = true;

		latitude = location.getLatitude();
		longitude = location.getLongitude();
		/*Log.e("intI","intI : "+intI);
		if (intI<listLati.size())
		{
			Log.e("intI","intI : 111111");
			latitude = listLati.get(intI);
			longitude = listLong.get(intI);
			location.setLatitude(latitude);
			location.setLongitude(longitude);
			this.location = location;
			intI++;
		}
		else
		{
			intI=0;
			latitude = listLati.get(intI);
			longitude = listLong.get(intI);
			location.setLatitude(latitude);
			location.setLongitude(longitude);
			this.location = location;
		}*/
		precision =  Math.pow(10, 6);
		new_latitude = (double)((int)(precision * latitude))/precision;
		new_longitude = (double)((int)(precision * longitude))/precision;
		Constants.newgpsLatitude = location.getLatitude()+"";
		Constants.newgpsLongitude = location.getLongitude()+"";
		Log.e("GPSTracker ","11111111111111111 : "+ Constants.newgpsLatitude+" "+ Constants.newgpsLongitude+" : Location");

		mCurrentLocation = location;
		/*calendar = Calendar.getInstance();

		currentTime = dateFormat.format(calendar.getTime());

		distance = getUpdatedDistance();
		Log.e("GPSTracker ","11111111111111111 : "+ distance +" : Main Distance");
		SessionSave.saveUserSession(Comman.SUM_FOR_LAT_LONG_METER, distance+"", mContext);

		double speedKmPerHour = 0.0000;
		if (Drawer_Activity.tv_speedTrip!=null)
		{
			speedKmPerHour = location.getSpeed() * 18 / 5;
			Log.e("intI","intI : 111111 speedKmPerHour: "+speedKmPerHour);

			if (Double.isNaN(speedKmPerHour) || Double.isInfinite(speedKmPerHour))
			{
				speedKmPerHour = 0.0000;
				Drawer_Activity.tv_speedTrip.setText(speedKmPerHour+" km/hr");
			}
			else
			{
				String speed = String.format( "%.2f", speedKmPerHour);
				Drawer_Activity.tv_speedTrip.setText(speed+" km/hr");
			}
		}
		else
		{
			Log.e("intI","intI : 111111 Drawer_Activity.tv_speedTrip null");
		}

		Map_Fragment map_fragment = new Map_Fragment();
		map_fragment.GetLocationGps(location, speedKmPerHour);*/
	}

	private float getUpdatedDistance()
	{
		if (SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, mContext)!=null && !SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, mContext).equalsIgnoreCase(""))
		{
			distance = Float.parseFloat(SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, mContext));
		}
		else
		{
			distance = 0;
			SessionSave.saveUserSession(Comman.SUM_FOR_LAT_LONG_METER, "", mContext);
		}

		Log.e("GPSTracker ","11111111111111111 : "+ distance +" : distance");

		if (mCurrentLocation.getAccuracy() > ACCURACY_THRESHOLD) {

			return distance;
		}


		if (oldLocation.getLatitude() == 0 && oldLocation.getLongitude() == 0) {

			oldLocation.setLatitude(mCurrentLocation.getLatitude());
			oldLocation.setLongitude(mCurrentLocation.getLongitude());

			newLocation.setLatitude(mCurrentLocation.getLatitude());
			newLocation.setLongitude(mCurrentLocation.getLongitude());

			oldTime = currentTime;
			latestTime = currentTime;

			return distance;
		} else {
			oldLocation.setLatitude(newLocation.getLatitude());
			oldLocation.setLongitude(newLocation.getLongitude());

			newLocation.setLatitude(mCurrentLocation.getLatitude());
			newLocation.setLongitude(mCurrentLocation.getLongitude());

			oldTime = latestTime;
			latestTime = currentTime;
		}

//		Float getNewDistance = Float.parseFloat("0");
		String meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, mContext);
		if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
		{
			if (meterActiveFlag.equalsIgnoreCase("1") || meterActiveFlag.equalsIgnoreCase("3"))
			{
				Float getNewDistance = newLocation.distanceTo(oldLocation)/1000;
				Log.e("GPSTracker ","11111111111111111 : "+ getNewDistance +" : getNewDistance");

				if (SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, mContext)!=null && SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, mContext).equals("1")) {
				} else {
					distance = distance + getNewDistance;
				}

				String abc = String.format( "%.2f", distance);
				if (Drawer_Activity.tv_meter_digit!=null)
				{
					Drawer_Activity.tv_meter_digit.setText(abc+"");
				}
				Log.e("GPSTracker ","11111111111111111 : "+ abc +" : abc");
			}
			else  if (meterActiveFlag.equalsIgnoreCase("2") || meterActiveFlag.equalsIgnoreCase("4"))
			{
				String abc = String.format( "%.2f", distance);
				if (Drawer_Activity.tv_meter_digit!=null)
				{
					Drawer_Activity.tv_meter_digit.setText(abc+"");
				}
				Log.e("GPSTracker ","11111111111111111 : Pause trip");
			}
			else
			{
				Log.e("GPSTracker ","11111111111111111 : No trip");
				if (Drawer_Activity.tv_meter_digit!=null)
				{
					Drawer_Activity.tv_meter_digit.setText("");
				}
			}
		}

		/*try
		{
			Date startDate = dateFormat.parse(oldTime);
			Date endDate = dateFormat.parse(latestTime);

			double different = endDate.getTime() - startDate.getTime();
			Log.e("GPSTracker ","11111111111111111 : "+ different +" : different");
			double seconds = different / 1000;
			double minutes = seconds / 60;
			double hours = minutes / 60;

			Log.e("GPSTracker ","11111111111111111 : "+ hours +" : hours");

			double speedKmPerHour = getNewDistance / hours;
			Log.e("GPSTracker ","11111111111111111 : "+ speedKmPerHour +" : speedKmPerHour");

			if (Drawer_Activity.tv_speedTrip!=null)
			{
				if (Double.isNaN(speedKmPerHour) || Double.isInfinite(speedKmPerHour))
				{
					speedKmPerHour = 0.0000;
					Drawer_Activity.tv_speedTrip.setText(speedKmPerHour+" km/hr");
				}
				else
				{
					String speed = String.format( "%.2f", speedKmPerHour);
					Drawer_Activity.tv_speedTrip.setText(speed+" km/hr");
				}
			}

			Map_Fragment map_fragment = new Map_Fragment();
			map_fragment.GetLocationGps(location, speedKmPerHour);

		} catch (ParseException e) {
			e.printStackTrace();
		}*/
		return distance;
	}

	public void showSettingsAlert(final Context mContext)
	{
		Log.e(TAG,"showSettingsAlert()");

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

		alertDialog.setTitle(mContext.getResources().getString(R.string.new_gps_settings_title));
		alertDialog.setMessage(mContext.getResources().getString(R.string.new_gps_settings_message));

		alertDialog.setPositiveButton(mContext.getResources().getString(R.string.setting),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});

		alertDialog.setNegativeButton(mContext.getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		AlertDialog alertDialog1 = alertDialog.create();
		Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
		Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

		if (Pbutton != null)
		{
			Pbutton.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
		}
		else
		{
			Log.e(TAG,"showSettingsAlert():- " + "Pbutton == null");
		}

		if (Nbutton != null)
		{
			Nbutton.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
		}
		else
		{
			Log.e(TAG,"showSettingsAlert():- " + "Nbutton == null");
		}
		alertDialog.show();
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.e("call","onProviderDisabled()");
	}

	@Override
	public void onProviderEnabled(String provider)
	{
		Log.e("call","onProviderEnabled()");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		Log.e("call","onStatusChanged() status = "+status);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();

			precision =  Math.pow(10, 6);
			new_latitude = (double)((int)(precision * latitude))/precision;

			//dFormat = new DecimalFormat("#.######"); 
			//latitude= Double.valueOf(dFormat .format(latitude));
		}

		// return latitude
		return new_latitude;
	}

	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();

			precision =  Math.pow(10, 6);
			new_longitude = (double)((int)(precision * longitude))/precision;

			//dFormat = new DecimalFormat("#.######"); 
			//longitude= Double.valueOf(dFormat .format(longitude));
		}

		// return longitude
		return new_longitude;
	}

	public boolean canGetLocation() {
		return this.canGetLocation;
	}
}