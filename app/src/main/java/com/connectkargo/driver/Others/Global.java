package com.connectkargo.driver.Others;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.text.DecimalFormat;

public class Global
{
	public static Context context;

	public static boolean isNetworkconn(Context ctx)
	{
		context = ctx;

		ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}

	public static String commaFormat(String x) {
		if(x != null && !x.equalsIgnoreCase("") && x.length() > 3){
			String c =  new DecimalFormat("#,###,###.00").format(Double.parseDouble(x));
			/*Log.e("TAG", "commaFormat() : "+c);
			c =  c.replace(",","/");
			Log.e("TAG", "commaFormat() : "+c);
			c =  c.replace(".",",");

			Log.e("TAG", "commaFormat() : "+c);*/

			return c/*.replace("/",".")*/;
//            return c;
		}else {
			return x;
		}
	}


}
