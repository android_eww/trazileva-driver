package com.connectkargo.driver.listner;

public interface OnLoadMoreListener {
    void onLoadMore();
}