package com.connectkargo.driver.singleton;

import android.content.Context;

import com.connectkargo.driver.listner.BidStatus;

public class AcceptRejectBid {

    public static AcceptRejectBid instance;
    public static BidStatus bidStatus;
    Context context;

    public AcceptRejectBid(Context context, BidStatus bidStatus)
    {
        this.context = context;
        this.bidStatus = bidStatus;
    }

    public static AcceptRejectBid setCallBack(Context context,BidStatus bidStatus)
    {
        new AcceptRejectBid(context,bidStatus);
        return instance;
    }

    public static BidStatus getCallBack()
    {
        return bidStatus;
    }
}
