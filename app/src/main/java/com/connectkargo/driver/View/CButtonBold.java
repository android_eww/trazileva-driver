package com.connectkargo.driver.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

public class CButtonBold extends AppCompatButton
{
    public CButtonBold(Context context)
    {
        super(context);
    }

    public CButtonBold(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CButtonBold(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void setText()
    {
        Typeface typeface= Typeface.createFromAsset(getContext().getAssets(), "roboto.bold.ttf");
        this.setTypeface(typeface);
    }
}
