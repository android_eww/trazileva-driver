package com.connectkargo.driver.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CTextViewMedium extends AppCompatTextView
{
    public CTextViewMedium(Context context)
    {
        super(context);
    }

    public CTextViewMedium(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CTextViewMedium(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void setText()
    {
        Typeface typeface= Typeface.createFromAsset(getContext().getAssets(), "roboto.medium.ttf");
        this.setTypeface(typeface);
    }
}
