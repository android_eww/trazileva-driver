package com.connectkargo.driver.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

public class CAutocompleteTextViewRegular extends AppCompatAutoCompleteTextView
{
    public CAutocompleteTextViewRegular(Context context)
    {
        super(context);
    }

    public CAutocompleteTextViewRegular(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CAutocompleteTextViewRegular(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void setText()
    {
        Typeface typeface= Typeface.createFromAsset(getContext().getAssets(), "roboto.regular.ttf");
        this.setTypeface(typeface);
    }
}
