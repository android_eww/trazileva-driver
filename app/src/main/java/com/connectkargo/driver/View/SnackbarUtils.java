package com.connectkargo.driver.View;

import android.content.Context;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.connectkargo.driver.R;
import com.google.android.material.snackbar.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class SnackbarUtils {
    private int BACKGROUND_COLOR;
    private int TEXT_COLOR;
    private int BUTTON_COLOR;
    private String TEXT;
    Context context;
    View viewById;


    public SnackbarUtils(View viewById, String aText, int aBgColor, int aTextColor, int aButtonColor,
                         Context context)
    {
        this.TEXT = aText;
        this.BACKGROUND_COLOR = aBgColor;
        this.TEXT_COLOR = aTextColor;
        this.BUTTON_COLOR = aButtonColor;
        this.viewById = viewById;
        this.context = context;
    }


    public Snackbar snackieBar()
    {
        Snackbar snackie = Snackbar.make(viewById, TEXT, Snackbar.LENGTH_LONG)
                .setAction(context.getResources().getString(R.string.dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });

        snackie.setActionTextColor(TEXT_COLOR);
        snackie.getView().setBackgroundColor(BACKGROUND_COLOR);
        View snackbarView = snackie.getView();

        final ViewGroup.LayoutParams params = snackie.getView().getLayoutParams();
        if (params instanceof CoordinatorLayout.LayoutParams)
        {
            ((CoordinatorLayout.LayoutParams) params).gravity = Gravity.BOTTOM;
        }
        else
        {
            ((FrameLayout.LayoutParams) params).gravity = Gravity.BOTTOM;
        }
        snackbarView.setLayoutParams(params);

        TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(TEXT_COLOR);
        textView.setMaxLines(5);
        snackie.show();

        return snackie;
    }
}