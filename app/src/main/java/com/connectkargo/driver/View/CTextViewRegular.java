package com.connectkargo.driver.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CTextViewRegular extends AppCompatTextView
{
    public CTextViewRegular(Context context)
    {
        super(context);
    }

    public CTextViewRegular(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CTextViewRegular(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void setText()
    {
        Typeface typeface= Typeface.createFromAsset(getContext().getAssets(), "roboto.regular.ttf");
        this.setTypeface(typeface);
    }
}
