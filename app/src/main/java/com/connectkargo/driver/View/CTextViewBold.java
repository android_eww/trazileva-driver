package com.connectkargo.driver.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CTextViewBold extends AppCompatTextView
{
    public CTextViewBold(Context context)
    {
        super(context);
        setText();
    }

    public CTextViewBold(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setText();
    }

    public CTextViewBold(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        setText();
    }

    public void setText()
    {
        Typeface typeface= Typeface.createFromAsset(getContext().getAssets(), "roboto.bold.ttf");
        this.setTypeface(typeface);
    }
}
