package com.connectkargo.driver.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.connectkargo.driver.R;

public class CEditText extends AppCompatEditText
{
    public CEditText(Context context)
    {
        super(context);
    }

    public CEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CEditText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void setText()
    {
        Typeface typeface= Typeface.createFromAsset(getContext().getAssets(), "roboto.regular.ttf");
        this.setTypeface(typeface);
    }
}
