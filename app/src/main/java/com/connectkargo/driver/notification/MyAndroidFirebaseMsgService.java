package com.connectkargo.driver.notification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import android.os.Build;
import android.util.Log;

import com.connectkargo.driver.Activity.ChatDetailActivity;
import com.connectkargo.driver.Activity.Chat_Activity;
import com.connectkargo.driver.Activity.Splash_Activity;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;
import java.util.Random;

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {
    private SharedPreferences pref;
    String body="", title="", type="", brief ="";
    int m;
    Random random;
    String obj="";//chat
    public String BookingId = "", ReceiverId = "", BookingType = "", UserName = "", UserImage = "",ReceiverName="";//chat
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        body="";
        title="";
        type="";
        brief ="";

        Log.e("call", "onMessageReceived");
        //Log data to Log Cat
        Log.e("MyAndroidFCMService", "remoteMessage");
        Log.w("MyAndroidFCMService", "DATA: " + remoteMessage.getData());
        Log.w("MyAndroidFCMService", "TO: " + remoteMessage.getTo());

        Object obj_title = remoteMessage.getData().get("title");
        Object obj_body = remoteMessage.getData().get("body");
        Object obj_type = remoteMessage.getData().get("type");
        Object obj_brief = remoteMessage.getData().get("brief");


        if (obj_title != null) {
            title = String.valueOf(obj_title.toString());
            Log.e("MyAndroidFCMService", "title" + title);
        }

        if (obj_body != null) {
            body = String.valueOf(obj_body.toString());
            Log.e("MyAndroidFCMService", "body" + body);
        }

        if (obj_type != null) {
            type = String.valueOf(obj_type.toString());
            Log.e("MyAndroidFCMService", "type" + type);
        }

        if (obj_brief != null) {
            brief = String.valueOf(obj_brief.toString());
            Log.e("MyAndroidFCMService", "brief" + brief);
        }
        if(type.equalsIgnoreCase("Chat"))
        {obj = remoteMessage.getData().get("result_data");}

        if(type.equalsIgnoreCase("Chat") && CabRideDriverApplication.currentActivity() != null &&
                CabRideDriverApplication.currentActivity() instanceof Chat_Activity) {

            obj = remoteMessage.getData().get("result_data");

            Intent intent = new Intent("com.CHAT");
            intent.putExtra("CHAT",obj);
            sendBroadcast(intent);
            return;
        }

        random = new Random();
        m = random.nextInt(9999 - 1000) + 1000;

        if (type!=null &&
                !type.equalsIgnoreCase("") &&
                !type.equalsIgnoreCase("Logout"))
        {
            if(type.equalsIgnoreCase("chatbid")) {

                if(ChatDetailActivity.activity != null) {
                    ChatDetailActivity.activity.addChatList(body);
                } else {
                    createNotification(title, body, type, m);
                }

            } else {
                createNotification(title, body, type, m);
            }
        }
        else
        {
            if (Drawer_Activity.activity!=null)
            {
                Drawer_Activity.activity.SessionExpiredDialog(title, body);
            }
            else
            {
                SessionSave.clearUserSession(getApplicationContext());
            }
        }
    }

    private void createNotification(String title, String messageBody, String type, int m)
    {
        Intent contentIntent;
        if(type.equalsIgnoreCase("Chat"))
        {
            getData();
            contentIntent =  new Intent(getApplicationContext(), Chat_Activity.class);
            contentIntent.putExtra(Constants.INTENT_BOOKING_ID, BookingId);
            contentIntent.putExtra(Constants.INTENT_RECEIVER_ID, ReceiverId);
            contentIntent.putExtra(Constants.INTENT_BOOKING_TYPE, BookingType);
            contentIntent.putExtra(Constants.INTENT_RECEIVER_NAME, ReceiverName);
            /*if (BookingType.equalsIgnoreCase("BookNow"))
            { contentIntent.putExtra(Constants.INTENT_BOOKING_TYPE, "book_now"); }
            else
            { contentIntent.putExtra(Constants.INTENT_BOOKING_TYPE, "book_later"); }*/
        }
        else
        {
            contentIntent =  new Intent(getApplicationContext(), Splash_Activity.class);
            contentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long[] v = {500,1000};
        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = "All Notifications";
            CharSequence channelName = getString(R.string.default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            Notification notification = new Notification.Builder(this, getApplicationContext().getResources()
                    .getString(R.string.channl_id))
                    .setSmallIcon(R.drawable.icon_notification_small)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorBlack))
                    .setSound(notificationSoundURI)
                    .setStyle(new Notification.BigTextStyle().bigText(brief))
                    .setContentIntent(pendingIntent)
                    .setGroupSummary(true)
                    .setGroup("KEY_NOTIFICATION_GROUP")
                    .setChannelId(channelId)
                    .build();

            notificationManager.notify(m, notification);
        }
        else
        {
            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this,
                    getApplicationContext().getResources().getString(R.string.channl_id))
                    .setSmallIcon(R.drawable.icon_notification_small)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorBlack))
                    .setSound(notificationSoundURI)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(brief))
                    .setContentIntent(pendingIntent)
                    .setGroupSummary(true)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setGroup("KEY_NOTIFICATION_GROUP");

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(m, mNotificationBuilder.build());
        }
    }

    private void getData()
    {
        try {
            JSONObject jsonObject = new JSONObject(obj);
            Log.e("TAG","jsonObject = "+jsonObject);
            if (jsonObject.has("BookingId"))
            {
                BookingId = jsonObject.getString("BookingId");
            }

            if (jsonObject.has("SenderId"))
            {
                ReceiverId = jsonObject.getString("SenderId");
            }
            if (jsonObject.has("ReceiverId"))
            {
                jsonObject.getString("ReceiverId");
            }

            if (jsonObject.has("BookingType"))
            {
                BookingType = jsonObject.getString("BookingType");
            }
            if (jsonObject.has("ReceiverName"))
            {
                ReceiverName = jsonObject.getString("ReceiverName");
            }
        }
        catch (Exception e){

        }

    }

    public static boolean isAppIsInBackground(Context context)
    {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}