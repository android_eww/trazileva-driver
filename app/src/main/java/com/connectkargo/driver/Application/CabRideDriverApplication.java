package com.connectkargo.driver.Application;

import android.app.Activity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.multidex.MultiDexApplication;

import androidx.core.content.ContextCompat;
import android.util.Log;

import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



public class CabRideDriverApplication extends MultiDexApplication {

    private static CabRideDriverApplication mInstance;
    private static Activity activity;


    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        mInstance = this;
    }

    public static void setCurrentActivity(Activity currentActivity)
    {
        activity = currentActivity;
        Log.e("PickNGo_Driver_App","setCurrentActivity : "+currentActivity.getLocalClassName().toString());

        FloatingActionButton fab = new FloatingActionButton(currentActivity);
        fab.setBackgroundDrawable(ContextCompat.getDrawable(currentActivity, R.drawable.icon_camera_yellow));
    }

    public static Activity currentActivity()
    {
        return activity;
    }

    public static boolean isEmailValid(CharSequence input) {
//        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
//        Matcher matcher = pattern.matcher(input);
//        return (!matcher.matches());
        return android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches();
    }

    public static synchronized CabRideDriverApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = dateFormat.format(new Date());
        Log.e("strDate","date = "+str);
        return dateFormat.format(new Date());
    }

}