package com.connectkargo.driver.Been;

public class ChatList_Been {

    private String id, booking_id, BookingType, sender_type, sender_id, receiver_type, receiver_id, message, created_at;

    public ChatList_Been(String id, String booking_id, String sender_type, String sender_id, String receiver_type,
                         String receiver_id, String message, String created_at, String bookingType)
    {
        this.id = id;
        this.booking_id = booking_id;
        this.sender_type = sender_type;
        this.sender_id = sender_id;
        this.receiver_type = receiver_type;
        this.receiver_id = receiver_id;
        this.message = message;
        this.created_at = created_at;
        BookingType = bookingType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getSender_type() {
        return sender_type;
    }

    public void setSender_type(String sender_type) {
        this.sender_type = sender_type;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_type() {
        return receiver_type;
    }

    public void setReceiver_type(String receiver_type) {
        this.receiver_type = receiver_type;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getBookingType() {
        return BookingType;
    }

    public void setBookingType(String bookingType) {
        BookingType = bookingType;
    }
}
