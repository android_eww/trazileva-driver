package com.connectkargo.driver.Been;


public class Pending_JobList_Been {

    private String Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus,PickupDateTime, DropTime,TripDuration,TripDistance,
            PickupLocation,DropoffLocation,NightFareApplicable,NightFare,TripFare,WaitingTime,WaitingTimeCost,TollFee,BookingCharge,Tax,
            PromoCode,Discount,SubTotal,GrandTotal,Status,OnTheWay,Reason,PaymentType,AdminAmount,CompanyAmount,PickupLat,PickupLng,DropOffLat,
            DropOffLon,Model,PassengerName,PassengerEmail,PassengerMobileNo,HistoryType, FlightNumber, Notes, dispatherEmail, dispatherFullname, dispatherMobileNo,
            ParcelName, RequestFor, Labour, Height, Length, Breadth, Weight, Quantity, BookingType, ReceiverName, ReceiverContactNo,
            ReceiverEmail,ParcelImage;

    public Pending_JobList_Been(String id, String passengerId, String modelId, String driverId, String createdDate, String transactionId,
                                String paymentStatus, String PickupDateTime, String dropTime, String tripDuration, String tripDistance,
                                String pickupLocation, String dropoffLocation, String nightFareApplicable, String nightFare, String tripFare,
                                String waitingTime, String waitingTimeCost, String tollFee, String bookingCharge, String tax, String promoCode,
                                String discount, String subTotal, String grandTotal, String status, String onTheWay, String reason, String paymentType,
                                String adminAmount, String companyAmount, String pickupLat, String pickupLng, String dropOffLat, String dropOffLon,
                                String model, String passengerName, String passengerEmail, String passengerMobileNo, String historyType, String FlightNumber, String Notes,
                                String dispatherEmail, String dispatherFullname, String dispatherMobileNo, String requestFor,
                                String parcelName, String labour, String height, String length, String breadth, String weigth,
                                String quantity, String bookingType, String receiverName, String receiverContactNo, String receiverEmail,
                                String ParcelImage)
    {
        Id = id;
        PassengerId = passengerId;
        ModelId = modelId;
        DriverId = driverId;
        CreatedDate = createdDate;
        TransactionId = transactionId;
        PaymentStatus = paymentStatus;
        this.PickupDateTime = PickupDateTime;
        DropTime = dropTime;
        TripDuration = tripDuration;
        TripDistance = tripDistance;
        PickupLocation = pickupLocation;
        DropoffLocation = dropoffLocation;
        NightFareApplicable = nightFareApplicable;
        NightFare = nightFare;
        TripFare = tripFare;
        WaitingTime = waitingTime;
        WaitingTimeCost = waitingTimeCost;
        TollFee = tollFee;
        BookingCharge = bookingCharge;
        Tax = tax;
        PromoCode = promoCode;
        Discount = discount;
        SubTotal = subTotal;
        GrandTotal = grandTotal;
        Status = status;
        OnTheWay = onTheWay;
        Reason = reason;
        PaymentType = paymentType;
        AdminAmount = adminAmount;
        CompanyAmount = companyAmount;
        PickupLat = pickupLat;
        PickupLng = pickupLng;
        DropOffLat = dropOffLat;
        DropOffLon = dropOffLon;
        Model = model;
        PassengerName = passengerName;
        PassengerEmail = passengerEmail;
        PassengerMobileNo = passengerMobileNo;
        HistoryType = historyType;
        this.FlightNumber= FlightNumber;
        this.Notes = Notes;
        this.dispatherEmail=dispatherEmail;
        this.dispatherFullname=dispatherFullname;
        this.dispatherMobileNo=dispatherMobileNo;
        ParcelName = parcelName;
        RequestFor = requestFor;
        Labour = labour;
        Height = height;
        Length = length;
        Breadth = breadth;
        Weight = weigth;
        Quantity = quantity;
        BookingType = bookingType;
        ReceiverName = receiverName;
        ReceiverContactNo = receiverContactNo;
        ReceiverEmail = receiverEmail;
        this.ParcelImage = ParcelImage;
    }

    public String getRequestFor() {
        return RequestFor;
    }

    public void setRequestFor(String requestFor) {
        RequestFor = requestFor;
    }

    public String getLabour() {
        return Labour;
    }

    public void setLabour(String labour) {
        Labour = labour;
    }

    public String getParcelName() {
        return ParcelName;
    }

    public void setParcelName(String parcelName) {
        ParcelName = parcelName;
    }

    public String getOnTheWay() {
        return OnTheWay;
    }

    public void setOnTheWay(String onTheWay) {
        OnTheWay = onTheWay;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getDriverId() {
        return DriverId;
    }

    public void setDriverId(String driverId) {
        DriverId = driverId;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public void setPickupDateTime(String pickupDateTime) {
        PickupDateTime = pickupDateTime;
    }

    public String getPickupDateTime() {
        return PickupDateTime;
    }

    public String getDropTime() {
        return DropTime;
    }

    public void setDropTime(String dropTime) {
        DropTime = dropTime;
    }

    public String getTripDuration() {
        return TripDuration;
    }

    public void setTripDuration(String tripDuration) {
        TripDuration = tripDuration;
    }

    public String getTripDistance() {
        return TripDistance;
    }

    public void setTripDistance(String tripDistance) {
        TripDistance = tripDistance;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }

    public String getNightFareApplicable() {
        return NightFareApplicable;
    }

    public void setNightFareApplicable(String nightFareApplicable) {
        NightFareApplicable = nightFareApplicable;
    }

    public String getNightFare() {
        return NightFare;
    }

    public void setNightFare(String nightFare) {
        NightFare = nightFare;
    }

    public String getTripFare() {
        return TripFare;
    }

    public void setTripFare(String tripFare) {
        TripFare = tripFare;
    }

    public String getWaitingTime() {
        return WaitingTime;
    }

    public void setWaitingTime(String waitingTime) {
        WaitingTime = waitingTime;
    }

    public String getWaitingTimeCost() {
        return WaitingTimeCost;
    }

    public void setWaitingTimeCost(String waitingTimeCost) {
        WaitingTimeCost = waitingTimeCost;
    }

    public String getTollFee() {
        return TollFee;
    }

    public void setTollFee(String tollFee) {
        TollFee = tollFee;
    }

    public String getBookingCharge() {
        return BookingCharge;
    }

    public void setBookingCharge(String bookingCharge) {
        BookingCharge = bookingCharge;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getPromoCode() {
        return PromoCode;
    }

    public void setPromoCode(String promoCode) {
        PromoCode = promoCode;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getAdminAmount() {
        return AdminAmount;
    }

    public void setAdminAmount(String adminAmount) {
        AdminAmount = adminAmount;
    }

    public String getCompanyAmount() {
        return CompanyAmount;
    }

    public void setCompanyAmount(String companyAmount) {
        CompanyAmount = companyAmount;
    }

    public String getPickupLat() {
        return PickupLat;
    }

    public void setPickupLat(String pickupLat) {
        PickupLat = pickupLat;
    }

    public String getPickupLng() {
        return PickupLng;
    }

    public void setPickupLng(String pickupLng) {
        PickupLng = pickupLng;
    }

    public String getDropOffLat() {
        return DropOffLat;
    }

    public void setDropOffLat(String dropOffLat) {
        DropOffLat = dropOffLat;
    }

    public String getDropOffLon() {
        return DropOffLon;
    }

    public void setDropOffLon(String dropOffLon) {
        DropOffLon = dropOffLon;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }

    public String getPassengerEmail() {
        return PassengerEmail;
    }

    public void setPassengerEmail(String passengerEmail) {
        PassengerEmail = passengerEmail;
    }

    public String getPassengerMobileNo() {
        return PassengerMobileNo;
    }

    public void setPassengerMobileNo(String passengerMobileNo) {
        PassengerMobileNo = passengerMobileNo;
    }

    public String getHistoryType() {
        return HistoryType;
    }

    public void setHistoryType(String historyType) {
        HistoryType = historyType;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }
    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
    public String getNotes() {
        return Notes;
    }


    public void setDispatherEmail(String dispatherEmail) {
        this.dispatherEmail = dispatherEmail;
    }
    public String getDispatherEmail() {
        return dispatherEmail;
    }

    public void setDispatherFullname(String dispatherFullname) {
        this.dispatherFullname = dispatherFullname;
    }
    public String getDispatherFullname() {
        return dispatherFullname;
    }

    public void setDispatherMobileNo(String dispatherMobileNo) {
        this.dispatherMobileNo = dispatherMobileNo;
    }
    public String getDispatherMobileNo() {
        return dispatherMobileNo;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getBreadth() {
        return Breadth;
    }

    public void setBreadth(String breadth) {
        Breadth = breadth;
    }

    public String getWeigth() {
        return Weight;
    }

    public void setWeigth(String weigth) {
        Weight = weigth;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getBookingType() {
        return BookingType;
    }

    public void setBookingType(String bookingType) {
        BookingType = bookingType;
    }

    public String getReceiverName() {
        return ReceiverName;
    }

    public void setReceiverName(String receiverName) {
        ReceiverName = receiverName;
    }

    public String getReceiverContactNo() {
        return ReceiverContactNo;
    }

    public void setReceiverContactNo(String receiverContactNo) {
        ReceiverContactNo = receiverContactNo;
    }

    public String getReceiverEmail() {
        return ReceiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        ReceiverEmail = receiverEmail;
    }

    public String getParcelImage() {
        return ParcelImage;
    }

    public void setParcelImage(String parcelImage) {
        ParcelImage = parcelImage;
    }
}
