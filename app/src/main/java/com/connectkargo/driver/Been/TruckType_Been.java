package com.connectkargo.driver.Been;



public class TruckType_Been
{
    private String Id, Name, Description, Image, ModelSizeImage, Height, Width, Capacity, Status;

    public TruckType_Been(String id, String name, String description, String image,
                          String modelSizeImage, String height, String width, String capacity, String status)
    {
        Id = id;
        Name = name;
        Description = description;
        Image = image;
        ModelSizeImage = modelSizeImage;
        Height = height;
        Width = width;
        Capacity = capacity;
        Status = status;
    }

    public String getId()
    {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getModelSizeImage() {
        return ModelSizeImage;
    }

    public void setModelSizeImage(String modelSizeImage) {
        ModelSizeImage = modelSizeImage;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }
}
