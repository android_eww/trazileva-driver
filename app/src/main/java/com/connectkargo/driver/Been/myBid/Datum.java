package com.connectkargo.driver.Been.myBid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("PassengerId")
    @Expose
    private String passengerId;
    @SerializedName("DriverId")
    @Expose
    private String driverId;
    @SerializedName("BidId")
    @Expose
    private String bidId;
    @SerializedName("Budget")
    @Expose
    private String budget;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Datetime")
    @Expose
    private String datetime;
    @SerializedName("DriverBids")
    @Expose
    private String driverBids;
    @SerializedName("ModelId")
    @Expose
    private String modelId;
    @SerializedName("PickupLocation")
    @Expose
    private String pickupLocation;
    @SerializedName("DropoffLocation")
    @Expose
    private String dropoffLocation;
    @SerializedName("PickupLat")
    @Expose
    private String pickupLat;
    @SerializedName("PickupLng")
    @Expose
    private String pickupLng;
    @SerializedName("DropOffLat")
    @Expose
    private String dropOffLat;
    @SerializedName("DropOffLon")
    @Expose
    private String dropOffLon;
    @SerializedName("PickupDateTime")
    @Expose
    private String pickupDateTime;
    @SerializedName("DeadHead")
    @Expose
    private String deadHead;
    @SerializedName("Distance")
    @Expose
    private String distance;
    @SerializedName("ShipperName")
    @Expose
    private String shipperName;
    @SerializedName("Weight")
    @Expose
    private String weight;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("CardId")
    @Expose
    private String cardId;
    @SerializedName("Notes")
    @Expose
    private String notes;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("ModelImage")
    @Expose
    private String modelImage;
    @SerializedName("DriverBudget")
    @Expose
    private String driverBudget;
    @SerializedName("DriverNotes")
    @Expose
    private String driverNotes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getBidId() {
        return bidId;
    }

    public void setBidId(String bidId) {
        this.bidId = bidId;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDriverBids() {
        return driverBids;
    }

    public void setDriverBids(String driverBids) {
        this.driverBids = driverBids;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropoffLocation() {
        return dropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        this.dropoffLocation = dropoffLocation;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getPickupLng() {
        return pickupLng;
    }

    public void setPickupLng(String pickupLng) {
        this.pickupLng = pickupLng;
    }

    public String getDropOffLat() {
        return dropOffLat;
    }

    public void setDropOffLat(String dropOffLat) {
        this.dropOffLat = dropOffLat;
    }

    public String getDropOffLon() {
        return dropOffLon;
    }

    public void setDropOffLon(String dropOffLon) {
        this.dropOffLon = dropOffLon;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getDeadHead() {
        return deadHead;
    }

    public void setDeadHead(String deadHead) {
        this.deadHead = deadHead;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModelImage() {
        return modelImage;
    }

    public void setModelImage(String modelImage) {
        this.modelImage = modelImage;
    }

    public String getDriverBudget() {
        return driverBudget;
    }

    public void setDriverBudget(String driverBudget) {
        this.driverBudget = driverBudget;
    }

    public String getDriverNotes() {
        return driverNotes;
    }

    public void setDriverNotes(String driverNotes) {
        this.driverNotes = driverNotes;
    }
}

