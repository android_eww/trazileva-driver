package com.connectkargo.driver.Been.HeatmapData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trip {

@SerializedName("PickupLat")
@Expose
private String pickupLat;
@SerializedName("PickupLng")
@Expose
private String pickupLng;

public String getPickupLat() {
return pickupLat;
}

public void setPickupLat(String pickupLat) {
this.pickupLat = pickupLat;
}

public String getPickupLng() {
return pickupLng;
}

public void setPickupLng(String pickupLng) {
this.pickupLng = pickupLng;
}

}
