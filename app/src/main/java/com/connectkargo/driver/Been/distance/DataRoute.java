package com.connectkargo.driver.Been.distance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataRoute {

    @SerializedName("routes")
    @Expose
    private List<Route> routes;


    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
