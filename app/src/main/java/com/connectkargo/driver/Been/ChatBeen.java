package com.connectkargo.driver.Been;

public class ChatBeen {
    private String Message,Date,Sender,Receiver;

    public ChatBeen(String message, String date, String sender, String receiver)
    {
        Message = message;
        Date = date;
        Sender = sender;
        Receiver = receiver;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }

    public String getReceiver() {
        return Receiver;
    }

    public void setReceiver(String receiver) {
        Receiver = receiver;
    }
}
