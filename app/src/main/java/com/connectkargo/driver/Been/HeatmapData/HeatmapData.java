package com.connectkargo.driver.Been.HeatmapData;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HeatmapData {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("trips")
@Expose
private List<Trip> trips = null;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<Trip> getTrips() {
return trips;
}

public void setTrips(List<Trip> trips) {
this.trips = trips;
}

}

