package com.connectkargo.driver.Been.distance;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Leg {

@SerializedName("distance")
@Expose
private Distance distance;

public Distance getDistance() {
return distance;
}

public void setDistance(Distance distance) {
this.distance = distance;
}


}