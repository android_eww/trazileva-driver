package com.connectkargo.driver.Been;

public class Future_BookingList_Been {


    private String tripId;
    private String PickupDateTime;
    private String pickupLocation;
    private String dropoffLocation;
    private String companyId;
    private String passengerId;
    private String tripDistance;
    private String modelId;
    private String passengerName;
    private String passengerContact;
    private String model;
    private String FlightNumber;
    private String Notes;
    private String PaymentType;
    private String dispatherEmail;
    private String dispatherFullname;
    private String dispatherMobileNo;
    private String RequestFor;
    private String ParcelName;
    private String EstimateFare;
    private String Labour, Height, Length, Breadth, Weigth, Quantity, BookingType, ReceiverName, ReceiverContactNo, ReceiverEmail,ParcelImage;

    public Future_BookingList_Been(String tripId, String PickupDateTime, String pickupLocation, String dropoffLocation, String companyId,
                                   String passengerId, String tripDistance, String modelId, String passengerName, String passengerContact,
                                   String model, String FlightNumber, String Notes, String PaymentType, String dispatherEmail,
                                   String dispatherFullname, String dispatherMobileNo, String requestFor, String parcelName,
                                   String labour, String height, String length, String breadth, String weigth, String quantity,
                                   String bookingType, String receiverName, String receiverContactNo, String receiverEmail,
                                   String ParcelImage)
    {
        this.tripId= tripId;
        this.PickupDateTime= PickupDateTime;
        this.pickupLocation= pickupLocation;
        this.dropoffLocation= dropoffLocation;
        this.companyId= companyId;
        this.passengerId= passengerId;
        this.tripDistance= tripDistance;
        this.modelId= modelId;
        this.passengerName= passengerName;
        this.passengerContact= passengerContact;
        this.model= model;
        this.FlightNumber= FlightNumber;
        this.Notes= Notes;
        this.PaymentType=PaymentType;
        this.dispatherEmail=dispatherEmail;
        this.dispatherFullname=dispatherFullname;
        this.dispatherMobileNo=dispatherMobileNo;
        this.RequestFor = requestFor;
        this.ParcelName = parcelName;
        this.Labour = labour;
        Height = height;
        Length = length;
        Breadth = breadth;
        Weigth = weigth;
        Quantity = quantity;
        BookingType = bookingType;
        ReceiverName = receiverName;
        ReceiverContactNo = receiverContactNo;
        ReceiverEmail = receiverEmail;
        this.ParcelImage = ParcelImage;
    }

    public Future_BookingList_Been(String tripId, String PickupDateTime, String pickupLocation, String dropoffLocation, String companyId,
                                   String passengerId, String tripDistance, String modelId, String passengerName, String passengerContact,
                                   String model, String FlightNumber, String Notes, String PaymentType, String dispatherEmail,
                                   String dispatherFullname, String dispatherMobileNo, String requestFor, String parcelName,
                                   String labour, String height, String length, String breadth, String weigth, String quantity,
                                   String bookingType, String receiverName, String receiverContactNo, String receiverEmail,
                                   String ParcelImage, String EstimateFare)
    {
        this.tripId= tripId;
        this.PickupDateTime= PickupDateTime;
        this.pickupLocation= pickupLocation;
        this.dropoffLocation= dropoffLocation;
        this.companyId= companyId;
        this.passengerId= passengerId;
        this.tripDistance= tripDistance;
        this.modelId= modelId;
        this.passengerName= passengerName;
        this.passengerContact= passengerContact;
        this.model= model;
        this.FlightNumber= FlightNumber;
        this.Notes= Notes;
        this.PaymentType=PaymentType;
        this.dispatherEmail=dispatherEmail;
        this.dispatherFullname=dispatherFullname;
        this.dispatherMobileNo=dispatherMobileNo;
        this.RequestFor = requestFor;
        this.ParcelName = parcelName;
        this.Labour = labour;
        Height = height;
        Length = length;
        Breadth = breadth;
        Weigth = weigth;
        Quantity = quantity;
        BookingType = bookingType;
        ReceiverName = receiverName;
        ReceiverContactNo = receiverContactNo;
        ReceiverEmail = receiverEmail;
        this.ParcelImage = ParcelImage;
        this.EstimateFare = EstimateFare;
    }

    public String getRequestFor() {
        return RequestFor;
    }

    public void setRequestFor(String requestFor) {
        RequestFor = requestFor;
    }

    public String getParcelName() {
        return ParcelName;
    }

    public void setParcelName(String parcelName) {
        ParcelName = parcelName;
    }

    public String getLabour() {
        return Labour;
    }

    public void setLabour(String labour) {
        Labour = labour;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
    public String getTripId() {
        return tripId;
    }

    public void setPickupDateTime(String pickupDateTime) {
        PickupDateTime = pickupDateTime;
    }

    public String getPickupDateTime() {
        return PickupDateTime;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }
    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        this.dropoffLocation = dropoffLocation;
    }
    public String getDropoffLocation() {
        return dropoffLocation;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getCompanyId() {
        return companyId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }
    public String getPassengerId() {
        return passengerId;
    }

    public void setTripDistance(String tripDistance) {
        this.tripDistance = tripDistance;
    }
    public String getTripDistance() {
        return tripDistance;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
    public String getModelId() {
        return modelId;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }
    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerContact(String passengerContact) {
        this.passengerContact = passengerContact;
    }
    public String getPassengerContact() {
        return passengerContact;
    }

    public void setModel(String model) {
        this.model = model;
    }
    public String getModel() {
        return model;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }
    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
    public String getNotes() {
        return Notes;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }
    public String getPaymentType() {
        return PaymentType;
    }

    public void setDispatherEmail(String dispatherEmail) {
        this.dispatherEmail = dispatherEmail;
    }
    public String getDispatherEmail() {
        return dispatherEmail;
    }

    public void setDispatherFullname(String dispatherFullname) {
        this.dispatherFullname = dispatherFullname;
    }
    public String getDispatherFullname() {
        return dispatherFullname;
    }

    public void setDispatherMobileNo(String dispatherMobileNo) {
        this.dispatherMobileNo = dispatherMobileNo;
    }
    public String getDispatherMobileNo() {
        return dispatherMobileNo;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getBreadth() {
        return Breadth;
    }

    public void setBreadth(String breadth) {
        Breadth = breadth;
    }

    public String getWeigth() {
        return Weigth;
    }

    public void setWeigth(String weigth) {
        Weigth = weigth;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getBookingType() {
        return BookingType;
    }

    public void setBookingType(String bookingType) {
        BookingType = bookingType;
    }

    public String getReceiverName() {
        return ReceiverName;
    }

    public void setReceiverName(String receiverName) {
        ReceiverName = receiverName;
    }

    public String getReceiverContactNo() {
        return ReceiverContactNo;
    }

    public void setReceiverContactNo(String receiverContactNo) {
        ReceiverContactNo = receiverContactNo;
    }

    public String getReceiverEmail() {
        return ReceiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        ReceiverEmail = receiverEmail;
    }

    public String getParcelImage() {
        return ParcelImage;
    }

    public void setParcelImage(String parcelImage) {
        ParcelImage = parcelImage;
    }

    public String getEstimateFare() {
        return EstimateFare;
    }

    public void setEstimateFare(String estimateFare) {
        EstimateFare = estimateFare;
    }
}
