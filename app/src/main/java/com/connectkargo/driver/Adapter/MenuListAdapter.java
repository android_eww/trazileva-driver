package com.connectkargo.driver.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Activity.BidListActivity;
import com.connectkargo.driver.Activity.Change_password_Activity;
import com.connectkargo.driver.Activity.ChatDetailActivity;
import com.connectkargo.driver.Activity.Create_Passcode_Activity;
import com.connectkargo.driver.Activity.Invite_Friend_Activity;
import com.connectkargo.driver.Activity.Profile_Activity;
import com.connectkargo.driver.Activity.Setting_Activity;
import com.connectkargo.driver.Activity.ShareRide_Activity;
import com.connectkargo.driver.Activity.TripToDestination_Activity;
import com.connectkargo.driver.Activity.Wallet__Activity;
import com.connectkargo.driver.Activity.WeeklyEarninigsActivity;
import com.connectkargo.driver.Been.MenuList_Been;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Activity.Login_Activity;
import com.connectkargo.driver.Activity.Wallet_Cards_Activity;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuListAdapter extends BaseAdapter{

    public String TAG = "MenuListAdapter";
    private Context context;
    private List<MenuList_Been> list;
    private DrawerLayout drawerLayout;
    private LinearLayout ll_MainDrawerLayout;
    private Handler handler;
    private Runnable runnable;
    private DialogClass dialogClass;
    private AQuery aQuery;

    public MenuListAdapter(Context context, List<MenuList_Been> list, DrawerLayout drawerLayout, LinearLayout ll_MainDrawerLayout) {

        this.context = context;
        this.list = list;
        this.drawerLayout = drawerLayout;
        this.ll_MainDrawerLayout = ll_MainDrawerLayout;
        dialogClass = new DialogClass(context,0);
        aQuery = new AQuery(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public MenuList_Been getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_menu_item, null);
            holder = new ViewHolder();

            holder.tv_Title = (TextView) convertView.findViewById(R.id.row_menu_title) ;
            holder.iv_Icon = (ImageView) convertView.findViewById(R.id.row_menu_icon);
            holder.ll_Row = (LinearLayout) convertView.findViewById(R.id.row_menu_main_row);
            holder.row = (LinearLayout) convertView.findViewById(R.id.row);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

//        if (position==(list.size()-1))
//        {
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//            layoutParams.setMargins(0,40,0,0);
//            holder.row.setLayoutParams(layoutParams);
//        }
//        else
//        {
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//            layoutParams.setMargins(0,0,0,0);
//            holder.row.setLayoutParams(layoutParams);
//        }

        if (position == 7)
        {
            holder.row.setVisibility(View.GONE);
        }
        else
        {
            holder.row.setVisibility(View.VISIBLE);
        }

        holder.tv_Title.setText(list.get(position).getTitle());
        holder.iv_Icon.setImageResource(list.get(position).getIcon());

        holder.ll_Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawerLayout.closeDrawer(ll_MainDrawerLayout);

                handler = new Handler();
                runnable = new Runnable() {

                    @Override
                    public void run() {
                        try
                        {

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        finally
                        {
                            displayView(position);
                        }
                    }
                };
                handler.postDelayed(runnable, 300);
            }
        });

        return convertView;
    }

    static class ViewHolder{
        private ImageView iv_Icon;
        private TextView tv_Title;//,tv_DescriptionOne,tv_DescriptionTwo
        private LinearLayout ll_Row,row;
    }

    public void displayView(int position)
    {
        if (list.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.home)))
        {
            Drawer_Activity.activity.setTab_Home();
        }
        if (list.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.my_job)))
        {
            Drawer_Activity.activity.setTab_MyJob();
        }
        else if (list.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.profile)))
        {
            callProfile();
        }
        else if (list.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.change_password)))
        {
            call_ChangePassword();
        } else if(list.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.bid_list))) {
            callBidActivity();
        }
        else if (list.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.logout)))
        {
            ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
            errorDialogClass.showDialog(context.getResources().getString(R.string.are_you_sure_want_to_logout),
                    context.getResources().getString(R.string.confirm_message), new ErrorDialogClass.SetOnClick()
            {
                @Override
                public void setOnClickListner()
                {
                    call_Logout();
                }
            });
        }
    }

    private void callBidActivity() {
        Intent intent = new Intent(context, BidListActivity.class);
        context.startActivity(intent);
        ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void callProfile()
    {
        Intent intent = new Intent(context, Profile_Activity.class);
        context.startActivity(intent);
        ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void call_Logout()
    {
        String driverId = SessionSave.getUserSession(Comman.USER_ID, context);
        String token = SessionSave.getToken(Comman.DEVICE_TOKEN, context);
        if(token!=null && !token.equalsIgnoreCase(""))
        {
            if (driverId != null && !driverId.equalsIgnoreCase("")) {
                if (ConnectivityReceiver.isConnected())
                {
                    DriverLogOut(driverId, token);
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                    errorDialogClass.showDialog(context.getResources().getString(R.string.not_connected_to_internet), context.getString(R.string.internet_error_message));
                }
            }
        }
        else
        {
            ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
            errorDialogClass.showDialog(context.getString(R.string.please_restart_app), context.getString(R.string.error_message));
        }
    }

    private void DriverLogOut(String driverId, String token)
    {

        aQuery = new AQuery(context);
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_DRIVER_LOGOUT + driverId+"/"+token;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null) {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();

                                SessionSave.clearUserSession(context);
                                SessionSave.clearMeterData(context);
                                Intent intent = new Intent(context, Login_Activity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                                ((Activity)context).finish();

                            }
                            else
                                {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                    errorDialogClass.showDialog(json.getString("message"),  context.getString(R.string.error_message));
                                }
                            }
                        }
                        else
                            {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                errorDialogClass.showDialog(json.getString("message"),  context.getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();

                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                        errorDialogClass.showDialog(context.getResources().getString(R.string.something_is_wrong),  context.getString(R.string.error_message));
                    }
                } catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                    errorDialogClass.showDialog(context.getResources().getString(R.string.something_is_wrong),  context.getString(R.string.error_message));
                    dialogClass.hideDialog();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void call_PaymentOption()
    {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    Intent intent = new Intent(context, Wallet_Cards_Activity.class);
                    intent.putExtra("from","drawer");
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_Wallet()
    {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    Drawer_Activity.PasscodeBackPress=0;
                    Intent intent = new Intent(context, Wallet__Activity.class);
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);

                    if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,context) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,context).equalsIgnoreCase("")
                            && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,context).equalsIgnoreCase("1"))
                    {
                        Intent intentv = new Intent(context, Create_Passcode_Activity.class);
                        context.startActivity(intentv);
                    }
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }


    private void call_Earning()
    {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {

                    Intent intent = new Intent(context, WeeklyEarninigsActivity.class);
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        };
        handler.postDelayed(runnable, 300);


    }

    public void call_InviteFriends()
    {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {

                    Intent intent = new Intent(context, Invite_Friend_Activity.class);
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    private void call_ChangePassword()
    {
        Intent intent = new Intent(context,Change_password_Activity.class);
        context.startActivity(intent);
        ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void call_Setting()
    {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {

                    Intent intent = new Intent(context, Setting_Activity.class);
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }


    public void call_Destination_Address()
    {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {

                    Intent intent = new Intent(context, TripToDestination_Activity.class);
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }

    public void call_Share_Rides()
    {
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try
                {

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {

                    Intent intent = new Intent(context, ShareRide_Activity.class);
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        };
        handler.postDelayed(runnable, 300);
    }
}
