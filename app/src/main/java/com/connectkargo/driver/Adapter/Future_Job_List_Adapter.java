package com.connectkargo.driver.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Fragment.Future_JobBooking_List_Fragment;
import com.connectkargo.driver.Fragment.MyJob_Fragment;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class Future_Job_List_Adapter extends ExpandableRecyclerView.Adapter<Future_Job_List_Adapter.ViewHolder>
{
    Context context;
    int POSITION;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    DialogClass dialogClass;
    private Dialog dialog;

    private Animation animBounce;

    public Future_Job_List_Adapter(LinearLayoutManager lm)
    {
        super(lm);

    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        LinearLayout ll_row_item_pastJob, ll_BookingId, ll_dropOffLoc, ll_dateTime, ll_pickUpLoc, ll_TripDistance, ll_passengerNo, ll_carModel, ll_PaymentType, ll_passengerFlightNo, ll_passengerNote
                ,ll_dispatcherName, ll_dispatcherEmail, ll_dispatcherNo, ll_parcelName, ll_Labour, llSize, llWeight, llQuantity, llReceiverName,
                ll_passengerEmail, ll_estFare;
        TextView tv_PassengerName, tv_BookingId, tv_drop_off_location, tv_PickUp_dateTime, tv_pickupLocation, tv_TripDistance, tv_passengerNo, tv_carModel, tv_PaymentType, tv_passengerFlightNo, tv_passengerNote
                ,tv_dispatcherName, tv_dispatcherEmail, tv_dispatcherNo, tv_parcelName, tv_Labour, tvSize, tvWeight, tvQuantity, tvReceiverName,
                tv_passengerEmail, tvEstFare;
        ImageView iv_status_active;
        ExpandableItem expandableItem;
        ImageView ivParcel;
        LinearLayout llImg;

        public ViewHolder(View v)
        {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);

            ll_row_item_pastJob = (LinearLayout) expandableItem.findViewById(R.id.ll_row_item_pastJob);
            ll_BookingId = (LinearLayout) expandableItem.findViewById(R.id.ll_BookingId);
            ll_dropOffLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffLoc);
            ll_dateTime = (LinearLayout) expandableItem.findViewById(R.id.ll_dateTime);
            ll_pickUpLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_pickUpLoc);
            ll_TripDistance = (LinearLayout) expandableItem.findViewById(R.id.ll_TripDistance);
            ll_passengerNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNo);
            ll_carModel = (LinearLayout) expandableItem.findViewById(R.id.ll_carModel);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_passengerFlightNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerFlightNo);
            ll_passengerNote = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNote);
            ll_dispatcherName = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherName);
            ll_dispatcherEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherEmail);
            ll_dispatcherNo = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherNo);
            ll_parcelName = (LinearLayout) expandableItem.findViewById(R.id.ll_parcelName);
            ll_Labour = (LinearLayout) expandableItem.findViewById(R.id.ll_Labour);
            llSize = expandableItem.findViewById(R.id.llSize);
            llWeight = expandableItem.findViewById(R.id.llWeight);
            llQuantity = expandableItem.findViewById(R.id.llQuantity);
            llReceiverName = expandableItem.findViewById(R.id.llReceiverName);
            ll_passengerEmail = expandableItem.findViewById(R.id.ll_passengerEmail);
            ll_estFare = expandableItem.findViewById(R.id.llEstfare);

            tv_PassengerName = (TextView) expandableItem.findViewById(R.id.tv_PassengerName);
            tv_BookingId = (TextView) expandableItem.findViewById(R.id.tv_BookingId);
            tv_drop_off_location = (TextView) expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_PickUp_dateTime = (TextView) expandableItem.findViewById(R.id.tv_PickUp_dateTime);
            tv_pickupLocation = (TextView) expandableItem.findViewById(R.id.tv_pickupLocation);
            tv_TripDistance = (TextView) expandableItem.findViewById(R.id.tv_TripDistance);
            tv_passengerNo = (TextView) expandableItem.findViewById(R.id.tv_passengerNo);
            tv_carModel = (TextView) expandableItem.findViewById(R.id.tv_carModel);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_passengerFlightNo = (TextView) expandableItem.findViewById(R.id.tv_passengerFlightNo);
            tv_passengerNote = (TextView) expandableItem.findViewById(R.id.tv_passengerNote);
            tv_dispatcherName = (TextView) expandableItem.findViewById(R.id.tv_dispatcherName);
            tv_dispatcherEmail = (TextView) expandableItem.findViewById(R.id.tv_dispatcherEmail);
            tv_dispatcherNo = (TextView) expandableItem.findViewById(R.id.tv_dispatcherNo);
            tv_parcelName = (TextView) expandableItem.findViewById(R.id.tv_parcelName);
            tv_Labour = (TextView) expandableItem.findViewById(R.id.tv_Labour);
            tvSize = expandableItem.findViewById(R.id.tvSize);
            tvWeight = expandableItem.findViewById(R.id.tvWeight);
            tvQuantity = expandableItem.findViewById(R.id.tvQuantity);
            tvReceiverName = expandableItem.findViewById(R.id.tvReceiverName);
            tv_passengerEmail = expandableItem.findViewById(R.id.tv_passengerEmail);
            tvEstFare = expandableItem.findViewById(R.id.tvEstFare);

            iv_status_active = (ImageView) expandableItem.findViewById(R.id.iv_status_active);

            ivParcel = expandableItem.findViewById(R.id.ivParcel);
            llImg = expandableItem.findViewById(R.id.llImg);

        }
    }


    @Override
    public Future_Job_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_future_job, parent, false);

        Future_Job_List_Adapter.ViewHolder vh = new Future_Job_List_Adapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Future_Job_List_Adapter.ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);

        if (Future_JobBooking_List_Fragment.list.get(position).getPassengerName()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.tv_PassengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName.setText(Future_JobBooking_List_Fragment.list.get(position).getPassengerName());
        }
        else
        {
            holder.tv_PassengerName.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getRequestFor()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getRequestFor().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getRequestFor().equalsIgnoreCase("taxi"))
        {
            holder.ll_parcelName.setVisibility(View.GONE);
            holder.ll_Labour.setVisibility(View.GONE);

            if (Future_JobBooking_List_Fragment.list.get(position).getParcelName()!=null &&
                    !Future_JobBooking_List_Fragment.list.get(position).getParcelName().equalsIgnoreCase("") &&
                    !Future_JobBooking_List_Fragment.list.get(position).getParcelName().equalsIgnoreCase("null"))
            {
                holder.tv_parcelName.setText(Future_JobBooking_List_Fragment.list.get(position).getParcelName());
            }
            else
            {
                holder.ll_parcelName.setVisibility(View.GONE);
            }

            if (Future_JobBooking_List_Fragment.list.get(position).getLabour()!=null &&
                    !Future_JobBooking_List_Fragment.list.get(position).getLabour().equalsIgnoreCase("") &&
                    !Future_JobBooking_List_Fragment.list.get(position).getLabour().equalsIgnoreCase("null") &&
                    !Future_JobBooking_List_Fragment.list.get(position).getLabour().equalsIgnoreCase("0"))
            {
                holder.tv_Labour.setText(Future_JobBooking_List_Fragment.list.get(position).getLabour());
            }
            else
            {
                holder.ll_Labour.setVisibility(View.GONE);
            }
        }
        else
        {
            holder.ll_parcelName.setVisibility(View.GONE);
            holder.ll_Labour.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getTripId()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getTripId().equalsIgnoreCase("") &&
                Future_JobBooking_List_Fragment.list.get(position).getBookingType() != null &&
                !Future_JobBooking_List_Fragment.list.get(position).getBookingType().equalsIgnoreCase(""))
        {
            holder.ll_BookingId.setVisibility(View.VISIBLE);
            if(Future_JobBooking_List_Fragment.list.get(position).getBookingType().equalsIgnoreCase("Book Now")) {
                holder.tv_BookingId.setText(Future_JobBooking_List_Fragment.list.get(position).getTripId() +
                        " (" + context.getResources().getString(R.string.bookNow) + ")");
            } else {
                holder.tv_BookingId.setText(Future_JobBooking_List_Fragment.list.get(position).getTripId() +
                        " (" + context.getResources().getString(R.string.bookLater) + ")");
            }
        }
        else
        {
            holder.ll_BookingId.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
            holder.tv_drop_off_location.setText(Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPickupDateTime()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase(""))
        {
            holder.ll_dateTime.setVisibility(View.VISIBLE);
            holder.tv_PickUp_dateTime.setText(Future_JobBooking_List_Fragment.list.get(position).getPickupDateTime());
        }
        else
        {
            holder.ll_dateTime.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPickupLocation()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.ll_pickUpLoc.setVisibility(View.VISIBLE);
            holder.tv_pickupLocation.setText(Future_JobBooking_List_Fragment.list.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_pickUpLoc.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getTripDistance()!=null && !Future_JobBooking_List_Fragment.list.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.ll_TripDistance.setVisibility(View.VISIBLE);
            holder.tv_TripDistance.setText(Future_JobBooking_List_Fragment.list.get(position).getTripDistance() + " km");
        }
        else
        {
            holder.ll_TripDistance.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getReceiverName()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getReceiverName().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getReceiverName().equalsIgnoreCase("null"))
        {
            holder.llReceiverName.setVisibility(View.VISIBLE);
            holder.tvReceiverName.setText(Future_JobBooking_List_Fragment.list.get(position).getReceiverName());
        }
        else
        {
            holder.llReceiverName.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getReceiverEmail()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getReceiverEmail().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getReceiverEmail().equalsIgnoreCase("null"))
        {
            holder.ll_passengerEmail.setVisibility(View.VISIBLE);
            holder.tv_passengerEmail.setText(Future_JobBooking_List_Fragment.list.get(position).getReceiverEmail());
        }
        else
        {
            holder.ll_passengerEmail.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getReceiverContactNo()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getReceiverContactNo().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getReceiverContactNo().equalsIgnoreCase("null"))
        {
            holder.ll_passengerNo.setVisibility(View.VISIBLE);
            holder.tv_passengerNo.setText(Future_JobBooking_List_Fragment.list.get(position).getReceiverContactNo());
        }
        else
        {
            holder.ll_passengerNo.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getModel()!=null && !Future_JobBooking_List_Fragment.list.get(position).getModel().equalsIgnoreCase(""))
        {
            holder.ll_carModel.setVisibility(View.VISIBLE);
            holder.tv_carModel.setText(Future_JobBooking_List_Fragment.list.get(position).getModel());
        }
        else
        {
            holder.ll_carModel.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getHeight()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getHeight().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getHeight().equalsIgnoreCase("null") &&
                !Future_JobBooking_List_Fragment.list.get(position).getHeight().equalsIgnoreCase("0") &&
                Future_JobBooking_List_Fragment.list.get(position).getLength()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getLength().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getLength().equalsIgnoreCase("null") &&
                !Future_JobBooking_List_Fragment.list.get(position).getLength().equalsIgnoreCase("0") &&
                Future_JobBooking_List_Fragment.list.get(position).getBreadth()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getBreadth().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getBreadth().equalsIgnoreCase("null") &&
                !Future_JobBooking_List_Fragment.list.get(position).getBreadth().equalsIgnoreCase("0"))
        {
            holder.llSize.setVisibility(View.VISIBLE);
            String size = "(" + Future_JobBooking_List_Fragment.list.get(position).getHeight() + " * " +
                    Future_JobBooking_List_Fragment.list.get(position).getLength() + " * " +
                    Future_JobBooking_List_Fragment.list.get(position).getBreadth() + ")";
            holder.tvSize.setText(size);
        }
        else
        {
            holder.llSize.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getWeigth()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getWeigth().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getWeigth().equalsIgnoreCase("null") &&
                !Future_JobBooking_List_Fragment.list.get(position).getWeigth().equalsIgnoreCase("0"))
        {
            holder.llWeight.setVisibility(View.VISIBLE);
            holder.tvWeight.setText(Future_JobBooking_List_Fragment.list.get(position).getWeigth());
        }
        else
        {
            holder.llWeight.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getQuantity()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getQuantity().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getQuantity().equalsIgnoreCase("null") &&
                !Future_JobBooking_List_Fragment.list.get(position).getQuantity().equalsIgnoreCase("0"))
        {
            holder.llQuantity.setVisibility(View.VISIBLE);
            holder.tvQuantity.setText(Future_JobBooking_List_Fragment.list.get(position).getQuantity());
        }
        else
        {
            holder.llQuantity.setVisibility(View.GONE);
        }

        if(Future_JobBooking_List_Fragment.list.get(position).getEstimateFare() != null &&
                !Future_JobBooking_List_Fragment.list.get(position).getEstimateFare().equalsIgnoreCase("") &&
                !Future_JobBooking_List_Fragment.list.get(position).getEstimateFare().equalsIgnoreCase("null") &&
                !Future_JobBooking_List_Fragment.list.get(position).getEstimateFare().equalsIgnoreCase("0")) {

            holder.ll_estFare.setVisibility(View.VISIBLE);
            holder.tvEstFare.setText("Kzs "+commaFormat(Future_JobBooking_List_Fragment.list.get(position).getEstimateFare()));

        } else {
            holder.ll_estFare.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPaymentType()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(Future_JobBooking_List_Fragment.list.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getFlightNumber()!=null && !Future_JobBooking_List_Fragment.list.get(position).getFlightNumber().equalsIgnoreCase(""))
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
            holder.tv_passengerFlightNo.setText(Future_JobBooking_List_Fragment.list.get(position).getFlightNumber());
        }
        else
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getNotes()!=null && !Future_JobBooking_List_Fragment.list.get(position).getNotes().equalsIgnoreCase(""))
        {
            holder.ll_passengerNote.setVisibility(View.VISIBLE);
            holder.tv_passengerNote.setText(Future_JobBooking_List_Fragment.list.get(position).getNotes());
        }
        else
        {
            holder.ll_passengerNote.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getDispatherFullname()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDispatherFullname().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherName.setVisibility(View.GONE);
            holder.tv_dispatcherName.setText(Future_JobBooking_List_Fragment.list.get(position).getDispatherFullname());
        }
        else
        {
            holder.ll_dispatcherName.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getDispatherEmail()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDispatherEmail().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherEmail.setVisibility(View.GONE);
            holder.tv_dispatcherEmail.setText(Future_JobBooking_List_Fragment.list.get(position).getDispatherEmail());
        }
        else
        {
            holder.ll_dispatcherEmail.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getDispatherMobileNo()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDispatherMobileNo().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherNo.setVisibility(View.GONE);
            holder.tv_dispatcherNo.setText(Future_JobBooking_List_Fragment.list.get(position).getDispatherMobileNo());
        }
        else
        {
            holder.ll_dispatcherNo.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getParcelImage()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getParcelImage().equalsIgnoreCase("")) {
            Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE + Future_JobBooking_List_Fragment.list.get(position).getParcelImage()).into(holder.ivParcel);
        } else {
            holder.llImg.setVisibility(View.GONE);
        }

        holder.iv_status_active.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                animBounce = AnimationUtils.loadAnimation(context, R.anim.bounce);
                holder.iv_status_active.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        String userId = SessionSave.getUserSession(Comman.USER_ID, context);
                        if (userId!=null && !userId.equalsIgnoreCase(""))
                        {
                            AcceptDispatchJob_Request(userId, Future_JobBooking_List_Fragment.list.get(position).getTripId(), holder.iv_status_active, position);
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return Future_JobBooking_List_Fragment.list.size();
    }


    private void AcceptDispatchJob_Request(String userId, String tripId, final ImageView iv_status_active, final int position)
    {
        aQuery = new AQuery(context);
        dialogClass = new DialogClass(context, 1);
        dialogClass.showDialog();
        iv_status_active.setVisibility(View.GONE);

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_ACCEPT_FUTURE_JOB_REQUEST + userId+"/"+tripId;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                Future_JobBooking_List_Fragment.list.remove(position);
                                notifyDataSetChanged();
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    showPopup(json.getString("message"),  context.getString(R.string.success_message));
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                iv_status_active.setVisibility(View.VISIBLE);
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                    errorDialogClass.showDialog(json.getString("message"), context.getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            iv_status_active.setVisibility(View.VISIBLE);
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        iv_status_active.setVisibility(View.VISIBLE);
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    iv_status_active.setVisibility(View.VISIBLE);
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void showPopup(String message,String title)
    {
        dialog = new Dialog(context,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_information);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        TextView tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        tv_Title.setText(title);

        ll_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyJob_Fragment.call_PendingList();
                    }
                }, 400);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyJob_Fragment.call_PendingList();
                    }
                }, 400);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyJob_Fragment.call_PendingList();
                    }
                }, 400);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        /*dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (Drawer_Activity.ringTone!=null)
                {
                    MainActivity.ringTone.stop();
                }
            }
        });*/

        dialog.show();

    }

    public static String commaFormat(String x) {
        if(x != null && !x.equalsIgnoreCase("") && x.length() > 3){
            String c =  new DecimalFormat("#,###,###.00").format(Double.parseDouble(x));
            c =  c.replace(",","/");
            c =  c.replace(".",",");
            return c.replace("/",".");
//            return c;
        }else {
            return x;
        }
    }
}
