package com.connectkargo.driver.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Activity.Add_Card_In_List_Activity;
import com.connectkargo.driver.Activity.Wallet_Balance_TopUp_Activity;
import com.connectkargo.driver.Been.CreditCard_List_Been;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Activity.Wallet_Cards_Activity;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CreditCardList_Adapter extends RecyclerView.Adapter<CreditCardList_Adapter.MyViewHolder> {

    private String TAG = "CreditCardList_Adapter";
    private Context mContext;
    private List<CreditCard_List_Been> list;
    public static String ClickecardNumber = "";
    public static String ClickcardId = "";
    public static List<CreditCard_List_Been> cardList = new ArrayList<>();


    private AQuery aQuery;
    DialogClass dialogClass;
    LinearLayout main_layout;
    private Animation animBounce;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView tv_cardNo, tv_Year , tv_Month; //tv_cardType
        public ImageView iv_creditCard, iv_delete;
        LinearLayout ll_row_card, ll_delete_card_layout, ll_addCard;
        CardView row_main;

        public MyViewHolder(View view)
        {
            super(view);

            row_main = (CardView) view.findViewById(R.id.row_main);
            tv_cardNo = (TextView) view.findViewById(R.id.tv_cardNo);
            tv_Month = (TextView) view.findViewById(R.id.tv_Month);
            tv_Year = (TextView) view.findViewById(R.id.tv_Year);
            iv_creditCard = (ImageView) view.findViewById(R.id.iv_creditCard);
            iv_delete = (ImageView) view.findViewById(R.id.iv_delete);
            ll_delete_card_layout = (LinearLayout) view.findViewById(R.id.delete_card_layout);
            ll_row_card = (LinearLayout) view.findViewById(R.id.ll_row_card);
            ll_addCard = (LinearLayout) view.findViewById(R.id.ll_addCard);

            aQuery = new AQuery(mContext);
            dialogClass = new DialogClass(mContext, 1);
        }
    }

    public CreditCardList_Adapter(Context mContext, List<CreditCard_List_Been> list, LinearLayout main_layout) {
        this.mContext = mContext;
        this.list = list;
        this.main_layout=main_layout;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_credit_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        if (list.get(position).getCardNo()!=null && !list.get(position).getCardNo().equalsIgnoreCase(""))
        {
            holder.tv_cardNo.setText( list.get(position).getCardNo());
        }

        if (list.get(position).getExpiry()!=null && !list.get(position).getExpiry().equalsIgnoreCase(""))
        {
            Log.e("getExpiry","getExpiry : "+list.get(position).getExpiry());
            String date = list.get(position).getExpiry();
            String date1[] = date.split("/");
            if (date1.length>1)
            {
                holder.tv_Month.setText(date1[0]);
                holder.tv_Year.setText(date1[1]);
            }
        }

//        Log.e("position","position : "+position);
//        if (position==list.size()-1)
//        {
//            holder.ll_addCard.setVisibility(View.VISIBLE);
//            holder.row_main.setVisibility(View.GONE);
//        }
//        else
//        {
//            holder.ll_addCard.setVisibility(View.GONE);
//            holder.row_main.setVisibility(View.VISIBLE);
//        }

        if (list.get(position).getCardType()!=null && !list.get(position).getCardType().equalsIgnoreCase(""))
        {
            if (list.get(position).getCardType().equalsIgnoreCase("visa"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_visa);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_blue);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("discover"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_discover);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_orange);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("amex"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_maestro);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_blue);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("diners"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_card_dinner);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_gray);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("jcb"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_card_jcb);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_gray);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("mastercard"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_card_master);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_blue);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("maestro"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_maestro);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_gray);
            }
            else
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_visa);
                holder.ll_row_card.setBackgroundResource(R.drawable.back_card_blue);
            }
        }

        holder.ll_addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wallet_Cards_Activity.resumeFlag=0;
                Intent i =  new Intent(mContext, Add_Card_In_List_Activity.class);
                i.putExtra("from","");
                mContext.startActivity(i);
                ((Wallet_Cards_Activity)mContext).overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });

        holder.ll_row_card.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                animBounce = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
                holder.ll_row_card.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (Wallet_Cards_Activity.from!=null && !Wallet_Cards_Activity.from.equalsIgnoreCase(""))
                        {
                            if (list.get(position).getCardNo()!=null && !list.get(position).getCardNo().equalsIgnoreCase(""))
                            {
                                if (Wallet_Cards_Activity.from.equalsIgnoreCase("TopUp"))
                                {
                                    Wallet_Balance_TopUp_Activity.flagResumeUse = 1;
                                    ClickecardNumber = list.get(position).getCardNo();
                                    ClickcardId = list.get(position).getId();
                                    ((Activity)mContext).finish();
                                }
//                                else
//                                {
//                                    Wallet_Balance_TransferToBank_Activity.flagResumeUse = 1;
//                                    ClickecardNumber = list.get(positionRow).getCardNo();
//                                    ClickcardId = list.get(positionRow).getId();
//                                    ((Activity)mContext).finish();
//                                }
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });


        holder.ll_delete_card_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                animBounce = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
                holder.ll_delete_card_layout.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (ConnectivityReceiver.isConnected())
                        {
                            if (list.get(position).getId()!=null && !list.get(position).getId().equalsIgnoreCase(""))
                            {
                                String driverId = SessionSave.getUserSession(Comman.USER_ID, mContext);

                                OpenDialogDeletCard(driverId,list.get(position).getId(), position);

//                                deleteCard(driverId, list.get(position).getId(), position);
                            }
                        }
                        else
                        {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                            errorDialogClass.showDialog(mContext.getString(R.string.not_connected_to_internet), mContext.getString(R.string.internet_error_message));
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    private void OpenDialogDeletCard(final String driverId, final String CardId, final int position)
    {
        final Dialog dialogNotice = new Dialog(mContext, R.style.PauseDialog);
        dialogNotice.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNotice.setCancelable(false);
        dialogNotice.setContentView(R.layout.dialog_app_notice);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogNotice.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogNotice.getWindow().setAttributes(lp);

        dialogNotice.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogNotice.getWindow().setAttributes(lp);

        final TextView  tv_title, tv_message, tv_update;
        final ImageView dialog_close;
        final LinearLayout dialog_ok_layout;

        tv_title = (TextView) dialogNotice.findViewById(R.id.tv_title);
        tv_message = (TextView) dialogNotice.findViewById(R.id.tv_message);
        tv_update = (TextView) dialogNotice.findViewById(R.id.tv_update);
        dialog_close = (ImageView) dialogNotice.findViewById(R.id.dialog_close);
        dialog_ok_layout = (LinearLayout) dialogNotice.findViewById(R.id.dialog_ok_layout);

//        tv_message.setText(mContext.getResources().getString(R.string.alert_mesage));
        tv_message.setText(mContext.getResources().getString(R.string.delete_card_message));
        tv_update.setText(mContext.getResources().getString(R.string.ok));

        tv_update.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                dialogNotice.dismiss();
                deleteCard(driverId, CardId, position);
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
                deleteCard(driverId, CardId, position);
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialogNotice.dismiss();
            }
        });
        dialogNotice.show();
    }

    public void deleteCard(String driverId, String CardId, final int Pos)
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.WEB_SERVICE_REMOVE_CARD + driverId + "/" + CardId;
        cardList.clear();
        Log.e(TAG, "URL = " + url);

        aQuery.ajax(url, null , JSONObject.class , new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if(json.getBoolean("status"))
                            {
                                Log.e(TAG,"Status = " + "true");
                                dialogClass.hideDialog();
                                list.remove(Pos);
                                notifyDataSetChanged();
                                if (json.has("cards"))
                                {
                                    String cards = json.getString("cards");
                                    if (cards!=null && !cards.equalsIgnoreCase(""))
                                    {
                                        JSONArray cardsArray = json.getJSONArray("cards");
                                        if (cardsArray!=null && cardsArray.length()>0)
                                        {
                                            for (int i=0; i<cardsArray.length(); i++)
                                            {
                                                String Id = "", CardNum="", CardNum2="", Type="", Alias="", Expiry="";

                                                JSONObject cardObj = cardsArray.getJSONObject(i);
                                                if (cardObj.has("Id"))
                                                {
                                                    Id = cardObj.getString("Id");
                                                }
                                                if (cardObj.has("CardNum"))
                                                {
                                                    CardNum = cardObj.getString("CardNum");
                                                }
                                                if (cardObj.has("CardNum2"))
                                                {
                                                    CardNum2 = cardObj.getString("CardNum2");
                                                }
                                                if (cardObj.has("Type"))
                                                {
                                                    Type = cardObj.getString("Type");
                                                }
                                                if (cardObj.has("Alias"))
                                                {
                                                    Alias = cardObj.getString("Alias");
                                                }
                                                if (cardObj.has("Expiry"))
                                                {
                                                    Expiry = cardObj.getString("Expiry");
                                                }
                                                Log.e("GetCardList","Id : "+Id+"\nCardNum : "+CardNum+"\nCardNum2 : "+CardNum2+"\nType : "+Type+"\nAlias : "+Alias);
                                                cardList.add(new CreditCard_List_Been(Id, Alias, CardNum2, Type, Expiry));
                                            }

                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, cardList.size()+"", mContext);
                                            notifyDataSetChanged();

                                            if (json.has("message"))
                                            {
                                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                                if (list.size()>0)
                                                {
                                                    errorDialogClass.showDialog(json.getString("message"), mContext.getResources().getString(R.string.success_message));
                                                    Wallet_Cards_Activity.activity.Set_NoCardLayout(0);
                                                }
                                                else
                                                {
                                                    errorDialogClass.showDialog(json.getString("message"), mContext.getResources().getString(R.string.success_message), new ErrorDialogClass.SetOnClick() {
                                                        @Override
                                                        public void setOnClickListner() {
                                                            Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                                                        }
                                                    });
                                                }

                                            }
                                        }
                                        else
                                        {
                                            Log.e("cardsArray", "null");
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", mContext);
                                            Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                                errorDialogClass.showDialog(json.getString("message"), mContext.getResources().getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("cards", "null");
                                        dialogClass.hideDialog();
                                        SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", mContext);
                                        Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                                        if (json.has("message"))
                                        {
                                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                            errorDialogClass.showDialog(json.getString("message"), mContext.getResources().getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("cards", "noy found");
                                    dialogClass.hideDialog();
                                    SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", mContext);
                                    Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                                    if (json.has("message"))
                                    {
                                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                        errorDialogClass.showDialog(json.getString("message"), mContext.getResources().getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                    Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                                    errorDialogClass.showDialog(json.getString("message"), mContext.getResources().getString(R.string.error_message));
                                }
                            }

                        }
                        else
                        {
                            Log.e("json", "no status found");
                            Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                                errorDialogClass.showDialog(mContext.getResources().getString(R.string.something_is_wrong), mContext.getResources().getString(R.string.error_message));
                            }

                        }

                    }
                    else
                    {
                        Log.e(TAG, "getMessage = "  + "Null");
                        Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                        dialogClass.hideDialog();
                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                        errorDialogClass.showDialog(mContext.getResources().getString(R.string.something_is_wrong), mContext.getResources().getString(R.string.error_message));

                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "somthing_is_wrong");
                    Wallet_Cards_Activity.activity.Set_NoCardLayout(1);
                    dialogClass.hideDialog();
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(mContext);
                    errorDialogClass.showDialog(mContext.getResources().getString(R.string.something_is_wrong), mContext.getResources().getString(R.string.error_message));

                }

            }
        }.header(WebServiceAPI.HEADER_KEY,WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}