package com.connectkargo.driver.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Fragment.Dispatched_Job_List_Fragment;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Dispatched_Job_List_Adapter extends ExpandableRecyclerView.Adapter<Dispatched_Job_List_Adapter.ViewHolder> {

    Context context;
    int POSITION;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    DialogClass dialogClass;


    public Dispatched_Job_List_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder
    {
        public View layout;
//        ImageView iv_status_active, iv_status_inactive;

        LinearLayout ll_row_item_pastJob, ll_pickUpLoc, ll_passengerNo, ll_tripDistance, ll_carModel, ll_suburb, ll_dropOffLoc, ll_PassengerEmail, ll_TripFare, ll_Tax, ll_SubTotal
                , ll_GrandTotal, ll_PaymentType, ll_date_time, ll_Status, ll_passengerFlightNo, ll_passengerNote;

        public TextView tv_drop_off_location, tv_pickupLocation, tv_date_time, tv_PassengerName, tv_passengerNo, tv_tripDistance, tv_carModel, tv_suburb, tv_PassengerEmail, tv_TripFare
                , tv_Tax, tv_SubTotal, tv_GrandTotal, tv_PaymentType, tv_Status, tv_passengerFlightNo, tv_passengerNote;

        ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);

            tv_drop_off_location = (TextView) expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_pickupLocation = (TextView) expandableItem.findViewById(R.id.tv_pickupLocation);
            tv_suburb = (TextView) expandableItem.findViewById(R.id.tv_suburb);
            tv_date_time = (TextView) expandableItem.findViewById(R.id.tv_date_time);
            tv_PassengerName = (TextView) expandableItem.findViewById(R.id.tv_PassengerName);
            tv_passengerNo = (TextView) expandableItem.findViewById(R.id.tv_passengerNo);
            tv_tripDistance = (TextView) expandableItem.findViewById(R.id.tv_tripDistance);
            tv_carModel = (TextView) expandableItem.findViewById(R.id.tv_carModel);

            tv_PassengerEmail = (TextView) expandableItem.findViewById(R.id.tv_PassengerEmail);
            tv_TripFare = (TextView) expandableItem.findViewById(R.id.tv_TripFare);
            tv_Tax = (TextView) expandableItem.findViewById(R.id.tv_Tax);
            tv_SubTotal = (TextView) expandableItem.findViewById(R.id.tv_SubTotal);
            tv_GrandTotal = (TextView) expandableItem.findViewById(R.id.tv_GrandTotal);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_Status = (TextView) expandableItem.findViewById(R.id.tv_Status);
            tv_passengerFlightNo = (TextView) expandableItem.findViewById(R.id.tv_passengerFlightNo);
            tv_passengerNote = (TextView) expandableItem.findViewById(R.id.tv_passengerNote);

            /*iv_status_active = (ImageView) expandableItem.findViewById(R.id.iv_status_active);
            iv_status_inactive = (ImageView) expandableItem.findViewById(R.id.iv_status_inactive);*/

            ll_row_item_pastJob = (LinearLayout) expandableItem.findViewById(R.id.ll_row_item_pastJob);

            ll_pickUpLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_pickUpLoc);
            ll_passengerNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNo);
            ll_tripDistance = (LinearLayout) expandableItem.findViewById(R.id.ll_tripDistance);
            ll_carModel = (LinearLayout) expandableItem.findViewById(R.id.ll_carModel);
            ll_suburb = (LinearLayout) expandableItem.findViewById(R.id.ll_suburb);
            ll_dropOffLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffLoc);

            ll_PassengerEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_PassengerEmail);
            ll_TripFare = (LinearLayout) expandableItem.findViewById(R.id.ll_TripFare);
            ll_Tax = (LinearLayout) expandableItem.findViewById(R.id.ll_Tax);
            ll_SubTotal = (LinearLayout) expandableItem.findViewById(R.id.ll_SubTotal);
            ll_GrandTotal = (LinearLayout) expandableItem.findViewById(R.id.ll_GrandTotal);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_date_time = (LinearLayout) expandableItem.findViewById(R.id.ll_date_time);
            ll_Status = (LinearLayout) expandableItem.findViewById(R.id.ll_Status);
            ll_passengerFlightNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerFlightNo);
            ll_passengerNote = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNote);
        }
    }


    @Override
    public Dispatched_Job_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_dispatched_job, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        /*holder.iv_status_active.setVisibility(View.GONE);
        holder.iv_status_inactive.setVisibility(View.GONE);*/

        if (Dispatched_Job_List_Fragment.list.get(position).getDropoffLocation()!=null && !Dispatched_Job_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
            holder.tv_drop_off_location.setText(Dispatched_Job_List_Fragment.list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        holder.ll_suburb.setVisibility(View.GONE);
        if (Dispatched_Job_List_Fragment.list.get(position).getStatus()!=null && !Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase(""))
        {
            holder.tv_Status.setText(Dispatched_Job_List_Fragment.list.get(position).getStatus());
        }
        else
        {
            holder.ll_Status.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPickupLocation()!=null && !Dispatched_Job_List_Fragment.list.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.ll_pickUpLoc.setVisibility(View.VISIBLE);
            holder.tv_pickupLocation.setText(Dispatched_Job_List_Fragment.list.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_pickUpLoc.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPickupDateTime()!=null && !Dispatched_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase(""))
        {
            holder.ll_date_time.setVisibility(View.VISIBLE);
            String pickUpDateTime = getDate(Long.parseLong(Dispatched_Job_List_Fragment.list.get(position).getPickupDateTime()+"000"), "HH:mm  dd/MM/yyyy");
            Log.e("pickUpDateTime","pickUpDateTime : "+pickUpDateTime);
            holder.tv_date_time.setText(pickUpDateTime);
        }
        else
        {
            holder.ll_date_time.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPassengerName()!=null && !Dispatched_Job_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.tv_PassengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName.setText(Dispatched_Job_List_Fragment.list.get(position).getPassengerName());
        }
        else
        {
            holder.tv_PassengerName.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPassengerContact()!=null && !Dispatched_Job_List_Fragment.list.get(position).getPassengerContact().equalsIgnoreCase(""))
        {
            holder.ll_passengerNo.setVisibility(View.VISIBLE);
            holder.tv_passengerNo.setText(Dispatched_Job_List_Fragment.list.get(position).getPassengerContact());
        }
        else
        {
            holder.ll_passengerNo.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getTripDistance()!=null && !Dispatched_Job_List_Fragment.list.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.ll_tripDistance.setVisibility(View.VISIBLE);
            holder.tv_tripDistance.setText(Dispatched_Job_List_Fragment.list.get(position).getTripDistance());
        }
        else
        {
            holder.ll_tripDistance.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getModel()!=null && !Dispatched_Job_List_Fragment.list.get(position).getModel().equalsIgnoreCase(""))
        {
            holder.ll_carModel.setVisibility(View.VISIBLE);
            holder.tv_carModel.setText(Dispatched_Job_List_Fragment.list.get(position).getModel());
        }
        else
        {
            holder.ll_carModel.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPassengerEmail()!=null && !Dispatched_Job_List_Fragment.list.get(position).getPassengerEmail().equalsIgnoreCase(""))
        {
            holder.ll_PassengerEmail.setVisibility(View.VISIBLE);
            holder.tv_PassengerEmail.setText(Dispatched_Job_List_Fragment.list.get(position).getPassengerEmail());
        }
        else
        {
            holder.ll_PassengerEmail.setVisibility(View.GONE);
        }
        if (Dispatched_Job_List_Fragment.list.get(position).getTripFare()!=null && !Dispatched_Job_List_Fragment.list.get(position).getTripFare().equalsIgnoreCase(""))
        {
            holder.ll_TripFare.setVisibility(View.VISIBLE);
            holder.tv_TripFare.setText(Dispatched_Job_List_Fragment.list.get(position).getTripFare());
        }
        else
        {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getTax()!=null && !Dispatched_Job_List_Fragment.list.get(position).getTax().equalsIgnoreCase(""))
        {
            holder.tv_Tax.setVisibility(View.VISIBLE);
            holder.tv_Tax.setText(Dispatched_Job_List_Fragment.list.get(position).getTax());
        }
        else
        {
            holder.tv_Tax.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getSubTotal()!=null && !Dispatched_Job_List_Fragment.list.get(position).getSubTotal().equalsIgnoreCase(""))
        {
            holder.tv_SubTotal.setVisibility(View.VISIBLE);
            holder.tv_SubTotal.setText(Dispatched_Job_List_Fragment.list.get(position).getSubTotal());
        }
        else
        {
            holder.tv_SubTotal.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getGrandTotal()!=null && !Dispatched_Job_List_Fragment.list.get(position).getGrandTotal().equalsIgnoreCase(""))
        {
            holder.ll_GrandTotal.setVisibility(View.VISIBLE);
            holder.tv_GrandTotal.setText(Dispatched_Job_List_Fragment.list.get(position).getGrandTotal());
        }
        else
        {
            holder.ll_GrandTotal.setVisibility(View.GONE);
        }
        if (Dispatched_Job_List_Fragment.list.get(position).getPaymentType()!=null && !Dispatched_Job_List_Fragment.list.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(Dispatched_Job_List_Fragment.list.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getFlightNumber()!=null && !Dispatched_Job_List_Fragment.list.get(position).getFlightNumber().equalsIgnoreCase(""))
        {
            holder.ll_passengerFlightNo.setVisibility(View.VISIBLE);
            holder.tv_passengerFlightNo.setText(Dispatched_Job_List_Fragment.list.get(position).getFlightNumber());
        }
        else
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getNotes()!=null && !Dispatched_Job_List_Fragment.list.get(position).getNotes().equalsIgnoreCase(""))
        {
            holder.ll_passengerNote.setVisibility(View.VISIBLE);
            holder.tv_passengerNote.setText(Dispatched_Job_List_Fragment.list.get(position).getNotes());
        }
        else
        {
            holder.ll_passengerNote.setVisibility(View.GONE);
        }
    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    @Override
    public int getItemCount() {
        return Dispatched_Job_List_Fragment.list.size();
    }

}
