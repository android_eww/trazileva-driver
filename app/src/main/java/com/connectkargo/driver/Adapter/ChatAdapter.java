package com.connectkargo.driver.Adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Been.ChatList_Been;
import com.connectkargo.driver.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    public String TAG = "Chat_Adapter";
    private List<ChatList_Been> list;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private LinearLayout ll_PassengerChat, ll_DriverChat;
        public TextView tv_Pass_Mesg, tv_Pass_Time, tv_Dr_Mesg, tv_Dr_Time;

        public MyViewHolder(View view)
        {
            super(view);

            ll_PassengerChat = view.findViewById(R.id.ll_PassengerChat);
            ll_DriverChat = view.findViewById(R.id.ll_DriverChat);

            tv_Pass_Mesg = view.findViewById(R.id.tv_Pass_Mesg);
            tv_Pass_Time = view.findViewById(R.id.tv_Pass_Time);
            tv_Dr_Mesg = view.findViewById(R.id.tv_Dr_Mesg);
            tv_Dr_Time = view.findViewById(R.id.tv_Dr_Time);
        }
    }

    public ChatAdapter(Activity activity, List<ChatList_Been> list)
    {
        this.mContext = activity;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_chat, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        if (list.get(position).getSender_type().equalsIgnoreCase("driver"))
        {
            holder.ll_DriverChat.setVisibility(View.VISIBLE);
            holder.ll_PassengerChat.setVisibility(View.GONE);

            holder.tv_Dr_Mesg.setText(list.get(position).getMessage());

            if (list.get(position).getCreated_at() != null &&
                    !TextUtils.isEmpty(list.get(position).getCreated_at()) &&
                    !list.get(position).getCreated_at().equalsIgnoreCase("null"))
            {
                String dateTime = getFormattedDateTime(list.get(position).getCreated_at(),
                        "yyyy-MM-dd HH:mm:ss","yyyy-MM-dd hh:mm a");
                Log.e(TAG,"onBindViewHolder dateTime:- " + dateTime);

                String[] separated = dateTime.split(" ");
                Log.e(TAG,"onBindViewHolder separated:- " + separated[1].concat(separated[2]));

                holder.tv_Dr_Time.setText(separated[1].concat(" ").concat(separated[2]));
            }

            if (position>0 && list.get(position).getSender_type().equalsIgnoreCase(list.get(position-1).getSender_type()))
            {
                holder.ll_DriverChat.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_chat_box2));
            }
            else
            {
                holder.ll_DriverChat.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_chat_box));
            }
        }
        else
        {
            holder.ll_DriverChat.setVisibility(View.GONE);
            holder.ll_PassengerChat.setVisibility(View.VISIBLE);

            holder.tv_Pass_Mesg.setText(list.get(position).getMessage());

            if (list.get(position).getCreated_at() != null &&
                    !TextUtils.isEmpty(list.get(position).getCreated_at()) &&
                    !list.get(position).getCreated_at().equalsIgnoreCase("null"))
            {
                String dateTime = getFormattedDateTime(list.get(position).getCreated_at(),
                        "yyyy-MM-dd HH:mm:ss","yyyy-MM-dd hh:mm a");
                Log.e(TAG,"onBindViewHolder dateTime:- " + dateTime);

                String[] separated = dateTime.split(" ");
                Log.e(TAG,"onBindViewHolder dateTime:- " + separated[1].concat(separated[2]));

                holder.tv_Pass_Time.setText(separated[1].concat(" ").concat(separated[2]));
            }


            if (position>0 && !list.get(position).getSender_type().equalsIgnoreCase(list.get(position-1).getSender_type()))
            {
                holder.ll_PassengerChat.setBackground(ContextCompat.getDrawable(mContext, R.drawable.image_chat_gray_up));
            }
            else
            {
                holder.ll_PassengerChat.setBackground(ContextCompat.getDrawable(mContext, R.drawable.image_chat_gray));
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static String getFormattedDateTime(String dateStr, String strReadFormat, String strWriteFormat)
    {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try
        {
            date = readFormat.parse(dateStr);
        }
        catch (ParseException e)
        {

        }

        if (date != null)
        {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }
}