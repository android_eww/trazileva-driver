package com.connectkargo.driver.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.connectkargo.driver.Activity.BidListActivity;
import com.connectkargo.driver.Been.myBid.Datum;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.listner.BidStatus;
import com.connectkargo.driver.singleton.AcceptRejectBid;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MyBidAdapter  extends RecyclerView.Adapter
{
    String FROM ;
    Context context;
    onBidClick onBidClick;

    List<com.connectkargo.driver.Been.openBid.Datum> list;//open bid
    List<com.connectkargo.driver.Been.myBid.Datum> myBidlist;

    private DialogClass dialogClass;
    private AQuery aQuery;
    int selectedPos;


    public MyBidAdapter(String x,Context context, List<com.connectkargo.driver.Been.openBid.Datum> data, String from, onBidClick onBidClick)
    {
        this.context = context;
        FROM = from;
        this.onBidClick = onBidClick;
        list= data;

        aQuery = new AQuery(context);
        dialogClass = new DialogClass(context, 1);
    }

    public MyBidAdapter(Context context, List<com.connectkargo.driver.Been.myBid.Datum> data, String from, onBidClick onBidClick)
    {
        this.context = context;
        FROM = from;
        this.onBidClick = onBidClick;
        myBidlist= data;

        aQuery = new AQuery(context);
        dialogClass = new DialogClass(context, 1);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bid, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder)holder;

            if (FROM.equalsIgnoreCase(BidListActivity.MY_BID))
            {
                //holder1.iv_delete.setVisibility(View.VISIBLE);
                holder1.total_bid.setVisibility(View.VISIBLE);
                holder1.tv_sendOffer.setText(context.getResources().getString(R.string.lbl_view_detail));
                holder1.tv_status.setVisibility(View.VISIBLE);

                if(myBidlist.get(position).getStatus() != null && myBidlist.get(position).getStatus().equalsIgnoreCase("1"))
                {
                    holder1.tv_status.setText(context.getResources().getString(R.string.lbl_status) + context.getResources().getString(R.string.accepted));
                }
                else if(myBidlist.get(position).getStatus() != null && myBidlist.get(position).getStatus().equalsIgnoreCase("3"))
                {
                    holder1.tv_status.setText(context.getResources().getString(R.string.lbl_status) + context.getResources().getString(R.string.completed));
                }
                else
                {
                    holder1.tv_status.setText(context.getResources().getString(R.string.lbl_status) + context.getResources().getString(R.string.pending));
                }

                if(myBidlist.get(position).getPickupLocation() != null)
                {holder1.tv_pickup.setText(myBidlist.get(position).getPickupLocation());}
                if(myBidlist.get(position).getDropoffLocation() != null)
                {holder1.tv_dropoff.setText(myBidlist.get(position).getDropoffLocation());}

                if(myBidlist.get(position).getPickupDateTime() != null)
                {
                    String pickupDate = "";
                    try
                    {
                        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        DateFormat outputFormat = new SimpleDateFormat("MMM - dd");
                        Date date = inputFormat.parse(myBidlist.get(position).getPickupDateTime());
                        pickupDate = outputFormat.format(date);

                        holder1.tv_pickDate.setText(pickupDate);
                    }
                    catch (Exception e)
                    {
                        Log.e("call","Exception = "+e.getMessage());
                    }
                }

                if(myBidlist.get(position).getDeadHead() != null)
                {
                    holder1.tv_deadHead.setText(myBidlist.get(position).getDeadHead());
                }
                if(myBidlist.get(position).getDistance() != null && !myBidlist.get(position).getDistance().equalsIgnoreCase("") &&
                        !myBidlist.get(position).getDistance().equalsIgnoreCase("null"))
                {
                    String distance = String.format("%.2f",Double.parseDouble(myBidlist.get(position).getDistance()));
                    holder1.tv_distance.setText(distance.replace(",",".")+" "+context.getResources().getString(R.string.km));
                }
                if(myBidlist.get(position).getBudget() != null && !myBidlist.get(position).getBudget().equalsIgnoreCase("") &&
                        !myBidlist.get(position).getBudget().equalsIgnoreCase("null"))
                {
                   // String price = String.format("%.2f", Double.parseDouble(myBidlist.get(position).getBudget()));
                    holder1.tv_price.setText(context.getResources().getString(R.string.currency)+" "+ commaFormat(myBidlist.get(position).getBudget())); /*.replace(",",".")*/
                }

                if(myBidlist.get(position).getModelImage() != null && !myBidlist.get(position).getModelImage().equalsIgnoreCase(""))
                {
                    Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE+myBidlist.get(position).getModelImage()).into(holder1.iv_vehicleype);
                }
                if(myBidlist.get(position).getName() != null && !myBidlist.get(position).getName().equalsIgnoreCase(""))
                {
                    holder1.tv_vehicleType.setText(myBidlist.get(position).getName());
                }
                if(myBidlist.get(position).getDriverBids() != null && !myBidlist.get(position).getDriverBids().equalsIgnoreCase(""))
                {
                    holder1.tv_BidCount.setText(myBidlist.get(position).getDriverBids());
                }

                holder1.tv_sendOffer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBidClick.onMyBidView(myBidlist.get(position));
                        selectedPos = position;
                    }
                });

                holder1.tv_BidId.setText("#"+myBidlist.get(position).getBidId());
            }
            else
            {
                holder1.iv_delete.setVisibility(View.GONE);
                holder1.tv_status.setVisibility(View.GONE);
                if(list.get(position).getDriverStatus() != null && list.get(position).getDriverStatus().equalsIgnoreCase("1"))
                {
                    holder1.tv_sendOffer.setText(context.getResources().getString(R.string.lbl_allread_made_bid));
                    holder1.tv_sendOffer.setClickable(false);
                    holder1.tv_sendOffer.setEnabled(false);
                    holder1.tv_sendOffer.setFocusable(false);
                }
                else if(list.get(position).getDriverStatus() != null && list.get(position).getDriverStatus().equalsIgnoreCase("0"))
                {
                    holder1.tv_sendOffer.setText(context.getResources().getString(R.string.lbl_reject_bid));
                    holder1.tv_sendOffer.setClickable(false);
                    holder1.tv_sendOffer.setEnabled(false);
                    holder1.tv_sendOffer.setFocusable(false);
                }
                else
                {
                    holder1.tv_sendOffer.setText(context.getResources().getString(R.string.lbl_send_offer));
                    holder1.tv_sendOffer.setClickable(true);
                    holder1.tv_sendOffer.setEnabled(true);
                    holder1.tv_sendOffer.setFocusable(true);

                }

                if(list.get(position).getPickupLocation() != null)
                {holder1.tv_pickup.setText(list.get(position).getPickupLocation());}
                if(list.get(position).getDropoffLocation() != null)
                {holder1.tv_dropoff.setText(list.get(position).getDropoffLocation());}

                if(list.get(position).getPickupDateTime() != null)
                {
                    String pickupDate = "";
                    try
                    {
                        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        DateFormat outputFormat = new SimpleDateFormat("MMM - dd");
                        Date date = inputFormat.parse(list.get(position).getPickupDateTime());
                        pickupDate = outputFormat.format(date);

                        holder1.tv_pickDate.setText(pickupDate);
                    }
                    catch (Exception e)
                    {
                        Log.e("call","Exception = "+e.getMessage());
                    }
                }

                if(list.get(position).getDeadHead() != null)
                {
                    holder1.tv_deadHead.setText(list.get(position).getDeadHead());
                }
                if(list.get(position).getDistance() != null && !list.get(position).getDistance().equalsIgnoreCase("") &&
                        !list.get(position).getDistance().equalsIgnoreCase("null"))
                {
                    String distance = String.format("%.2f",Double.parseDouble(list.get(position).getDistance()));
                    holder1.tv_distance.setText(distance.replace(",",".")+" "+context.getResources().getString(R.string.km));
                }
                if(list.get(position).getBudget() != null && !list.get(position).getBudget().equalsIgnoreCase("") &&
                        !list.get(position).getBudget().equalsIgnoreCase("null"))
                {
                    //String price = String.format("%.2f",Double.parseDouble(list.get(position).getBudget()));
                    holder1.tv_price.setText(context.getResources().getString(R.string.currency)+" "+commaFormat(list.get(position).getBudget()));
                }

                if(list.get(position).getModelImage() != null && !list.get(position).getModelImage().equalsIgnoreCase(""))
                {
                    Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE+list.get(position).getModelImage()).into(holder1.iv_vehicleype);
                }
                if(list.get(position).getName() != null && !list.get(position).getName().equalsIgnoreCase(""))
                {
                    holder1.tv_vehicleType.setText(list.get(position).getName());
                }

                holder1.tv_sendOffer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBidClick.onBidView(list.get(position));
                        selectedPos = position;
                    }
                });

                AcceptRejectBid.setCallBack(context, new BidStatus() {
                    @Override
                    public void acceptBid(boolean isAccept) {
                        if(isAccept)
                        {list.get(selectedPos).setDriverStatus("1");}
                        else
                        {list.get(selectedPos).setDriverStatus("0");}
                        notifyDataSetChanged();
                    }
                });
                holder1.tv_BidId.setText("#"+list.get(position).getId());
            }

            holder1.iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }


    @Override
    public int getItemCount() {
        if(FROM.equalsIgnoreCase(BidListActivity.OPEN_BID))
        {
            return list.size(); //open bid
        }
        else
        {
            return myBidlist.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView iv_vehicleype,iv_delete;
        TextView tv_vehicleType,tv_pickup,tv_dropoff,tv_pickDate,tv_deadHead,tv_distance,tv_price,tv_sendOffer;
        TextView tv_BidCount,tv_status,tv_BidId;
        LinearLayout total_bid;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            tv_sendOffer = itemView.findViewById(R.id.tv_sendOffer);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_deadHead = itemView.findViewById(R.id.tv_deadHead);
            tv_pickDate = itemView.findViewById(R.id.tv_pickDate);
            tv_dropoff = itemView.findViewById(R.id.tv_dropoff);
            tv_pickup = itemView.findViewById(R.id.tv_pickup);
            tv_vehicleType = itemView.findViewById(R.id.tv_vehicleType);
            iv_vehicleype = itemView.findViewById(R.id.iv_vehicleype);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            total_bid = itemView.findViewById(R.id.total_bid);
            tv_BidCount = itemView.findViewById(R.id.tv_BidCount);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_BidId = itemView.findViewById(R.id.tv_BidId);
        }
    }

    public interface onBidClick
    {
        void onBidView(com.connectkargo.driver.Been.openBid.Datum openBib);
        void onMyBidView(com.connectkargo.driver.Been.myBid.Datum myBib);
    }

    public static String commaFormat(String x) {
        if(x != null && !x.equalsIgnoreCase("") && x.length() > 3){
            String c =  new DecimalFormat("#,###,###.00").format(Double.parseDouble(x));
            c =  c.replace(",","/");
            c =  c.replace(".",",");
            return c.replace("/",".");
//            return c;
        }else {
            return x;
        }
    }
}

