package com.connectkargo.driver.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Been.TruckType_Been;
import com.connectkargo.driver.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class TruckType_Multi_Selection_Adapter extends BaseAdapter {

    private String TAG = "TruckType_Multi_Selection_Adapter";
    private Activity activity;
    private static LayoutInflater inflater=null;
    ArrayList<TruckType_Been> list_vehicleItem_truck;
    LinearLayout ll_dialog;
    int MINIMUM_CHECK_BOX_SELECTED=3;
    private int clickPosition = -1;

    public TruckType_Multi_Selection_Adapter(Activity a, ArrayList<TruckType_Been> list_vehicleItem_truck, LinearLayout ll_dialog)
    {
        activity = a;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list_vehicleItem_truck = list_vehicleItem_truck;
        this.ll_dialog =  ll_dialog;
    }

    public int getCount() {
        return list_vehicleItem_truck.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent)
    {
        Log.e(TAG,"getView()");

        View vi = convertView;
        final LinearLayout row_vehicleType_item, ll_truck_info;
        TextView tv_vehicle_type, tv_name, tv_height, tv_width, tv_capacity, tv_vehicle_description;
        ImageView iv_truck, iv_truck_size, iv_truck_info;
        final CheckBox ckBox_vehi_type;

        if(convertView==null)
        {
            vi = inflater.inflate(R.layout.listview_row_cartype_multi_selection, null);
        }

        row_vehicleType_item = (LinearLayout) vi.findViewById(R.id.row_vehicleType_item);
        ll_truck_info = (LinearLayout) vi.findViewById(R.id.ll_truck_info);

        tv_vehicle_type = (TextView)vi.findViewById(R.id.tv_vehicle_type);
        tv_name = (TextView)vi.findViewById(R.id.tv_name);
        tv_height = (TextView)vi.findViewById(R.id.tv_height);
        tv_width = (TextView)vi.findViewById(R.id.tv_width);
        tv_capacity = (TextView)vi.findViewById(R.id.tv_capacity);
        tv_vehicle_description = (TextView)vi.findViewById(R.id.tv_vehicle_description);

        iv_truck_info = (ImageView) vi.findViewById(R.id.iv_truck_info);
        iv_truck_size = (ImageView) vi.findViewById(R.id.iv_truck_size);
        iv_truck = (ImageView) vi.findViewById(R.id.iv_truck);

        ckBox_vehi_type = (CheckBox)vi.findViewById(R.id.ckBox_vehi_type);
        ckBox_vehi_type.setClickable(false);

        if (list_vehicleItem_truck.get(position).getName()!=null &&
                !list_vehicleItem_truck.get(position).getName().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getName().equalsIgnoreCase("null"))
        {
            row_vehicleType_item.setVisibility(View.VISIBLE);
            tv_vehicle_type.setText(list_vehicleItem_truck.get(position).getName());
        }
        else
        {
            row_vehicleType_item.setVisibility(View.GONE);
        }

        if (list_vehicleItem_truck.get(position).getImage()!=null &&
                !list_vehicleItem_truck.get(position).getImage().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getImage().equalsIgnoreCase("null"))
        {
            Log.e(TAG,"getView() iv_truck:- " + list_vehicleItem_truck.get(position).getImage());
            iv_truck.setVisibility(View.VISIBLE);
            iv_truck_info.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(list_vehicleItem_truck.get(position).getImage())
                    .placeholder(R.drawable.ic_car_yellow)
                    .into(iv_truck);
        }
        else
        {
            Log.e(TAG,"getView() iv_truck:- null");
            iv_truck.setVisibility(View.VISIBLE);
            iv_truck_info.setVisibility(View.VISIBLE);
            iv_truck.setImageResource(R.drawable.ic_car_yellow);
        }

        if (list_vehicleItem_truck.get(position).getModelSizeImage()!=null &&
                !list_vehicleItem_truck.get(position).getModelSizeImage().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getModelSizeImage().equalsIgnoreCase("null"))
        {
            Log.e(TAG,"getView() iv_truck_size:- " + list_vehicleItem_truck.get(position).getModelSizeImage());
            Picasso.get()
                    .load(list_vehicleItem_truck.get(position).getModelSizeImage())
                    .placeholder(R.drawable.ic_car_yellow)
                    .into(iv_truck_size);
        }
        else
        {
            Log.e(TAG,"getView() iv_truck_size:- null");
            iv_truck_size.setImageResource(R.drawable.ic_car_yellow);
        }

        if (list_vehicleItem_truck.get(position).getName()!=null &&
                !list_vehicleItem_truck.get(position).getName().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getName().equalsIgnoreCase("null"))
        {
            tv_name.setText(list_vehicleItem_truck.get(position).getName());
        }
        else
        {
            tv_name.setText("-");
        }

        if (list_vehicleItem_truck.get(position).getHeight()!=null &&
                !list_vehicleItem_truck.get(position).getHeight().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getHeight().equalsIgnoreCase("null"))
        {
            tv_height.setText(list_vehicleItem_truck.get(position).getHeight());
        }
        else
        {
            tv_height.setText("-");
        }

        if (list_vehicleItem_truck.get(position).getWidth()!=null &&
                !list_vehicleItem_truck.get(position).getWidth().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getWidth().equalsIgnoreCase("null"))
        {
            tv_width.setText(list_vehicleItem_truck.get(position).getWidth());
        }
        else
        {
            tv_width.setText("-");
        }

        if (list_vehicleItem_truck.get(position).getCapacity()!=null &&
                !list_vehicleItem_truck.get(position).getCapacity().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getCapacity().equalsIgnoreCase("null"))
        {
            tv_capacity.setText(list_vehicleItem_truck.get(position).getCapacity());
        }
        else
        {
            tv_capacity.setText("-");
        }

        if (list_vehicleItem_truck.get(position).getDescription()!=null &&
                !list_vehicleItem_truck.get(position).getDescription().equalsIgnoreCase("") &&
                !list_vehicleItem_truck.get(position).getDescription().equalsIgnoreCase(""))
        {
            tv_vehicle_description.setText(list_vehicleItem_truck.get(position).getDescription());
        }
        else
        {
            tv_vehicle_description.setText("-");
        }


        if (list_vehicleItem_truck.get(position).getStatus().equalsIgnoreCase("1"))
        {
            ckBox_vehi_type.setChecked(true);
        }
        else
        {
            ckBox_vehi_type.setChecked(false);
        }

        if (position==clickPosition)
        {
            ll_truck_info.setVisibility(View.VISIBLE);
        }
        else
        {
            ll_truck_info.setVisibility(View.GONE);
        }

        iv_truck_info.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                clickPosition = position;
                if (ll_truck_info.getVisibility()==View.VISIBLE)
                {
                    ll_truck_info.setVisibility(View.GONE);
                }
                else
                {
                    ll_truck_info.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();
                }
            }
        });


        row_vehicleType_item.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int CheckedNumber = 0;
                for (int i=0; i<list_vehicleItem_truck.size() ; i++)
                {
                    if (list_vehicleItem_truck.get(i).getStatus().equalsIgnoreCase("1"))
                    {
                        CheckedNumber++;
                    }
                }

                if (ckBox_vehi_type.isChecked())
                {
                    ckBox_vehi_type.setChecked(false);
                    list_vehicleItem_truck.get(position).setStatus("0");
                }
                else
                {
                    if (CheckedNumber<MINIMUM_CHECK_BOX_SELECTED)
                    {
                        ckBox_vehi_type.setChecked(true);
                        list_vehicleItem_truck.get(position).setStatus("1");
                    }
                    else
                    {
                        new SnackbarUtils(ll_dialog, activity.getResources().getString(R.string.you_can_select_only_three_type),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                    }
                }
            }
        });


        return vi;
    }
}