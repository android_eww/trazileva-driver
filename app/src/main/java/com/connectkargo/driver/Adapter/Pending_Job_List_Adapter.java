package com.connectkargo.driver.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Been.Pending_JobList_Been;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Fragment.Past_Job_List_Fragment;
import com.connectkargo.driver.Fragment.Pending_Job_List_Fragment;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Activity.Drawer_Activity;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


public class Pending_Job_List_Adapter  extends ExpandableRecyclerView.Adapter<Pending_Job_List_Adapter.ViewHolder> {

    Context context;
    String TAG = "Pending_Job_List_Adapter";
    private Animation animation;
    private Animation.AnimationListener animationListener;
    private int positionList = 0;
    private List<Pending_JobList_Been> list;


    public Pending_Job_List_Adapter(LinearLayoutManager lm, List<Pending_JobList_Been> list)
    {
        super(lm);
        this.list = list;
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        LinearLayout ll_row_item_pastJob, ll_pickUpLoc, ll_passengerNo, ll_tripDistance, ll_carModel, ll_BookingId, ll_dropOffLoc,
                ll_dateTime, ll_passengerEmail, ll_PaymentType, ll_passengerFlightNo, ll_passengerNote ,ll_dispatcherName, ll_dispatcherEmail,
                ll_dispatcherNo, ll_parcelName, ll_Labour, llSize, llWeight, llQuantity, llReceiverName, ll_GrandTotal;

        public TextView tv_drop_off_location, tv_pickupLocation, tv_PickUp_dateTime, tv_PassengerName, tv_passengerNo, tv_tripDistance, tv_carModel, tv_BookingId, tv_passengerEmail
                , tv_startTrip, tv_PaymentType, tv_passengerFlightNo, tv_passengerNote ,tv_dispatcherName, tv_dispatcherEmail, tv_dispatcherNo,
                tv_parcelName, tv_Labour, tvSize, tvWeight, tvQuantity, tvReceiverName, tv_GrandTotal;

        ExpandableItem expandableItem;
        ImageView ivParcel;
        LinearLayout llImg;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = v. findViewById(R.id.row);

            tv_drop_off_location = expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_pickupLocation = expandableItem.findViewById(R.id.tv_pickupLocation);
            tv_BookingId = expandableItem.findViewById(R.id.tv_BookingId);
            tv_PickUp_dateTime = expandableItem.findViewById(R.id.tv_PickUp_dateTime);
            tv_PassengerName = expandableItem.findViewById(R.id.tv_PassengerName);
            tv_passengerNo = expandableItem.findViewById(R.id.tv_passengerNo);
            tv_tripDistance = expandableItem.findViewById(R.id.tv_tripDistance);
            tv_carModel = expandableItem.findViewById(R.id.tv_carModel);
            tv_passengerEmail = expandableItem.findViewById(R.id.tv_passengerEmail);
            tv_startTrip = expandableItem.findViewById(R.id.tv_startTrip);
            tv_PaymentType = expandableItem.findViewById(R.id.tv_PaymentType);
            tv_passengerFlightNo = expandableItem.findViewById(R.id.tv_passengerFlightNo);
            tv_passengerNote = expandableItem.findViewById(R.id.tv_passengerNote);
            tv_dispatcherName = expandableItem.findViewById(R.id.tv_dispatcherName);
            tv_dispatcherEmail = expandableItem.findViewById(R.id.tv_dispatcherEmail);
            tv_dispatcherNo = expandableItem.findViewById(R.id.tv_dispatcherNo);
            tv_parcelName = expandableItem.findViewById(R.id.tv_parcelName);
            tv_Labour = expandableItem.findViewById(R.id.tv_Labour);
            tvSize = expandableItem.findViewById(R.id.tvSize);
            tvWeight = expandableItem.findViewById(R.id.tvWeight);
            tvQuantity = expandableItem.findViewById(R.id.tvQuantity);
            tv_GrandTotal = expandableItem.findViewById(R.id.tv_GrandTotal);
            llReceiverName = expandableItem.findViewById(R.id.llReceiverName);

            ll_row_item_pastJob = expandableItem.findViewById(R.id.ll_row_item_pastJob);
            ll_pickUpLoc = expandableItem.findViewById(R.id.ll_pickUpLoc);
            ll_passengerNo = expandableItem.findViewById(R.id.ll_passengerNo);
            ll_tripDistance = expandableItem.findViewById(R.id.ll_tripDistance);
            ll_carModel = expandableItem.findViewById(R.id.ll_carModel);
            ll_BookingId = expandableItem.findViewById(R.id.ll_BookingId);
            ll_dropOffLoc = expandableItem.findViewById(R.id.ll_dropOffLoc);
            ll_dateTime = expandableItem.findViewById(R.id.ll_dateTime);
            ll_passengerEmail = expandableItem.findViewById(R.id.ll_passengerEmail);
            ll_PaymentType = expandableItem.findViewById(R.id.ll_PaymentType);
            ll_passengerFlightNo = expandableItem.findViewById(R.id.ll_passengerFlightNo);
            ll_passengerNote = expandableItem.findViewById(R.id.ll_passengerNote);
            ll_dispatcherName = expandableItem.findViewById(R.id.ll_dispatcherName);
            ll_dispatcherEmail = expandableItem.findViewById(R.id.ll_dispatcherEmail);
            ll_dispatcherNo = expandableItem.findViewById(R.id.ll_dispatcherNo);
            ll_parcelName = expandableItem.findViewById(R.id.ll_parcelName);
            ll_Labour = expandableItem.findViewById(R.id.ll_Labour);
            llSize = expandableItem.findViewById(R.id.llSize);
            llWeight = expandableItem.findViewById(R.id.llWeight);
            llQuantity = expandableItem.findViewById(R.id.llQuantity);
            tvReceiverName = expandableItem.findViewById(R.id.tvReceiverName);
            ll_GrandTotal = expandableItem.findViewById(R.id.ll_GrandTotal);


            ivParcel = expandableItem.findViewById(R.id.ivParcel);
            llImg = expandableItem.findViewById(R.id.llImg);
        }
    }


    @Override
    public Pending_Job_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_pending_job, parent, false);

        Pending_Job_List_Adapter.ViewHolder vh = new Pending_Job_List_Adapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final Pending_Job_List_Adapter.ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        Log.e(TAG,"Pending_Job_Li position : "+position);
        Log.e(TAG,"onBindViewHolder getOnTheWay:- " + Pending_Job_List_Fragment.list.get(position).getOnTheWay());

        if (list.get(position).getDropoffLocation()!=null && !list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
            holder.tv_drop_off_location.setText(list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        if (list.get(position).getOnTheWay() !=null &&
                !list.get(position).getOnTheWay().equalsIgnoreCase("") &&
                !list.get(position).getOnTheWay().equalsIgnoreCase("null") &&
                list.get(position).getOnTheWay().equalsIgnoreCase("0"))
        {
            holder.tv_startTrip.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.tv_startTrip.setVisibility(View.GONE);
        }


        if (list.get(position).getRequestFor()!=null &&
                !list.get(position).getRequestFor().equalsIgnoreCase("") &&
                !list.get(position).getRequestFor().equalsIgnoreCase("taxi"))
        {
            holder.ll_parcelName.setVisibility(View.GONE);
            holder.ll_Labour.setVisibility(View.GONE);

            if (list.get(position).getParcelName()!=null &&
                    !list.get(position).getParcelName().equalsIgnoreCase("") &&
                    !list.get(position).getParcelName().equalsIgnoreCase("null"))
            {
                holder.tv_parcelName.setText(list.get(position).getParcelName());
            }
            else
            {
                holder.ll_parcelName.setVisibility(View.GONE);
            }

            if (list.get(position).getLabour()!=null &&
                    !list.get(position).getLabour().equalsIgnoreCase("") &&
                    !list.get(position).getLabour().equalsIgnoreCase("null") &&
                    !list.get(position).getLabour().equalsIgnoreCase("0"))
            {
                holder.tv_Labour.setText(list.get(position).getLabour());
            }
            else
            {
                holder.ll_Labour.setVisibility(View.GONE);
            }
        }
        else
        {
            holder.ll_parcelName.setVisibility(View.GONE);
            holder.ll_Labour.setVisibility(View.GONE);
        }

        if (list.get(position).getId()!=null &&
                !list.get(position).getId().equalsIgnoreCase("") &&
                list.get(position).getBookingType()!=null &&
                !list.get(position).getBookingType().equalsIgnoreCase(""))
        {
            holder.ll_BookingId.setVisibility(View.VISIBLE);
            if(list.get(position).getBookingType().equalsIgnoreCase("Book Now")) {
                holder.tv_BookingId.setText(list.get(position).getId() +
                        " (" + context.getResources().getString(R.string.bookNow) + ")");
            } else {
                holder.tv_BookingId.setText(list.get(position).getId() +
                        " (" + context.getResources().getString(R.string.bookLater) + ")");
            }
        }
        else
        {
            holder.ll_BookingId.setVisibility(View.GONE);
        }


        if (list.get(position).getPickupLocation()!=null && !list.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.ll_pickUpLoc.setVisibility(View.VISIBLE);
            holder.tv_pickupLocation.setText(list.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_pickUpLoc.setVisibility(View.GONE);
        }

        if (list.get(position).getPickupDateTime()!=null && !list.get(position).getPickupDateTime().equalsIgnoreCase(""))
        {
            Log.e("getPickupDateTime", "getPickupDateTime : "+list.get(position).getPickupDateTime());
            holder.ll_dateTime.setVisibility(View.VISIBLE);
            holder.tv_PickUp_dateTime.setText(list.get(position).getPickupDateTime());
        }
        else
        {
            Log.e("getPickupDateTime", "getPickupDateTime : "+list.get(position).getPickupDateTime());
            holder.ll_dateTime.setVisibility(View.GONE);
        }

        if (list.get(position).getPassengerName()!=null && !list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.tv_PassengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName.setText(list.get(position).getPassengerName());
        }
        else
        {
            holder.tv_PassengerName.setVisibility(View.GONE);
        }

        if (list.get(position).getReceiverName()!=null &&
                !list.get(position).getReceiverName().equalsIgnoreCase("") &&
                !list.get(position).getReceiverName().equalsIgnoreCase("null"))
        {
            holder.llReceiverName.setVisibility(View.VISIBLE);
            holder.tvReceiverName.setText(list.get(position).getReceiverName());
        }
        else
        {
            holder.llReceiverName.setVisibility(View.GONE);
        }

        if (list.get(position).getReceiverContactNo()!=null &&
                !list.get(position).getReceiverContactNo().equalsIgnoreCase("") &&
                !list.get(position).getReceiverContactNo().equalsIgnoreCase("null"))
        {
            holder.ll_passengerNo.setVisibility(View.VISIBLE);
            holder.tv_passengerNo.setText(list.get(position).getReceiverContactNo());
        }
        else
        {
            holder.ll_passengerNo.setVisibility(View.GONE);
        }

        if (list.get(position).getTripDistance()!=null && !list.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.ll_tripDistance.setVisibility(View.VISIBLE);
            holder.tv_tripDistance.setText(list.get(position).getTripDistance() + " km");
        }
        else
        {
            holder.ll_tripDistance.setVisibility(View.GONE);
        }

        if (list.get(position).getModel()!=null && !list.get(position).getModel().equalsIgnoreCase(""))
        {
            holder.ll_carModel.setVisibility(View.VISIBLE);
            holder.tv_carModel.setText(list.get(position).getModel());
        }
        else
        {
            holder.ll_carModel.setVisibility(View.GONE);
        }

        if (list.get(position).getHeight()!=null &&
                !list.get(position).getHeight().equalsIgnoreCase("") &&
                !list.get(position).getHeight().equalsIgnoreCase("null") &&
                !list.get(position).getHeight().equalsIgnoreCase("0") &&
                list.get(position).getLength()!=null &&
                !list.get(position).getLength().equalsIgnoreCase("") &&
                !list.get(position).getLength().equalsIgnoreCase("null") &&
                !list.get(position).getLength().equalsIgnoreCase("0") &&
                list.get(position).getBreadth()!=null &&
                !list.get(position).getBreadth().equalsIgnoreCase("") &&
                !list.get(position).getBreadth().equalsIgnoreCase("null") &&
                !list.get(position).getBreadth().equalsIgnoreCase("0"))
        {
            holder.llSize.setVisibility(View.VISIBLE);
            String size = "(" + list.get(position).getHeight() + " * " +
                    list.get(position).getLength() + " * " +
                    list.get(position).getBreadth() + ")";
            holder.tvSize.setText(size);
        }
        else
        {
            holder.llSize.setVisibility(View.GONE);
        }

        if (list.get(position).getWeigth()!=null &&
                !list.get(position).getWeigth().equalsIgnoreCase("") &&
                !list.get(position).getWeigth().equalsIgnoreCase("null") &&
                !list.get(position).getWeigth().equalsIgnoreCase("0"))
        {
            holder.llWeight.setVisibility(View.VISIBLE);
            holder.tvWeight.setText(list.get(position).getWeigth());
        }
        else
        {
            holder.llWeight.setVisibility(View.GONE);
        }

        if (list.get(position).getGrandTotal()!=null &&
                !list.get(position).getGrandTotal().equalsIgnoreCase("") &&
                !list.get(position).getGrandTotal().equalsIgnoreCase("null") &&
                !list.get(position).getGrandTotal().equalsIgnoreCase("0"))
        {
            holder.ll_GrandTotal.setVisibility(View.VISIBLE);
            holder.tv_GrandTotal.setText(Global.commaFormat(list.get(position).getGrandTotal()));
        }
        else
        {
            holder.ll_GrandTotal.setVisibility(View.GONE);
        }

        if (list.get(position).getQuantity()!=null &&
                !list.get(position).getQuantity().equalsIgnoreCase("") &&
                !list.get(position).getQuantity().equalsIgnoreCase("null") &&
                !list.get(position).getQuantity().equalsIgnoreCase("0"))
        {
            holder.llQuantity.setVisibility(View.VISIBLE);
            holder.tvQuantity.setText(list.get(position).getQuantity());
        }
        else
        {
            holder.llQuantity.setVisibility(View.GONE);
        }

        if (list.get(position).getReceiverEmail()!=null &&
                !list.get(position).getReceiverEmail().equalsIgnoreCase("") &&
                !list.get(position).getReceiverEmail().equalsIgnoreCase("null"))
        {
            holder.ll_passengerEmail.setVisibility(View.VISIBLE);
            holder.tv_passengerEmail.setText(list.get(position).getReceiverEmail());
        }
        else
        {
            holder.ll_passengerEmail.setVisibility(View.GONE);
        }

        if (list.get(position).getPaymentType()!=null && !list.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(list.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (list.get(position).getFlightNumber()!=null && !list.get(position).getFlightNumber().equalsIgnoreCase(""))
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
            holder.tv_passengerFlightNo.setText(list.get(position).getFlightNumber());
        }
        else
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
        }

        if (list.get(position).getNotes()!=null && !list.get(position).getNotes().equalsIgnoreCase(""))
        {
            holder.ll_passengerNote.setVisibility(View.VISIBLE);
            holder.tv_passengerNote.setText(list.get(position).getNotes());
        }
        else
        {
            holder.ll_passengerNote.setVisibility(View.GONE);
        }

        if (list.get(position).getDispatherFullname()!=null && !list.get(position).getDispatherFullname().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherName.setVisibility(View.GONE);
            holder.tv_dispatcherName.setText(list.get(position).getDispatherFullname());
        }
        else
        {
            holder.ll_dispatcherName.setVisibility(View.GONE);
        }

        if (list.get(position).getDispatherEmail()!=null && !list.get(position).getDispatherEmail().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherEmail.setVisibility(View.GONE);
            holder.tv_dispatcherEmail.setText(list.get(position).getDispatherEmail());
        }
        else
        {
            holder.ll_dispatcherEmail.setVisibility(View.GONE);
        }

        if (list.get(position).getDispatherMobileNo()!=null && !list.get(position).getDispatherMobileNo().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherNo.setVisibility(View.GONE);
            holder.tv_dispatcherNo.setText(list.get(position).getDispatherMobileNo());
        }
        else
        {
            holder.ll_dispatcherNo.setVisibility(View.GONE);
        }

        if (list.get(position).getParcelImage()!=null &&
                !list.get(position).getParcelImage().equalsIgnoreCase("")) {
            Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE + list.get(position).getParcelImage()).into(holder.ivParcel);
        } else {
            holder.llImg.setVisibility(View.GONE);
        }

        holder.tv_startTrip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                animation = AnimationUtils.loadAnimation(context,R.anim.bounce);
                holder.tv_startTrip.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {

                        String AcceptFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, context);
                        if (AcceptFlag!=null && !AcceptFlag.equalsIgnoreCase("") && !AcceptFlag.equalsIgnoreCase("1"))
                        {
                            String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, context);
                            if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && !DutyStatus.equalsIgnoreCase("0"))
                            {
                                String bookingId = list.get(position).getId();
                                if (bookingId!=null && !bookingId.equalsIgnoreCase(""))
                                {
                                    Log.e("Pending_Job_Li", "position111 : "+position);
                                    Log.e("Pending_Job_Li", "bookingId : "+bookingId);
                                    ((Drawer_Activity)context).NotifyPassengerForAdvancedTrip(bookingId);
                                }
                            }
                            else
                            {
                                if (Drawer_Activity.main_layout!=null)
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                                    errorDialogClass.showDialog(context.getResources().getString(R.string.get_online_first),
                                            context.getString(R.string.info_message));

                                }
                            }
                        }
                        else
                        {
                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(context);
                            errorDialogClass.showDialog(context.getResources().getString(R.string.first_you_have_finish_your_job),
                                    context.getString(R.string.info_message));
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }
        });
    }


    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}