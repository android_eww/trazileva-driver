package com.connectkargo.driver.Adapter;


import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Been.Transfer_History_Been;
import com.connectkargo.driver.R;

import java.util.List;

public class Transfer_History_Adapter extends RecyclerView.Adapter<Transfer_History_Adapter.MyViewHolder> {

    private Context mContext;
    private List<Transfer_History_Been> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_historyName, tv_historyDateTime, tv_historyMoney, tv_pending, tv_TransactionFail;
        LinearLayout rl_row_card;

        public MyViewHolder(View view) {
            super(view);

            tv_historyName = (TextView) view.findViewById(R.id.tv_historyName);
            tv_historyDateTime = (TextView) view.findViewById(R.id.tv_historyDateTime);
            tv_historyMoney = (TextView) view.findViewById(R.id.tv_historyMoney);
            tv_pending = (TextView) view.findViewById(R.id.tv_pending);
            tv_TransactionFail = (TextView) view.findViewById(R.id.tv_TransactionFail);
            rl_row_card = (LinearLayout) view.findViewById(R.id.rl_row_card);
        }
    }

    public Transfer_History_Adapter(Context mContext, List<Transfer_History_Been> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_transfer_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {
        holder.tv_historyName.setText(list.get(position).getHistory_Name());
        holder.tv_historyDateTime.setText(list.get(position).getHistoryDate_Time());


        String mm = "";

        if (list.get(position).getHistory_Money()!=null && !list.get(position).getHistory_Money().trim().equalsIgnoreCase(""))
        {
            mm = String.format("%.2f",Double.parseDouble(list.get(position).getHistory_Money().trim()));
            holder.tv_historyMoney.setText(list.get(position).getType()+" " + mContext.getResources().getString(R.string.currency) + " " + mm);
        }

        if (list.get(position).getStatus()!=null && !list.get(position).getStatus().equalsIgnoreCase(""))
        {
            if (list.get(position).getStatus().equalsIgnoreCase("pending"))
            {
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.colorRed));
                holder.tv_pending.setVisibility(View.VISIBLE);
                holder.tv_TransactionFail.setVisibility(View.GONE);
            }
            else if (list.get(position).getStatus().equalsIgnoreCase("failed"))
            {
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.colorRed));
                holder.tv_pending.setVisibility(View.GONE);
                holder.tv_TransactionFail.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.mediumGray2));
                holder.tv_pending.setVisibility(View.GONE);
                holder.tv_TransactionFail.setVisibility(View.GONE);
            }
        }
        else
        {
            holder.tv_pending.setVisibility(View.GONE);
            holder.tv_TransactionFail.setVisibility(View.GONE);
            if (list.get(position).getType()!=null && !list.get(position).getType().equalsIgnoreCase(""))
            {
                if (list.get(position).getType().contains("+"))
                {
                    holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.green));
                }
                else
                {
                    holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.mediumGray2));
                }
            }
            else
            {
                holder.tv_historyMoney.setTextColor(ContextCompat.getColor(mContext, R.color.mediumGray2));
            }
        }


        holder.rl_row_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}