package com.connectkargo.driver.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Been.VehicleType_Been;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.SnackbarUtils;

import java.util.ArrayList;

public class CarType_Multi_Selection_Adapter extends BaseAdapter {

    private Activity activity;
    private static LayoutInflater inflater=null;
    ArrayList<VehicleType_Been> list_vehicleItem;
    LinearLayout ll_dialog;
    int MINIMUM_CHECK_BOX_SELECTED=3;

    public CarType_Multi_Selection_Adapter(Activity a, ArrayList<VehicleType_Been> list_vehicleItem, LinearLayout ll_dialog)
    {
        activity = a;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list_vehicleItem = list_vehicleItem;
        this.ll_dialog =  ll_dialog;
    }

    public int getCount() {
        return list_vehicleItem.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View vi=convertView;
        LinearLayout row_vehicleType_item;
        TextView tv_vehicle_type, tv_vehicle_description;
        final CheckBox ckBox_vehi_type;

        if(convertView==null)
        {
            vi = inflater.inflate(R.layout.listview_row_cartype_multi_selection, null);
        }

        row_vehicleType_item = (LinearLayout) vi.findViewById(R.id.row_vehicleType_item);

        tv_vehicle_type = (TextView)vi.findViewById(R.id.tv_vehicle_type);
        tv_vehicle_description = (TextView)vi.findViewById(R.id.tv_vehicle_description);

        ckBox_vehi_type = (CheckBox)vi.findViewById(R.id.ckBox_vehi_type);
        ckBox_vehi_type.setClickable(false);

        if (list_vehicleItem.get(position).getName()!=null && !list_vehicleItem.get(position).getName().equalsIgnoreCase(""))
        {
            row_vehicleType_item.setVisibility(View.VISIBLE);
            tv_vehicle_type.setText(list_vehicleItem.get(position).getName());
        }
        else
        {
            row_vehicleType_item.setVisibility(View.GONE);
        }

        if (list_vehicleItem.get(position).getDescription()!=null && !list_vehicleItem.get(position).getDescription().equalsIgnoreCase(""))
        {
            tv_vehicle_description.setVisibility(View.GONE);
            tv_vehicle_description.setText(list_vehicleItem.get(position).getDescription());
        }
        else
        {
            tv_vehicle_description.setVisibility(View.GONE);
        }


        if (list_vehicleItem.get(position).getStatus().equalsIgnoreCase("1"))
        {
            ckBox_vehi_type.setChecked(true);
        }
        else
        {
            ckBox_vehi_type.setChecked(false);
        }


        row_vehicleType_item.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int CheckedNumber = 0;
                for (int i=0; i<list_vehicleItem.size() ; i++)
                {
                    if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
                    {
                        CheckedNumber++;
                    }
                }

                if (ckBox_vehi_type.isChecked())
                {
                    ckBox_vehi_type.setChecked(false);
                    list_vehicleItem.get(position).setStatus("0");
                }
                else
                {
                    if (CheckedNumber<MINIMUM_CHECK_BOX_SELECTED)
                    {
                        ckBox_vehi_type.setChecked(true);
                        list_vehicleItem.get(position).setStatus("1");
                    }
                    else
                    {
                        new SnackbarUtils(ll_dialog, activity.getResources().getString(R.string.you_can_select_only_three_type),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                    }
                }
            }
        });


        return vi;
    }
}