package com.connectkargo.driver.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Fragment.Past_Job_List_Fragment;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.listner.OnLoadMoreListener;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Past_Job_List_Adapter extends ExpandableRecyclerView.Adapter<ExpandableRecyclerView.ViewHolder>
{
    String TAG = "Past_Job_List_Adapter";
    Context context;
    int POSITION;

    private LinearLayoutManager linearLayoutManager;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = 2;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;


//    public Past_Job_List_Adapter(LinearLayoutManager lm)
//    {
//        super(lm);
//    }

    public Past_Job_List_Adapter(LinearLayoutManager layoutManager, ExpandableRecyclerView recyclerView, Context context)
    {
        super(layoutManager);
        this.context = context;
        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                Log.e(TAG, "call totalItemCount:- " + totalItemCount);
                Log.e(TAG, "call lastVisibleItem:- " + lastVisibleItem);
                Log.e(TAG, "call isLoading:- " + isLoading);

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold))
                {
                    if (Past_Job_List_Fragment.list.size()>0 &&
                            Past_Job_List_Fragment.list.get(Past_Job_List_Fragment.list.size()-1) != null &&
                            onLoadMoreListener != null)
                    {
                        if (Past_Job_List_Fragment.list.size()<10)
                        {
                            isLoading = false;
                        }
                        else
                        {
                            onLoadMoreListener.onLoadMore();
                            isLoading = true;
                        }
                    }
                }
                else
                {
                    isLoading = false;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener)
    {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position)
    {
        return Past_Job_List_Fragment.list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public ExpandableRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
//        this.context = parent.getContext();
//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        View v = inflater.inflate(R.layout.row_layout_past_job, parent, false);
//
//        Past_Job_List_Adapter.ViewHolder vh = new Past_Job_List_Adapter.ViewHolder(v);
//        return vh;

        if (viewType == VIEW_TYPE_ITEM)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_past_job, parent, false);
            Log.e(TAG,"VIEW_TYPE_ITEM data is load");
            return new MyViewHolder(view);

        }
        else if (viewType == VIEW_TYPE_LOADING)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            Log.e(TAG,"VIEW_TYPE_LOADING Please wait data load");
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final ExpandableRecyclerView.ViewHolder viewHolder, final int position)
    {
        super.onBindViewHolder(viewHolder, position);
        POSITION = position;

        if (viewHolder instanceof MyViewHolder)
        {
            final MyViewHolder holder = (MyViewHolder) viewHolder;

            if (Past_Job_List_Fragment.list.get(position).getDropoffLocation()!=null && !Past_Job_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
            {
                holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
                holder.tv_drop_off_location.setText(Past_Job_List_Fragment.list.get(position).getDropoffLocation());
            }
            else
            {
                holder.ll_dropOffLoc.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getId()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getId().equalsIgnoreCase("") &&
                    Past_Job_List_Fragment.list.get(position).getBookingType()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getBookingType().equalsIgnoreCase(""))
            {
                holder.ll_BookingId.setVisibility(View.VISIBLE);
                if(Past_Job_List_Fragment.list.get(position).getBookingType().equalsIgnoreCase("Book Now")) {
                    holder.tv_BookingId.setText(Past_Job_List_Fragment.list.get(position).getId() +
                            " (" + context.getResources().getString(R.string.bookNow) + ")");
                } else {
                    holder.tv_BookingId.setText(Past_Job_List_Fragment.list.get(position).getId() +
                            " (" + context.getResources().getString(R.string.bookLater) + ")");
                }
            }
            else
            {
                holder.ll_BookingId.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getRequestFor()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getRequestFor().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getRequestFor().equalsIgnoreCase("taxi"))
            {
                holder.ll_parcelName.setVisibility(View.GONE);
                holder.ll_Labour.setVisibility(View.GONE);

                if (Past_Job_List_Fragment.list.get(position).getParcelName()!=null &&
                        !Past_Job_List_Fragment.list.get(position).getParcelName().equalsIgnoreCase("") &&
                        !Past_Job_List_Fragment.list.get(position).getParcelName().equalsIgnoreCase("null"))
                {
                    holder.tv_parcelName.setText(Past_Job_List_Fragment.list.get(position).getParcelName());
                }
                else
                {
                    holder.ll_parcelName.setVisibility(View.GONE);
                }

                if (Past_Job_List_Fragment.list.get(position).getLabour()!=null &&
                        !Past_Job_List_Fragment.list.get(position).getLabour().equalsIgnoreCase("") &&
                        !Past_Job_List_Fragment.list.get(position).getLabour().equalsIgnoreCase("null") &&
                        !Past_Job_List_Fragment.list.get(position).getLabour().equalsIgnoreCase("0"))
                {
                    holder.tv_Labour.setText(Past_Job_List_Fragment.list.get(position).getLabour());
                }
                else
                {
                    holder.ll_Labour.setVisibility(View.GONE);
                }
            }
            else
            {
                holder.ll_parcelName.setVisibility(View.GONE);
                holder.ll_Labour.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getPickupLocation()!=null && !Past_Job_List_Fragment.list.get(position).getPickupLocation().equalsIgnoreCase(""))
            {
                holder.ll_pickUpLoc.setVisibility(View.VISIBLE);
                holder.tv_pickupLocation.setText(Past_Job_List_Fragment.list.get(position).getPickupLocation());
            }
            else
            {
                holder.ll_pickUpLoc.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getPickupDateTime()!=null && !Past_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase(""))
            {
                holder.ll_dateTime.setVisibility(View.VISIBLE);
                Log.e("pickUpDateTime","pickUpDateTime : "+Past_Job_List_Fragment.list.get(position).getPickupDateTime());
                holder.tv_PickUp_dateTime.setText(Past_Job_List_Fragment.list.get(position).getPickupDateTime());
            }
            else
            {
                holder.ll_dateTime.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getPassengerName()!=null && !Past_Job_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
            {
                holder.tv_PassengerName.setVisibility(View.VISIBLE);
                holder.tv_PassengerName.setText(Past_Job_List_Fragment.list.get(position).getPassengerName());
            }
            else
            {
                holder.tv_PassengerName.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getTripDistance()!=null && !Past_Job_List_Fragment.list.get(position).getTripDistance().equalsIgnoreCase(""))
            {
                holder.ll_tripDistance.setVisibility(View.VISIBLE);
                holder.tv_tripDistance.setText(Past_Job_List_Fragment.list.get(position).getTripDistance()+" km");
            }
            else
            {
                holder.ll_tripDistance.setVisibility(View.GONE);
                holder.tv_tripDistance.setText(0.00+" km");
            }

            if (Past_Job_List_Fragment.list.get(position).getStatus()!=null && !Past_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase(""))
            {
                holder.ll_TripStatus.setVisibility(View.VISIBLE);
                holder.tv_TripStatus.setAllCaps(true);
                holder.tv_TripStatus.setText(Past_Job_List_Fragment.list.get(position).getStatus());
                if (Past_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("completed"))
                {
                    holder.ll_tripDistance.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder.ll_tripDistance.setVisibility(View.GONE);
                }
            }
            else
            {
                holder.ll_TripStatus.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getModel()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getModel().equalsIgnoreCase(""))
            {
                holder.ll_carModel.setVisibility(View.VISIBLE);
                holder.tv_carModel.setText(Past_Job_List_Fragment.list.get(position).getModel());
            }
            else
            {
                holder.ll_carModel.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getHeight()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getHeight().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getHeight().equalsIgnoreCase("null") &&
                    !Past_Job_List_Fragment.list.get(position).getHeight().equalsIgnoreCase("0") &&
                    Past_Job_List_Fragment.list.get(position).getLength()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getLength().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getLength().equalsIgnoreCase("null") &&
                    !Past_Job_List_Fragment.list.get(position).getLength().equalsIgnoreCase("0") &&
                    Past_Job_List_Fragment.list.get(position).getBreadth()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getBreadth().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getBreadth().equalsIgnoreCase("null") &&
                    !Past_Job_List_Fragment.list.get(position).getBreadth().equalsIgnoreCase("0"))
            {
                holder.llSize.setVisibility(View.VISIBLE);
                String size = "(" + Past_Job_List_Fragment.list.get(position).getHeight() + " * " +
                        Past_Job_List_Fragment.list.get(position).getLength() + " * " +
                        Past_Job_List_Fragment.list.get(position).getBreadth() + ")";
                holder.tvSize.setText(size);
            }
            else
            {
                holder.llSize.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getWeigth()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getWeigth().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getWeigth().equalsIgnoreCase("null") &&
                    !Past_Job_List_Fragment.list.get(position).getWeigth().equalsIgnoreCase("0"))
            {
                holder.llWeight.setVisibility(View.VISIBLE);
                holder.tvWeight.setText(Past_Job_List_Fragment.list.get(position).getWeigth());
            }
            else
            {
                holder.llWeight.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getQuantity()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getQuantity().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getQuantity().equalsIgnoreCase("null") &&
                    !Past_Job_List_Fragment.list.get(position).getQuantity().equalsIgnoreCase("0"))
            {
                holder.llQuantity.setVisibility(View.VISIBLE);
                holder.tvQuantity.setText(Past_Job_List_Fragment.list.get(position).getQuantity());
            }
            else
            {
                holder.llQuantity.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getReceiverName()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getReceiverName().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getReceiverName().equalsIgnoreCase("null"))
            {
                holder.llPassengerName.setVisibility(View.VISIBLE);
                holder.tvPassengerName.setText(Past_Job_List_Fragment.list.get(position).getReceiverName());
            }
            else
            {
                holder.llPassengerName.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getReceiverEmail()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getReceiverEmail().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getReceiverEmail().equalsIgnoreCase("null"))
            {
                holder.ll_passengerEmail.setVisibility(View.VISIBLE);
                holder.tv_passengerEmail.setText(Past_Job_List_Fragment.list.get(position).getReceiverEmail());
            }
            else
            {
                holder.ll_passengerEmail.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getReceiverContactNo()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getReceiverContactNo().equalsIgnoreCase("") &&
                    !Past_Job_List_Fragment.list.get(position).getReceiverContactNo().equalsIgnoreCase("null"))
            {
                holder.ll_passengerNo.setVisibility(View.VISIBLE);
                holder.tv_passengerNo.setText(Past_Job_List_Fragment.list.get(position).getReceiverContactNo());
            }
            else
            {
                holder.ll_passengerNo.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getDropTime()!=null && !Past_Job_List_Fragment.list.get(position).getDropTime().equalsIgnoreCase(""))
            {
                holder.ll_dropOffTime.setVisibility(View.VISIBLE);
                String getDropTime = getDate(Long.parseLong(Past_Job_List_Fragment.list.get(position).getDropTime()+"000"), "yyyy-MM-dd HH:mm:ss");
                Log.e("pickUpDateTime","getDropTime : "+getDropTime);
                holder.tv_dropOffTime.setText(getDropTime);
            }
            else
            {
                holder.ll_dropOffTime.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getTripDuration()!=null && !Past_Job_List_Fragment.list.get(position).getTripDuration().equalsIgnoreCase(""))
            {
                long totalSecs = Long.parseLong(Past_Job_List_Fragment.list.get(position).getTripDuration());
                holder.ll_tripDuration.setVisibility(View.VISIBLE);
                long hours = totalSecs / 3600;
                long minutes = (totalSecs % 3600) / 60;
                long seconds = totalSecs % 60;

                String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
//            holder.tv_tripDuration.setText(Past_Job_List_Fragment.list.get(position).getTripDuration());
                holder.tv_tripDuration.setText(timeString);
            }
            else
            {
                holder.ll_tripDuration.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getNightFare()!=null && !Past_Job_List_Fragment.list.get(position).getNightFare().equalsIgnoreCase(""))
            {
                holder.ll_nightFare.setVisibility(View.GONE);
                holder.tv_nightFare.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getNightFare()));
            }
            else
            {
                holder.ll_nightFare.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getTripFare()!=null && !Past_Job_List_Fragment.list.get(position).getTripFare().equalsIgnoreCase(""))
            {
                holder.ll_tripFare.setVisibility(View.VISIBLE);
                holder.tv_tripFare.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getTripFare()));
            }
            else
            {
                holder.ll_tripFare.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getWaitingTime()!=null && !Past_Job_List_Fragment.list.get(position).getWaitingTime().equalsIgnoreCase(""))
            {
                holder.ll_waitingTime.setVisibility(View.GONE);
//            holder.tv_waitingTime.setText(Past_Job_List_Fragment.list.get(position).getWaitingTime());
                double douSec = Double.parseDouble(Past_Job_List_Fragment.list.get(position).getWaitingTime());
                long totalSecs = (long) douSec;
                holder.ll_tripDuration.setVisibility(View.GONE);
                long hours = totalSecs / 3600;
                long minutes = (totalSecs % 3600) / 60;
                long seconds = totalSecs % 60;

                String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
//            holder.tv_tripDuration.setText(Past_Job_List_Fragment.list.get(position).getTripDuration());
                holder.tv_waitingTime.setText(timeString);
            }
            else
            {
                holder.ll_waitingTime.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getWaitingTimeCost()!=null && !Past_Job_List_Fragment.list.get(position).getWaitingTimeCost().equalsIgnoreCase(""))
            {
                holder.ll_waitingTimeCost.setVisibility(View.GONE);
                holder.tv_waitingTimeCost.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getWaitingTimeCost()));
            }
            else
            {
                holder.ll_waitingTimeCost.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getTollFee()!=null && !Past_Job_List_Fragment.list.get(position).getTollFee().equalsIgnoreCase(""))
            {
                holder.ll_tollFee.setVisibility(View.GONE);
                holder.tv_tollFee.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getTollFee()));
            }
            else
            {
                holder.ll_tollFee.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getBookingCharge()!=null && !Past_Job_List_Fragment.list.get(position).getBookingCharge().equalsIgnoreCase(""))
            {
                holder.ll_BookingCharge.setVisibility(View.VISIBLE);
                holder.tv_BookingCharge.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getBookingCharge()));
            }
            else
            {
                holder.ll_BookingCharge.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getTax()!=null && !Past_Job_List_Fragment.list.get(position).getTax().equalsIgnoreCase(""))
            {
                holder.ll_tax.setVisibility(View.VISIBLE);
                holder.tv_tax.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getTax()));
            }
            else
            {
                holder.ll_tax.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getPromoCode()!=null && !Past_Job_List_Fragment.list.get(position).getPromoCode().equalsIgnoreCase(""))
            {
                holder.ll_promocode.setVisibility(View.VISIBLE);
                holder.tv_promocode.setText(Past_Job_List_Fragment.list.get(position).getPromoCode());
            }
            else
            {
                holder.ll_promocode.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getDiscount()!=null && !Past_Job_List_Fragment.list.get(position).getDiscount().equalsIgnoreCase(""))
            {
                holder.ll_discount.setVisibility(View.VISIBLE);
                holder.tv_discount.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getDiscount()));
            }
            else
            {
                holder.ll_discount.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getSubTotal()!=null && !Past_Job_List_Fragment.list.get(position).getSubTotal().equalsIgnoreCase(""))
            {
                holder.ll_subTotal.setVisibility(View.GONE);
                holder.tv_subTotal.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getSubTotal()));
            }
            else
            {
                holder.ll_subTotal.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getGrandTotal()!=null && !Past_Job_List_Fragment.list.get(position).getGrandTotal().equalsIgnoreCase(""))
            {
                holder.ll_grandTotal.setVisibility(View.VISIBLE);
                holder.tv_grandTotal.setText(Global.commaFormat(Past_Job_List_Fragment.list.get(position).getGrandTotal()));
            }
            else
            {
                holder.ll_grandTotal.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getPaymentType()!=null && !Past_Job_List_Fragment.list.get(position).getPaymentType().equalsIgnoreCase(""))
            {
                holder.ll_PaymentType.setVisibility(View.VISIBLE);
                holder.tv_PaymentType.setText(Past_Job_List_Fragment.list.get(position).getPaymentType());
            }
            else
            {
                holder.ll_PaymentType.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getFlightNumber()!=null && !Past_Job_List_Fragment.list.get(position).getFlightNumber().equalsIgnoreCase(""))
            {
                holder.ll_passengerFlightNo.setVisibility(View.GONE);
                holder.tv_passengerFlightNo.setText(Past_Job_List_Fragment.list.get(position).getFlightNumber());
            }
            else
            {
                holder.ll_passengerFlightNo.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getNotes()!=null && !Past_Job_List_Fragment.list.get(position).getNotes().equalsIgnoreCase(""))
            {
                holder.ll_passengerNote.setVisibility(View.VISIBLE);
                holder.tv_passengerNote.setText(Past_Job_List_Fragment.list.get(position).getNotes());
            }
            else
            {
                holder.ll_passengerNote.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getDispatherFullname()!=null && !Past_Job_List_Fragment.list.get(position).getDispatherFullname().equalsIgnoreCase(""))
            {
                holder.ll_dispatcherName.setVisibility(View.GONE);
                holder.tv_dispatcherName.setText(Past_Job_List_Fragment.list.get(position).getDispatherFullname());
            }
            else
            {
                holder.ll_dispatcherName.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getDispatherEmail()!=null && !Past_Job_List_Fragment.list.get(position).getDispatherEmail().equalsIgnoreCase(""))
            {
                holder.ll_dispatcherEmail.setVisibility(View.GONE);
                holder.tv_dispatcherEmail.setText(Past_Job_List_Fragment.list.get(position).getDispatherEmail());
            }
            else
            {
                holder.ll_dispatcherEmail.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getDispatherMobileNo()!=null && !Past_Job_List_Fragment.list.get(position).getDispatherMobileNo().equalsIgnoreCase(""))
            {
                holder.ll_dispatcherNo.setVisibility(View.GONE);
                holder.tv_dispatcherNo.setText(Past_Job_List_Fragment.list.get(position).getDispatherMobileNo());
            }
            else
            {
                holder.ll_dispatcherNo.setVisibility(View.GONE);
            }

            if (Past_Job_List_Fragment.list.get(position).getParcelImage()!=null &&
                    !Past_Job_List_Fragment.list.get(position).getParcelImage().equalsIgnoreCase("")) {
                Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE + Past_Job_List_Fragment.list.get(position).getParcelImage()).into(holder.ivParcel);
            } else {
                holder.llImg.setVisibility(View.GONE);
            }

            if(Past_Job_List_Fragment.list.get(position).getCompanyAmount() != null &&
                    !Past_Job_List_Fragment.list.get(position).getCompanyAmount().equalsIgnoreCase("")
                    && !Past_Job_List_Fragment.list.get(position).getCompanyAmount().equalsIgnoreCase("null")){
                holder.tv_earning.setText(context.getResources().getString(R.string.currency) +" " +
                        Global.commaFormat(Past_Job_List_Fragment.list.get(position).getCompanyAmount()));
            }else {
                holder.llEarning.setVisibility(View.GONE);
            }

            if(Past_Job_List_Fragment.list.get(position).getAdminAmount() != null &&
                    !Past_Job_List_Fragment.list.get(position).getAdminAmount().equalsIgnoreCase("")
                    && !Past_Job_List_Fragment.list.get(position).getAdminAmount().equalsIgnoreCase("null")){
                holder.tv_commission.setText(context.getResources().getString(R.string.currency) +" " +
                        Global.commaFormat(Past_Job_List_Fragment.list.get(position).getAdminAmount()));
            }else {
                holder.llCommission.setVisibility(View.GONE);
            }
        }
        else if (viewHolder instanceof LoadingViewHolder)
        {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewHolder;

            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return Past_Job_List_Fragment.list.size();
    }

    public void setLoaded()
    {
        isLoading = false;
    }

    private class LoadingViewHolder extends ExpandableRecyclerView.ViewHolder
    {
        public View layout;

        //header
        public ProgressBar progressBar;

        private ExpandableItem expandableItem;

        public LoadingViewHolder(View view)
        {
            super(view);

            layout = view;
            expandableItem = (ExpandableItem) view. findViewById(R.id.row);

            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }

    public class MyViewHolder extends ExpandableRecyclerView.ViewHolder
    {
        public View layout;

        LinearLayout ll_row_item_pastJob, ll_pickUpLoc, ll_passengerNo, ll_tripDistance, ll_carModel, ll_BookingId, ll_dropOffLoc,
                ll_dateTime, llPassengerName, ll_passengerEmail, ll_dropOffTime, ll_tripDuration, ll_nightFare, ll_tripFare, ll_waitingTime,
                ll_waitingTimeCost,ll_tollFee, ll_BookingCharge, ll_tax, ll_promocode, ll_discount, ll_subTotal, ll_grandTotal,
                ll_TripStatus, ll_PaymentType, ll_passengerFlightNo, ll_passengerNote,ll_dispatcherName, ll_dispatcherEmail, ll_dispatcherNo,
                ll_parcelName, ll_Labour, llSize, llWeight, llQuantity;

        public TextView tv_drop_off_location, tv_pickupLocation, tv_PickUp_dateTime, tv_PassengerName, tv_passengerNo, tv_tripDistance,
                tv_carModel, tv_BookingId, tvPassengerName, tv_passengerEmail, tv_dropOffTime, tv_tripDuration, tv_nightFare, tv_tripFare, tv_waitingTime,
                tv_waitingTimeCost,tv_tollFee, tv_BookingCharge, tv_tax, tv_promocode, tv_discount, tv_subTotal, tv_grandTotal,
                tv_TripStatus, tv_PaymentType, tv_passengerFlightNo, tv_passengerNote,tv_dispatcherName, tv_dispatcherEmail, tv_dispatcherNo,
                tv_parcelName, tv_Labour, tvSize, tvWeight, tvQuantity;

        ExpandableItem expandableItem;

        ImageView ivParcel;
        LinearLayout llImg;

        LinearLayout llEarning,llCommission;
        TextView tv_earning,tv_commission;

        public MyViewHolder(View view)
        {
            super(view);

            layout = view;
            expandableItem = view. findViewById(R.id.row);

            tv_drop_off_location = expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_pickupLocation = expandableItem.findViewById(R.id.tv_pickupLocation);
            tv_BookingId = expandableItem.findViewById(R.id.tv_BookingId);
            tv_PickUp_dateTime = expandableItem.findViewById(R.id.tv_PickUp_dateTime);
            tv_PassengerName = expandableItem.findViewById(R.id.tv_PassengerName);
            tv_passengerNo = expandableItem.findViewById(R.id.tv_passengerNo);
            tv_tripDistance = expandableItem.findViewById(R.id.tv_tripDistance);
            tv_carModel = expandableItem.findViewById(R.id.tv_carModel);
            tv_passengerEmail = expandableItem.findViewById(R.id.tv_passengerEmail);
            tvPassengerName = expandableItem.findViewById(R.id.tvPassengerName);

            tv_dropOffTime = expandableItem.findViewById(R.id.tv_dropOffTime);
            tv_tripDuration = expandableItem.findViewById(R.id.tv_tripDuration);
            tv_nightFare = expandableItem.findViewById(R.id.tv_nightFare);
            tv_tripFare = expandableItem.findViewById(R.id.tv_tripFare);
            tv_waitingTime = expandableItem.findViewById(R.id.tv_waitingTime);

            tv_waitingTimeCost = expandableItem.findViewById(R.id.tv_waitingTimeCost);
            tv_tollFee = expandableItem.findViewById(R.id.tv_tollFee);
            tv_BookingCharge = expandableItem.findViewById(R.id.tv_BookingCharge);
            tv_tax = expandableItem.findViewById(R.id.tv_tax);
            tv_promocode = expandableItem.findViewById(R.id.tv_promocode);
            tv_parcelName = expandableItem.findViewById(R.id.tv_parcelName);
            tv_Labour = expandableItem.findViewById(R.id.tv_Labour);

            tv_discount = expandableItem.findViewById(R.id.tv_discount);
            tv_subTotal = expandableItem.findViewById(R.id.tv_subTotal);
            tv_grandTotal = expandableItem.findViewById(R.id.tv_grandTotal);
            tv_TripStatus = expandableItem.findViewById(R.id.tv_TripStatus);
            tv_PaymentType = expandableItem.findViewById(R.id.tv_PaymentType);
            tv_passengerFlightNo = expandableItem.findViewById(R.id.tv_passengerFlightNo);
            tv_passengerNote = expandableItem.findViewById(R.id.tv_passengerNote);
            tv_dispatcherName = expandableItem.findViewById(R.id.tv_dispatcherName);
            tv_dispatcherEmail = expandableItem.findViewById(R.id.tv_dispatcherEmail);
            tv_dispatcherNo = expandableItem.findViewById(R.id.tv_dispatcherNo);
            tvSize = expandableItem.findViewById(R.id.tvSize);
            tvWeight = expandableItem.findViewById(R.id.tvWeight);
            tvQuantity = expandableItem.findViewById(R.id.tvQuantity);


            ll_row_item_pastJob = expandableItem.findViewById(R.id.ll_row_item_pastJob);
            ll_pickUpLoc = expandableItem.findViewById(R.id.ll_pickUpLoc);
            ll_passengerNo = expandableItem.findViewById(R.id.ll_passengerNo);
            ll_tripDistance = expandableItem.findViewById(R.id.ll_tripDistance);
            ll_carModel = expandableItem.findViewById(R.id.ll_carModel);
            ll_BookingId = expandableItem.findViewById(R.id.ll_BookingId);
            ll_dropOffLoc = expandableItem.findViewById(R.id.ll_dropOffLoc);
            ll_dateTime = expandableItem.findViewById(R.id.ll_dateTime);
            llPassengerName = expandableItem.findViewById(R.id.llPassengerName);
            ll_passengerEmail = expandableItem.findViewById(R.id.ll_passengerEmail);

            ll_dropOffTime = expandableItem.findViewById(R.id.ll_dropOffTime);
            ll_tripDuration = expandableItem.findViewById(R.id.ll_tripDuration);
            ll_nightFare = expandableItem.findViewById(R.id.ll_nightFare);
            ll_tripFare = expandableItem.findViewById(R.id.ll_tripFare);
            ll_waitingTime = expandableItem.findViewById(R.id.ll_waitingTime);

            ll_waitingTimeCost = expandableItem.findViewById(R.id.ll_waitingTimeCost);
            ll_tollFee = expandableItem.findViewById(R.id.ll_tollFee);
            ll_BookingCharge = expandableItem.findViewById(R.id.ll_BookingCharge);
            ll_tax = expandableItem.findViewById(R.id.ll_tax);
            ll_promocode = expandableItem.findViewById(R.id.ll_promocode);

            ll_discount = expandableItem.findViewById(R.id.ll_discount);
            ll_subTotal = expandableItem.findViewById(R.id.ll_subTotal);
            ll_grandTotal = expandableItem.findViewById(R.id.ll_grandTotal);
            ll_TripStatus = expandableItem.findViewById(R.id.ll_TripStatus);
            ll_PaymentType = expandableItem.findViewById(R.id.ll_PaymentType);
            ll_passengerFlightNo = expandableItem.findViewById(R.id.ll_passengerFlightNo);
            ll_passengerNote = expandableItem.findViewById(R.id.ll_passengerNote);

            ll_dispatcherName = expandableItem.findViewById(R.id.ll_dispatcherName);
            ll_dispatcherEmail = expandableItem.findViewById(R.id.ll_dispatcherEmail);
            ll_dispatcherNo = expandableItem.findViewById(R.id.ll_dispatcherNo);
            ll_parcelName = expandableItem.findViewById(R.id.ll_parcelName);
            ll_Labour = expandableItem.findViewById(R.id.ll_Labour);
            llSize = expandableItem.findViewById(R.id.llSize);
            llWeight = expandableItem.findViewById(R.id.llWeight);
            llQuantity = expandableItem.findViewById(R.id.llQuantity);

            ivParcel = expandableItem.findViewById(R.id.ivParcel);
            llImg = expandableItem.findViewById(R.id.llImg);

            llEarning = expandableItem.findViewById(R.id.llEarning);
            llCommission = expandableItem.findViewById(R.id.llCommission);
            tv_earning = expandableItem.findViewById(R.id.tv_earning);
            tv_commission = expandableItem.findViewById(R.id.tv_commission);
        }
    }
}