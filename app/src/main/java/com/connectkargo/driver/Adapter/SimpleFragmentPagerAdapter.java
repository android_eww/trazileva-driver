package com.connectkargo.driver.Adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.connectkargo.driver.Fragment.MyBidFragment;
import com.connectkargo.driver.Fragment.OpenBidFragment;
import com.connectkargo.driver.R;

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter
{
    Context context;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fragmentManager)
    {
        super(fragmentManager);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position)
    {
        if(position == 0)
        {
            return new MyBidFragment();
        }
        else if(position == 1)
        {
            return new OpenBidFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return context.getString(R.string.my_bid);
            case 1:
                return context.getString(R.string.open_bid);
            default:
                return null;
        }
    }
}

