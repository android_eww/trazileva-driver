package com.connectkargo.driver.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.connectkargo.driver.Been.ChatBeen;
import com.connectkargo.driver.R;

import java.util.ArrayList;

public class BidChatAdapter extends RecyclerView.Adapter
{
    Context context;
    ArrayList<ChatBeen> list;

    public BidChatAdapter(Context activity, ArrayList<ChatBeen> list)
    {
        context = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder)holder;

            holder1.tv_SenderMessage.setText(list.get(position).getMessage());
            holder1.tv_ReceiverMessage.setText(list.get(position).getMessage());
            holder1.tv_SenderTime.setText(list.get(position).getDate());
            holder1.tv_ReceiverTime.setText(list.get(position).getDate());


            if (list.get(position).getSender().toLowerCase().equalsIgnoreCase("passenger"))
            {
                holder1.ll_SenderMain.setVisibility(View.GONE);
                holder1.ll_ReceiverMain.setVisibility(View.VISIBLE);
                if (position==0)
                {
                    holder1.ll_ReceiverBG.setBackgroundResource(R.drawable.ans_side_image_1);
                }
                else if (position>0 && !list.get(position).getSender().equalsIgnoreCase(list.get(position - 1).getSender()))
                {
                    holder1.ll_ReceiverBG.setBackgroundResource(R.drawable.ans_side_image_1);
                }
                else
                {
                    holder1.ll_ReceiverBG.setBackgroundResource(R.drawable.ans_side_image_2);
                }
            }
            else
            {
                holder1.ll_SenderMain.setVisibility(View.VISIBLE);
                holder1.ll_ReceiverMain.setVisibility(View.GONE);
                if (position==0)
                {
                    holder1.ll_SenderBG.setBackgroundResource(R.drawable.blue_chat_box);
                }
                else if (position>0 && !list.get(position).getSender().equalsIgnoreCase(list.get(position - 1).getSender()))
                {
                    holder1.ll_SenderBG.setBackgroundResource(R.drawable.blue_chat_box);
                }
                else
                {
                    holder1.ll_SenderBG.setBackgroundResource(R.drawable.blue_chat_box2);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tv_SenderMessage,tv_SenderTime,tv_ReceiverMessage,tv_ReceiverTime;
        private LinearLayout ll_SenderMain,ll_SenderBG,ll_ReceiverMain,ll_ReceiverBG;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            tv_SenderMessage = itemView.findViewById(R.id.tv_SenderMessage);
            tv_SenderTime = itemView.findViewById(R.id.tv_SenderTime);
            ll_SenderMain = itemView.findViewById(R.id.ll_SenderMain);
            ll_SenderBG = itemView.findViewById(R.id.ll_SenderBG);
            tv_ReceiverMessage = itemView.findViewById(R.id.tv_ReceiverMessage);
            tv_ReceiverTime = itemView.findViewById(R.id.tv_ReceiverTime);
            ll_ReceiverMain = itemView.findViewById(R.id.ll_ReceiverMain);
            ll_ReceiverBG = itemView.findViewById(R.id.ll_ReceiverBG);
        }


    }

}

