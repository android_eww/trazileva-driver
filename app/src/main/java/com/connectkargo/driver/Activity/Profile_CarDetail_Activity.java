package com.connectkargo.driver.Activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.connectkargo.driver.Adapter.CarType_Multi_Selection_Adapter;
import com.connectkargo.driver.Adapter.TruckType_Multi_Selection_Adapter;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Been.TruckType_Been;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Been.VehicleType_Been;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Profile_CarDetail_Activity extends BaseActivity implements View.OnClickListener{

    private String TAG = "Profile_CarDetail_Activity";
    Profile_CarDetail_Activity activity;

    LinearLayout  main_layout, main_layoutDialog, ll_back, ll_toolbar_RightImage, ll_Sos, ll_save;
    ListView dialog_ListView, dialog_ListView_truck;
    TextView tv_register_CarType, tv_toolbarTitle;
    EditText et_vehiRegistrationNo, et_register_CarModel;
    ImageView iv_carImage, iv_toolbar_RightImage;
    public ProgressBar progress_bar;

    AppCompatRadioButton cb_car_service, cb_truck_service;
    String CAR="car", TRUCK="truck", checkBox_Selected ="", USER_PROFILE_CATEGORY="";

    ArrayList<VehicleType_Been> list_vehicleItem = new ArrayList<>();
    CarType_Multi_Selection_Adapter adapter;

    ArrayList<TruckType_Been> list_vehicleItem_turck = new ArrayList<>();
    TruckType_Multi_Selection_Adapter adapterTruck;

    String selectedModelId = "";

    DialogClass dialogClass;
    AQuery aQuery;

    Intent intent;
    ErrorDialogClass errorDialogClass;
    int clickflag = 0;

    List<String> list_deviceModelId ;
    List<String> list_ModelName = new ArrayList<String>();

    public static boolean isUpadet = false;
    public static String sessionIds = "";

    public ImageView ivCarFront, ivCarLeft, ivCarRight;
    public byte[] Car_front_ByteImage=null, Car_left_ByteImage=null, Car_Right_ByteImage=null;
    public static String ImageViewClicked = "", DRIVER_CAR_FRONT="carFront", DRIVER_CAR_LEFT="carLeft", DRIVER_CAR_RIGHT="carRight";
    public static Bitmap bitmapSumsung_CarFront, bitmapSumsung_CarLeft, bitmapSumsung_CarRight;

    private static final int MY_REQUEST_CODE_CAMERA = 101;
    private static final int MY_REQUEST_CODE_STORAGE = 100;
    private int GALLERY = 1, CAMERA = 0, REQUEST_CAMERA_SUMSUNG = 5;
    public String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private Uri imageUri;
    Bitmap bitmapImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car_list_driver);

        activity = Profile_CarDetail_Activity.this;
        clickflag = 0;

        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);
        checkBox_Selected = "";
        USER_PROFILE_CATEGORY = "";
        sessionIds = SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity);

        initUI();
    }

    private void initUI()
    {
        Log.e(TAG,"initUI()");

        ll_back= (LinearLayout) findViewById(R.id.ll_back);
        ll_toolbar_RightImage= (LinearLayout) findViewById(R.id.ll_toolbar_RightImage);
        ll_save = (LinearLayout)  findViewById(R.id.ll_save);

        et_vehiRegistrationNo = (EditText) findViewById(R.id.et_vehiRegistrationNo);
        et_register_CarModel = (EditText) findViewById(R.id.et_register_CarModel);

        tv_register_CarType = (TextView) findViewById(R.id.tv_register_CarType);
        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle. setText(activity.getResources().getString(R.string.vehicle_option));
        iv_carImage = (ImageView) findViewById(R.id.iv_carImage);
        progress_bar = findViewById(R.id.progress_bar);
        iv_toolbar_RightImage = (ImageView) findViewById(R.id.iv_toolbar_RightImage);
        iv_toolbar_RightImage.setVisibility(View.GONE);
        iv_toolbar_RightImage.setImageResource(R.drawable.icon_save_white);
        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        ivCarFront =  findViewById(R.id.ivCarFront);
        ivCarLeft =  findViewById(R.id.ivCarLeft);
        ivCarRight = findViewById(R.id.ivCarRight);

        cb_car_service = (AppCompatRadioButton) findViewById(R.id.cb_car_service);
        cb_truck_service = (AppCompatRadioButton)findViewById(R.id.cb_truck_service);

        tv_register_CarType.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);
        ivCarFront.setOnClickListener(this);
        ivCarLeft.setOnClickListener(this);
        ivCarRight.setOnClickListener(this);

        if (SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity)!= null && !SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity).equalsIgnoreCase(""))
        {
            et_register_CarModel.setText(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity).equalsIgnoreCase(""))
        {
            et_vehiRegistrationNo.setText(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, activity) != null &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, activity).equalsIgnoreCase("") &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, activity).equalsIgnoreCase("null"))
        {
            String VehiImage = SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, activity);

            if(VehiImage!=null && !VehiImage.equalsIgnoreCase(""))
            {
                Picasso.get()
                        .load(VehiImage)
//                        .into(iv_carImage, new Callback()
                        .into(ivCarFront, new Callback()
                        {
                            @Override
                            public void onSuccess()
                            {
                                progress_bar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e)
                            {
                                progress_bar.setVisibility(View.GONE);
                                iv_carImage.setImageResource(R.drawable.ic_car_yellow);
                                ivCarFront.setImageResource(R.drawable.ic_car_yellow);
                            }
                        });
            }
        }
        else
        {
            progress_bar.setVisibility(View.GONE);
            iv_carImage.setImageResource(R.drawable.ic_car_yellow);
            ivCarFront.setImageResource(R.drawable.ic_car_yellow);
        }

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, activity) != null &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, activity).equalsIgnoreCase("") &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, activity).equalsIgnoreCase("null"))
        {
            String VehiImage = SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, activity);

            if(VehiImage!=null && !VehiImage.equalsIgnoreCase(""))
            {
                Picasso.get()
                        .load(VehiImage)
                        .into(ivCarLeft, new Callback()
                        {
                            @Override
                            public void onSuccess()
                            {
                            }

                            @Override
                            public void onError(Exception e)
                            {
                                ivCarLeft.setImageResource(R.drawable.ic_car_yellow);
                            }
                        });
            }
        }
        else
        {
            ivCarLeft.setImageResource(R.drawable.ic_car_yellow);
        }

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, activity) != null &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, activity).equalsIgnoreCase("") &&
                !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, activity).equalsIgnoreCase("null"))
        {
            String VehiImage = SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, activity);

            if(VehiImage!=null && !VehiImage.equalsIgnoreCase(""))
            {
                Picasso.get()
                        .load(VehiImage)
                        .into(ivCarRight, new Callback()
                        {
                            @Override
                            public void onSuccess()
                            {
                            }

                            @Override
                            public void onError(Exception e)
                            {
                                ivCarRight.setImageResource(R.drawable.ic_car_yellow);
                            }
                        });
            }
        }
        else
        {
            ivCarRight.setImageResource(R.drawable.ic_car_yellow);
        }


        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).equalsIgnoreCase(""))
        {
            list_deviceModelId = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).split("\\s*,\\s*"));
        }

//        SetCarType();

        if (SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,activity) != null &&
                !SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,activity).equalsIgnoreCase(""))
        {
            USER_PROFILE_CATEGORY = SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,activity);

            if (USER_PROFILE_CATEGORY.equalsIgnoreCase("1"))
            {
                checkBox_Selected = CAR;
                Log.e(TAG,"initUI() checkBox_Selected:- " + checkBox_Selected);

                selectCar();
            }
            else
            {
                checkBox_Selected = TRUCK;
                Log.e(TAG,"initUI() checkBox_Selected:- " + checkBox_Selected);
                selectTruck();
            }
        }
        else
        {
            Log.e(TAG,"initUI() checkBox_Selected:- null" );
        }


        /*tv_register_CarType.setEnabled(false);
        et_vehiRegistrationNo.setEnabled(false);
        et_register_CarModel.setEnabled(false);
        ll_toolbar_RightImage.setVisibility(View.GONE);*/

        cb_car_service.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tv_register_CarType.setText("");
//                SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,"",activity);
                selectedModelId = "";

                selectCar();
            }
        });
        cb_truck_service.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tv_register_CarType.setText("");
//                SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,"",activity);
                selectedModelId = "";

                selectTruck();
            }
        });

        if (Global.isNetworkconn(activity))
        {
            FindWhichTypeIs_Visible();
        }
        else
        {
            errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
        }
    }

    public void SetCarType()
    {
        list_ModelName.clear();
        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity).equalsIgnoreCase(""))
        {
//            list_ModelName = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity).split("\\s*,\\s*"));
            list_ModelName = new LinkedList<>(Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity).split("\\s*,\\s*")));
        }

        Log.e(TAG,"initUI() list_ModelName size = "+list_ModelName.size());
        Log.e(TAG,"initUI() list_deviceModelId size = "+list_deviceModelId.size());

        String SelectedVehicle = "";
        for (int i=0; i<list_ModelName.size() ; i++)
        {
            if (SelectedVehicle !=null && !SelectedVehicle.equalsIgnoreCase(""))
            {
                SelectedVehicle = SelectedVehicle + "," + list_ModelName.get(i);
                selectedModelId = selectedModelId + "," + list_deviceModelId.get(i);
            }
            else
            {
                SelectedVehicle = list_ModelName.get(i);
                selectedModelId =  list_deviceModelId.get(i);
            }
        }

        if (SelectedVehicle != null && !SelectedVehicle.equalsIgnoreCase(""))
        {
            Log.e(TAG,"initUI() SelectedVehicle:- " + SelectedVehicle);
            tv_register_CarType.setText(SelectedVehicle);
        }
        else
        {
            Log.e(TAG,"initUI() SelectedVehicle:- null");

            tv_register_CarType.setText(getResources().getString(R.string.car_type));
            selectedModelId = "";
        }
    }

    public void selectCar()
    {
        Log.e(TAG,"selectCar()");

        checkBox_Selected = CAR;
        cb_car_service.setChecked(true);
        cb_truck_service.setChecked(false);
        if (USER_PROFILE_CATEGORY.equalsIgnoreCase("1"))
        {
            SetCarType();
        }
    }

    public void selectTruck()
    {
        Log.e(TAG,"selectTruck()");

        checkBox_Selected = TRUCK;
        cb_car_service.setChecked(false);
        cb_truck_service.setChecked(true);
        if (USER_PROFILE_CATEGORY.equalsIgnoreCase("2"))
        {
            SetCarType();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.tv_register_CarType:
                /*if (SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity)!=null && !SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity).equalsIgnoreCase("")
                        && !SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity).equalsIgnoreCase("0"))
                {
                    errorDialogClass.showDialog("First, you have to please stop your private meter trip.", getString(R.string.info_message));
                }
                else
                {*/
                    SelectCarOrTruckType();
                //}
                break;

            case R.id.ll_save:

                if (ConnectivityReceiver.isConnected())
                {
                    CheckData();
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet),
                            getString(R.string.internet_error_message));
                }
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;

            case R.id.ivCarFront:
                ImageViewClicked = DRIVER_CAR_FRONT;
                ShowPictureDialog();
                break;

            case R.id.ivCarLeft:
                ImageViewClicked = DRIVER_CAR_LEFT;
                ShowPictureDialog();
                break;

            case R.id.ivCarRight:
                ImageViewClicked = DRIVER_CAR_RIGHT;
                ShowPictureDialog();
                break;
        }
    }

    private void SelectCarOrTruckType()
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_vehicle_type);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        ImageView dialogClose;
        final LinearLayout dialog_ok_layout, ll_select_cars, ll_select_truck;

        main_layoutDialog = (LinearLayout) dialog.findViewById(R.id.main_layoutDialog);

        dialog_ListView = (ListView) dialog.findViewById(R.id.lv_vehicle_type);
        dialog_ListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        dialog_ListView_truck = (ListView) dialog.findViewById(R.id.lv_truck_type);
        dialog_ListView_truck.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        adapter = new CarType_Multi_Selection_Adapter(activity, list_vehicleItem, main_layoutDialog);
        dialog_ListView.setAdapter(adapter);

        adapterTruck = new TruckType_Multi_Selection_Adapter(activity, list_vehicleItem_turck, main_layoutDialog);
        dialog_ListView_truck.setAdapter(adapterTruck);

        dialogClose = (ImageView) dialog.findViewById(R.id.dialog_close);
        dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ll_select_cars = (LinearLayout) dialog.findViewById(R.id.ll_select_cars);
        ll_select_truck = (LinearLayout) dialog.findViewById(R.id.ll_select_truck);

        dialogClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                String SelectedVehicle = "";
                selectedModelId ="";
                if (checkBox_Selected.equalsIgnoreCase(CAR))
                {
                    for (int i=0; i<list_vehicleItem.size() ; i++)
                    {
                        if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
                        {
                            if (SelectedVehicle !=null && !SelectedVehicle.equalsIgnoreCase(""))
                            {
                                SelectedVehicle = SelectedVehicle + "," + list_vehicleItem.get(i).getName();
                                selectedModelId = selectedModelId + "," + list_vehicleItem.get(i).getId();
                            }
                            else
                            {
                                SelectedVehicle = list_vehicleItem.get(i).getName();
                                selectedModelId =  list_vehicleItem.get(i).getId();
                            }
                        }
                    }
                }
                else
                {
                    for (int i=0; i<list_vehicleItem_turck.size() ; i++)
                    {
                        if (list_vehicleItem_turck.get(i).getStatus().equalsIgnoreCase("1"))
                        {
                            if (SelectedVehicle !=null && !SelectedVehicle.equalsIgnoreCase(""))
                            {
                                SelectedVehicle = SelectedVehicle + "," + list_vehicleItem_turck.get(i).getName();
                                selectedModelId = selectedModelId + "," + list_vehicleItem_turck.get(i).getId();
                            }
                            else
                            {
                                SelectedVehicle = list_vehicleItem_turck.get(i).getName();
                                selectedModelId =  list_vehicleItem_turck.get(i).getId();
                            }
                        }
                    }
                }

                if (SelectedVehicle != null && !SelectedVehicle.equalsIgnoreCase(""))
                {
                    tv_register_CarType.setText(SelectedVehicle);
//                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,selectedModelId,activity);
//                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME,SelectedVehicle,activity);

                }
                else
                {
                    tv_register_CarType.setText(getResources().getString(R.string.car_type));
                    selectedModelId = "";
//                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,"",activity);
//                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME,"",activity);
                }
            }
        });

        if (checkBox_Selected.equalsIgnoreCase(CAR))
        {
            dialog_ListView.setVisibility(View.VISIBLE);
            dialog_ListView_truck.setVisibility(View.GONE);
        }

        if (checkBox_Selected.equalsIgnoreCase(TRUCK))
        {
            dialog_ListView.setVisibility(View.GONE);
            dialog_ListView_truck.setVisibility(View.VISIBLE);
        }

        dialog.show();

        dialogClass.showDialog();
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                FindWhichTypeIs_Visible();
            }
        },100);
    }

    private void FindWhichTypeIs_Visible()
    {
        list_vehicleItem.clear();
        list_vehicleItem_turck.clear();

        if (adapter!=null)
        {
            adapter.notifyDataSetChanged();
        }
        if (adapterTruck!=null)
        {
            adapterTruck.notifyDataSetChanged();
        }

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_FOR_VEHICLE_MODEL_LIST;

        Log.e("url", "FindWhichTypeIs_Visible = " + url);
        Log.e("param", "FindWhichTypeIs_Visible = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "FindWhichTypeIs_Visible = " + responseCode);
                    Log.e("Response", "FindWhichTypeIs_Visible = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String Id ="", Name="", Description="";
                                String Image ="", ModelSizeImage = "", Height="", Width="", Capacity="";
                                Log.e("status", "true");


                                if (json.has("cars_and_taxi"))
                                {
                                    String cars_and_taxi = json.getString("cars_and_taxi");
                                    if (cars_and_taxi!=null && !cars_and_taxi.equalsIgnoreCase(""))
                                    {
                                        JSONArray arrayCarTaxi = json.getJSONArray("cars_and_taxi");
                                        for (int i= 0; i<arrayCarTaxi.length(); i++)
                                        {
                                            JSONObject objectCarTexi = arrayCarTaxi.getJSONObject(i);
                                            if (objectCarTexi.has("Id"))
                                            {
                                                Id = objectCarTexi.getString("Id");
                                            }

                                            if (objectCarTexi.has("Name"))
                                            {
                                                Name = objectCarTexi.getString("Name");
                                            }

                                            if (objectCarTexi.has("Description"))
                                            {
                                                Description = objectCarTexi.getString("Description");
                                            }
                                            int checked=0;
                                            if (list_deviceModelId!=null)
                                            {
                                                for (int l=0; l<list_deviceModelId.size(); l++)
                                                {
                                                    Log.e(TAG,"cars_and_taxi list_deviceModelId22 : "+list_deviceModelId.get(l));
                                                    if (list_deviceModelId.get(l).equalsIgnoreCase(Id))
                                                    {
                                                        checked=1;
                                                    }
                                                }
                                            }
                                            if (checked==1)
                                            {
                                                list_vehicleItem.add(new VehicleType_Been(Id, Name, Description, "1"));
                                            }
                                            else
                                            {
                                                list_vehicleItem.add(new VehicleType_Been(Id, Name, Description, "0"));
                                            }
                                        }
                                        if (adapter!=null)
                                        {
                                            adapter.notifyDataSetChanged();
                                        }
                                        Log.e(TAG, "cars_and_taxi list_vehicleItem.size() :- "+ list_vehicleItem.size());
                                    }
                                    else
                                    {
                                        Log.e(TAG, "cars_and_taxi == null");
                                    }
                                }

                                if (json.has("delivery_services"))
                                {
                                    String delivery_services = json.getString("delivery_services");

                                    if (delivery_services!=null && !delivery_services.equalsIgnoreCase(""))
                                    {
                                        JSONArray arrayDelevryServices = json.getJSONArray("delivery_services");

                                        for (int i= 0; i<arrayDelevryServices.length(); i++)
                                        {
                                            JSONObject objectdeleveryServices = arrayDelevryServices.getJSONObject(i);

                                            if (objectdeleveryServices.has("Id"))
                                            {
                                                Id = objectdeleveryServices.getString("Id");
                                            }

                                            if (objectdeleveryServices.has("Name"))
                                            {
                                                Name = objectdeleveryServices.getString("Name");
                                            }

                                            if (objectdeleveryServices.has("Description"))
                                            {
                                                Description = objectdeleveryServices.getString("Description");
                                            }

                                            if (objectdeleveryServices.has("Image"))
                                            {
                                                if (objectdeleveryServices.getString("Image") != null &&
                                                        !objectdeleveryServices.getString("Image").equalsIgnoreCase("") &&
                                                        !objectdeleveryServices.getString("Image").equalsIgnoreCase("null"))
                                                {
                                                    Image = WebServiceAPI.BASE_URL_IMAGE + objectdeleveryServices.getString("Image");
                                                }

                                            }

                                            if (objectdeleveryServices.has("ModelSizeImage"))
                                            {
                                                if (objectdeleveryServices.getString("ModelSizeImage") != null &&
                                                        !objectdeleveryServices.getString("ModelSizeImage").equalsIgnoreCase("") &&
                                                        !objectdeleveryServices.getString("ModelSizeImage").equalsIgnoreCase("null"))
                                                {
                                                    ModelSizeImage = WebServiceAPI.BASE_URL_IMAGE + objectdeleveryServices.getString("ModelSizeImage");
                                                }
                                            }

                                            if (objectdeleveryServices.has("Height"))
                                            {
                                                Height = objectdeleveryServices.getString("Height");
                                            }

                                            if (objectdeleveryServices.has("Width"))
                                            {
                                                Width = objectdeleveryServices.getString("Width");
                                            }

                                            if (objectdeleveryServices.has("Capacity"))
                                            {
                                                Capacity = objectdeleveryServices.getString("Capacity");
                                            }


                                            int checked=0;

                                            if (list_deviceModelId!=null)
                                            {
                                                for (int l=0; l<list_deviceModelId.size(); l++)
                                                {
                                                    Log.e(TAG,"delivery_services list_deviceModelId22 : "+list_deviceModelId.get(l));
                                                    if (list_deviceModelId.get(l).equalsIgnoreCase(Id))
                                                    {
                                                        checked=1;
                                                    }
                                                }
                                            }
                                            if (checked==1)
                                            {
                                                list_vehicleItem_turck.add(new TruckType_Been(Id, Name, Description, Image, ModelSizeImage, Height, Width, Capacity, "1"));
                                            }
                                            else
                                            {
                                                list_vehicleItem_turck.add(new TruckType_Been(Id, Name, Description, Image, ModelSizeImage, Height, Width, Capacity, "0"));
                                            }
                                        }
                                        if (adapterTruck!=null)
                                        {
                                            adapterTruck.notifyDataSetChanged();
                                        }
                                        Log.e("list size", "delivery_services list_vehicleItem_turck.size() :- "+ list_vehicleItem_turck.size());
                                    }
                                    else
                                    {
                                        Log.e(TAG, "delivery_services == null");
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e("FindWhichTypeIs_Visible", "Exception : " + e.toString());
                    dialogClass.hideDialog();

                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void CheckData()
    {
        Log.e(TAG,"CheckData()");
        Log.e(TAG,"CheckData() et_vehiRegistrationNo:- " + et_vehiRegistrationNo.getText().toString().trim());
        Log.e(TAG,"CheckData() et_register_CarModel:- " + et_register_CarModel.getText().toString().trim());
        Log.e(TAG,"CheckData() tv_register_CarType:- " + tv_register_CarType.getText().toString().trim());
        Log.e(TAG,"CheckData() checkBox_Selected:- " + checkBox_Selected);

        if (TextUtils.isEmpty(et_vehiRegistrationNo.getText().toString().trim()))
        {
            Log.e(TAG, "CheckData() Registration number");

            new SnackbarUtils(main_layout, getString(R.string.please_enter_vehicle_registration_no),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_register_CarModel.getText().toString().trim()))
        {
            Log.e(TAG, "CheckData() car model");

            new SnackbarUtils(main_layout, getString(R.string.please_enter_car_model),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(tv_register_CarType.getText().toString().trim()))
        {
            Log.e(TAG, "CheckData() car type");

            new SnackbarUtils(main_layout, getString(R.string.please_select_car_type),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        {
            GotoNextPage();
        }
    }

    private void GotoNextPage()
    {
        Log.e(TAG,"GotoNextPage() ");
        Log.e(TAG,"GotoNextPage() checkBox_Selected:- "+ checkBox_Selected);
        Log.e(TAG,"GotoNextPage() list_vehicleItem.size():- "+ list_vehicleItem.size());
        Log.e(TAG,"GotoNextPage() list_vehicleItem_turck.size()" + list_vehicleItem_turck.size());

        String AllSelectedVehicle="";
        selectedModelId = "";

        if (checkBox_Selected.equalsIgnoreCase(CAR))
        {
            for (int i=0; i<list_vehicleItem.size() ; i++)
            {
                if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
                {
                    if (AllSelectedVehicle !=null && !AllSelectedVehicle.equalsIgnoreCase(""))
                    {
                        AllSelectedVehicle = AllSelectedVehicle + "," + list_vehicleItem.get(i).getName();
                        selectedModelId = selectedModelId + "," + list_vehicleItem.get(i).getId();
                    }
                    else
                    {
                        AllSelectedVehicle = list_vehicleItem.get(i).getName();
                        selectedModelId =  list_vehicleItem.get(i).getId();
                    }
                }
            }
        }
        else
        {
            for (int i=0; i<list_vehicleItem_turck.size() ; i++)
            {
                if (list_vehicleItem_turck.get(i).getStatus().equalsIgnoreCase("1"))
                {
                    if (AllSelectedVehicle !=null && !AllSelectedVehicle.equalsIgnoreCase(""))
                    {
                        AllSelectedVehicle = AllSelectedVehicle + "," + list_vehicleItem_turck.get(i).getName();
                        selectedModelId = selectedModelId + "," + list_vehicleItem_turck.get(i).getId();
                    }
                    else
                    {
                        AllSelectedVehicle = list_vehicleItem_turck.get(i).getName();
                        selectedModelId =  list_vehicleItem_turck.get(i).getId();
                    }
                }
            }
        }

        if (selectedModelId!= null && !selectedModelId.equalsIgnoreCase(""))
        {
            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, selectedModelId, activity);
            SessionSave.saveUserSession(Comman.REGISTER_VEHICLE_ACTIVITY, "1", activity);

            Log.e(TAG, "GotoNextPage()  AllSelectedVehicle:- :" + AllSelectedVehicle);

            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    UpdateCarDetail(userId);
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
            }
        }
        else
        {
            Log.e(TAG,"GotoNextPage() select_car_type = null");

            new SnackbarUtils(main_layout, getString(R.string.please_select_car_type),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
    }

    private void UpdateCarDetail(String driverId)
    {
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_UPDATE_DRIVER_CAR_INFO ;

        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_DIVER_ID, driverId);
        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_REGISTRATION_NO, et_vehiRegistrationNo.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_MODEL, et_register_CarModel.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_CLASS, SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL, activity));

        if (Car_front_ByteImage!=null)
        {
            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE, Car_front_ByteImage); // Front
        }

        if (Car_left_ByteImage != null)
        {
            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE_LEFT, Car_left_ByteImage); // Left
        }

        if (Car_Right_ByteImage != null)
        {
            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE_RIGHT, Car_Right_ByteImage); // Right
        }

        Log.e("url", "UpdateCarDetail = " + url);
        Log.e("param", "UpdateCarDetail = " + params);

        dialogClass.showDialog();
        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateCarDetail = " + responseCode);
                    Log.e("Response", "UpdateCarDetail = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                isUpadet = true;
                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");

                                    if (driverProfile!=null)
                                    {
                                        String VehicleId, VehicleModel, CarCompany, VehicleRegistrationNo, VehicleModelName, CategoryId,
                                                VehicleImage, VehicleLeftImage, VehicleRightImage;

                                        if (driverProfile.has("CategoryId"))
                                        {
                                            CategoryId = driverProfile.getString("CategoryId");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, activity);
                                        }

                                        if (driverProfile.has("Vehicle"))
                                        {
                                            JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                            if (driverVehicle!=null)
                                            {
                                                if (driverVehicle.has("Id"))
                                                {
                                                    VehicleId = driverVehicle.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                }
                                                if (driverVehicle.has("VehicleModel"))
                                                {
                                                    VehicleModel = driverVehicle.getString("VehicleModel");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                }
                                                if (driverVehicle.has("Company"))
                                                {
                                                    CarCompany = driverVehicle.getString("Company");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                }
                                                if (driverVehicle.has("VehicleRegistrationNo"))
                                                {
                                                    VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                }
                                                if (driverVehicle.has("VehicleImage"))
                                                {
                                                    VehicleImage = driverVehicle.getString("VehicleImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                }
                                                if (driverVehicle.has("VehicleLeftImage"))
                                                {
                                                    VehicleLeftImage = driverVehicle.getString("VehicleLeftImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, VehicleLeftImage, activity);
                                                }
                                                if (driverVehicle.has("VehicleRightImage"))
                                                {
                                                    VehicleRightImage = driverVehicle.getString("VehicleRightImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, VehicleRightImage, activity);
                                                }

                                                if (driverVehicle.has("VehicleClass"))
                                                {
                                                    VehicleModelName = driverVehicle.getString("VehicleClass");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                    Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                    if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Business Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Economy"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Taxi"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                    }
                                                    else if (VehicleModelName.contains("LUX-VAN"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Disability"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                } else {
                                                    SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                }
                                            }
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                SuccessDialog(json.getString("message"), getString(R.string.success_message));
//                                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.success_message));

                                            }
                                            else
                                            {
                                                SuccessDialog(getString(R.string.vehical_update_successfully), getString(R.string.success_message));
//                                                errorDialogClass.showDialog("Your Vehicle update successfully!", getString(R.string.success_message));
                                            }
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                }

                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("UpdateCarDetail", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void SuccessDialog(String message, String title)
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok, tv_Message, tv_Title;
        LinearLayout ll_Ok;
        ImageView iv_close;

        tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);
            }
        });

        if (message!=null && !message.equalsIgnoreCase(""))
        {
            tv_Message.setText(message);
        }
        else
        {
            tv_Message.setVisibility(View.GONE);
        }

        if (title!=null && !title.equalsIgnoreCase(""))
        {
            tv_Title.setText(title);
        }
        else
        {
            tv_Title.setVisibility(View.GONE);
        }


        dialog.show();
    }

    private void ShowPictureDialog()
    {
        Log.e(TAG, "ShowPictureDialog()");

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE_CAMERA);
        }
        else if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE_STORAGE);
        }
        else
        {
            final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_photo_choose);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            //lp.width = Comman.DEVICE_WIDTH;
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);

            TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
            TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
            TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
            LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
            ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

            ll_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            tv_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            iv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    dialog.dismiss();
                }
            });

            tv_gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            choosePhotoFromGallary();
                        }
                    },800);
                }
            });

            tv_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            takePhotoFromCamera();
                        }
                    },800);

                }
            });

            dialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
            {
                Car_front_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
            {
                Car_left_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
            {
                Car_Right_ByteImage = null;
            }
        }

        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
            {
                Car_front_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
            {
                Car_left_ByteImage = null;
            }
            else if(ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
            {
                Car_Right_ByteImage = null;
            }
        }

        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }
        else
        {
            userChoosenTask = chooseCamera;
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
            startActivityForResult(intent, CAMERA);
        }
    }

    public String getDeviceName()
    {
        String manufacturer = Build.MANUFACTURER;
        return manufacturer;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission CAMERA permission granted");
                    ShowPictureDialog();
                }
                break;

            case MY_REQUEST_CODE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission GALLERY permission granted");
                    ShowPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                Log.e("hahahahahahahaha","11111111111111111111");
                onCaptureImageResult(data);
            }
        }
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try
        {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
            {
                bitmapSumsung_CarFront = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCarFront);
                Car_front_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
            {
                bitmapSumsung_CarLeft = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCarLeft);
                Car_left_ByteImage = ConvertToByteArray(bitmapImage);
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
            {
                bitmapSumsung_CarRight = bitmapImage;
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCarRight);
                Car_Right_ByteImage = ConvertToByteArray(bitmapImage);
            }
        }
    }

    private void onCaptureImageResult(Intent data)
    {
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
                {
                    bitmapSumsung_CarFront = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarFront);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
                {
                    bitmapSumsung_CarLeft = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarLeft);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
                {
                    bitmapSumsung_CarRight = (Bitmap) data.getExtras().get("data");
                    imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarRight);
                }
            }
        }

        try
        {
            Log.e(TAG, "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), imageUri);
            Log.e(TAG,"######### bmp height = "+thumbnail.getHeight());
            Log.e(TAG,"######### bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_FRONT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCarFront);
                    Car_front_ByteImage = ConvertToByteArray(thumbnail);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_LEFT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCarLeft);
                    Car_left_ByteImage = ConvertToByteArray(thumbnail);
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_CAR_RIGHT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCarRight);
                    Car_Right_ByteImage = ConvertToByteArray(thumbnail);
                }
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"call exception = "+e.getMessage());
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "PickNGo", null);
        return Uri.parse(path);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }

        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            if (bitmapSumsung_CarFront!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarFront);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivCarFront);
                Car_front_ByteImage = ConvertToByteArray(bitmapSumsung_CarFront);
            }
            else
            {
                ivCarFront.setImageBitmap(null);
            }

            if (bitmapSumsung_CarLeft!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarLeft);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivCarLeft);
                Car_left_ByteImage = ConvertToByteArray(bitmapSumsung_CarLeft);
            }
            else
            {
                ivCarLeft.setImageBitmap(null);
            }

            if (bitmapSumsung_CarRight!=null)
            {
                imageUri = getImageUri(Registration_Activity.activity, bitmapSumsung_CarRight);
                Picasso.get()
                        .load(imageUri)
                        .fit()
                        .into(ivCarRight);
                Car_Right_ByteImage = ConvertToByteArray(bitmapSumsung_CarRight);
            }
            else
            {
                ivCarRight.setImageBitmap(null);
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");

        super.onBackPressed();
        if (isUpadet == false)
        {
            Log.e(TAG,"onBackPressed() isUpadet == false");
            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,sessionIds,activity);
        }
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isUpadet == false)
        {
            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL,sessionIds,activity);
        }
    }
}
