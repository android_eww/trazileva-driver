package com.connectkargo.driver.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Adapter.SimpleFragmentPagerAdapter;
import com.connectkargo.driver.R;
import com.google.android.material.tabs.TabLayout;

public class BidListActivity extends BaseActivity implements View.OnClickListener {

    //ActivityBidListBinding binding;
    LinearLayout ll_back,ll_toolbar_RightImage;
    TextView tv_toolbarTitle;
    ImageView iv_toolbar_RightImage;

    TabLayout sliding_tabs;
    ViewPager viewpager;

    public static String MY_BID = "my bid";
    public static String OPEN_BID = "open bid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid_list);

        init();
    }

    private void init()
    {
        ll_back = findViewById(R.id.ll_back);
        ll_back.setOnClickListener(this);
        tv_toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(getString(R.string.bid_list));
        ll_toolbar_RightImage = findViewById(R.id.ll_toolbar_RightImage);
        ll_toolbar_RightImage.setVisibility(View.VISIBLE);
        iv_toolbar_RightImage = findViewById(R.id.iv_toolbar_RightImage);
        iv_toolbar_RightImage.setVisibility(View.GONE);
        iv_toolbar_RightImage.setImageResource(R.drawable.icon_plus_black);

        viewpager = findViewById(R.id.viewpager);
        sliding_tabs = findViewById(R.id.sliding_tabs);

        SimpleFragmentPagerAdapter simpleFragmentPagerAdapter = new SimpleFragmentPagerAdapter(this,getSupportFragmentManager());
        viewpager.setAdapter(simpleFragmentPagerAdapter);
        sliding_tabs.setupWithViewPager(viewpager);
    }

    @Override
    public void onClick(View v)
    {
        if(v == ll_back)
        {
            onBackPressed();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

}