package com.connectkargo.driver.Activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ForgotPassword_Activity extends BaseActivity implements View.OnClickListener {

    public static ForgotPassword_Activity activity;

    TextView  tv_forgotPass_submit, tv_Title_Toolbar;
    EditText et_email;
    RelativeLayout main_layout;
    ImageView iv_Back_Toolbar;

    private Dialog dialog;
    DialogClass dialogClass;
    AQuery aQuery;

    Animation animBounce;

    ErrorDialogClass errorDialogClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        activity = ForgotPassword_Activity.this;

        aQuery = new AQuery(ForgotPassword_Activity.this);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        init();
    }

    private void init() {

        main_layout = findViewById(R.id.main_layout);
        iv_Back_Toolbar = (ImageView) findViewById(R.id.iv_Back_Toolbar);
        tv_Title_Toolbar = (TextView) findViewById(R.id.tv_Title_Toolbar);
        tv_Title_Toolbar.setText(getString(R.string.title_forgot_password));

        tv_forgotPass_submit = (TextView) findViewById(R.id.tv_forgotPass_submit);
        et_email = (EditText) findViewById(R.id.et_email);


        iv_Back_Toolbar.setOnClickListener(this);
        tv_forgotPass_submit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_Back_Toolbar:
                onBackPressed();
                break;

            case R.id.tv_forgotPass_submit:
                animBounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
                tv_forgotPass_submit.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        SubmitEmail_ForgotPass();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
        }
    }

    private void SubmitEmail_ForgotPass()
    {
        if (TextUtils.isEmpty(et_email.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_email),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        if(!isValidMail(et_email.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_email),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        {
            if (Global.isNetworkconn(activity))
            {
                userForgotPassword();
            }
            else
            {
                errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),
                        getResources().getString(R.string.internet_error_message));
            }

        }
    }

    private void userForgotPassword()
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_FORGOT_PASSWORD;

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_EMAIL, et_email.getText().toString().trim());

        Log.e("url", "userForgotPassword = " + url);
        Log.e("param", "userForgotPassword = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "userForgotPassword = " + responseCode);
                    Log.e("Response", "userForgotPassword = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                showPopup(json.getString("message"),getResources().getString(R.string.success_message));
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                                            getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                                        getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                                getResources().getString(R.string.error_message));

                    }
                } catch (Exception e)
                {
                    Log.e("userForgotPassword", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void showPopup(String message,String title) {

        dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_information);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        TextView tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        tv_Title.setText(title);

        ll_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);

            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);

            }
        });

        dialog.show();
    }

    private boolean isValidMail(String email)
    {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }
}
