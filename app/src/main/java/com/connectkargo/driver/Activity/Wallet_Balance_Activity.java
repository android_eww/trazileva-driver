package com.connectkargo.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Adapter.Transfer_History_Adapter;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Been.Transfer_History_Been;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Wallet_Balance_Activity extends BaseActivity implements View.OnClickListener
{
    public static Wallet_Balance_Activity activity;

    LinearLayout ll_back, main_layout;

    ImageView iv_bank,iv_dollar,iv_history;
    TextView tv_toolbarTitle, tv_bank, tv_dollar, tv_history, tv_currentBalance;
    LinearLayout ll_dollar,ll_bank,ll_history, ll_Sos;

    RecyclerView rv_transferHistory;
    private List<Transfer_History_Been> list = new ArrayList<>();
    Transfer_History_Adapter adapter;

    private AQuery aQuery;
    DialogClass dialogClass;

    String walleteBallence;
    int resumeFlag=1;

    private Animation animation;
    private Animation.AnimationListener animationListener;
    private int clickflag = 0;
    public static boolean transferToBank = false;
    ErrorDialogClass errorDialogClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance);

        activity = Wallet_Balance_Activity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        transferToBank = false;

        errorDialogClass = new ErrorDialogClass(activity);

        resumeFlag = 1;
        clickflag = 0;

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        tv_toolbarTitle = (TextView)findViewById(R.id.tv_toolbarTitle);

        ll_dollar = (LinearLayout)findViewById(R.id.ll_dollar);
        ll_bank = (LinearLayout)findViewById(R.id.ll_bank);
        ll_history = (LinearLayout)findViewById(R.id.ll_history);

        iv_dollar = (ImageView)findViewById(R.id.iv_dollar);
        iv_bank = (ImageView)findViewById(R.id.iv_bank);
        iv_history = (ImageView)findViewById(R.id.iv_history);

        tv_dollar = (TextView)findViewById(R.id.tv_dollar);
        tv_bank = (TextView)findViewById(R.id.tv_bank);
        tv_history = (TextView)findViewById(R.id.tv_history);
        tv_currentBalance = (TextView)findViewById(R.id.tv_currentBalance);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        ll_Sos.setOnClickListener(this);

        tv_toolbarTitle.setText(getResources().getText(R.string.balance));

        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
        if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
        {
            String mm = activity.getResources().getString(R.string.currency) +String.format("%.2f",Double.parseDouble(walleteBallence));
            tv_currentBalance.setText(mm);
        }
        else
        {
            tv_currentBalance.setVisibility(View.GONE);
        }

        rv_transferHistory = (RecyclerView) findViewById(R.id.rv_transferHistory);
        rv_transferHistory.setVisibility(View.GONE);

        adapter = new Transfer_History_Adapter(activity, list);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rv_transferHistory.setLayoutManager(mLayoutManager);
        rv_transferHistory.setItemAnimator(new DefaultItemAnimator());
        rv_transferHistory.setAdapter(adapter);

        ll_dollar.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        iv_dollar.setImageResource(R.drawable.cab_currency_red);
                        tv_dollar.setTextColor(ContextCompat.getColor(activity, R.color.colorSelectText));
                        break;

                    case MotionEvent.ACTION_UP:
                        iv_dollar.setImageResource(R.drawable.cab_currency_gray);
                        tv_dollar.setTextColor(ContextCompat.getColor(activity, R.color.colorUnselectText));
                        break;
                }

                return false;
            }
        });

        ll_bank.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        iv_bank.setImageResource(R.drawable.icon_bank_select);
                        tv_bank.setTextColor(ContextCompat.getColor(activity, R.color.colorSelectText));
                        break;

                    case MotionEvent.ACTION_UP:
                        iv_bank.setImageResource(R.drawable.icon_bank_unselect);
                        tv_bank.setTextColor(ContextCompat.getColor(activity, R.color.colorUnselectText));
                        break;
                }
                return false;
            }
        });

        ll_history.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        iv_history.setImageResource(R.drawable.icon_history_select);
                        tv_history.setTextColor(ContextCompat.getColor(activity, R.color.colorSelectText));
                        break;

                    case MotionEvent.ACTION_UP:
                        iv_history.setImageResource(R.drawable.icon_history_unselect);
                        tv_history.setTextColor(ContextCompat.getColor(activity, R.color.colorUnselectText));
                        break;
                }

                return false;
            }
        });

        ll_back.setOnClickListener(this);
        ll_dollar.setOnClickListener(this);
        ll_bank.setOnClickListener(this);
        ll_history.setOnClickListener(this);

        animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if (clickflag == 1)
                {

                    Goto_TopUp_dollar();
                }
                else if (clickflag == 2)
                {
                    Goto_TransferToBank();

                }
                else if (clickflag == 3)
                {
                    Goto_TransferHistory();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
        if (driverId!=null && !driverId.equalsIgnoreCase(""))
        {
            if (ConnectivityReceiver.isConnected())
            {
                GetHistory(driverId);
            }
            else
            {
                errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),  getString(R.string.internet_error_message));
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_dollar:
                clickflag = 1;
                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_dollar.startAnimation(animation);
                break;

            case R.id.ll_bank:
                clickflag = 2;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_bank.startAnimation(animation);
                break;

            case R.id.ll_history:
                clickflag = 3;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_history.startAnimation(animation);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void Goto_TopUp_dollar()
    {
        resumeFlag = 0;
        Intent intent = new Intent(Wallet_Balance_Activity.this, Wallet_Balance_TopUp_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void Goto_TransferToBank()
    {
        resumeFlag = 0;
        Intent intent = new Intent(Wallet_Balance_Activity.this, Wallet_Balance_TransferToBank_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void Goto_TransferHistory()
    {
        resumeFlag = 0;
        Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void GetHistory(String driverId)
    {
        list.clear();
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_TRANSACTION_HISTORY + driverId;

        Log.e("url", "GetCardList = " + url);
        Log.e("param", "GetCardList = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "GetCardList = " + responseCode);
                    Log.e("Response", "GetCardList = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, walletBalance, activity);
                                    }
                                }
                                if (json.has("history"))
                                {
                                    Log.e("GetHistory2222222222","1111111111");
                                    String history = json.getString("history");
                                    if (history!=null && !history.equalsIgnoreCase(""))
                                    {
                                        Log.e("GetHistory2222222222","2222222222222");
                                        JSONArray historyArray = json.getJSONArray("history");
                                        if (historyArray!=null && historyArray.length()>0)
                                        {
                                            Log.e("GetHistory2222222222","33333333333333");
//                                            for (int i=historyArray.length()-1; i>=0; i--)
                                            for (int i=0; i<historyArray.length(); i++)
                                            {
                                                Log.e("GetHistory2222222222","4444444444444444");
                                                String WalletId="", UpdatedDate="", Amount="", Type="", Description="", Status="";
                                                JSONObject historyObj = historyArray.getJSONObject(i);
                                                if (historyObj.has("WalletId"))
                                                {
                                                    WalletId = historyObj.getString("WalletId");
                                                }
                                                if (historyObj.has("UpdatedDate"))
                                                {
                                                    UpdatedDate = historyObj.getString("UpdatedDate");
                                                }
                                                if (historyObj.has("Amount"))
                                                {
                                                    Amount = historyObj.getString("Amount");
                                                }
                                                if (historyObj.has("Type"))
                                                {
                                                    Type = historyObj.getString("Type");
                                                }
                                                if (historyObj.has("Description"))
                                                {
                                                    Description = historyObj.getString("Description");
                                                }
                                                if (historyObj.has("Status"))
                                                {
                                                    Status = historyObj.getString("Status");
                                                }
//                                                if (i>historyArray.length()-5)
                                                if (i<5)
                                                {
                                                    Log.e("GetHistory","WalletId : "+WalletId+"\nUpdatedDate : "+UpdatedDate+"\nAmount : "+Amount+"\nType : "+Type+"\nDescription : "+Description);
                                                    list.add(new Transfer_History_Been(Description , UpdatedDate, Type, Amount, Status));
                                                }
                                                else
                                                {
                                                    Log.e("GetHistory2222222222","WalletId : "+WalletId+"\nUpdatedDate : "+UpdatedDate+"\nAmount : "+Amount+"\nType : "+Type+"\nDescription : "+Description);
                                                }
                                            }
                                            dialogClass.hideDialog();
                                            adapter.notifyDataSetChanged();
                                            rv_transferHistory.setVisibility(View.VISIBLE);
                                        }
                                        else
                                        {
                                            Log.e("history", "no history");
                                            dialogClass.hideDialog();
                                            rv_transferHistory.setVisibility(View.GONE);
                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("history", "no history");
                                        dialogClass.hideDialog();
                                        rv_transferHistory.setVisibility(View.GONE);
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("history", "no history");
                                    dialogClass.hideDialog();
                                    rv_transferHistory.setVisibility(View.GONE);
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                rv_transferHistory.setVisibility(View.GONE);
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            rv_transferHistory.setVisibility(View.GONE);
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        rv_transferHistory.setVisibility(View.GONE);
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));

                    }
                }
                catch (Exception e)
                {
                    Log.e("GetCardList", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    rv_transferHistory.setVisibility(View.GONE);
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }

        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
        if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
        {
            String mm = activity.getResources().getString(R.string.currency) + " " +String.format("%.2f",Double.parseDouble(walleteBallence));
            tv_currentBalance.setText(mm);
        }
        else
        {
            tv_currentBalance.setVisibility(View.GONE);
        }

        if (transferToBank == true)
        {
            transferToBank = false;

            String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (driverId!=null && !driverId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    GetHistory(driverId);
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),  getString(R.string.internet_error_message));
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
                if (Wallet_Balance_TopUp_Activity.activity!=null)
                {
                    Wallet_Balance_TopUp_Activity.activity.finish();
                }
                if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                {
                    Wallet_Balance_TransferToBank_Activity.activity.finish();
                }
                if (Wallet_Cards_Activity.activity!=null)
                {
                    Wallet_Cards_Activity.activity.finish();
                }
                if (Wallet_Transfer_Activity.activity!=null)
                {
                    Wallet_Transfer_Activity.activity.finish();
                }
                if (Wallet_Transfer_History_Activity.activity!=null)
                {
                    Wallet_Transfer_History_Activity.activity.finish();
                }
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }
}