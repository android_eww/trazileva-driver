package com.connectkargo.driver.Activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Change_password_Activity extends BaseActivity implements View.OnClickListener{

    Change_password_Activity activity;
    String TAG = "changeDriverPassword";
    LinearLayout main_layout, ll_back, ll_layout_submit, ll_Sos;
    EditText etOldPass, et_new_pass, et_confirm_pass;
    TextView tv_submit_password, tv_toolbarTitle;

    private AQuery aQuery;
    DialogClass dialogClass;
    private Dialog dialog;

    private Animation animation;
    private Animation.AnimationListener animationListener;

    ErrorDialogClass errorDialogClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        activity = Change_password_Activity.this;

        aQuery = new AQuery(activity);
        errorDialogClass = new ErrorDialogClass(activity);
        dialogClass = new DialogClass(activity, 1);

        main_layout = (LinearLayout)findViewById(R.id.main_layout);
        ll_back = (LinearLayout)findViewById(R.id.ll_back);
        ll_layout_submit = (LinearLayout)findViewById(R.id.ll_layout_submit);

        etOldPass = (EditText)findViewById(R.id.etOldPass);
        et_new_pass = (EditText)findViewById(R.id.et_new_pass);
        et_confirm_pass = (EditText) findViewById(R.id.et_confirm_pass);
        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        ll_Sos.setVisibility(View.GONE);

        tv_submit_password = (TextView)findViewById(R.id.tv_submit_password);
        tv_toolbarTitle = (TextView)findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(activity.getResources().getString(R.string.change_password));

        tv_submit_password.setOnClickListener(this);
        ll_layout_submit.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);

        animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                checkData();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_submit_password:
                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_layout_submit.startAnimation(animation);
                break;

            case R.id.ll_layout_submit:
                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_layout_submit.startAnimation(animation);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;

            case R.id.ll_back:
                onBackPressed();
                break;
        }
    }

    private void checkData() {

        if (TextUtils.isEmpty(etOldPass.getText().toString().trim()) &&
                TextUtils.isEmpty(et_new_pass.getText().toString().trim()) &&
                TextUtils.isEmpty(et_confirm_pass.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_fill_all_details),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else if (TextUtils.isEmpty(etOldPass.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_old_password),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_new_pass.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_new_password),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else if (et_new_pass.getText().toString().trim().length() < 8)
        {
            new SnackbarUtils(main_layout, activity.getResources().getString(R.string.new_password_contain_at_lest_8_charcter),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_confirm_pass.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_repassword),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
//        else if (et_confirm_pass.getText().toString().trim().length() < 8)
//        {
//            new SnackbarUtils(main_layout, "Confirm password must contain atleast 8 characters.",
//                    ContextCompat.getColor(activity, R.color.snakbar_color),
//                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
//                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
//        }
        else if (!et_new_pass.getText().toString().trim().equals(et_confirm_pass.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, activity.getResources().getString(R.string.new_password_and_confirm_pass_must_be_same),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        {
            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    changeDriverPassword(userId);
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),
                            getResources().getString(R.string.internet_error_message));
                }
            }
            /*else if (SessionSave.getUserSession(Comman.USER_FORGOT_PASSWORD_DRIVER_ID, activity)!=null && !SessionSave.getUserSession(Comman.USER_FORGOT_PASSWORD_DRIVER_ID, activity).equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    Log.e("wertyu","555555555555");
                    changeDriverPassword(SessionSave.getUserSession(Comman.USER_FORGOT_PASSWORD_DRIVER_ID, activity));
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet), getResources().getString(R.string.internet_error_message));
                }
            }*/
        }
    }

    private void changeDriverPassword(String userId)
    {
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CHANGE_PASSWORD;

        params.put(WebServiceAPI.CHANGE_PASSWORD_PARAM_DRIVER_ID,userId);
        params.put(WebServiceAPI.CHANGE_PASSWORD_PARAM_PASSWORD,et_new_pass.getText().toString().trim());
        params.put(WebServiceAPI.CHANGE_PASSWORD_PARAM_OLD_PASSWORD,etOldPass.getText().toString().trim());

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status") &&
                                json.getBoolean("status"))
                        {
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                String message = json.getString("message");
                                showPopup(message,getString(R.string.success_message));
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    private void showPopup(String message,String title)
    {
        dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_information);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        TextView tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        tv_Title.setText(title);

        ll_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                       /* if (ForgotPassword_Activity.activity!=null)
                        {
                            SessionSave.saveUserSession(Comman.USER_FORGOT_PASSWORD_OTP, "", activity);
                            SessionSave.saveUserSession(Comman.USER_FORGOT_PASSWORD_DRIVER_ID, "", activity);
                            ForgotPassword_Activity.activity.finish();
                        }*/
                        onBackPressed();
                    }
                }, 1000);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                       /* if (ForgotPassword_Activity.activity!=null)
                        {
                            SessionSave.saveUserSession(Comman.USER_FORGOT_PASSWORD_OTP, "", activity);
                            SessionSave.saveUserSession(Comman.USER_FORGOT_PASSWORD_DRIVER_ID, "", activity);
                            ForgotPassword_Activity.activity.finish();
                        }*/
                        onBackPressed();
                    }
                }, 1000);
            }
        });

        dialog.show();
    }
}

