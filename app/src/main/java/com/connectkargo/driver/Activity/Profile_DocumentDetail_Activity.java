package com.connectkargo.driver.Activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Profile_DocumentDetail_Activity extends BaseActivity implements View.OnClickListener{

    Profile_DocumentDetail_Activity activity;

    TextView  tv_driver_licence_expiry, tv_accreditation_expiry, tv_carRegistration_expiry, tv_vehicle_Insurance_expiry, tv_toolbarTitle,
            tvGovernmentIdExpiry;
    LinearLayout ll_driver_licence, ll_acrreditation_certy, ll_car_registration_certy, ll_vehicle_Insurance, ll_Sos,
            llGovernmentId, llCriminalCertificate, llAddressProof;
    ImageView iv_driverLicence, iv_accreditation_certy, iv_car_Registration, iv_vehicle_insurance, ivGovernmentIdFront, ivGovernmentIdBack,
            ivCriminalCertificate, ivAddressProof,iv_driverLicence_Front;

    public static String ImageViewClicked = "", DRIVER_LICENCE="driverLicence", ACRREDITATION= "accreditation_certy",
            CAR_REGISTRATION="car_Registration", VEHICLE_INSURANCE="vehicle_insurance", CAR_IMAGE="driver_Image",
            GOVERNMENT_ID_FORNT = "government_id_front", GOVERNMENT_ID_BACK = "government_id_back",CRIMINAL_CERTI = "criminal_certi",
            ADDRESS_PROOF = "address_proof",DRIVER_LICENCE_FRONT="driverLicenceFront";
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    RelativeLayout rl_carImage;

    Intent intent;

    private int GALLERY = 1, CAMERA = 0;
    private static final int MY_REQUEST_CODE_CAMERA = 101;
    private static final int MY_REQUEST_CODE_STORAGE = 100;

    Bitmap finalImageBitmap, oldBitmap;

    byte[] DriverLicence_ByteImage=null, Accreditation_certy_ByteImage=null, Car_Registration_ByteImage=null,
            Vehicle_insurance_ByteImage=null, CarImage_ByteImage=null, Government_id_front_ByteImage=null, Government_id_back_ByteImage=null,
            Criminal_Certi_ByteImage=null, Address_Proof_ByteImage=null,DriverLicence_Front_ByteImage=null;

    DialogClass dialogClass;
    AQuery aQuery;

    ProgressBar progressbar;


    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    LinearLayout ll_back,main_layout;
    ImageView iv_carImage;

    Transformation mTransformation;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;
    ErrorDialogClass errorDialogClass;
    public static Bitmap bitmapSumsung;
    public static int ClickCameraFlagResume = 0;
    public static int ClickCameraFlagResumeCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_document_detail);

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        activity = Profile_DocumentDetail_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        initUI();
    }

    private void initUI() {

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        ll_back = (LinearLayout) findViewById(R.id.ll_back);

        iv_carImage = (ImageView) findViewById(R.id.iv_carImage);
        rl_carImage = (RelativeLayout) findViewById(R.id.rl_carImage);

        ll_driver_licence = (LinearLayout) findViewById(R.id.ll_driver_licence);
        ll_acrreditation_certy = (LinearLayout) findViewById(R.id.ll_acrreditation_certy);
        ll_car_registration_certy = (LinearLayout) findViewById(R.id.ll_car_registration_certy);
        ll_vehicle_Insurance = (LinearLayout) findViewById(R.id.ll_vehicle_Insurance);
        llGovernmentId = (LinearLayout) findViewById(R.id.llGovernmentId);
        llCriminalCertificate = (LinearLayout) findViewById(R.id.llCriminalCertificate);
        llAddressProof = (LinearLayout) findViewById(R.id.llAddressProof);

        tv_driver_licence_expiry = (TextView) findViewById(R.id.tv_driver_licence_expiry);
        tv_accreditation_expiry = (TextView) findViewById(R.id.tv_accreditation_expiry);
        tv_carRegistration_expiry = (TextView) findViewById(R.id.tv_carRegistration_expiry);
        tv_carRegistration_expiry.setVisibility(View.GONE);
        tv_vehicle_Insurance_expiry = (TextView) findViewById(R.id.tv_vehicle_Insurance_expiry);
        tvGovernmentIdExpiry = (TextView) findViewById(R.id.tvGovernmentIdExpiry);
        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(getResources().getText(R.string.document));

        iv_driverLicence = (ImageView) findViewById(R.id.iv_driverLicence);
        iv_driverLicence_Front = (ImageView) findViewById(R.id.iv_driverLicence_Front);
        iv_accreditation_certy = (ImageView) findViewById(R.id.iv_accreditation_certy);
        iv_car_Registration = (ImageView) findViewById(R.id.iv_car_Registration);
        iv_vehicle_insurance = (ImageView) findViewById(R.id.iv_vehicle_insurance);
        ivGovernmentIdFront = (ImageView) findViewById(R.id.ivGovernmentIdFront);
        ivGovernmentIdBack = (ImageView) findViewById(R.id.ivGovernmentIdBack);
        ivCriminalCertificate = (ImageView) findViewById(R.id.ivCriminalCertificate);
        ivAddressProof = (ImageView) findViewById(R.id.ivAddressProof);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.appColor))
                .borderWidthDp(2)
                .oval(true)
                .build();

        ll_back.setOnClickListener(this);

        rl_carImage.setOnClickListener(this);
        ll_driver_licence.setOnClickListener(this);
        iv_driverLicence.setOnClickListener(this);
        iv_driverLicence_Front.setOnClickListener(this);
        ll_acrreditation_certy.setOnClickListener(this);
        ll_car_registration_certy.setOnClickListener(this);
        ll_vehicle_Insurance.setOnClickListener(this);
        ivGovernmentIdFront.setOnClickListener(this);
        ivGovernmentIdBack.setOnClickListener(this);
        llCriminalCertificate.setOnClickListener(this);
        llAddressProof.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);


        String CarImage = SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, activity);
        String DriverLicense = SessionSave.getUserSession(Comman.USER_DRIVER_LICENCE_BACK, activity);
        String DriverLicenseFront = SessionSave.getUserSession(Comman.USER_DRIVER_LICENCE_FRONT, activity);
        String AccreditationCertificate = SessionSave.getUserSession(Comman.USER_ACCREDITATION_CERTY, activity);
        String RegistrationCertificate = SessionSave.getUserSession(Comman.USER_REGISTRATION_CERTY, activity);
        String VehicleInsuranceCertificate = SessionSave.getUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, activity);
        String GovernmentFrontImage = SessionSave.getUserSession(Comman.USER_PROFILE_GOVERNMENT_FRONT_ID, activity);
        String GovernmentBackImage = SessionSave.getUserSession(Comman.USER_PROFILE_GOVERNMENT_BACK_ID, activity);
        String CriminalCertificate = SessionSave.getUserSession(Comman.USER_PROFILE_CRIMINAL_CERTI, activity);
        String AddressProof = SessionSave.getUserSession(Comman.USER_PROFILE_ADDRESS_PROOF, activity);

        String DriverLicenseExpire = SessionSave.getUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, activity);
        String AccreditationCertificateExpire = SessionSave.getUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, activity);
        String RegistrationCertificateExpire = SessionSave.getUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, activity);
        String VehicleInsuranceCertificateExpire = SessionSave.getUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, activity);
        String GovernmentIdExpiry = SessionSave.getUserSession(Comman.USER_PROFILE_GOVERNMENT_EXPIRY, activity);

        if(DriverLicenseExpire!=null && !DriverLicenseExpire.equalsIgnoreCase(""))
        {
            tv_driver_licence_expiry.setText(DriverLicenseExpire);
        }
        else
        {
            tv_driver_licence_expiry.setText("");
        }

        if(AccreditationCertificateExpire!=null && !AccreditationCertificateExpire.equalsIgnoreCase(""))
        {
            tv_accreditation_expiry.setText(AccreditationCertificateExpire);
        }
        else
        {
            tv_accreditation_expiry.setText("");
        }

        if(RegistrationCertificateExpire!=null && !RegistrationCertificateExpire.equalsIgnoreCase(""))
        {
            tv_carRegistration_expiry.setText(RegistrationCertificateExpire);
        }
        else
        {
            tv_carRegistration_expiry.setText("");
        }

        if(VehicleInsuranceCertificateExpire!=null && !VehicleInsuranceCertificateExpire.equalsIgnoreCase(""))
        {
            tv_vehicle_Insurance_expiry.setText(VehicleInsuranceCertificateExpire);
        }
        else
        {
            tv_vehicle_Insurance_expiry.setText("");
        }

        if(GovernmentIdExpiry!=null && !GovernmentIdExpiry.equalsIgnoreCase(""))
        {
            tvGovernmentIdExpiry.setText(GovernmentIdExpiry);
        }
        else
        {
            tvGovernmentIdExpiry.setText("");
        }

        if(CarImage!=null && !CarImage.equalsIgnoreCase(""))
        {
            progressbar.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(CarImage)
                    .into(iv_carImage, new Callback() {

                        @Override
                        public void onSuccess() {
                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            iv_carImage.setImageResource(R.drawable.icon_camera_yellow);
                            progressbar.setVisibility(View.GONE);
                        }
                    });
        }
        else
        {
            progressbar.setVisibility(View.GONE);
            iv_carImage.setImageResource(R.drawable.icon_camera_yellow);
        }


        if(DriverLicense!=null && !DriverLicense.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(DriverLicense)
                    .fit()
                    .into(iv_driverLicence, new Callback() {

                        @Override
                        public void onSuccess() {
                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            iv_driverLicence.setImageResource(R.drawable.icon_camera_yellow);
                            progressbar.setVisibility(View.GONE);
                        }
                    });

        }
        else
        {
            iv_driverLicence.setImageResource(R.drawable.icon_camera_yellow);
        }

        if(DriverLicenseFront!=null && !DriverLicenseFront.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(DriverLicenseFront)
                    .fit()
                    .into(iv_driverLicence_Front, new Callback() {

                        @Override
                        public void onSuccess() {
                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            iv_driverLicence_Front.setImageResource(R.drawable.icon_camera_yellow);
                            progressbar.setVisibility(View.GONE);
                        }
                    });

        }
        else
        {
            iv_driverLicence_Front.setImageResource(R.drawable.icon_camera_yellow);
        }



        if(AccreditationCertificate!=null && !AccreditationCertificate.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(AccreditationCertificate)
                    .fit()
                    .into(iv_accreditation_certy, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            iv_accreditation_certy.setImageResource(R.drawable.icon_camera_yellow);
                        }
                    });
        }
        else
        {
            iv_accreditation_certy.setImageResource(R.drawable.icon_camera_yellow);
        }

        if(RegistrationCertificate!=null && !RegistrationCertificate.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(RegistrationCertificate)
                    .fit()
                    .into(iv_car_Registration, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            iv_car_Registration.setImageResource(R.drawable.icon_camera_yellow);
                        }
                    });
        }
        else
        {
            iv_car_Registration.setImageResource(R.drawable.icon_camera_yellow);
        }

        if(VehicleInsuranceCertificate!=null && !VehicleInsuranceCertificate.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(VehicleInsuranceCertificate)
                    .fit()
                    .into(iv_vehicle_insurance, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            iv_vehicle_insurance.setImageResource(R.drawable.icon_camera_yellow);
                        }
                    });
        }
        else
        {
            iv_vehicle_insurance.setImageResource(R.drawable.icon_camera_yellow);
        }

        if(GovernmentFrontImage!=null && !GovernmentFrontImage.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(GovernmentFrontImage)
                    .fit()
                    .into(ivGovernmentIdFront, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            ivGovernmentIdFront.setImageResource(R.drawable.icon_camera_yellow);
                        }
                    });
        }
        else
        {
            ivGovernmentIdFront.setImageResource(R.drawable.icon_camera_yellow);
        }

        if(GovernmentBackImage!=null && !GovernmentBackImage.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(GovernmentBackImage)
                    .fit()
                    .into(ivGovernmentIdBack, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            ivGovernmentIdBack.setImageResource(R.drawable.icon_camera_yellow);
                        }
                    });
        }
        else
        {
            ivGovernmentIdBack.setImageResource(R.drawable.icon_camera_yellow);
        }

        if(CriminalCertificate!=null && !CriminalCertificate.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(CriminalCertificate)
                    .fit()
                    .into(ivCriminalCertificate, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            ivCriminalCertificate.setImageResource(R.drawable.icon_camera_yellow);
                        }
                    });
        }
        else
        {
            ivCriminalCertificate.setImageResource(R.drawable.icon_camera_yellow);
        }

        if(AddressProof!=null && !AddressProof.equalsIgnoreCase(""))
        {
            Picasso.get()
                    .load(AddressProof)
                    .fit()
                    .into(ivAddressProof, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            ivAddressProof.setImageResource(R.drawable.icon_camera_yellow);
                        }
                    });
        }
        else
        {
            ivAddressProof.setImageResource(R.drawable.icon_camera_yellow);
        }

        /*rl_carImage.setEnabled(false);
        ll_driver_licence.setEnabled(false);
        ll_acrreditation_certy.setEnabled(false);
        ll_car_registration_certy.setEnabled(false);
        ll_vehicle_Insurance.setEnabled(false);*/

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.rl_carImage:
                ImageViewClicked = CAR_IMAGE;
                showPictureDialog();
                break;

            case R.id.iv_driverLicence:
                ImageViewClicked = DRIVER_LICENCE;
                showPictureDialog();
                break;

            case R.id.iv_driverLicence_Front:
                ImageViewClicked = DRIVER_LICENCE_FRONT;
                showPictureDialog();
                break;

            case R.id.ll_acrreditation_certy:
                ImageViewClicked = ACRREDITATION;
                showPictureDialog();
                break;

            case R.id.ll_car_registration_certy:
                ImageViewClicked = CAR_REGISTRATION;
                showPictureDialog();
                break;

            case R.id.ll_vehicle_Insurance:
                ImageViewClicked = VEHICLE_INSURANCE;
                showPictureDialog();
                break;

            case R.id.ivGovernmentIdFront:
                ImageViewClicked = GOVERNMENT_ID_FORNT;
                showPictureDialog();
                break;

            case R.id.ivGovernmentIdBack:
                ImageViewClicked = GOVERNMENT_ID_BACK;
                showPictureDialog();
                break;

            case R.id.llCriminalCertificate:
                ImageViewClicked = CRIMINAL_CERTI;
                showPictureDialog();
                break;

            case R.id.llAddressProof:
                ImageViewClicked = ADDRESS_PROOF;
                showPictureDialog();
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;

            case R.id.ll_back:
                onBackPressed();
                break;
        }
    }

    private void UpdateDocumentWithExpiry(String driverId, byte[] image, String expiry, int flag)
    {
        if (ClickCameraFlagResumeCount==0)
        {
            ClickCameraFlagResumeCount=1;
            Log.e("nnnnnnnnnnn","nnnnnnnnnnnnnnnnnnnnnnnnnn : "+flag);
            if (ConnectivityReceiver.isConnected())
            {
                if (dialogClass!=null)
                {
                    dialogClass.hideDialog();
                }
                dialogClass.showDialog();
                Map<String, Object> params = new HashMap<String, Object>();

                String url = WebServiceAPI.WEB_SERVICE_UPDATE_DOCS;

                params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_ID, driverId);
                if (flag==0)
                {
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_LICENCE_FRONT,image);
                    }
                }
                if (flag==1)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_LICENSE_EXPIRY, expiry);
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_LICENCE_BACK,image);
                    }
                }
                else if (flag==2)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_ACCREDITATION_EXPIRY, expiry);
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_ACCREDITATION_CERTY,image);
                    }
                }
                else if (flag==3)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_REGISTERATION_CERTY_EXPIRY, expiry);
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_REGISTRATION_CERTY,image);
                    }
                }
                else if (flag==4)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_INSURANCE_CERTY_EXPIRY, expiry);
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_VEHICLE_INSURANCE_CERY,image);
                    }
                }
                else if (flag==5)
                {
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_VEHICLE_IMAGE,image);
                    }
                }
                else if (flag==6)
                {
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GOVERNMENT_ID_FRONT,image);
                    }
                }
                else if (flag==7)
                {
                    params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GOVERNMENT_ID_EXPIRY, expiry);
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GOVERNMENT_ID_BACK,image);
                    }
                }
                else if (flag==8)
                {
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CRIMINAL_CERTI,image);
                    }
                }
                else if (flag==9)
                {
                    if (image!=null)
                    {
                        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ADDRESS_PROOF,image);
                    }
                }

                Log.e("url", "UpdateDriverProfile = " + url);
                Log.e("param", "UpdateDriverProfile = " + params);


                aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                    @Override
                    public void callback(String url, JSONObject json, AjaxStatus status) {

                        try
                        {
                            int responseCode = status.getCode();
                            Log.e("responseCode", "UpdateDriverProfile = " + responseCode);
                            Log.e("Response", "UpdateDriverProfile = " + json);

                            dialogClass.hideDialog();
                            if (json != null)
                            {
                                if (json.has("status"))
                                {
                                    if (json.has("profile"))
                                    {
                                        JSONObject driverProfile = json.getJSONObject("profile");
                                        if (driverProfile!=null)
                                        {
                                            String DriverLicense="", AccreditationCertificate="", DriverLicenseExpire="", AccreditationCertificateExpire="",FrontGovIdCard="",
                                                    BackGovIdCard="",CriminalCertificate="",AddressProof="",GovIdExpire="";


                                            if (driverProfile.has("DriverLicenseFront"))
                                            {
                                                DriverLicense = driverProfile.getString("DriverLicenseFront");
                                                SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE_FRONT, DriverLicense, activity);
                                            }
                                            if (driverProfile.has("DriverLicenseBack"))
                                            {
                                                SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE_BACK, driverProfile.getString("DriverLicenseBack"), activity);
                                            }


                                            /*if (driverProfile.has("DriverDadMobile"))
                                            {
                                                SessionSave.saveUserSession(Comman.USER_DAD_NUMBER, driverProfile.getString("DriverDadMobile"),  getActivity());
                                            }
                                            if (driverProfile.has("DriverWifeMobile"))
                                            {
                                                SessionSave.saveUserSession(Comman.USER_WIFE_NUMBER, driverProfile.getString("DriverWifeMobile"),  getActivity());
                                            }*/


                                            if (driverProfile.has("AccreditationCertificate"))
                                            {
                                                AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                                SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, activity);
                                            }
                                            if (driverProfile.has("DriverLicenseExpire"))
                                            {
                                                DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                                SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, activity);
                                            }
                                            if (driverProfile.has("AccreditationCertificateExpire"))
                                            {
                                                AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                                SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, activity);
                                            }

                                            if (driverProfile.has("FrontGovIdCard"))
                                            {
                                                FrontGovIdCard = driverProfile.getString("FrontGovIdCard");
                                                SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_FRONT_ID, FrontGovIdCard, activity);
                                            }
                                            if (driverProfile.has("BackGovIdCard"))
                                            {
                                                BackGovIdCard = driverProfile.getString("BackGovIdCard");
                                                SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_BACK_ID, BackGovIdCard, activity);
                                            }
                                            if (driverProfile.has("CriminalCertificate"))
                                            {
                                                CriminalCertificate = driverProfile.getString("CriminalCertificate");
                                                SessionSave.saveUserSession(Comman.USER_PROFILE_CRIMINAL_CERTI, CriminalCertificate, activity);
                                            }
                                            if (driverProfile.has("AddressProof"))
                                            {
                                                AddressProof = driverProfile.getString("AddressProof");
                                                SessionSave.saveUserSession(Comman.USER_PROFILE_ADDRESS_PROOF, AddressProof, activity);
                                            }
                                            if (driverProfile.has("GovIdExpire"))
                                            {
                                                GovIdExpire = driverProfile.getString("GovIdExpire");
                                                SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_EXPIRY, GovIdExpire, activity);
                                            }

                                            if (driverProfile.has("Vehicle"))
                                            {
                                                JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                                if (driverVehicle!=null)
                                                {
                                                    String RegistrationCertificate, VehicleInsuranceCertificate
                                                            , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage,
                                                            VehicleLeftImage, VehicleRightImage;

                                                    if (driverVehicle.has("RegistrationCertificate"))
                                                    {
                                                        RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                        SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                    }
                                                    if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                    {
                                                        VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                        SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                    }
                                                    if (driverVehicle.has("RegistrationCertificateExpire"))
                                                    {
                                                        RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                        SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                    }
                                                    if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                    {
                                                        VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                        SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                    }
                                                    if (driverVehicle.has("VehicleImage"))
                                                    {
                                                        VehicleImage = driverVehicle.getString("VehicleImage");
                                                        SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                    }
                                                    if (driverVehicle.has("VehicleLeftImage"))
                                                    {
                                                        VehicleLeftImage = driverVehicle.getString("VehicleLeftImage");
                                                        SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, VehicleLeftImage, activity);
                                                    }
                                                    if (driverVehicle.has("VehicleRightImage"))
                                                    {
                                                        VehicleRightImage = driverVehicle.getString("VehicleRightImage");
                                                        SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, VehicleRightImage, activity);
                                                    }
                                                }

                                                dialogClass.hideDialog();
                                                if (json.has("message"))
                                                {
                                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.success_message));
                                                }
                                            }
                                            else
                                            {
                                                dialogClass.hideDialog();
                                                if (json.has("message"))
                                                {
                                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.success_message));
                                                }
                                            }
                                            Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, activity));
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"),  getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("json", "no status found");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("json", "null");
                                dialogClass.hideDialog();
                                errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                            }
                        } catch (Exception e) {
                            Log.e("UpdateDriverProfile", "Exception : " + e.toString());
                            dialogClass.hideDialog();

                            errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));

                        }
                    }
                }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
    }

    private void showPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE_CAMERA);
        }
        else if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE_STORAGE);
        }
        else
        {
            final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_photo_choose);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            //lp.width = Comman.DEVICE_WIDTH;
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);

            TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
            TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
            TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
            LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
            ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

            ll_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            tv_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            iv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    dialog.dismiss();
                }
            });

            tv_gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            choosePhotoFromGallary();
                        }
                    },800);
                }
            });

            tv_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            takePhotoFromCamera();
                        }
                    },800);

                }
            });

            dialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        bitmapSumsung=null;
        ClickCameraFlagResumeCount=0;
        ClickCameraFlagResume = 1;
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        bitmapSumsung=null;
        ClickCameraFlagResumeCount=0;
        ClickCameraFlagResume = 1;
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }
        else
        {
            userChoosenTask = chooseCamera;
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
            startActivityForResult(intent, CAMERA);
        }
    }

    public String getDeviceName()
    {
        String manufacturer = Build.MANUFACTURER;
        return manufacturer;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission CAMERA permission granted");
                    showPictureDialog();
                }
                break;

            case MY_REQUEST_CODE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission GALLERY permission granted");
                    showPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                onCaptureImageResult(data);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_driverLicence_Front);
                DriverLicence_Front_ByteImage = ConvertToByteArray(bitmapImage);
                if (DriverLicence_Front_ByteImage!=null)
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                        UpdateDocumentWithExpiry(driverId, DriverLicence_Front_ByteImage, "", 0);
                    }
                }
                else
                {
                    errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                }
            }
            else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_driverLicence);
                DriverLicence_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
            }
            else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_accreditation_certy);
                Accreditation_certy_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
            }
            else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_car_Registration);
                Car_Registration_ByteImage = ConvertToByteArray(bitmapImage);
                if (Car_Registration_ByteImage!=null)
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                        UpdateDocumentWithExpiry(driverId, Car_Registration_ByteImage, "", 3);
                    }
                }
                else
                {
                    errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                }
//                OpenPopopFor_datePicker(tv_carRegistration_expiry, iv_car_Registration);
            }
            else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_vehicle_insurance);
                Vehicle_insurance_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
            }
            else if(ImageViewClicked.equalsIgnoreCase(CAR_IMAGE))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(iv_carImage);
                CarImage_ByteImage = ConvertToByteArray(bitmapImage);
                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                if (driverId!=null && !driverId.equalsIgnoreCase(""))
                {
                    UpdateDocumentWithExpiry(driverId, CarImage_ByteImage, "", 5);
                }
            }
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivGovernmentIdFront);
                Government_id_front_ByteImage = ConvertToByteArray(bitmapImage);
                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                if (driverId!=null && !driverId.equalsIgnoreCase(""))
                {
                    UpdateDocumentWithExpiry(driverId, Government_id_front_ByteImage, "", 6);
                }
            }
            else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
            {
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivGovernmentIdBack);
                Government_id_back_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdBack);
            }
            else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI)){
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivCriminalCertificate);
                Criminal_Certi_ByteImage = ConvertToByteArray(bitmapImage);

                if (Criminal_Certi_ByteImage!=null)
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                        UpdateDocumentWithExpiry(driverId, Criminal_Certi_ByteImage, "", 8);
                    }
                }
                else
                {
                    errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                }

            }
            else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF)){
                Picasso.get()
                        .load(data.getData())
                        .fit()
                        .into(ivAddressProof);
                Address_Proof_ByteImage = ConvertToByteArray(bitmapImage);

                if (Address_Proof_ByteImage!=null)
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                        UpdateDocumentWithExpiry(driverId, Address_Proof_ByteImage, "", 9);
                    }
                }
                else
                {
                    errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "PickNGo", null);
        return Uri.parse(path);
    }

    private void onCaptureImageResult(Intent data)
    {
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            bitmapSumsung = (Bitmap) data.getExtras().get("data");
            imageUri = getImageUri(activity, bitmapSumsung);
        }
        try
        {
            Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            Log.e("#########","bmp height = "+thumbnail.getHeight());
            Log.e("#########","bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if(ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_driverLicence_Front);
                    DriverLicence_Front_ByteImage = ConvertToByteArray(thumbnail);
                    if (DriverLicence_Front_ByteImage!=null)
                    {
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                        if (driverId!=null && !driverId.equalsIgnoreCase("")) {

                            UpdateDocumentWithExpiry(driverId, DriverLicence_Front_ByteImage, "", 0);

                        }
                    }
                    else
                    {
                        errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                    }
                }
                else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_driverLicence);
                    DriverLicence_ByteImage = ConvertToByteArray(thumbnail);

                    OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
                }
                else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_accreditation_certy);
                    Accreditation_certy_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
                }
                else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_car_Registration);
                    Car_Registration_ByteImage = ConvertToByteArray(thumbnail);
                    if (Car_Registration_ByteImage!=null)
                    {
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                        if (driverId!=null && !driverId.equalsIgnoreCase("")) {

                                UpdateDocumentWithExpiry(driverId, Car_Registration_ByteImage, "", 3);

                        }
                    }
                    else
                    {
                        errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                    }
                }
                else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_vehicle_insurance);
                    Vehicle_insurance_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
                }
                else if(ImageViewClicked.equalsIgnoreCase(CAR_IMAGE))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(iv_carImage);
                    CarImage_ByteImage = ConvertToByteArray(thumbnail);
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase("")) {

                            Log.e("kkkkkkkk","ClickCameraFlagResumeCount : "+ClickCameraFlagResumeCount);
                            UpdateDocumentWithExpiry(driverId, CarImage_ByteImage, "", 5);

                    }
                }
                else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivGovernmentIdFront);
                    Government_id_front_ByteImage = ConvertToByteArray(thumbnail);
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        UpdateDocumentWithExpiry(driverId, Government_id_front_ByteImage, "", 6);
                    }
                }
                else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivGovernmentIdBack);
                    Government_id_back_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdBack);
                }
                else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivCriminalCertificate);
                    Criminal_Certi_ByteImage = ConvertToByteArray(thumbnail);
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        UpdateDocumentWithExpiry(driverId, Criminal_Certi_ByteImage, "", 8);
                    }
                }
                else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
                {
                    Picasso.get()
                            .load(imageUri)
                            .fit()
                            .into(ivAddressProof);
                    Address_Proof_ByteImage = ConvertToByteArray(thumbnail);
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        UpdateDocumentWithExpiry(driverId, Address_Proof_ByteImage, "", 9);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","exception = "+e.getMessage());
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void OpenPopopFor_datePicker(final TextView textView, final ImageView imageView)
    {
        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 1000);*/
        if (ClickCameraFlagResume == 1)
        {
            Calendar newCalendar = Calendar.getInstance();
            if (fromDatePickerDialog!=null && fromDatePickerDialog.isShowing())
            {
                fromDatePickerDialog.dismiss();
            }
            fromDatePickerDialog = new DatePickerDialog(activity, R.style.DialogThemeReal, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    ClickCameraFlagResume = 0;
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    textView.setText(dateFormatter.format(newDate.getTime()));

                    if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
                    {
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                        if (driverId!=null && !driverId.equalsIgnoreCase(""))
                        {
                            if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                            {
                                UpdateDocumentWithExpiry(driverId,DriverLicence_ByteImage,tv_driver_licence_expiry.getText().toString().trim(),1);
                            }
                            else if (ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                            {
                                UpdateDocumentWithExpiry(driverId,Accreditation_certy_ByteImage,tv_accreditation_expiry.getText().toString().trim(),2);
                            }
                            else if (ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                            {
                                UpdateDocumentWithExpiry(driverId,Car_Registration_ByteImage,tv_carRegistration_expiry.getText().toString().trim(),3);
                            }
                            else if (ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                            {
                                UpdateDocumentWithExpiry(driverId,Vehicle_insurance_ByteImage,tv_vehicle_Insurance_expiry.getText().toString().trim(),4);
                            }
                            else if (ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
                            {
                                UpdateDocumentWithExpiry(driverId,Government_id_back_ByteImage,tvGovernmentIdExpiry.getText().toString().trim(),7);
                            }

                        }
                    }
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        fromDatePickerDialog.setSpinnersShown
            fromDatePickerDialog.setTitle(activity.getResources().getString(R.string.please_add_expiry_date));
            fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            fromDatePickerDialog.show();
            fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    ClickCameraFlagResume = 0;
                    textView.setText("");
                    imageView.setImageResource(R.drawable.icon_camera_yellow);
                }
            });
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (fromDatePickerDialog!=null)
        {
            fromDatePickerDialog.dismiss();
        }

        if (dialogClass!=null)
        {
            dialogClass.hideDialog();
        }
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity != null) {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            if (bitmapSumsung != null)
            {
                imageUri = getImageUri(activity, bitmapSumsung);
                try
                {
                    Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
                    Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    Log.e("#########","bmp height = "+thumbnail.getHeight());
                    Log.e("#########","bmp width = "+thumbnail.getWidth());
                    thumbnail = getResizedBitmap(thumbnail,400);
                    bitmapImage=thumbnail;

                    if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
                    {
                        if(ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE_FRONT))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(iv_driverLicence_Front);
                            DriverLicence_Front_ByteImage = ConvertToByteArray(thumbnail);
                            if (DriverLicence_Front_ByteImage!=null)
                            {
                                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                                if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                                    UpdateDocumentWithExpiry(driverId, DriverLicence_Front_ByteImage, "", 0);
                                }
                            }
                            else
                            {
                                errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                            }
                        }
                        else if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(iv_driverLicence);
                            DriverLicence_ByteImage = ConvertToByteArray(thumbnail);

                            OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
                        }
                        else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(iv_accreditation_certy);
                            Accreditation_certy_ByteImage = ConvertToByteArray(thumbnail);
                            OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
                        }
                        else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(iv_car_Registration);
                            Car_Registration_ByteImage = ConvertToByteArray(thumbnail);
                            if (Car_Registration_ByteImage!=null)
                            {
                                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                                if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                                    UpdateDocumentWithExpiry(driverId, Car_Registration_ByteImage, "", 3);
                                }
                            }
                            else
                            {
                                errorDialogClass.showDialog(activity.getResources().getString(R.string.no_media_select), getString(R.string.error_message));
                            }
                        }
                        else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(iv_vehicle_insurance);
                            Vehicle_insurance_ByteImage = ConvertToByteArray(thumbnail);
                            OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
                        }
                        else if(ImageViewClicked.equalsIgnoreCase(CAR_IMAGE))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(iv_carImage);
                            CarImage_ByteImage = ConvertToByteArray(thumbnail);
                            String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                            if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                                UpdateDocumentWithExpiry(driverId, CarImage_ByteImage, "", 5);
                            }
                        }
                        else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_FORNT))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(ivGovernmentIdFront);
                            Government_id_front_ByteImage = ConvertToByteArray(thumbnail);
                            String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                            if (driverId!=null && !driverId.equalsIgnoreCase(""))
                            {
                                UpdateDocumentWithExpiry(driverId, Government_id_front_ByteImage, "", 6);
                            }
                        }
                        else if(ImageViewClicked.equalsIgnoreCase(GOVERNMENT_ID_BACK))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(ivGovernmentIdBack);
                            Government_id_back_ByteImage = ConvertToByteArray(bitmapImage);
                            OpenPopopFor_datePicker(tvGovernmentIdExpiry, ivGovernmentIdBack);
                        }
                        else if(ImageViewClicked.equalsIgnoreCase(CRIMINAL_CERTI))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(ivCriminalCertificate);
                            Criminal_Certi_ByteImage = ConvertToByteArray(bitmapImage);
                            String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                            if (driverId!=null && !driverId.equalsIgnoreCase(""))
                            {
                                UpdateDocumentWithExpiry(driverId, Criminal_Certi_ByteImage, "", 8);
                            }

                        }
                        else if(ImageViewClicked.equalsIgnoreCase(ADDRESS_PROOF))
                        {
                            Picasso.get()
                                    .load(imageUri)
                                    .fit()
                                    .into(ivAddressProof);
                            Address_Proof_ByteImage = ConvertToByteArray(bitmapImage);
                            String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                            if (driverId!=null && !driverId.equalsIgnoreCase(""))
                            {
                                UpdateDocumentWithExpiry(driverId, Address_Proof_ByteImage, "", 9);
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    Log.e("call","exception = "+e.getMessage());
                }
            }
        }
    }
}