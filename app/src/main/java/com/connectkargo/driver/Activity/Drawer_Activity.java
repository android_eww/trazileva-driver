package com.connectkargo.driver.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import androidx.annotation.NonNull;

import com.connectkargo.driver.Adapter.MenuListAdapter;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.SocketSingleObject;
import com.connectkargo.driver.Fragment.DispatchJob_Fragment;
import com.connectkargo.driver.Fragment.Hailed_Fair_Fragment;
import com.connectkargo.driver.Fragment.Map_Fragment;
import com.connectkargo.driver.Fragment.MyJob_Fragment;
import com.connectkargo.driver.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.connectkargo.driver.Been.MenuList_Been;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.LocationService;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.GPSTracker;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import library.minimize.com.chronometerpersist.ChronometerPersist;


public class Drawer_Activity extends BaseActivity {

    public String TAG = "Drawer_Activity";
    public static Drawer_Activity activity;

    private Toolbar toolbar;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private MenuListAdapter drawerListAdapter;
    private List<MenuList_Been> menuList_beens = new ArrayList<MenuList_Been>();
    private DrawerLayout drawer;
    public LinearLayout ll_DrawerLayout, ll_ProfileDetail, ll_drawer_icon, ll_rating;
    public TextView header_name, header_email, header_ratePoint;
    public CircleImageView header_Prof_image;
    public ProgressBar progBar_HeaderImage;
    private ListView lv_drawer;

    public static LinearLayout main_layout;

    public TextView  tv_title_screen;
    public ImageView iv_title;

    public SwitchCompat switch_online;
    public static Transformation mTransformation;

    ////for Passcode back press
    public static int PasscodeBackPress = 0;

    private static final int PERMISSION_REQUEST_CODE = 1, REQUEST_CALL_PHONE = 3, DRAW_OVER_OTHER_APP_PERMISSION=99;

    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static int tabItemIndex = 0;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;

    public GPSTracker gpsTracker;
    public Intent intent;

    public Runnable runnable;
    public Handler handler, handler1;
    public int count = 30;

    public static BottomSheetBehavior mBottomSheetBehavior;
    public static LinearLayout ll_bottomShit_Main;

    public static TextView tv_drop_off_location, tv_pick_up_location, tv_passName, tv_PassengerNote, tv_PassengerFlightNo,
            tv_passContactNo;
    public static CircleImageView iv_passenger;
    public static ImageView ivCallPassenger, ivChat;
    public static LinearLayout ll_closeInfo, ll_PassengerNote, ll_PassengerFlightNo;

    public String DriverDuty;
    public int requestDialogOntimeVisible=0;

    //////////////////
//    FrameLayout frame_home_fragment;

    public LinearLayout ll_home, ll_dispatch, ll_mayJob, ll_hailed_fare, ll_Sos;
    public ImageView iv_home, iv_dispatch, iv_mayJob, iv_hailed_fare;
    public static LinearLayout ll_Meter;
    public static TextView tv_meterFareOutSide;
    public TextView tv_home, tv_dispatch, tv_mayJob, tv_hailed_fare;
    public Map_Fragment _fragmentMap;
    public DispatchJob_Fragment dispatchJob_fragment;
    public MyJob_Fragment myJob_fragment;
    public Hailed_Fair_Fragment hailed_fail_fragment;
    public static String passMobNo="";

    public static int callCardListMethod = 0;

    public Ringtone ringTone;
    public Vibrator vibrateFlag;

    public static int flagResumePass=0;

    public ErrorDialogClass errorDialogClass;
    public static Dialog AssignAllDialog;
    public String meterActiveFlag;

    public static Chronometer chronometerView;
    public static ChronometerPersist chronometerPersist;
    public static TextView tv_meter_digit, tv_waitinCost, tv_waitingTime, tv_BaseFare, tv_hireFee, tv_startTripMeter, tv_endTripMeter,
            tv_start_waitingTime, tv_end_waitingTime, tv_speedTrip;
    public LinearLayout ll_back_Dailog, ll_Sos_Dailog;
    public static Spinner spinner;
    public List<String> ModelNameList = new ArrayList<String>();
    public List<String> ModelIdList = new ArrayList<String>();
    public ArrayAdapter<String> spinnerArrayAdapter;
    public static String selectedModelId = "";
    public DialogClass dialogClass;
    public AQuery aQuery;

    public static Dialog dialogMeter;
    public LocationService myService;
    public LocationManager locationManager;
    public String BookingId = "", Booking_message = "", Booking_Type="", Booking_PickupLocation="", Booking_DropoffLocation="", Booking_GrandTotal="", estFare = "";
    public String ModelName="", baseFare="", kmTaken="", per_km_charge="", tripFare="", bookingFee="", Total="", waitingCost="";

    public static int flagCallDriverOnline = 0;
    public int flagBookError=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        activity = Drawer_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        gpsTracker = new GPSTracker(Drawer_Activity.this);
        meterActiveFlag="";
        flagCallDriverOnline = 0;

        bindService();

        try
        {
            Uri path =  Uri.parse("android.resource://"+ activity.getPackageName() + "/raw/ring_pick_n_go");
            ringTone = RingtoneManager.getRingtone(getApplicationContext(), path);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        vibrateFlag = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        callCardListMethod = 0;
        PasscodeBackPress = 0;
        flagResumePass=0;
        checkGPS();

        chronometerView = findViewById(R.id.chronometerView);
        SharedPreferences sharedPreferences = getSharedPreferences("ChronometerSample", MODE_PRIVATE);
        chronometerPersist = ChronometerPersist.getInstance(chronometerView, sharedPreferences);
        chronometerPersist.hourFormat(true);

        chronometerView.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer)
            {
                GetAllWaitingMilisec();
            }
        });

        ll_Meter = (LinearLayout) findViewById(R.id.ll_Meter);
        tv_meterFareOutSide = (TextView) findViewById(R.id.tv_meterFareOutSide);
        ll_Meter.setVisibility(View.GONE);
        ll_Meter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                OpenDialogForMeter();
            }
        });


        init();
    }

    private void init() {

        toolbar = new Toolbar(activity);
        setSupportActionBar(toolbar);

        lv_drawer = findViewById(R.id.lv_drawer);
        ll_DrawerLayout = findViewById(R.id.ll_drawer_layout);
        ll_ProfileDetail = findViewById(R.id.ll_ProfileDetail);
        ll_drawer_icon = findViewById(R.id.ll_drawer_icon);
        ll_rating = findViewById(R.id.ll_rating);

        header_name = findViewById(R.id.header_name);
        header_email = findViewById(R.id.header_email);
        header_Prof_image = findViewById(R.id.img_header_bg);
        header_ratePoint = findViewById(R.id.header_ratePoint);
        progBar_HeaderImage = findViewById(R.id.progBar_HeaderImage);

        ll_Sos = findViewById(R.id.ll_Sos);
        ll_Sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CallSos(activity);
            }
        });
        switch_online = findViewById(R.id.switch_online);

        iv_title = findViewById(R.id.iv_title);
        tv_title_screen = findViewById(R.id.tv_title_screen);

//        mTransformation = new RoundedTransformationBuilder()
//                .cornerRadiusDp(100)
//                .borderColor(ContextCompat.getColor(activity, R.color.appColor))
//                .borderWidthDp(2)
//                .oval(true)
//                .build();


        header_Prof_image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                drawer.closeDrawers();
                handler = new Handler();
                runnable = new Runnable() {

                    @Override
                    public void run() {
                        try
                        {

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        finally
                        {
//                            Intent intent = new Intent(activity, Profile_Activity.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        }
                    }
                };
                handler.postDelayed(runnable, 300);
            }
        });

        ll_ProfileDetail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                drawer.closeDrawers();
                handler = new Handler();
                runnable = new Runnable() {

                    @Override
                    public void run() {
                        try
                        {

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        finally
                        {
//                            Intent intent = new Intent(activity, Profile_Activity.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        }
                    }
                };
                handler.postDelayed(runnable, 300);

            }
        });

        handler = new Handler();
        handler1 = new Handler();


       /* switch_online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("onCheckedChanged","onCheckedChanged : "+isChecked);
                if (isChecked)
                {
                    StartConnectSocket();
                }
                else
                {
                    StartDisconnectSocket();
                }
            }
        });*/

        switch_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                OnSwitchChange();
            }
        });

        DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
        if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") && DriverDuty.equalsIgnoreCase("1"))
        {
            switch_online.setChecked(true);
            StartConnectSocket();
        }
        else
        {
            switch_online.setChecked(false);
            StartDisconnectSocket();
        }

        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.home), "","", R.drawable.ic_home_white));
        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.my_job), "","", R.drawable.ic_my_job_white));
        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.bid_list), "", "", R.drawable.ic_auction));
        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.profile), "","", R.drawable.ic_profile_white));
        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.change_password),"","", R.drawable.ic_change_pass));
        menuList_beens.add(new MenuList_Been(getResources().getString(R.string.logout), "","", R.drawable.ic_logout_white));

//        menuList_beens.add(new MenuList_Been("Payment Options","","", R.drawable.icon_payment));
//        menuList_beens.add(new MenuList_Been("My Wallet","","", R.drawable.icon_wallet_white));
////        menuList_beens.add(new MenuList_Been("Weekly Earning","","",R.drawable.icon_earning));
//        menuList_beens.add(new MenuList_Been("Invite Drivers","","", R.drawable.icon_invite));
//        menuList_beens.add(new MenuList_Been("Settings","","", R.drawable.icon_setting_white));
//        menuList_beens.add(new MenuList_Been("Meter","","", R.drawable.icon_meter_white));
//        menuList_beens.add(new MenuList_Been("Trip to Destination","","", R.drawable.ic_trip_to_destin));
//        menuList_beens.add(new MenuList_Been("Share Rides","","", R.drawable.ic_share_ride));
//        menuList_beens.add(new MenuList_Been("Logout","","", R.drawable.icon_logout));


        ll_bottomShit_Main = findViewById(R.id.ll_bottomShit_Main);
        tv_drop_off_location = findViewById(R.id.tv_drop_off_location);
        tv_pick_up_location = findViewById(R.id.tv_pick_up_location);
        tv_passName = findViewById(R.id.tv_passName);
        iv_passenger = findViewById(R.id.iv_passenger);
        ivCallPassenger = findViewById(R.id.ivCallPassenger);
        ivChat = findViewById(R.id.ivChat);
        ll_closeInfo = findViewById(R.id.ll_closeInfo);
        ll_PassengerFlightNo = findViewById(R.id.ll_PassengerFlightNo);
        tv_PassengerFlightNo = findViewById(R.id.tv_PassengerFlightNo);
        tv_passContactNo = findViewById(R.id.tv_passContactNo);
        ll_PassengerNote = findViewById(R.id.ll_PassengerNote);
        tv_PassengerNote = findViewById(R.id.tv_PassengerNote);

        ivCallPassenger.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (passMobNo!=null && !passMobNo.equalsIgnoreCase(""))
                {
                    Log.e("passMobNo","passMobNo : "+passMobNo);
                    callToDriver();
                }
            }
        });

        ivChat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.e(TAG,"ivChat.setOnClickListener()");

                Intent intent = new Intent(Drawer_Activity.activity, Chat_Activity.class);
                intent.putExtra(Constants.INTENT_BOOKING_ID,
                        SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, activity));
                intent.putExtra(Constants.INTENT_RECEIVER_ID, SessionSave.getUserSession(Comman.PASSENGER_ID, activity));
                intent.putExtra(Constants.INTENT_RECEIVER_NAME, SessionSave.getUserSession(Comman.PASSENGER_NAME, activity));
                if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR,
                        Drawer_Activity.activity).equalsIgnoreCase("0"))
                {
                    intent.putExtra(Constants.INTENT_BOOKING_TYPE, "book_now");
                }
                else
                {
                    intent.putExtra(Constants.INTENT_BOOKING_TYPE, "book_later");
                }
                startActivity(intent);
                Drawer_Activity.activity.overridePendingTransition(R.anim.right_in,R.anim.left_out);
            }
        });

        ll_closeInfo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        ll_bottomShit_Main.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

            }
        });



        FrameLayout parentThatHasBottomSheetBehavior = (FrameLayout) findViewById(R.id.frame_layout);
        mBottomSheetBehavior = BottomSheetBehavior.from(parentThatHasBottomSheetBehavior);

        if (mBottomSheetBehavior != null) {
            Log.e("call", "mBottomSheetBehavior = " + mBottomSheetBehavior.getState());
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    Log.e("call", "onStateChanged = " + mBottomSheetBehavior.getState());
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    Log.e("call", "onSlide = " + mBottomSheetBehavior.getState());
                }
            });
        }
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);


        ll_home =  findViewById(R.id.ll_home);
        ll_dispatch = findViewById(R.id.ll_dispatch);
        ll_mayJob = findViewById(R.id.ll_mayJob);
        ll_hailed_fare = findViewById(R.id.ll_hailed_fare);

        iv_home = findViewById(R.id.iv_home);
        iv_dispatch = findViewById(R.id.iv_dispatch);
        iv_mayJob = findViewById(R.id.iv_mayJob);
        iv_hailed_fare = findViewById(R.id.iv_hailed_fare);

        tv_home = findViewById(R.id.tv_home);
        tv_dispatch = findViewById(R.id.tv_dispatch);
        tv_mayJob = findViewById(R.id.tv_mayJob);
        tv_hailed_fare = findViewById(R.id.tv_hailed_fare);


        _fragmentMap = new Map_Fragment();
        dispatchJob_fragment = new DispatchJob_Fragment();
        myJob_fragment = new MyJob_Fragment();
        hailed_fail_fragment = new Hailed_Fair_Fragment();

        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_Home();
            }
        });
        ll_dispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_Dispatch();
            }
        });
        ll_mayJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_mayJob.setEnabled(false);
                setTab_MyJob();
            }
        });
        ll_hailed_fare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_HailedFare();
            }
        });

        SetDrawer();

        setTab_Home();
    }

    public void OnSwitchChange()
    {
        switch_online.setEnabled(false);
        String startTRipFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, activity);
        meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity);
        if (startTRipFlag!=null && !startTRipFlag.equalsIgnoreCase("") && startTRipFlag.equalsIgnoreCase("1"))
        {
            String TripFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, activity);
            if (TripFlag!=null && !TripFlag.equalsIgnoreCase(""))
            {
                if (TripFlag.equalsIgnoreCase("0"))
                {
                    Map_Fragment.clearMap();
                }
            }
            String UserId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                switch_online.setChecked(false);
                StartDisconnectSocket();
                switch_online.setEnabled(true);
            }
            else
            {
                try
                {
                    int locationMode = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
                    int LOCATION_MODE_HIGH_ACCURACY = 3;
                    if (locationMode == LOCATION_MODE_HIGH_ACCURACY)
                    {
                        if (checkPermission())
                        {
                            gpsTracker = new GPSTracker(Drawer_Activity.this);
                            if (gpsTracker.canGetLocation())
                            {
                                gpsTracker.getLocation();

                                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";

                                Log.e("latitude", "" + Constants.newgpsLatitude);
                                Log.e("longitude", "" + Constants.newgpsLongitude);

                                if (UserId != null && !UserId.equalsIgnoreCase(""))
                                {
                                    ChangeDriverOnlineStatus(UserId);
                                }
                                else
                                {
                                    switch_online.setEnabled(true);
                                    switch_online.setChecked(false);
                                    StartDisconnectSocket();
                                }
                            }
                            else
                            {
                                switch_online.setEnabled(true);
                                switch_online.setChecked(false);
                                StartDisconnectSocket();
                            }
                        }
                        else
                        {
                            requestPermission();
                        }
                    }
                    else
                    {
                        switch_online.setEnabled(true);
                        switch_online.setChecked(false);
                        StartDisconnectSocket();

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Drawer_Activity.this);
                        alertDialog.setTitle(activity.getResources().getString(R.string.new_gps_settings_title));
                        alertDialog.setMessage(activity.getResources().getString(R.string.please_set_your_location_mode_to_high_accuracy));
                        alertDialog.setPositiveButton(activity.getResources().getString(R.string.setting),
                                new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                    }
                                });
                        alertDialog.show();

                        AlertDialog alertDialog1 = alertDialog.create();
                        Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
                        Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

                        if (Pbutton != null)
                        {
                            Pbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                        }
                        else
                        {
                            Log.e("Drwaer_activity","takePermision():- " + "Pbutton == null");
                        }

                        if (Pbutton != null)
                        {
                            Nbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                        }
                        else
                        {
                            Log.e("Drawer_activity","takePermision():- " + "Nbutton == null");

                        }
                    }
                }
                catch (Settings.SettingNotFoundException e)
                {
                    e.printStackTrace();
                    switch_online.setEnabled(true);
                    switch_online.setChecked(false);
                    StartDisconnectSocket();
                }
            }
        }
        else
        {
            meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity);
            if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
            {
                errorDialogClass.showDialog("First, you have to stop your private meter trip.", getString(R.string.info_message));
                DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                switch_online.setChecked(false);
                switch_online.setEnabled(true);
                StartDisconnectSocket();
            }
            else
            {
                String TripFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, activity);
                if (TripFlag!=null && !TripFlag.equalsIgnoreCase(""))
                {
                    if (TripFlag.equalsIgnoreCase("0"))
                    {
                        Map_Fragment.clearMap();
                    }
                }
                String UserId = SessionSave.getUserSession(Comman.USER_ID, activity);
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                }
                else
                {
                    try
                    {
                        int locationMode = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
                        int LOCATION_MODE_HIGH_ACCURACY = 3;
                        if (locationMode == LOCATION_MODE_HIGH_ACCURACY)
                        {
                            if (checkPermission())
                            {
                                gpsTracker = new GPSTracker(Drawer_Activity.this);
                                if (gpsTracker.canGetLocation())
                                {
                                    gpsTracker.getLocation();

                                    Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                                    Constants.newgpsLongitude = gpsTracker.getLongitude() + "";

                                    Log.e("latitude", "" + Constants.newgpsLatitude);
                                    Log.e("longitude", "" + Constants.newgpsLongitude);

                                    if (UserId != null && !UserId.equalsIgnoreCase(""))
                                    {
                                        ChangeDriverOnlineStatus(UserId);
                                    }
                                    else
                                    {
                                        switch_online.setEnabled(true);
                                        switch_online.setChecked(false);
                                        StartDisconnectSocket();
                                    }
                                }
                                else
                                {
                                    switch_online.setEnabled(true);
                                    switch_online.setChecked(false);
                                    StartDisconnectSocket();
                                }
                            }
                            else
                            {
                                requestPermission();
                            }
                        }
                        else
                        {
                            switch_online.setEnabled(true);
                            switch_online.setChecked(false);
                            StartDisconnectSocket();

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Drawer_Activity.this);
                            alertDialog.setTitle(activity.getResources().getString(R.string.new_gps_settings_title));
                            alertDialog.setMessage(activity.getResources().getString(R.string.please_set_your_location_mode_to_high_accuracy));
                            alertDialog.setPositiveButton(activity.getResources().getString(R.string.setting),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which)
                                        {
                                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                        }
                                    });

                            AlertDialog alertDialog1 = alertDialog.create();
                            Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
                            Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

                            if (Pbutton != null)
                            {
                                Pbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                            }
                            else
                            {
                                Log.e("Drawer_activity","takePermision():- " + "Pbutton == null");
                            }

                            if (Pbutton != null)
                            {
                                Nbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                            }
                            else
                            {
                                Log.e("Drawer_Activity","takePermision():- " + "Nbutton == null");

                            }

                            alertDialog.show();
                        }
                    }
                    catch (Settings.SettingNotFoundException e)
                    {
                        e.printStackTrace();
                        switch_online.setEnabled(true);
                        switch_online.setChecked(false);
                        StartDisconnectSocket();
                    }
                }
            }
        }
    }

    private void SetDrawer()
    {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerListAdapter = new MenuListAdapter(Drawer_Activity.this, menuList_beens,drawer,ll_DrawerLayout);
        lv_drawer.setAdapter(drawerListAdapter);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawer,toolbar, R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        ll_drawer_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START))
                {
                    drawer.closeDrawer(ll_DrawerLayout);
                }
                else
                {
                    drawer.openDrawer(ll_DrawerLayout);
                    loadNavHeader();
                }
            }
        });

        loadNavHeader();
    }

    public void askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }

    public void setTab_Home()
    {
        Log.e(TAG,"setTab_Home()");

        tabItemIndex=0;
        flagResumePass=0;
        navItemIndex=0;
//        activity.stopService(new Intent(activity, BubbleService.class));
        shouldLoadHomeFragOnBackPress = false;
        /*https://guides.codepath.com/android/Creating-and-Using-Fragments*/
        setImageHeaderOnToolbar();

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_home_white));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_my_job_white));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_hometab_hailed_fare_unselect));

        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (dispatchJob_fragment.isAdded())
        {
            ft.hide(dispatchJob_fragment);
        }
        if (myJob_fragment.isAdded())
        {
            ft.hide(myJob_fragment);
        }
        if (hailed_fail_fragment.isAdded())
        {
            ft.hide(hailed_fail_fragment);
        }

        if (_fragmentMap.isAdded())
        {
            ft.show(_fragmentMap);
        }
        else
        {
            ft.add(R.id.frame_main, _fragmentMap, "mapFragment");
        }
        if (activity != null)
        {
            ft.commitAllowingStateLoss();
        }
    }

    public void setTab_Dispatch()
    {
        tabItemIndex=1;
        navItemIndex=0;
        flagResumePass=0;
        shouldLoadHomeFragOnBackPress = true;
        setTitleOfScreenToolbar("Dispatch Jobs");

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_select));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_my_job_white));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_hometab_hailed_fare_unselect));

        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (_fragmentMap.isAdded()) {
            ft.hide(_fragmentMap);
        }
        if (myJob_fragment.isAdded()) {
            ft.hide(myJob_fragment);
        }
        if (hailed_fail_fragment.isAdded()) {
            ft.hide(hailed_fail_fragment);
        }
        if (dispatchJob_fragment.isAdded()) {
            ft.show(dispatchJob_fragment);
            dispatchJob_fragment.goBackToFirst();
        } else {
            ft.add(R.id.frame_main, dispatchJob_fragment);
        }
        if (activity != null) {
            ft.commitAllowingStateLoss();
        }
    }

    public void setTab_MyJob()
    {
        tabItemIndex=2;
        navItemIndex=0;
        flagResumePass=0;
        shouldLoadHomeFragOnBackPress = true;
        setTitleOfScreenToolbar(activity.getResources().getString(R.string.my_job));

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_my_job_white));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_hometab_hailed_fare_unselect));


        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();


        if (_fragmentMap.isAdded()) {
            ft.hide(_fragmentMap);
        }
        if (dispatchJob_fragment.isAdded()) {
            ft.hide(dispatchJob_fragment);
        }
        if (hailed_fail_fragment.isAdded()) {
            ft.hide(hailed_fail_fragment);
        }
        if (myJob_fragment.isAdded()) {
            ft.show(myJob_fragment);
        } else {
            ft.add(R.id.frame_main, myJob_fragment);
        }
        if (activity != null) {
            ft.commitAllowingStateLoss();
        }
        ll_mayJob.setEnabled(true);
    }

    private void setTab_HailedFare() {
        tabItemIndex=2;
        navItemIndex=0;
        flagResumePass=0;
        shouldLoadHomeFragOnBackPress = true;
        setTitleOfScreenToolbar("Hailed Fare");

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_my_job_white));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_hometab_hailed_fare_select));


        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorTextWhite));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (dispatchJob_fragment.isAdded()) {
            ft.hide(dispatchJob_fragment);
        }
        if (myJob_fragment.isAdded()) {
            ft.hide(myJob_fragment);
        }
        if (_fragmentMap.isAdded()) {
            ft.hide(_fragmentMap);
        }
        if (hailed_fail_fragment.isAdded()) { // if the fragment is already in container
            ft.show(hailed_fail_fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.frame_main, hailed_fail_fragment);
        }

        if (activity != null) {
            ft.commitAllowingStateLoss();
        }
    }

    private void loadNavHeader()
    {
        String UserName = SessionSave.getUserSession(Comman.USER_FULL_NAME, activity);
        Log.e("UserName", "UserName : " + UserName);
        String UserEmail = SessionSave.getUserSession(Comman.USER_EMAIL, activity);
        String UserImage = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);
        String Rating = SessionSave.getUserSession(Comman.USER_DRIVER_RATE,activity);

        if (UserName != null && !UserName.equalsIgnoreCase(""))
        {
            header_name.setText(UserName);
        }
        else
        {
            header_name.setVisibility(View.INVISIBLE);
        }
        if (UserEmail != null && !UserEmail.equalsIgnoreCase(""))
        {
            header_email.setText(UserEmail);
        }
        else
        {
            header_email.setVisibility(View.GONE);
        }

        String UserProfile = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);
        Log.e("UserProfile", "UserProfile : " + UserProfile);

        if (UserImage != null && !UserImage.equalsIgnoreCase(""))
        {
            progBar_HeaderImage.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(UserImage)
//                    .transform(mTransformation)
//                    .fit()
                    .into(header_Prof_image, new Callback()
                    {

                        @Override
                        public void onSuccess()
                        {
                            progBar_HeaderImage.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e)
                        {
                            header_Prof_image.setImageResource(R.drawable.cab_dummy_profile);
                            progBar_HeaderImage.setVisibility(View.GONE);
                        }
                    });
        }
        else
        {
            header_Prof_image.setImageResource(R.drawable.cab_dummy_profile);
        }

        if (Rating != null && !Rating.equalsIgnoreCase("") &&
                !Rating.equalsIgnoreCase("null") &&
                !Rating.equalsIgnoreCase("0.0"))
        {
            header_ratePoint.setText(String.format( " %.1f",Double.parseDouble(Rating)));
        }
        else
        {
            header_ratePoint.setText("");
            ll_rating.setVisibility(View.GONE);
        }
    }

    public void checkGPS() {
        if (checkPermission()) {
            if (gpsTracker.canGetLocation()) {
                gpsTracker.getLocation();

                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                Log.e("latitude", "" + Constants.newgpsLatitude);
                Log.e("longitude", "" + Constants.newgpsLongitude);
            }
        } else {
            requestPermission();
        }
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(Drawer_Activity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("call", "onRequestPermissionsResult() 11 = " + requestCode);
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE :
                switch_online.setEnabled(true);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("call", "onRequestPermissionsResult() 22 = ");
                    if (gpsTracker.canGetLocation())
                    {
                        Log.e("call", "onRequestPermissionsResult() 33 = ");
                        gpsTracker = new GPSTracker(Drawer_Activity.this);
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    }
                    else
                    {
                        Log.e("call", "onRequestPermissionsResult() 44 = ");
                    }
                    switch_online.setChecked(false);
                    StartDisconnectSocket();
                }
                else
                {
                    switch_online.setChecked(false);
                    StartDisconnectSocket();
                    errorDialogClass.showDialog(getResources().getString(R.string.permission_denied_for_location),"Location!!");
                }
                break;

            case REQUEST_CALL_PHONE:
                callToDriver();
                break;
        }
    }

    public void setTitleOfScreenToolbar(String titleName) {
        if (titleName != null && !titleName.equalsIgnoreCase("")) {
            iv_title.setVisibility(View.GONE);
            tv_title_screen.setVisibility(View.VISIBLE);
            tv_title_screen.setText(titleName);
        } else {
            tv_title_screen.setText(getString(R.string.app_name));
            iv_title.setVisibility(View.GONE);
            tv_title_screen.setVisibility(View.VISIBLE);
        }
    }

    public void setImageHeaderOnToolbar() {
        tv_title_screen.setText(getString(R.string.app_name));
        iv_title.setVisibility(View.GONE);
        tv_title_screen.setVisibility(View.VISIBLE);
    }

    private void ChangeDriverOnlineStatus(String userId)
    {
        if (Global.isNetworkconn(activity))
        {
            dialogClass.showDialog();

            String url = WebServiceAPI.WEB_SERVICE_CHANGE_DRIVER_SHIFT_STATUS;

            Map<String, Object> params = new HashMap<String, Object>();
            params.put(WebServiceAPI.CHANGE_DRIVER_SHIFT_STATUS_PARAM_USER_ID, userId);
            params.put(WebServiceAPI.CHANGE_DRIVER_SHIFT_STATUS_PARAM_LAT, Constants.newgpsLatitude);
            params.put(WebServiceAPI.CHANGE_DRIVER_SHIFT_STATUS_PARAM_LNG, Constants.newgpsLongitude);


            Log.e("url", "ChangeDriverOnlineStatus = " + url);
            Log.e("param", "ChangeDriverOnlineStatus = " + params);

            DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);

            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("responseCode", "ChangeDriverOnlineStatus = " + responseCode);
                        Log.e("Response", "ChangeDriverOnlineStatus = " + json);

                        if (json != null) {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");
                                    dialogClass.hideDialog();

                                    if (json.has("duty"))
                                    {
                                        String duty = json.getString("duty");
                                        if (duty!=null && !duty.equalsIgnoreCase(""))
                                        {
                                            if (duty.equalsIgnoreCase("on"))
                                            {
                                                SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "1", activity);
                                                switch_online.setChecked(true);
                                                StartConnectSocket();
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                                                switch_online.setChecked(false);
                                                StartDisconnectSocket();
                                            }
                                            switch_online.setEnabled(true);

                                            if (json.has("message"))
                                            {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                                            }
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            switch_online.setEnabled(true);
                                            if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") && DriverDuty.equalsIgnoreCase("1"))
                                            {
                                                switch_online.setChecked(true);
                                                StartConnectSocket();
                                            }
                                            else
                                            {
                                                switch_online.setChecked(false);
                                                StartDisconnectSocket();
                                            }
                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        switch_online.setEnabled(true);
                                        if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") &&
                                                DriverDuty.equalsIgnoreCase("1"))
                                        {
                                            switch_online.setChecked(true);
                                            StartConnectSocket();
                                        }
                                        else
                                        {
                                            switch_online.setChecked(false);
                                            StartDisconnectSocket();
                                        }
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    switch_online.setEnabled(true);
                                    if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") &&
                                            DriverDuty.equalsIgnoreCase("1"))
                                    {
                                        switch_online.setChecked(true);
                                        StartConnectSocket();
                                    }
                                    else
                                    {
                                        switch_online.setChecked(false);
                                        StartDisconnectSocket();
                                    }
                                    if (json.has("static-alert"))
                                    {
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"),
                                                    activity.getResources().getString(R.string.system_alert));
                                        }
                                    }
                                    else
                                    {
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"),
                                                    getString(R.string.error_message));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                switch_online.setEnabled(true);
                                if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") && DriverDuty.equalsIgnoreCase("1"))
                                {
                                    switch_online.setChecked(true);
                                    StartConnectSocket();
                                }
                                else
                                {
                                    switch_online.setChecked(false);
                                    StartDisconnectSocket();
                                }
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),
                                            getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "null");
                            dialogClass.hideDialog();
                            switch_online.setEnabled(true);
                            if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") && DriverDuty.equalsIgnoreCase("1"))
                            {
                                switch_online.setChecked(true);
                                StartConnectSocket();
                            }
                            else
                            {
                                switch_online.setChecked(false);
                                StartDisconnectSocket();
                            }
                            errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("ChangeDriverStatus", "Exception : " + e.toString());
                        dialogClass.hideDialog();
                        switch_online.setEnabled(true);
                        if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") && DriverDuty.equalsIgnoreCase("1"))
                        {
                            switch_online.setChecked(true);
                            StartConnectSocket();
                        }
                        else
                        {
                            switch_online.setChecked(false);
                            StartDisconnectSocket();
                        }
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
            }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
        }
        else
        {
            dialogClass.hideDialog();
            switch_online.setEnabled(true);
            if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") && DriverDuty.equalsIgnoreCase("1"))
            {
                switch_online.setChecked(true);
                StartConnectSocket();
            }
            else
            {
                switch_online.setChecked(false);
                StartDisconnectSocket();
            }
            errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
        }
    }

    public void StartConnectSocket()
    {
        Log.e("call", "StartConnectSocket()");
        flagCallDriverOnline=0;
        StartDisconnectSocket();
        try
        {
            if (Comman.socket != null && Comman.socket.connected())
            {
                Comman.socket.disconnect();
                Comman.socket = null;
                SocketSingleObject.instance = null;
            }
            Log.e("call", "Ping socket try to connecting...");
            Comman.socket = null;
            SocketSingleObject.instance = null;
            Log.e("onCheckedChanged","To create Object : ");
            Comman.socket = SocketSingleObject.get(activity).getSocket();
            Comman.socket.on(WebServiceAPI.SERVER_SOCKET_DRIVER_ARRIVE_BOOKING_REQUEST, onReceiveBooking_Request);
            Comman.socket.on(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST, onReceiveBookingInfo);
            Comman.socket.on(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION, onReceive_CancelTrip_Notification);
            Comman.socket.on(Socket.EVENT_CONNECT, onConnect);
            Comman.socket.on(Socket.EVENT_DISCONNECT, onDisconnect);

            Comman.socket.on(WebServiceAPI.SERVER_SOCKET_ARIVE_ADVANCE_BOOKING_REQUEST, onReceiveBookLater_Request);
            Comman.socket.on(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION_LATER, onReceive_CancelTrip_Notification_Later);
            Comman.socket.on(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST_LATER, onReceiveBookingInfoLater);

            Comman.socket.on(WebServiceAPI.SERVER_SOCKET_BOOK_LATER_DRIVER_NOTIFY, onBookLaterDriverNotify);
            Comman.socket.on(WebServiceAPI.SOCKET_SESSION_ERROR, onSessionError);
            Comman.socket.on(WebServiceAPI.SOCKET_START_TRIP_ERROR, StartTripTimeError);

            Comman.socket.connect();
        }
        catch (Exception e)
        {
            Log.e("call","Socket connect exception = "+e.getMessage());
        }

       /* try {
            if (Comman.socket != null && Comman.socket.connected()) {
                Log.e("call", "Ping socket already connected");
                flagCallDriverOnline=0;
                startTimer();
            } else {

            }
        } catch (Exception e) {
            Log.e("call", "Socket connect exception = " + e.getMessage());
        }*/
    }

    public void StartDisconnectSocket()
    {
        if (Comman.socket != null && Comman.socket.connected()) {
            Comman.socket.disconnect();
            Comman.socket.off(Socket.EVENT_CONNECT, onConnect);
            Comman.socket.off(Socket.EVENT_DISCONNECT, onDisconnect);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_DRIVER_ARRIVE_BOOKING_REQUEST, onReceiveBooking_Request);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST, onReceiveBookingInfo);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION, onReceive_CancelTrip_Notification);

            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_ARIVE_ADVANCE_BOOKING_REQUEST, onReceiveBookLater_Request);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION_LATER, onReceive_CancelTrip_Notification_Later);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST_LATER, onReceiveBookingInfoLater);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_BOOK_LATER_DRIVER_NOTIFY, onBookLaterDriverNotify);
            Comman.socket.off(WebServiceAPI.SOCKET_SESSION_ERROR, onSessionError);
            Comman.socket.off(WebServiceAPI.SOCKET_START_TRIP_ERROR, StartTripTimeError);
            Comman.socket = null;
            SocketSingleObject.instance = null;
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("socket", "connected");
                    flagCallDriverOnline=0;
//                    startTimer();
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Log.e(TAG,"onDisconnect() socket diconnected");
                }
            });
        }
    };


    private Emitter.Listener onReceiveBooking_Request = new Emitter.Listener()
    {
        @Override
        public void call(final Object... args)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Log.e(TAG, "onReceiveBooking_Request args.length :" + args.length);
                    if (args.length > 0 && args[0] != null)
                    {
                        Log.e(TAG, "onReceiveBooking_Request args[0] :" + args[0]);

                        BookingId="";
                        Booking_message="";
                        Booking_PickupLocation="";
                        Booking_DropoffLocation="";
                        Booking_GrandTotal="";
                        Booking_Type="";
                        String Height ="", Length="", Breadth="", Weigth="", Quantity="",ParcelImage="";

                        try
                        {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            if (jsonObject != null)
                            {
                                if (jsonObject.has("BookingId"))
                                {
                                    BookingId = jsonObject.getString("BookingId");
                                }
                                if (jsonObject.has("message"))
                                {
                                    Booking_message = jsonObject.getString("message");
                                }
                                if (jsonObject.has("PickupLocation"))
                                {
                                    Booking_PickupLocation = jsonObject.getString("PickupLocation");
                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, Booking_PickupLocation, activity);
                                }
                                if (jsonObject.has("DropoffLocation"))
                                {
                                    Booking_DropoffLocation = jsonObject.getString("DropoffLocation");
                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, Booking_DropoffLocation, activity);
                                }
                                if (jsonObject.has("GrandTotal"))
                                {
                                    Booking_GrandTotal = jsonObject.getString("GrandTotal");
                                }
                                if (jsonObject.has("BookingType"))
                                {
                                    Booking_Type = jsonObject.getString("BookingType");
                                }
                                else
                                {
                                    Booking_Type = "";
                                }
                                if (jsonObject.has("Height"))
                                {
                                    Height = jsonObject.getString("Height");
                                    SessionSave.saveUserSession(Comman.BOOKING_HEIGHT, Height, activity);
                                }
                                else
                                {
                                    Height = "";
                                }
                                if (jsonObject.has("Length"))
                                {
                                    Length = jsonObject.getString("Length");
                                    SessionSave.saveUserSession(Comman.BOOKING_LENGTH, Length, activity);
                                }
                                else
                                {
                                    Length = "";
                                }
                                if (jsonObject.has("Breadth"))
                                {
                                    Breadth = jsonObject.getString("Breadth");
                                    SessionSave.saveUserSession(Comman.BOOKING_BREADTH, Breadth, activity);
                                }
                                else
                                {
                                    Breadth = "";
                                }
                                if (jsonObject.has("Weight"))
                                {
                                    Weigth = jsonObject.getString("Weight");
                                    SessionSave.saveUserSession(Comman.BOOKING_WEIGHT, Weigth, activity);
                                }
                                else
                                {
                                    Weigth = "";
                                }
                                if (jsonObject.has("Quantity"))
                                {
                                    Quantity = jsonObject.getString("Quantity");
                                    SessionSave.saveUserSession(Comman.BOOKING_QUANTITY, Quantity, activity);
                                }
                                else
                                {
                                    Quantity = "";
                                }

                                if(jsonObject.has("EstimateFare")) {
                                    estFare = jsonObject.getString("EstimateFare");
                                }

                                if (jsonObject.has("ParcelImage"))
                                {
                                    ParcelImage = jsonObject.getString("ParcelImage");
                                    SessionSave.saveUserSession(Comman.PARCEL_IMG, ParcelImage, activity);
                                }
                                else
                                {
                                    ParcelImage = "";
                                    SessionSave.saveUserSession(Comman.PARCEL_IMG, "", Drawer_Activity.activity);
                                }
                                Log.e(TAG, "onReceiveBooking_Request" + "\n BookingId : " + BookingId
                                        + "\n Booking_message: " + Booking_message
                                        + "\n Booking_PickupLocation: " + Booking_PickupLocation
                                        + "\n Booking_DropoffLocation: " + Booking_DropoffLocation
                                        + "\n Booking_GrandTotal: " + Booking_GrandTotal
                                        + "\n Booking_BookingType: " + Booking_Type
                                        + "\n Height: " + Height
                                        + "\n Length: " + Length
                                        + "\n Breadth: " + Breadth
                                        + "\n Weigth: " + Weigth
                                        + "\n Quantity: " + Quantity);
                            }
                            Log.e("PickNGo_Driver_App","setCurrentActivity : "+ CabRideDriverApplication.currentActivity().getLocalClassName());

                            Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Drawer_Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (CabRideDriverApplication.currentActivity()!=null)
                                    {
                                        OpenDialog_ReceiveBookingRequest(CabRideDriverApplication.currentActivity(), BookingId,
                                                Booking_message, "onReceiveBooking_Request", Booking_Type, Booking_GrandTotal, estFare);
                                    }
                                    else
                                    {
                                        OpenDialog_ReceiveBookingRequest(activity, BookingId, Booking_message,
                                                "onReceiveBooking_Request", Booking_Type, Booking_GrandTotal, estFare);
                                    }
                                }
                            },1000);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onBookLaterDriverNotify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onBookLaterDriverNotify args.length :" + args.length);
                    if (args.length > 0 && args[0] != null) {
//                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", activity);
                        Log.e("kkkkkkkkk", "kkkkkkkkkkk onBookLaterDriverNotify args[0] :" + args[0]);
                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String Booking_message = "New Book Later request arrived from Cab Ride";
                            if (jsonObject != null)
                            {
                                if (jsonObject.has("message"))
                                {
                                    Booking_message = jsonObject.getString("message");
                                }
                                Log.e("kkkkkkkkk", "message message: " + Booking_message);
                            }
                            Log.e("PickNGo_Driver_App","setCurrentActivity : "+ CabRideDriverApplication.currentActivity().getLocalClassName());

                            final Dialog dialog;
                            if(ringTone.isPlaying())
                            {
                                ringTone.stop();
                            }
                            else
                            {
                                ringTone.play();
                            }

                            if (CabRideDriverApplication.currentActivity()!=null)
                            {
                                dialog = new Dialog(CabRideDriverApplication.currentActivity(), R.style.DialogTheme);
                            }
                            else
                            {
                                dialog = new Dialog(activity, R.style.DialogTheme);
                            }
                            AssignAllDialog = dialog;

                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(dialog.getWindow().getAttributes());
                            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                            dialog.getWindow().setAttributes(lp);
                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                            TextView tv_title, tv_message;
                            ImageView dialog_close;
                            LinearLayout dialog_ok_layout;

                            tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                            tv_title.setVisibility(View.VISIBLE);
                            dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
                            tv_message = (TextView) dialog.findViewById(R.id.tv_message);
                            dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

                            tv_title.setText(getString(R.string.app_name));
                            tv_message.setText(Booking_message);

                            dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ringTone.stop();
                                    dialog.dismiss();
                                }
                            });

                            dialog_close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ringTone.stop();
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();

                        } catch (JSONException e) {
                            if (ringTone!=null)
                            {
                                ringTone.stop();
                            }
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onSessionError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onSessionError args.length :" + args.length);
                    SessionExpiredDialog(activity.getResources().getString(R.string.session_expire),
                            getResources().getString(R.string.your_session_is_expire));
                }
            });
        }
    };

    public void SessionExpiredDialog(final String title, final String Message)
    {
        SessionSave.clearUserSession(activity);
        if (CabRideDriverApplication.currentActivity()!=null)
        {
            CabRideDriverApplication.currentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final Dialog dialog = new Dialog(CabRideDriverApplication.currentActivity(), R.style.DialogTheme);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    if (dialog.getWindow().getAttributes()!=null)
                    {
                        lp.copyFrom(dialog.getWindow().getAttributes());
                    }

                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                    dialog.getWindow().setAttributes(lp);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                    TextView tv_title, tv_message;
                    ImageView dialog_close;
                    LinearLayout dialog_ok_layout;

                    tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    tv_title.setVisibility(View.VISIBLE);
                    dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
                    dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);
                    tv_message = (TextView) dialog.findViewById(R.id.tv_message);

                    tv_title.setText(title);
                    tv_message.setText(Message);

                    dialog_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Login_Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            CabRideDriverApplication.currentActivity().startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Login_Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            CabRideDriverApplication.currentActivity().startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
        }
    }

    private Emitter.Listener onReceiveBookLater_Request = new Emitter.Listener()
    {
        @Override
        public void call(final Object... args)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBookLater_Request args.length :" + args.length);
                    if (args.length > 0 && args[0] != null)
                    {
                        Log.e(TAG, "onReceiveBooking_Request args[0] :" + args[0]);

                        BookingId="";
                        Booking_message="";
                        Booking_PickupLocation="";
                        Booking_DropoffLocation="";
                        Booking_GrandTotal="";
                        Booking_Type="";
                        String Height ="", Length="", Breadth="", Weigth="", Quantity="",ParcelImage="";

                        try
                        {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            if (jsonObject != null)
                            {
                                if (jsonObject.has("BookingId"))
                                {
                                    BookingId = jsonObject.getString("BookingId");
                                }
                                if (jsonObject.has("message"))
                                {
                                    Booking_message = jsonObject.getString("message");
                                }
                                if (jsonObject.has("PickupLocation"))
                                {
                                    Booking_PickupLocation = jsonObject.getString("PickupLocation");
                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, Booking_PickupLocation, activity);
                                }
                                if (jsonObject.has("DropoffLocation"))
                                {
                                    Booking_DropoffLocation = jsonObject.getString("DropoffLocation");
                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, Booking_DropoffLocation, activity);
                                }
                                if (jsonObject.has("GrandTotal"))
                                {
                                    Booking_GrandTotal = jsonObject.getString("GrandTotal");
                                }
                                if (jsonObject.has("BookingType"))
                                {
                                    Booking_Type = jsonObject.getString("BookingType");
                                }
                                else
                                {
                                    Booking_Type = "";
                                }
                                if (jsonObject.has("Height"))
                                {
                                    Height = jsonObject.getString("Height");
                                    SessionSave.saveUserSession(Comman.BOOKING_HEIGHT, Height, activity);
                                }
                                else
                                {
                                    Height = "";
                                }
                                if (jsonObject.has("Length"))
                                {
                                    Length = jsonObject.getString("Length");
                                    SessionSave.saveUserSession(Comman.BOOKING_LENGTH, Length, activity);
                                }
                                else
                                {
                                    Length = "";
                                }
                                if (jsonObject.has("Breadth"))
                                {
                                    Breadth = jsonObject.getString("Breadth");
                                    SessionSave.saveUserSession(Comman.BOOKING_BREADTH, Breadth, activity);
                                }
                                else
                                {
                                    Breadth = "";
                                }
                                if (jsonObject.has("Weight"))
                                {
                                    Weigth = jsonObject.getString("Weight");
                                    SessionSave.saveUserSession(Comman.BOOKING_WEIGHT, Weigth, activity);
                                }
                                else
                                {
                                    Weigth = "";
                                }
                                if (jsonObject.has("Quantity"))
                                {
                                    Quantity = jsonObject.getString("Quantity");
                                    SessionSave.saveUserSession(Comman.BOOKING_QUANTITY, Quantity, activity);
                                }
                                else
                                {
                                    Quantity = "";
                                }

                                if (jsonObject.has("ParcelImage"))
                                {
                                    ParcelImage = jsonObject.getString("ParcelImage");
                                    SessionSave.saveUserSession(Comman.PARCEL_IMG, ParcelImage, activity);
                                }
                                else
                                {
                                    ParcelImage = "";
                                    SessionSave.saveUserSession(Comman.PARCEL_IMG, "", Drawer_Activity.activity);
                                }
                                Log.e(TAG, "onReceiveBooking_Request" + "\n BookingId : " + BookingId
                                        + "\n Booking_message: " + Booking_message
                                        + "\n Booking_PickupLocation: " + Booking_PickupLocation
                                        + "\n Booking_DropoffLocation: " + Booking_DropoffLocation
                                        + "\n Booking_GrandTotal: " + Booking_GrandTotal
                                        + "\n Booking_BookingType: " + Booking_Type
                                        + "\n Height: " + Height
                                        + "\n Length: " + Length
                                        + "\n Breadth: " + Breadth
                                        + "\n Weigth: " + Weigth
                                        + "\n Quantity: " + Quantity);
                            }
                            Log.e("PickNGo_Driver_App","setCurrentActivity : "+ CabRideDriverApplication.currentActivity().getLocalClassName());

                            Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Drawer_Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (CabRideDriverApplication.currentActivity()!=null)
                                    {
                                        OpenDialog_ReceiveBookingRequest(CabRideDriverApplication.currentActivity(), BookingId,
                                                Booking_message, "onReceiveBookLater_Request", "", "", "");
                                    }
                                    else {
                                        OpenDialog_ReceiveBookingRequest(activity, BookingId, Booking_message,
                                                "onReceiveBookLater_Request", "", "", "");
                                    }
                                }
                            },1000);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };



    private Emitter.Listener onReceiveBookingInfo = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBookingInfo args.length :" + args.length);
                    String acceptedFlag = SessionSave.getUserSession(Comman.START_TRIP_FLAG, activity);
                    if (acceptedFlag!=null && acceptedFlag.trim().equalsIgnoreCase("1")) {

                    }
                    else
                    {
                        if (args.length > 0 && args[0] != null)
                        {
                            Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBookingInfo args[0] :" + args[0]);

                            try
                            {
                                JSONObject jsonObject = new JSONObject(args[0].toString());
                                String BookingId = "", PassengerId = "", DropOffLat = "", DropOffLon = "", DropoffLocation = "", PickupLat = "", PickupLng = "", PickupLocation = ""
                                        , Status = "", Notes="", BookingType="", PaymentType="", CancellationFee="";

                                if (jsonObject != null)
                                {
                                    if (jsonObject.has("BookingInfo"))
                                    {

                                        SessionSave.saveUserSession(Comman.USER_TRIP_FLAG,"1",activity);

                                        flagCallDriverOnline=10;
//                                        startTimer();

                                        if (jsonObject.has("Model"))
                                        {
                                            String ModelInfo = jsonObject.getString("Model");

                                            if (ModelInfo != null && !ModelInfo.equalsIgnoreCase(""))
                                            {
                                                JSONArray modelInfoArray = jsonObject.getJSONArray("Model");

                                                for (int i = 0; i < modelInfoArray.length(); i++)
                                                {
                                                    JSONObject modelInfoObj = modelInfoArray.getJSONObject(i);

                                                    if (modelInfoObj.has("CancellationFee"))
                                                    {
                                                        CancellationFee = modelInfoObj.getString("CancellationFee");
                                                        SessionSave.saveUserSession(Comman.DRIVER_CANCELATION_FEE, CancellationFee, activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.DRIVER_CANCELATION_FEE, "", activity);
                                                    }
                                                }
                                            }
                                        }

                                        JSONArray bookingInfoArray = jsonObject.getJSONArray("BookingInfo");

                                        for (int i = 0; i < bookingInfoArray.length(); i++)
                                        {
                                            JSONObject bookingInfoObject = bookingInfoArray.getJSONObject(i);
                                            if (bookingInfoObject != null)
                                            {
                                                if (bookingInfoObject.has("Id"))
                                                {
                                                    BookingId = bookingInfoObject.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                                }
                                                if (bookingInfoObject.has("PassengerId"))
                                                {
                                                    PassengerId = bookingInfoObject.getString("PassengerId");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_ID, PassengerId, activity);
                                                }
                                                if (bookingInfoObject.has("DropOffLat"))
                                                {
                                                    DropOffLat = bookingInfoObject.getString("DropOffLat");
                                                    if (DropOffLat != null && !DropOffLat.equalsIgnoreCase(""))
                                                    {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, DropOffLat, activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "0", activity);
                                                    }
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "0", activity);
                                                }
                                                if (bookingInfoObject.has("DropOffLon"))
                                                {
                                                    DropOffLon = bookingInfoObject.getString("DropOffLon");
                                                    if (DropOffLon != null && !DropOffLon.equalsIgnoreCase("")) {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, DropOffLon, activity);
                                                    } else {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "0", activity);
                                                    }
                                                } else {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "0", activity);
                                                }
                                                if (bookingInfoObject.has("DropoffLocation"))
                                                {

                                                    DropoffLocation = bookingInfoObject.getString("DropoffLocation");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, DropoffLocation, activity);
                                                }
                                                if (bookingInfoObject.has("PickupLat"))
                                                {
                                                    PickupLat = bookingInfoObject.getString("PickupLat");
                                                    if (PickupLat != null && !PickupLat.equalsIgnoreCase(""))
                                                    {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, PickupLat, activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "0", activity);
                                                    }
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "0", activity);
                                                }
                                                if (bookingInfoObject.has("PickupLng"))
                                                {
                                                    PickupLng = bookingInfoObject.getString("PickupLng");
                                                    if (PickupLng != null && !PickupLng.equalsIgnoreCase(""))
                                                    {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, PickupLng, activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "0", activity);
                                                    }
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "0", activity);
                                                }
                                                if (bookingInfoObject.has("PickupLocation")) {
                                                    PickupLocation = bookingInfoObject.getString("PickupLocation");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, PickupLocation, activity);
                                                }
                                                if (bookingInfoObject.has("Status")) {
                                                    Status = bookingInfoObject.getString("Status");
                                                }
                                                if (bookingInfoObject.has("BookingType"))
                                                {
                                                    BookingType = bookingInfoObject.getString("BookingType");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, BookingType, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, "", activity);
                                                }

                                                if (bookingInfoObject.has("PaymentType"))
                                                {
                                                    PaymentType = bookingInfoObject.getString("PaymentType");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, PaymentType, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, "", activity);
                                                }

                                                if (bookingInfoObject.has("Notes")) {
                                                    Notes = bookingInfoObject.getString("Notes");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_NOTE, Notes, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_NOTE, "", activity);
                                                }

                                                if (bookingInfoObject.has("ModelId")) {
                                                    String ModelId = bookingInfoObject.getString("ModelId");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_MODEL_ID, ModelId, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_MODEL_ID, "", activity);
                                                }
                                                Log.e("kkkkkkkkk", "BookingId : " + BookingId
                                                        + "\n PassengerId: " + PassengerId
                                                        + "\n DropOffLat: " + DropOffLat
                                                        + "\n DropOffLon: " + DropOffLon
                                                        + "\n DropoffLocation: " + DropoffLocation
                                                        + "\n PickupLat: " + PickupLat
                                                        + "\n PickupLng: " + PickupLng
                                                        + "\n PickupLocation: " + PickupLocation
                                                        + "\n Status: " + Status
                                                        + "\n Notes: " + Notes);
                                            }
                                        }

                                        if (jsonObject.has("PassengerInfo"))
                                        {
                                            String PassengerInfo = jsonObject.getString("PassengerInfo");
                                            if (PassengerInfo != null && !PassengerInfo.equalsIgnoreCase(""))
                                            {
                                                JSONArray passInfoArray = jsonObject.getJSONArray("PassengerInfo");

                                                for (int i = 0; i < passInfoArray.length(); i++)
                                                {
                                                    JSONObject passInfoObj = passInfoArray.getJSONObject(i);
                                                    if (passInfoObj.has("Fullname")) {
                                                        String Fullname = passInfoObj.getString("Fullname");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, activity);
                                                    }
                                                    if (passInfoObj.has("MobileNo")) {
                                                        String MobileNo = passInfoObj.getString("MobileNo");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, MobileNo, activity);
                                                    }
                                                    if (passInfoObj.has("Image")) {
                                                        String Image = WebServiceAPI.BASE_URL_IMAGE + passInfoObj.getString("Image");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, Image, activity);
                                                    }
                                                }

                                                Log.e("PickNGo_Driver_App","CabRideDriverApplication.currentActivity() : "+ CabRideDriverApplication.currentActivity());
                                                if (CabRideDriverApplication.currentActivity() instanceof Drawer_Activity)
                                                {
                                                    Log.e("PickNGo_Driver_App","");
                                                }
                                                else
                                                {
                                                    Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Drawer_Activity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                    startActivity(intent);
                                                }

                                                setTab_Home();
                                                Map_Fragment _fragmentMap = new Map_Fragment();
                                                _fragmentMap.SetDriverToPickUpLocation(0);
                                                _fragmentMap.removeHeatMap();
                                            }
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onReceiveBookingInfoLater = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "onReceiveBookingInfoLater args.length :" + args.length);

                    if (args.length > 0 && args[0] != null) {
                        Log.e("kkkkkkkkk", "onReceiveBookingInfoLater args[0] :" + args[0]);

                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String BookingId = "", PassengerId = "", DropOffLat = "", DropOffLon = "", DropoffLocation = "", PickupLat = "", PickupLng = "", PickupLocation = "", Status = ""
                                    , FlightNumber="", Notes="", PassengerType="", PassengerContact="", PaymentType="", BookingType="", CancellationFee="";
                            if (jsonObject != null)
                            {
                                if (jsonObject.has("BookingInfo"))
                                {
                                    flagCallDriverOnline=10;
//                                    startTimer();

                                    SessionSave.saveUserSession(Comman.USER_TRIP_FLAG,"1",activity);

                                    if (jsonObject.has("Model"))
                                    {
                                        String ModelInfo = jsonObject.getString("Model");

                                        if (ModelInfo != null && !ModelInfo.equalsIgnoreCase(""))
                                        {
                                            JSONArray modelInfoArray = jsonObject.getJSONArray("Model");

                                            for (int i = 0; i < modelInfoArray.length(); i++)
                                            {
                                                JSONObject modelInfoObj = modelInfoArray.getJSONObject(i);

                                                if (modelInfoObj.has("CancellationFee"))
                                                {
                                                    CancellationFee = modelInfoObj.getString("CancellationFee");
                                                    SessionSave.saveUserSession(Comman.DRIVER_CANCELATION_FEE, CancellationFee, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.DRIVER_CANCELATION_FEE, "", activity);
                                                }
                                            }
                                        }
                                    }

                                    JSONArray bookingInfoArray = jsonObject.getJSONArray("BookingInfo");
                                    for (int i = 0; i < bookingInfoArray.length(); i++)
                                    {
                                        JSONObject bookingInfoObject = bookingInfoArray.getJSONObject(i);

                                        if (bookingInfoObject != null)
                                        {
                                            if (bookingInfoObject.has("Id")) {
                                                BookingId = bookingInfoObject.getString("Id");
                                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                            }
                                            if (bookingInfoObject.has("PassengerId")) {
                                                PassengerId = bookingInfoObject.getString("PassengerId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_ID, PassengerId, activity);
                                            }
                                            if (bookingInfoObject.has("DropOffLat")) {
                                                DropOffLat = bookingInfoObject.getString("DropOffLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, DropOffLat, activity);
                                            }
                                            if (bookingInfoObject.has("DropOffLon")) {
                                                DropOffLon = bookingInfoObject.getString("DropOffLon");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, DropOffLon, activity);
                                            }
                                            if (bookingInfoObject.has("DropoffLocation")) {
                                                DropoffLocation = bookingInfoObject.getString("DropoffLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, DropoffLocation, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLat")) {
                                                PickupLat = bookingInfoObject.getString("PickupLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, PickupLat, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLng")) {
                                                PickupLng = bookingInfoObject.getString("PickupLng");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, PickupLng, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLocation")) {
                                                PickupLocation = bookingInfoObject.getString("PickupLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, PickupLocation, activity);
                                            }
                                            if (bookingInfoObject.has("Status")) {
                                                Status = bookingInfoObject.getString("Status");
                                            }

                                            if (bookingInfoObject.has("PaymentType"))
                                            {
                                                PaymentType = bookingInfoObject.getString("PaymentType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, PaymentType, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("PassengerType"))
                                            {
                                                PassengerType = bookingInfoObject.getString("PassengerType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_TYPE, PassengerType, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("PassengerName"))
                                            {
                                                String Fullname = bookingInfoObject.getString("PassengerName");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, Drawer_Activity.activity);
                                            }

                                            if (bookingInfoObject.has("PassengerContact"))
                                            {
                                                PassengerContact = bookingInfoObject.getString("PassengerContact");
                                                SessionSave.saveUserSession(Comman.PASSENGER_MOB_NO_OTHER, PassengerContact, activity);
                                            }

                                            if (bookingInfoObject.has("BookingType")) {
                                                BookingType = bookingInfoObject.getString("BookingType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, BookingType, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("FlightNumber")) {
                                                FlightNumber = bookingInfoObject.getString("FlightNumber");
                                                SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, FlightNumber, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, "", activity);
                                            }

                                            if (bookingInfoObject.has("Notes")) {
                                                Notes = bookingInfoObject.getString("Notes");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, Notes, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, "", activity);
                                            }

                                            if (bookingInfoObject.has("ModelId")) {
                                                String ModelId = bookingInfoObject.getString("ModelId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_MODEL_ID, ModelId, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_MODEL_ID, "", activity);
                                            }

                                            if (bookingInfoObject.has("BidId")) {
                                                String BidId = bookingInfoObject.getString("BidId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_BIDID, BidId, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_BIDID, "0", activity);
                                            }

                                            Log.e("kkkkkkkkk", "BookingId : " + BookingId
                                                    + "\n PassengerId: " + PassengerId
                                                    + "\n DropOffLat: " + DropOffLat
                                                    + "\n DropOffLon: " + DropOffLon
                                                    + "\n DropoffLocation: " + DropoffLocation
                                                    + "\n PickupLat: " + PickupLat
                                                    + "\n PickupLng: " + PickupLng
                                                    + "\n PickupLocation: " + PickupLocation
                                                    + "\n Status: " + Status
                                                    + "\n FlightNumber: " + FlightNumber
                                                    + "\n Notes: " + Notes
                                                    + "\n PassengerType: " + PassengerType);
                                        }
                                    }
                                    if (PassengerType!=null && !PassengerType.equalsIgnoreCase("") && (PassengerType.equalsIgnoreCase("others") || PassengerType.equalsIgnoreCase("other")))
                                    {

                                    }
                                    else
                                    {
                                        if (jsonObject.has("PassengerInfo")) {
                                            String PassengerInfo = jsonObject.getString("PassengerInfo");
                                            if (PassengerInfo != null && !PassengerInfo.equalsIgnoreCase("")) {
                                                JSONArray passInfoArray = jsonObject.getJSONArray("PassengerInfo");
                                                for (int i = 0; i < passInfoArray.length(); i++) {
                                                    JSONObject passInfoObj = passInfoArray.getJSONObject(i);
                                                    if (passInfoObj.has("Fullname")) {
                                                        String Fullname = passInfoObj.getString("Fullname");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, activity);
                                                    }
                                                    if (passInfoObj.has("MobileNo")) {
                                                        String MobileNo = passInfoObj.getString("MobileNo");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, MobileNo, activity);
                                                    }
                                                    if (passInfoObj.has("Image")) {
                                                        String image = passInfoObj.getString("Image");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, image, activity);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    Log.e("PickNGo_Driver_App","CabRideDriverApplication.currentActivity() : "+ CabRideDriverApplication.currentActivity());
                                    if (CabRideDriverApplication.currentActivity() instanceof Drawer_Activity){
                                        Log.e("PickNGo_Driver_App","");
                                    }
                                    else
                                    {
                                        Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Drawer_Activity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        startActivity(intent);
                                    }

                                    setTab_Home();
//                                    ExpandBottomShit();
                                    Map_Fragment _fragmentMap = new Map_Fragment();
                                    _fragmentMap.SetDriverToPickUpLocation(0);
                                    _fragmentMap.removeHeatMap();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onReceive_CancelTrip_Notification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification args.length :" + args.length);
                    ////{"message":"Trip has been canceled by passenger"}
                    if (args!=null && args.length>0)
                    {
                        Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification args[0] :" + args[0]);
                        try {
                            JSONObject object = new JSONObject(args[0].toString());
                            if (object!=null)
                            {
                                if (object.has("BookingInfo") && object.getString("BookingInfo")!=null && !object.getString("BookingInfo").equalsIgnoreCase(""))
                                {
                                    JSONArray jsonArray = object.getJSONArray("BookingInfo");
                                    if (jsonArray!=null && jsonArray.length()>0)
                                    {
                                        JSONObject jObj = jsonArray.getJSONObject(0);
                                        if (jObj!=null)
                                        {
                                            String PassengerName = "";
                                            if (jObj.has("PassengerName") && !jObj.getString("PassengerName").equals(""))
                                            {
                                                PassengerName = jObj.getString("PassengerName");
                                            }
                                            if (jObj.has("Id") && !jObj.getString("Id").equals(""))
                                            {
                                                Log.e("mmmmmmmmmmmm","has Id");

                                                String Message = "Trip has been cancelled.";
                                                if (object.has("message") && object.getString("message")!=null && !object.getString("message").equalsIgnoreCase(""))
                                                {
                                                    Message = object.getString("message").trim();
                                                }
                                                if (SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity)!=null
                                                        && !SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity).equalsIgnoreCase(""))
                                                {
                                                    Log.e("mmmmmmmmmmmm","Booking Id Not blank");
                                                    if (SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity).equalsIgnoreCase(jObj.getString("Id")))
                                                    {
                                                        Log.e("mmmmmmmmmmmm","Booking id match");
                                                        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Comman.STOP_WAITING, "0", activity);
                                                        SessionSave.saveUserSession(Comman.START_WAITING, "0", activity);
                                                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "", activity);
                                                        SessionSave.saveUserSession(Comman.BOOKING_PENDING, "0", activity);

                                                        if (CabRideDriverApplication.currentActivity()!=null)
                                                        {
                                                            OpenDialog_CancelTrip_Notification(CabRideDriverApplication.currentActivity(), PassengerName, 1, Message);
                                                        }
                                                        else
                                                        {
                                                            OpenDialog_CancelTrip_Notification(activity, PassengerName, 1, Message);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Log.e("mmmmmmmmmmmm","Booking id nottttttttttt match");
                                                        if (CabRideDriverApplication.currentActivity()!=null)
                                                        {
                                                            OpenDialog_CancelTrip_Notification(CabRideDriverApplication.currentActivity(), PassengerName, 0, Message);
                                                        }
                                                        else
                                                        {
                                                            OpenDialog_CancelTrip_Notification(activity, PassengerName, 0, Message);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
//                    SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity);
                }
            });
        }
    };

    private Emitter.Listener onReceive_CancelTrip_Notification_Later = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification_Later args.length :" + args.length);
                    if (args!=null && args.length>0)
                    {
                        Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification_Later args[0] :" + args[0]);
                        try {
                            JSONObject object = new JSONObject(args[0].toString());
                            if (object!=null)
                            {
                                if (object.has("BookingInfo") && object.getString("BookingInfo")!=null && !object.getString("BookingInfo").equalsIgnoreCase(""))
                                {
                                    JSONArray jsonArray = object.getJSONArray("BookingInfo");
                                    if (jsonArray!=null && jsonArray.length()>0)
                                    {
                                        JSONObject jObj = jsonArray.getJSONObject(0);
                                        if (jObj!=null)
                                        {
                                            String PassengerName = "";
                                            if (jObj.has("PassengerName") && !jObj.getString("PassengerName").equals(""))
                                            {
                                                PassengerName = jObj.getString("PassengerName");
                                            }
                                            if (jObj.has("Id") && !jObj.getString("Id").equals(""))
                                            {
                                                Log.e("mmmmmmmmmmmm","has Id");
                                                String Message = "Trip has been cancelled.";
                                                if (object.has("message") && object.getString("message")!=null && !object.getString("message").equalsIgnoreCase(""))
                                                {
                                                    Message = object.getString("message").trim();
                                                }
                                                if (SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity)!=null
                                                        && !SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity).equalsIgnoreCase(""))
                                                {
                                                    Log.e("mmmmmmmmmmmm","Booking Id Not blank");
                                                    if(SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity).equalsIgnoreCase(jObj.getString("Id")))
                                                    {
                                                        Log.e("mmmmmmmmmmmm","Booking id match");
                                                        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", activity);
                                                        SessionSave.saveUserSession(Comman.STOP_WAITING, "0", activity);
                                                        SessionSave.saveUserSession(Comman.START_WAITING, "0", activity);
                                                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "", activity);
                                                        SessionSave.saveUserSession(Comman.BOOKING_PENDING, "0", activity);

                                                        if (CabRideDriverApplication.currentActivity()!=null)
                                                        {
                                                            OpenDialog_CancelTrip_Notification(CabRideDriverApplication.currentActivity(), PassengerName, 1, Message);
                                                        }
                                                        else
                                                        {
                                                            OpenDialog_CancelTrip_Notification(activity, PassengerName, 1, Message);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Log.e("mmmmmmmmmmmm","Booking id nottttttttttt match");
                                                        if (CabRideDriverApplication.currentActivity()!=null)
                                                        {
                                                            OpenDialog_CancelTrip_Notification(CabRideDriverApplication.currentActivity(), PassengerName, 0, Message);
                                                        }
                                                        else
                                                        {
                                                            OpenDialog_CancelTrip_Notification(activity, PassengerName, 0, Message);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Log.e("mmmmmmmmmmmm","Booking blank");
                                                    SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", activity);
                                                    SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", activity);
                                                    SessionSave.saveUserSession(Comman.STOP_WAITING, "0", activity);
                                                    SessionSave.saveUserSession(Comman.START_WAITING, "0", activity);
                                                    SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "", activity);
                                                    SessionSave.saveUserSession(Comman.BOOKING_PENDING, "0", activity);

                                                    if (CabRideDriverApplication.currentActivity()!=null)
                                                    {
                                                        OpenDialog_CancelTrip_Notification(CabRideDriverApplication.currentActivity(), PassengerName, 1, Message);
                                                    }
                                                    else
                                                    {
                                                        OpenDialog_CancelTrip_Notification(activity, PassengerName, 1, Message);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Log.e("mmmmmmmmmmmm","has Noooo Id");
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };


    public void startTimer()
    {
        String version="";
        try
        {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
        if (flagCallDriverOnline==0)
        {
            flagCallDriverOnline++;
            Log.e(TAG,"startTimer() 111111111111");

            String UserValidStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
            if (UserValidStatus != null && !UserValidStatus.equalsIgnoreCase("") &&
                    UserValidStatus.equalsIgnoreCase("1"))
            {
                try
                {
                    if (Comman.socket != null && Comman.socket.connected())
                    {
                        // Sending an object
                        JSONObject obj = new JSONObject();
                        try
                        {
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LAT, Constants.newgpsLatitude);
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LNG, Constants.newgpsLongitude);
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_APP_VERSION, version);
                            String Token = SessionSave.getToken(Comman.DEVICE_TOKEN, activity);
                            if (Token!=null &&
                                    !Token.equalsIgnoreCase(""))
                            {
                                obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_TOKEN, Token);
                                Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_UPDATE_DRIVER_LOACATION_KEY, obj);
                            }
                            Log.e(TAG,"startTimer() Object : "+obj.toString());
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "startTimer() socket is not connected.......");
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "startTimer() error in sending latlong");
                }
            }
            else
            {
                Log.e(TAG,"startTimer() startTimer 333333333333");
            }
        }
//        else if (flagCallDriverOnline<0 || flagCallDriverOnline>10)
        else if (flagCallDriverOnline<0 || flagCallDriverOnline>2)
        {
            flagCallDriverOnline=0;
            Log.e(TAG,"startTimer() startTimer 000000000000");
        }
        else
        {
            flagCallDriverOnline++;
            Log.e(TAG,"startTimer() startTimer incremental state");
        }
    }

    public void startTimerPickUp()
    {
        if (flagCallDriverOnline==0)
        {
            flagCallDriverOnline++;
            Log.e(TAG,"startTimerPickUp() 111111111111");

            String UserValidStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
            if (UserValidStatus != null && !UserValidStatus.equalsIgnoreCase("") &&
                    UserValidStatus.equalsIgnoreCase("1"))
            {
                try
                {
                    if (Comman.socket != null && Comman.socket.connected())
                    {
                        // Sending an object
                        JSONObject obj = new JSONObject();
                        try
                        {
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LAT, Constants.newgpsLatitude);
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LNG, Constants.newgpsLongitude);

                            if (SessionSave.getUserSession(Comman.PASSENGER_ID, activity) != null &&
                                !SessionSave.getUserSession(Comman.PASSENGER_ID,activity).equalsIgnoreCase("") &&
                                !SessionSave.getUserSession(Comman.PASSENGER_ID,activity).equalsIgnoreCase("null"))
                            {
                                obj.put(WebServiceAPI.SERVER_SOCKET_OBJ_TRACK_TRIP_PICK_UP_PASSENGER_ID,
                                        SessionSave.getUserSession(Comman.PASSENGER_ID, activity));
                            }

                            if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, activity) != null &&
                                !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT,activity).equalsIgnoreCase("") &&
                                !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT,activity).equalsIgnoreCase("null"))
                            {
                                obj.put(WebServiceAPI.SERVER_SOCKET_OBJ_TRACK_TRIP_PICK_UP_LAT,
                                        SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, activity));
                            }

                            if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, activity) != null &&
                                !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG,activity).equalsIgnoreCase("") &&
                                !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG,activity).equalsIgnoreCase("null"))
                            {
                                obj.put(WebServiceAPI.SERVER_SOCKET_OBJ_TRACK_TRIP_PICK_UP_LONG,
                                        SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, activity));
                            }

                            Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_EMIT_TRACK_TRIP_PICK_UP, obj);

                            Log.e(TAG,"startTimerPickUp() Object : "+obj.toString());
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "startTimerPickUp() socket is not connected.......");
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "startTimerPickUp() error in sending latlong");
                }
            }
            else
            {
                Log.e(TAG,"startTimerPickUp() startTimerPickUp 333333333333");
            }
        }
//        else if (flagCallDriverOnline<0 || flagCallDriverOnline>10)
        else if (flagCallDriverOnline<0 || flagCallDriverOnline>2)
        {
            flagCallDriverOnline=0;
            Log.e(TAG,"startTimerPickUp() startTimerPickUp 000000000000");
        }
        else
        {
            flagCallDriverOnline++;
            Log.e(TAG,"startTimerPickUp() startTimerPickUp incremental state");
        }
    }

    public void startTimerDropOff()
    {
        if (flagCallDriverOnline==0)
        {
            flagCallDriverOnline++;
            Log.e(TAG,"startTimerDropOff() 111111111111");

            String UserValidStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
            if (UserValidStatus != null && !UserValidStatus.equalsIgnoreCase("") &&
                    UserValidStatus.equalsIgnoreCase("1"))
            {
                try
                {
                    if (Comman.socket != null && Comman.socket.connected())
                    {
                        // Sending an object
                        JSONObject obj = new JSONObject();
                        try
                        {
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LAT, Constants.newgpsLatitude);
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LNG, Constants.newgpsLongitude);

                            if (SessionSave.getUserSession(Comman.PASSENGER_ID, activity) != null &&
                                    !SessionSave.getUserSession(Comman.PASSENGER_ID,activity).equalsIgnoreCase("") &&
                                    !SessionSave.getUserSession(Comman.PASSENGER_ID,activity).equalsIgnoreCase("null"))
                            {
                                obj.put(WebServiceAPI.SERVER_SOCKET_OBJ_TRACK_TRIP_DROP_OFF_PASSENGER_ID,
                                        SessionSave.getUserSession(Comman.PASSENGER_ID, activity));
                            }

                            if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, activity) != null &&
                                    !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT,activity).equalsIgnoreCase("") &&
                                    !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT,activity).equalsIgnoreCase("null"))
                            {
                                obj.put(WebServiceAPI.SERVER_SOCKET_OBJ_TRACK_TRIP_DROP_OFF_LAT,
                                        SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, activity));
                            }

                            if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, activity) != null &&
                                    !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG,activity).equalsIgnoreCase("") &&
                                    !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG,activity).equalsIgnoreCase("null"))
                            {
                                obj.put(WebServiceAPI.SERVER_SOCKET_OBJ_TRACK_TRIP_DROP_OFF_LONG,
                                        SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, activity));
                            }

                            Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_EMIT_TRACK_TRIP_DROP_OFF, obj);

                            Log.e(TAG,"startTimerDropOff() Object : "+obj.toString());
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "startTimerDropOff() socket is not connected.......");
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "startTimerDropOff() error in sending latlong");
                }
            }
            else
            {
                Log.e(TAG,"startTimerDropOff() startTimerDropOff 333333333333");
            }
        }
//        else if (flagCallDriverOnline<0 || flagCallDriverOnline>10)
        else if (flagCallDriverOnline<0 || flagCallDriverOnline>2)
        {
            flagCallDriverOnline=0;
            Log.e(TAG,"startTimerDropOff() startTimerDropOff 000000000000");
        }
        else
        {
            flagCallDriverOnline++;
            Log.e(TAG,"startTimerDropOff() startTimerDropOff incremental state");
        }
    }

    public void OpenDialog_ReceiveBookingRequest(Activity activity, final String bookingId, String booking_message, final String requestType,
                                                 String Booking_Type, String Booking_GrandTotal, String estFare)
    {
        if (requestDialogOntimeVisible==0)
        {
            Intent intent = new Intent(CabRideDriverApplication.currentActivity(), Drawer_Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);

            if(ringTone.isPlaying())
            {
                ringTone.stop();
            }
            else
            {
                ringTone.play();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrateFlag.vibrate(VibrationEffect.createOneShot(30000,VibrationEffect.DEFAULT_AMPLITUDE));
            }else{
                //deprecated in API 26
                vibrateFlag.vibrate(30000);
            }

            requestDialogOntimeVisible=1;
            if (AssignAllDialog!=null && AssignAllDialog.isShowing())
            {
                AssignAllDialog.dismiss();
            }
            final Dialog dialogBookingRequest = new Dialog(activity, R.style.DialogTheme);
            dialogBookingRequest.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialogBookingRequest.getWindow().getAttributes());
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
            dialogBookingRequest.getWindow().setAttributes(lp);
            dialogBookingRequest.setCancelable(false);
            dialogBookingRequest.setContentView(R.layout.dialog_receive_request);

            final TextView tv_dialog_reject, tv_dialog_accept, tv_message, tv_count, tv_GrandTotal, tv_ReqPickUp,
                    tv_ReqDropOff, tvSize, tvWeight, tvQuantity, tvEstimateFare;
            final RelativeLayout rl_pickUpDropOff;
            final ImageView ivParcel;
            LinearLayout llImg;

            tv_count = dialogBookingRequest.findViewById(R.id.tv_count);
            tv_message = dialogBookingRequest.findViewById(R.id.tv_message);
            tv_message.setText(booking_message);
            tv_dialog_reject = dialogBookingRequest.findViewById(R.id.tv_dialog_reject);
            tv_dialog_accept = dialogBookingRequest.findViewById(R.id.tv_dialog_accept);
            tv_GrandTotal = dialogBookingRequest.findViewById(R.id.tv_GrandTotal);
            tv_ReqPickUp = dialogBookingRequest.findViewById(R.id.tv_ReqPickUp);
            tv_ReqDropOff = dialogBookingRequest.findViewById(R.id.tv_ReqDropOff);
            rl_pickUpDropOff = dialogBookingRequest.findViewById(R.id.rl_pickUpDropOff);
            tvSize = dialogBookingRequest.findViewById(R.id.tvSize);
            tvWeight = dialogBookingRequest.findViewById(R.id.tvWeight);
            tvQuantity = dialogBookingRequest.findViewById(R.id.tvQuantity);
            ivParcel = dialogBookingRequest.findViewById(R.id.ivParcel);
            llImg = dialogBookingRequest.findViewById(R.id.llImg);
            tvEstimateFare = dialogBookingRequest.findViewById(R.id.tvEstimateFare);
            tvEstimateFare.setText("Kzs"+commaFormat(estFare));
            String Height = "", Length = "", Breadth = "", Weight = "", Quantity="";

//            if (Booking_Type!=null &&
//                    !Booking_Type.equalsIgnoreCase("") &&
//                    Booking_Type.equalsIgnoreCase("dispatch"))
//            {
//                rl_pickUpDropOff.setVisibility(View.VISIBLE);
//                tv_GrandTotal.setVisibility(View.VISIBLE);
//                if (Booking_GrandTotal!=null &&
//                        !Booking_GrandTotal.equalsIgnoreCase(""))
//                {
//                    tv_GrandTotal.setText(activity.getResources().getString(R.string.grand_total_is_doller) + Booking_GrandTotal);
//                }
//
//                String pickUp = SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LOCATION, activity);
//                String dropOff = SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, activity);
//
//                if (pickUp!=null && !pickUp.equalsIgnoreCase(""))
//                {
//                    tv_ReqPickUp.setText(pickUp);
//                }
//                if (dropOff!=null && !dropOff.equalsIgnoreCase(""))
//                {
//                    tv_ReqDropOff.setText(dropOff);
//                }
//            }
//            else
//            {
//                rl_pickUpDropOff.setVisibility(View.GONE);
//                tv_GrandTotal.setVisibility(View.GONE);
//            }

            String pickUp = SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LOCATION, activity);
            String dropOff = SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, activity);
            String parcelImg = SessionSave.getUserSession(Comman.PARCEL_IMG, activity);

            if(parcelImg != null && !parcelImg.equalsIgnoreCase("")){
                Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE + parcelImg).into(ivParcel);
            }else {
                llImg.setVisibility(View.GONE);
            }

            if (pickUp!=null && !pickUp.equalsIgnoreCase(""))
            {
                tv_ReqPickUp.setText(pickUp);
            }
            else
            {
                tv_ReqPickUp.setText("-");
            }
            if (dropOff!=null && !dropOff.equalsIgnoreCase(""))
            {
                tv_ReqDropOff.setText(dropOff);
            }
            else
            {
                tv_ReqDropOff.setText("-");
            }

            if (Booking_GrandTotal!=null &&
                        !Booking_GrandTotal.equalsIgnoreCase(""))
            {
                tv_GrandTotal.setText(activity.getResources().getString(R.string.grand_total_is_doller) + Booking_GrandTotal);
            }
            else
            {
                tv_GrandTotal.setVisibility(View.GONE);
            }

            Height = SessionSave.getUserSession(Comman.BOOKING_HEIGHT,activity);
            Length = SessionSave.getUserSession(Comman.BOOKING_LENGTH,activity);
            Breadth = SessionSave.getUserSession(Comman.BOOKING_BREADTH,activity);

            if (Height != null && !Height.equalsIgnoreCase("") && !Height.equalsIgnoreCase("0") &&
                    Length != null && !Length.equalsIgnoreCase("") && !Length.equalsIgnoreCase("0") &&
                    Breadth!= null && !Breadth.equalsIgnoreCase("") && !Breadth.equalsIgnoreCase("0"))
            {
                String size = "(" + Height + " * " + Length + " * " + Breadth + ")";
                tvSize.setText(size);
            }
            else
            {
                tvSize.setText(activity.getResources().getString(R.string.null_text));
            }

            Weight = SessionSave.getUserSession(Comman.BOOKING_WEIGHT,activity);
            if (Weight != null && !Weight.equalsIgnoreCase("") && !Weight.equalsIgnoreCase("0"))
            {
                tvWeight.setText(Weight);
            }
            else
            {
                tvWeight.setText(activity.getResources().getString(R.string.null_text));
            }

            Quantity = SessionSave.getUserSession(Comman.BOOKING_QUANTITY,activity);
            if (Quantity != null && !Quantity.equalsIgnoreCase("") && !Quantity.equalsIgnoreCase("0"))
            {
                tvQuantity.setText(Quantity);
            }
            else
            {
                tvQuantity.setText(activity.getResources().getString(R.string.null_text));
            }

            CircularProgressBar circularProgressBar = (CircularProgressBar) dialogBookingRequest.findViewById(R.id.yourCircularProgressbar);

            circularProgressBar.setColor(ContextCompat.getColor(this, R.color.colorThemeYellow));
            circularProgressBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorThemeYellow));
            circularProgressBar.setProgressBarWidth(17);
            circularProgressBar.setBackgroundProgressBarWidth(10);
            int animationDuration = 31000; // 2500ms = 2,5s
            circularProgressBar.setProgress(0);
            circularProgressBar.setProgressWithAnimation(100, animationDuration);
            count = 30;
            handler.postDelayed(new Runnable()
            {
                public void run()
                {
                    if (count > 0)
                    {
                        tv_count.setText(count + "");
                        count--;
                        handler.postDelayed(this, 1000);
                    }
                    else
                    {
                        tv_count.setText("0");
                        handler.removeCallbacksAndMessages(null);
                        requestDialogOntimeVisible=0;
                        RequestRejected(bookingId, requestType);
                        ringTone.stop();
                        vibrateFlag.cancel();
                        if (dialogBookingRequest!=null && dialogBookingRequest.isShowing())
                        {
                            dialogBookingRequest.dismiss();
                        }
                    }

                }
            }, 1000);

            tv_dialog_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.removeCallbacksAndMessages(null);
                    requestDialogOntimeVisible=0;
                    RequestRejected(bookingId, requestType);
                    ringTone.stop();
                    vibrateFlag.cancel();
                    if (dialogBookingRequest!=null && dialogBookingRequest.isShowing())
                    {
                        dialogBookingRequest.dismiss();
                    }
                }
            });

            tv_dialog_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.removeCallbacksAndMessages(null);
                    RequestAccepted(bookingId, requestType);
                    ringTone.stop();
                    vibrateFlag.cancel();
                    if (dialogBookingRequest!=null && dialogBookingRequest.isShowing())
                    {
                        dialogBookingRequest.dismiss();
                    }
                }
            });

            dialogBookingRequest.show();
        }
        else
        {
            requestDialogOntimeVisible=1;
            RequestRejected(bookingId, requestType);
        }
    }

    private void RequestAccepted(String BookingId, String requestType)
    {
        requestDialogOntimeVisible=0;
        Log.e("rrrrrrrrrrrrrrrrr","RequestAccepted TRIP_ACCEPT: "+SessionSave.getUserSession(Comman.TRIP_ACCEPT,activity));
        if (SessionSave.getUserSession(Comman.TRIP_ACCEPT,activity)!=null &&
                SessionSave.getUserSession(Comman.TRIP_ACCEPT,activity).equalsIgnoreCase("1"))
        {
            if (requestType.equalsIgnoreCase("onReceiveBooking_Request"))
            {
                JSONObject obj = new JSONObject();
                try {
                    gpsTracker = new GPSTracker(activity);
                    gpsTracker.getLocation();

                    if (gpsTracker.getLatitude()!=0 && gpsTracker.getLongitude()!=0)
                    {
                        Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                        Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

                        Log.e("kkkkkkkkk", "RequestAccepted Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_BOOKING_ID, BookingId);
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT, gpsTracker.getLatitude());
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG, gpsTracker.getLongitude());
                        obj.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PENDING, "1");
                        Log.e("rrrrrrrrrrrrrrrrr","RequestAccepted BookNow111: "+obj);
                        SessionSave.saveUserSession(Comman.BOOKING_PENDING, "1", activity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST, obj);
            }
            else
            {
                JSONObject obj = new JSONObject();
                try {
                    gpsTracker = new GPSTracker(activity);
                    gpsTracker.getLocation();

                    if (gpsTracker.getLatitude()!=0 && gpsTracker.getLongitude()!=0)
                    {
                        Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                        Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

                        Log.e("kkkkkkkkk", "RequestAccepted Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_BOOKING_ID, BookingId);
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT, gpsTracker.getLatitude());
                        obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG, gpsTracker.getLongitude());
                        obj.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PENDING, "1");
                        Log.e("rrrrrrrrrrrrrrrrr","RequestAccepted BookLater111: "+obj);
                        SessionSave.saveUserSession(Comman.BOOKING_PENDING, "1", activity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST, obj);
            }
        }
        else
        {
            SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "1",activity);
            Log.e("avooooooooooooo","avoooooooooooooooooooo333333333333333333333333");
            SessionSave.saveUserSession(Comman.BOOKING_PENDING, "0", activity);
            try {
                if (Comman.socket != null && Comman.socket.connected())
                {
                    // Sending an object
                    JSONObject obj = new JSONObject();
                    if (requestType != null && !requestType.equalsIgnoreCase(""))
                    {
                        if (requestType.equalsIgnoreCase("onReceiveBooking_Request"))
                        {
                            SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", activity);
                            try {
                                gpsTracker = new GPSTracker(activity);
                                gpsTracker.getLocation();

                                if (gpsTracker.getLatitude()!=0 && gpsTracker.getLongitude()!=0)
                                {
                                    Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                                    Constants.newgpsLongitude = gpsTracker.getLongitude()+"";
                                    Log.e("kkkkkkkkk", "RequestAccepted Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_BOOKING_ID, BookingId);
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT, gpsTracker.getLatitude());
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG, gpsTracker.getLongitude());
                                    Log.e("rrrrrrrrrrrrrrrrr","RequestAccepted BookNow222: "+obj);
                                    SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                    SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", activity);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("RequestAccepted","RequestAccepted");
                            Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST, obj);
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", activity);
                            try {
                                gpsTracker = new GPSTracker(activity);
                                gpsTracker.getLocation();

                                if (gpsTracker.getLatitude()!=0 && gpsTracker.getLongitude()!=0)
                                {
                                    Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                                    Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

                                    Log.e("kkkkkkkkk", "RequestAccepted Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_BOOKING_ID, BookingId);
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT, gpsTracker.getLatitude());
                                    obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG, gpsTracker.getLongitude());
                                    Log.e("rrrrrrrrrrrrrrrrr","RequestAccepted BookLater222: "+obj);
                                    SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                    SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", activity);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST, obj);
                        }
                    }
                } else {
                    Log.e("call", "socket is not connected.......");
                }
            } catch (Exception e) {
                Log.e("call", "error in sending latlong");
            }
        }
        flagCallDriverOnline=10;
//        startTimer();
    }


    private void RequestRejected(String BookingId, String requestType)
    {
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                // Sending an object
                JSONObject obj = new JSONObject();
                if (requestType != null && !requestType.equalsIgnoreCase("")) {
                    if (requestType.equalsIgnoreCase("onReceiveBooking_Request")) {
                        try {
                            Log.e("kkkkkkkkk", "RequestRejected Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOKING_REQUEST_BOOKING_ID, BookingId);
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_REJECT_BOOKING_REQUEST, obj);
                    } else {
                        try {
                            Log.e("kkkkkkkkk", "RequestRejected Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_BOOKING_ID, BookingId);
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST, obj);
                    }
                }
            } else {
                Log.e("call", "socket is not connected.......");
            }
        } catch (Exception e) {
            Log.e("call", "error in sending latlong");
        }
        flagCallDriverOnline=10;
//        startTimer();
    }


    private void OpenDialog_CancelTrip_Notification(Activity activity, String PassengerName, int flag, String Message)
    {
        /// flag=1 for clear map, 0 for not clear map
        if (flag==1)
        {
            Map_Fragment.clearMap();
        }
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        AssignAllDialog = dialog;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_receive_cancel_trip);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        TextView tv_title, tv_message;
        LinearLayout dialog_ok_layout;
        ImageView dialog_close;

        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        dialog_ok_layout = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

//        tv_title.setText(getResources().getString(R.string.cancel_request));

        /*if (PassengerName!=null && !PassengerName.equalsIgnoreCase(""))
        {
            tv_message.setText("Trip has been canceled by "+PassengerName);
        }
        else
        {
            tv_message.setText(getResources().getString(R.string.trip_canceled_by_passenger));
        }*/
        if (Message!=null && !Message.trim().equalsIgnoreCase(""))
        {
            tv_message.setText(Message);
        }
        else
        {
            tv_message.setText(activity.getResources().getString(R.string.trip_has_been_cancelled));
        }


        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void StartTrip_BookNow(String bookingId, String driverId)
    {
        Log.e(TAG, "StartTrip_BookNow()");
        try
        {
            if (Comman.socket != null && Comman.socket.connected())
            {
                SessionSave.saveUserSession(Comman.USER_TRIP_FLAG, "2",activity);
                SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "1",activity);
                Log.e(TAG, "StartTrip_BookNow() socket connected");
                // Sending an object
                JSONObject obj = new JSONObject();

                Log.e(TAG, "StartTrip_BookNow() Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));

                obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_BOOKING_ID, bookingId);
                obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_DRIVER_ID, driverId);

                Log.e(TAG, "StartTrip_BookNow() Obj : "+obj);

                Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_PICKUP_PASSENGER, obj);
                flagCallDriverOnline=10;
//                startTimer();
                Map_Fragment.SetDriverTo_DropOffLocation(1, 0);

            }
            else
            {
                Log.e(TAG, "StartTrip_BookNow() socket is not connected.......");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, "StartTrip_BookNow() error in sending latlong");
        }
    }

    public void StartTrip_BookLater(String bookingId, String driverId)
    {
        dialogClass.showDialog();
        try {
            if (Comman.socket != null && Comman.socket.connected())
            {
                Log.e(TAG, "StartTrip_BookLater() socket connected");
                // Sending an object
                flagBookError=1;
                JSONObject obj = new JSONObject();
                Log.e(TAG, "StartTrip_BookLater() Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                Log.e(TAG, "StartTrip_BookLater() bookingId : " + bookingId);

                obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_BOOK_LATER_BOOKING_ID, bookingId);
                obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_BOOK_LATER_DRIVER_ID, driverId);

                Log.e(TAG, "StartTrip_BookLater() Obj : "+obj);

                Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_PICKUP_PASSENGER_BOOK_LATER, obj);
                flagCallDriverOnline=10;
//                startTimer();
            }
            else
            {
                dialogClass.hideDialog();
                Log.e(TAG, "StartTrip_BookLater() socket is not connected.......");
            }
        }
        catch (Exception e)
        {
            dialogClass.hideDialog();
            Log.e(TAG, "StartTrip_BookLater() error in sending latlong");
        }
    }

    private Emitter.Listener StartTripTimeError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("StartTripTimeError", "StartTripTimeError");
                    if (args.length > 0 && args[0] != null)
                    {
                        Log.e("kkkkkkkkk", "kkkkkkkkkkk StartTripTimeError args[0] :" + args[0]);
                        Log.e("kkkkkkkkk", "kkkkkkkkkkk StartTripTimeError flagBookError :" + flagBookError);
                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            if (jsonObject!=null)
                            {
                                dialogClass.hideDialog();
                                if(jsonObject.has("status") && jsonObject.getString("status")!=null && jsonObject.getString("status").equalsIgnoreCase("1"))
                                {
                                    if (flagBookError==1)
                                    {
                                        SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "1",activity);
                                        startTripMeter();
                                        Map_Fragment.SetDriverTo_DropOffLocation(0, 0);
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Comman.TRIP_ACCEPT, "1",activity);
                                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", activity);
                                        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", activity);
                                        setTab_Home();
                                    }
                                }
                                else
                                {
                                    if(jsonObject.has("message") &&  jsonObject.getString("message")!=null  && !jsonObject.getString("message").equalsIgnoreCase(""))
                                    {
                                        errorDialogClass.showDialog(jsonObject.getString("message"), getString(R.string.info_message));
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    public void NotifyPassengerForAdvancedTrip(String BookingId)
    {
        dialogClass.showDialog();
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                // Sending an object
                JSONObject obj = new JSONObject();
                try {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    Log.e("kkkkkkkkk", "NotifyPassengerForAdvancedTrip driverId : " + driverId);
                    Log.e("kkkkkkkkk", "NotifyPassengerForAdvancedTrip BookingId : " + BookingId);
                    if(driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        obj.put(WebServiceAPI.SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_DRIVER_ID, driverId);
                        obj.put(WebServiceAPI.SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_BOOKING_ID, BookingId);
                    }
                    else
                    {
                        Log.e("kkkkkkkkk", "NotifyPassengerForAdvancedTrip driverId null");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                flagBookError=2;
                Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP, obj);
            }
            else
            {
                dialogClass.hideDialog();
                Log.e("call NotifyPassTrip", "socket is not connected.......");
            }
        } catch (Exception e) {
            dialogClass.hideDialog();
            Log.e("call NotifyPassTrip", "error in sending latlong");
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (AssignAllDialog!=null && AssignAllDialog.isShowing())
        {
            AssignAllDialog.dismiss();
        }

        Thread.currentThread();
        StartDisconnectSocket();
        handler.removeCallbacks(runnable);
        handler.removeCallbacksAndMessages(null);
        handler1.removeCallbacks(runnable);
        handler1.removeCallbacksAndMessages(null);

        unbindService();
    }

    @Override
    public void onBackPressed()
    {

        if (drawer.isDrawerVisible(GravityCompat.START))
        {
            drawer.closeDrawer(ll_DrawerLayout);
            return;
        }

        if (shouldLoadHomeFragOnBackPress) {
            setTab_Home();
            return;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public static void ExpandBottomShit()
    {
        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            String dropLoc = SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, activity);
            String pickLoc = SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LOCATION, activity);
            String passName = SessionSave.getUserSession(Comman.PASSENGER_NAME, activity);
            passMobNo = SessionSave.getUserSession(Comman.PASSENGER_MOBILE_NO, activity);
            String passImage = SessionSave.getUserSession(Comman.PASSENGER_IMAGE, activity);
            String passFlightNo = SessionSave.getUserSession(Comman.PASSENGER_FLIGHT_NO, activity);
            String passNote = SessionSave.getUserSession(Comman.PASSENGER_NOTE, activity);
            String passType = SessionSave.getUserSession(Comman.PASSENGER_TYPE, activity);

            if (passImage!=null & !passImage.equalsIgnoreCase(""))
            {
                if (passImage.contains("http")) {
                }
                else
                {
                    passImage = WebServiceAPI.BASE_URL_IMAGE + passImage;
                }
            }
            else
            {
                passImage="";
            }

            if (dropLoc != null && !dropLoc.equalsIgnoreCase("")) {
                tv_drop_off_location.setText(dropLoc);
            }

            if (pickLoc != null && !pickLoc.equalsIgnoreCase("")) {
                tv_pick_up_location.setText(pickLoc);
            }

            if (passName != null && !passName.equalsIgnoreCase("")) {
                tv_passName.setText(passName);
            }

            if (passType!=null && !passType.equalsIgnoreCase("") && (passType.equalsIgnoreCase("others") ||
                    passType.equalsIgnoreCase("other")))
            {
                passMobNo = SessionSave.getUserSession(Comman.PASSENGER_MOB_NO_OTHER, activity);
            }
            else
            {
                passMobNo = SessionSave.getUserSession(Comman.PASSENGER_MOBILE_NO, activity);
            }

            Log.e("passMobNo","passMobNo : "+passMobNo);
            tv_passContactNo.setText(passMobNo);

            if (passImage!=null && !passImage.equalsIgnoreCase(""))
            {
//                Picasso.get()
//                        .load(passImage)
//                        .transform(mTransformation)
//                        .fit()
//                        .into(iv_passenger, new Callback() {
//
//                            @Override
//                            public void onSuccess() {
//                            }
//
//                            @Override
//                            public void onError() {
//                                iv_passenger.setImageResource(R.drawable.cab_dummy_profile);
//                            }
//                        });

                Glide.with(activity)
                        .load(passImage)
                        .placeholder(R.drawable.cab_dummy_profile)
                        .listener(new RequestListener<String, GlideDrawable>()
                        {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource)
                            {
                                iv_passenger.setImageResource(R.drawable.cab_dummy_profile);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource)
                            {
                                return false;
                            }
                        })
                        .into(iv_passenger);
            }
            else
            {
                iv_passenger.setImageResource(R.drawable.cab_dummy_profile);
            }

            if (passFlightNo != null && !passFlightNo.equalsIgnoreCase("")) {
                ll_PassengerFlightNo.setVisibility(View.VISIBLE);
                tv_PassengerFlightNo.setText(passFlightNo);
            }
            else
            {
                ll_PassengerFlightNo.setVisibility(View.GONE);
            }

            if (passNote != null && !passNote.equalsIgnoreCase("")) {
                ll_PassengerNote.setVisibility(View.VISIBLE);
                tv_PassengerNote.setText(passNote);
            }
            else
            {
                ll_PassengerNote.setVisibility(View.GONE);
            }

        }
    }

    public void callToDriver()
    {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + passMobNo));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE);
        }
        else
        {
            startActivity(intent);
        }

//        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        int simState = telMgr.getSimState();
//
//        switch (simState)
//        {
//            case TelephonyManager.SIM_STATE_READY:
//
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:" + passMobNo));
//                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
//                {
//                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE);
//                }
//                else
//                {
//                    startActivity(intent);
//                }
//                break;
//            case TelephonyManager.SIM_STATE_ABSENT:
//                errorDialogClass.showDialog("Sim card not available",
//                        getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
//                errorDialogClass.showDialog("Sim state network locked", getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
//                errorDialogClass.showDialog("Sim state pin required", getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
//                errorDialogClass.showDialog("Sim state puk required", getString(R.string.info_message));
//                break;
//            case TelephonyManager.SIM_STATE_UNKNOWN:
//                errorDialogClass.showDialog("Sim state unknown", getString(R.string.info_message));
//                break;
//        }
    }

    private ServiceConnection sc = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationService.LocalBinder binder = (LocationService.LocalBinder) service;
            myService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    void bindService()
    {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
//            return;
        }
        Intent i = new Intent(getApplicationContext(), LocationService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            startForegroundService(i);
        }
        bindService(i, sc, BIND_AUTO_CREATE);
    }

    void unbindService()
    {
        Intent i = new Intent(getApplicationContext(), LocationService.class);
        unbindService(sc);
    }

    public static void startChronometer()
    {
        Log.e("MeterTrip","startChronometer : ");
        if (spinner!=null)
        {
            spinner.setEnabled(false);
        }

        if (chronometerPersist!=null)
        {
            chronometerPersist.startChronometer();
        }
    }

    public static void PauseChronometer()
    {
        Log.e("MeterTrip","PauseChronometer : ");
        if (spinner!=null)
        {
            spinner.setEnabled(false);
        }
        if (chronometerPersist!=null)
        {
            chronometerPersist.pauseChronometer();
        }
    }

    public static void stopChronometer()
    {
        Log.e("MeterTrip","stopChronometer : ");
        if (spinner!=null)
        {
            spinner.setEnabled(true);
        }
        GetAllWaitingMilisec();
        if (chronometerPersist!=null)
        {
            chronometerPersist.stopChronometer();
        }
    }

    public static void GetAllWaitingMilisec()
    {
        long getBaseCH =  chronometerView.getBase();
        Log.e("MeterTrip","mBinding.getBaseCH : "+getBaseCH);
        Log.e("MeterTrip","mBinding.chronometerView.getText() : "+chronometerView.getText());
        long hour=00, minutes=00, seconds=00;

        String meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, Drawer_Activity.activity);
        if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0") && !meterActiveFlag.equalsIgnoreCase("1")
                && !meterActiveFlag.equalsIgnoreCase("3"))
        {
            if (SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, Drawer_Activity.activity)!=null && SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, Drawer_Activity.activity).equals("1"))
            {
                if (chronometerView.getText()!=null && !chronometerView.getText().equals(""))
                {
                    String abc = chronometerView.getText().toString();
                    String[] abc1 = abc.split(":");
                    if (abc1.length>2)
                    {
                        if (abc1[0]!=null && !abc1[0].equalsIgnoreCase("") && Double.parseDouble(abc1[0])>0)
                        {
                            hour = Long.parseLong(abc1[0]);
                        }

                        if (abc1[1]!=null && !abc1[1].equalsIgnoreCase("") && Double.parseDouble(abc1[1])>0)
                        {
                            minutes = Long.parseLong(abc1[1]);
                        }

                        if (abc1[2]!=null && !abc1[2].equalsIgnoreCase("") && Double.parseDouble(abc1[2])>0)
                        {
                            seconds = Long.parseLong(abc1[2]);
                        }
                    }
                    else
                    {
                        if (abc1[0] != null && !abc1[0].equalsIgnoreCase("") && Double.parseDouble(abc1[0]) > 0) {
                            minutes = Long.parseLong(abc1[0]);
                        }

                        if (abc1[1] != null && !abc1[1].equalsIgnoreCase("") && Double.parseDouble(abc1[1]) > 0) {
                            seconds = Long.parseLong(abc1[1]);
                        }
                    }
                }
            }
            else{
                hour = (((SystemClock.elapsedRealtime() - getBaseCH)/1000) / 60)/60;
                minutes = ((SystemClock.elapsedRealtime() - getBaseCH)/1000) / 60;
                seconds = ((SystemClock.elapsedRealtime() - getBaseCH)/1000) % 60;
            }
        }
        else
        {
            if (chronometerView.getText()!=null && !chronometerView.getText().equals(""))
            {
                String abc = chronometerView.getText().toString();
                String[] abc1 = abc.split(":");
                if (abc1.length>2) {
                    if (abc1[0]!=null && !abc1[0].equalsIgnoreCase("") && Double.parseDouble(abc1[0])>0)
                    {
                        hour = Long.parseLong(abc1[0]);
                    }

                    if (abc1[1]!=null && !abc1[1].equalsIgnoreCase("") && Double.parseDouble(abc1[1])>0)
                    {
                        minutes = Long.parseLong(abc1[1]);
                    }

                    if (abc1[2]!=null && !abc1[2].equalsIgnoreCase("") && Double.parseDouble(abc1[2])>0)
                    {
                        seconds = Long.parseLong(abc1[2]);
                    }
                }
                else {
                    if (abc1[0]!=null && !abc1[0].equalsIgnoreCase("") && Double.parseDouble(abc1[0])>0)
                    {
                        minutes = Long.parseLong(abc1[0]);
                    }

                    if (abc1[1]!=null && !abc1[1].equalsIgnoreCase("") && Double.parseDouble(abc1[1])>0)
                    {
                        seconds = Long.parseLong(abc1[1]);
                    }
                }
            }
        }

        double totalMin = minutes + (hour*60)+(seconds/60);
        SessionSave.saveMeterData(Comman.METER_TOTAL_MIN_CALCULATE, ((totalMin*60)+seconds)+"", activity);
        if (tv_waitingTime!=null)
        {
            tv_waitingTime.setText(hour+":"+minutes+":"+seconds);
        }
        String witingTimePerMIn = SessionSave.getMeterData(Comman.METER_WAITING_TIME_PER_MIN, activity);
        if (witingTimePerMIn!=null && !witingTimePerMIn.equalsIgnoreCase(""))
        {
            Log.e("MeterTrip","totalMin : "+totalMin);
            totalMin = minutes + (hour*60)+((double)seconds/60);
            double waitingCost = totalMin * Double.parseDouble(witingTimePerMIn);
            String abc = String.format( "%.2f", waitingCost);
            SessionSave.saveMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, abc+"", activity);
            if (tv_waitinCost!=null)
            {
                tv_waitinCost.setText(abc+"");
            }
        }
        GetBaseFareOffline(SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, Drawer_Activity.activity));
    }

    public void OpenDialogForMeter()
    {
        dialogMeter = new Dialog(Drawer_Activity.activity, R.style.PauseDialog);
        dialogMeter.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMeter.setContentView(R.layout.dialog_meter_screen);
        dialogMeter.setCancelable(true);

        tv_waitingTime = (TextView) dialogMeter.findViewById(R.id.tv_waitingTime);
        tv_waitinCost = (TextView) dialogMeter.findViewById(R.id.tv_waitinCost);
        tv_BaseFare = (TextView) dialogMeter.findViewById(R.id.tv_BaseFare);
        tv_hireFee = (TextView) dialogMeter.findViewById(R.id.tv_hireFee);
        tv_startTripMeter = (TextView) dialogMeter.findViewById(R.id.tv_startTripMeter);
        tv_endTripMeter = (TextView) dialogMeter.findViewById(R.id.tv_endTripMeter);
        tv_start_waitingTime = (TextView) dialogMeter.findViewById(R.id.tv_start_waitingTime);
        tv_end_waitingTime = (TextView) dialogMeter.findViewById(R.id.tv_end_waitingTime);
        tv_speedTrip = (TextView) dialogMeter.findViewById(R.id.tv_speedTrip);
        tv_meter_digit = (TextView) dialogMeter.findViewById(R.id.tv_meter_digit);
        ll_back_Dailog = (LinearLayout) dialogMeter.findViewById(R.id.ll_back);
        ll_Sos_Dailog = (LinearLayout) dialogMeter.findViewById(R.id.ll_Sos);

        spinner = (Spinner) dialogMeter.findViewById(R.id.spinner);
        spinner.setEnabled(false);

        ModelNameList = new ArrayList<String>();
        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity).equalsIgnoreCase(""))
        {
            ModelNameList = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity).split("\\s*,\\s*"));
        }
        Log.e("ModelNameList","ModelNameList : "+ModelNameList.size());
        spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,ModelNameList);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);

        ModelIdList = new ArrayList<String>();
        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).equalsIgnoreCase(""))
        {
            ModelIdList = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).split("\\s*,\\s*"));
        }
        Log.e("ModelIdList","ModelIdList : "+ModelIdList.size());

        String spinnerSelectedItemPosition = SessionSave.getMeterData(Comman.METER_SELECTED_CAR_MODEL_POSITION, activity);
        if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("0"))
        {
            if (SessionSave.getUserSession(Comman.PASSENGER_MODEL_ID, activity)!=null && !SessionSave.getUserSession(Comman.PASSENGER_MODEL_ID, activity).equalsIgnoreCase(""))
            {
                int pos = 0;
                if (ModelIdList.size()>0)
                {
                    for (int i=0; i<ModelIdList.size(); i++)
                    {
                        if (ModelIdList.get(i).equalsIgnoreCase(SessionSave.getUserSession(Comman.PASSENGER_MODEL_ID, activity)))
                        {
                            pos= i;
                        }
                    }
                }
                spinner.setSelection(pos);
            }
            else
            {
                if (spinnerSelectedItemPosition!=null && !spinnerSelectedItemPosition.equalsIgnoreCase(""))
                {
                    spinner.setSelection(Integer.parseInt(SessionSave.getMeterData(Comman.METER_SELECTED_CAR_MODEL_POSITION, activity)));
                }
                else
                {
                    spinner.setSelection(0);
                }
            }
        }
        else
        {
            if (spinnerSelectedItemPosition!=null && !spinnerSelectedItemPosition.equalsIgnoreCase(""))
            {
                spinner.setSelection(Integer.parseInt(SessionSave.getMeterData(Comman.METER_SELECTED_CAR_MODEL_POSITION, activity)));
            }
            else
            {
                spinner.setSelection(0);
            }
        }


        String hireCost = SessionSave.getMeterData(Comman.METER_HIRE_COST, activity);
        if (hireCost!=null && !hireCost.equalsIgnoreCase(""))
        {
            tv_hireFee.setText(hireCost);
        }


        String waitingCost = SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity);
        if (waitingCost!=null && !waitingCost.equalsIgnoreCase(""))
        {
            tv_waitinCost.setText(waitingCost);
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedModelId = ModelIdList.get(position);
                OnItemSelect_GetDetail(selectedModelId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity);
        if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
        {
            if (meterActiveFlag.equalsIgnoreCase("1") || meterActiveFlag.equalsIgnoreCase("3"))
            {
                tv_startTripMeter.setVisibility(View.GONE);
                tv_endTripMeter.setVisibility(View.VISIBLE);
            }
            else if (meterActiveFlag.equalsIgnoreCase("2"))
            {
                tv_startTripMeter.setVisibility(View.GONE);
                tv_endTripMeter.setVisibility(View.VISIBLE);
            }

            float distance;
            if (SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, activity)!=null && !SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, activity).equalsIgnoreCase(""))
            {
                distance = Float.parseFloat(SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, activity));
            }
            else
            {
                distance = 0;
                SessionSave.saveUserSession(Comman.SUM_FOR_LAT_LONG_METER, "", activity);
            }
            String abc = String.format( "%.2f", distance);
            tv_meter_digit.setText(abc);

            if (SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, activity)!=null && SessionSave.getMeterData(Comman.METER_TIMER_BOTH_PAUSE, activity).equals("1"))
            {
                tv_start_waitingTime.setVisibility(View.GONE);
                tv_end_waitingTime.setVisibility(View.VISIBLE);
            }
            else
            {
                tv_start_waitingTime.setVisibility(View.VISIBLE);
                tv_end_waitingTime.setVisibility(View.GONE);
            }
        }
        else
        {
            stopChronometer();
            tv_meter_digit.setText("0.0");
            tv_startTripMeter.setVisibility(View.VISIBLE);
            tv_endTripMeter.setVisibility(View.GONE);
            tv_start_waitingTime.setVisibility(View.GONE);
            tv_end_waitingTime.setVisibility(View.GONE);
        }

        if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("0"))
        {
            tv_startTripMeter.setVisibility(View.GONE);
        }


        ll_back_Dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMeter.dismiss();
            }
        });

        ll_Sos_Dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMeter.dismiss();
                new CallSos(activity);
            }
        });

        tv_start_waitingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_start_waitingTime.setVisibility(View.GONE);
                tv_end_waitingTime.setVisibility(View.VISIBLE);
                SessionSave.saveMeterData(Comman.METER_TIMER_BOTH_PAUSE, "1", activity);
                SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "3", Drawer_Activity.activity);
                PauseChronometer();
            }
        });

        tv_end_waitingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_start_waitingTime.setVisibility(View.VISIBLE);
                tv_end_waitingTime.setVisibility(View.GONE);
                SessionSave.saveMeterData(Comman.METER_TIMER_BOTH_PAUSE, "0", activity);
                startChronometer();
                SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "2", Drawer_Activity.activity);
            }
        });

        tv_endTripMeter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                        && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                {
                    if (dialogMeter!=null)
                    {
                        dialogMeter.dismiss();
                    }
                    Map_Fragment map_fragment=new Map_Fragment();
                    map_fragment.callCompl1("");
                }
                else
                {
                    if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase(""))
                    {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(activity, Locale.getDefault());
                        try
                        {
                            addresses = geocoder.getFromLocation(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude), 1);
                            String address = addresses.get(0).getAddressLine(0);
                            SessionSave.saveMeterData(Comman.METER_DROP_OFF_LOCATION, address, activity);
                            stopTripMeter();
                        } catch (IOException e) {
                            e.printStackTrace();
                            SessionSave.saveMeterData(Comman.METER_DROP_OFF_LOCATION, "", activity);
                        }
                    }
                }
            }
        });

        tv_startTripMeter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                }
                else
                {
                    try {
                        int locationMode = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
                        int LOCATION_MODE_HIGH_ACCURACY = 3;
                        if (locationMode == LOCATION_MODE_HIGH_ACCURACY)
                        {
                            //request location updates
                            if (checkPermission())
                            {
                                String AcceptFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, activity);
                                if (AcceptFlag!=null && !AcceptFlag.equalsIgnoreCase("") && !AcceptFlag.equalsIgnoreCase("1"))
                                {
                                    String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                                    if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && DutyStatus.equalsIgnoreCase("1"))
                                    {
                                        errorDialogClass.showDialog(activity.getResources().getString(R.string.first_you_have_offline),
                                                getString(R.string.info_message));
                                    }
                                    else
                                    {
                                        if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase(""))
                                        {
                                            Geocoder geocoder;
                                            List<Address> addresses;
                                            geocoder = new Geocoder(activity, Locale.getDefault());
                                            try
                                            {
                                                addresses = geocoder.getFromLocation(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude), 1);
                                                if (addresses.size()>0)
                                                {
                                                    String address = addresses.get(0).getAddressLine(0);
                                                    SessionSave.saveMeterData(Comman.METER_PICKUP_LOCATION, address, activity);
                                                    OpenDialog_AddPassengerMobNo();
                                                }
                                                else
                                                {
                                                    SessionSave.saveMeterData(Comman.METER_PICKUP_LOCATION, "", activity);
                                                    errorDialogClass.showDialog(activity.getResources().getString(R.string.we_can_not_fatch_current_location),
                                                            getString(R.string.info_message));
                                                }
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                                SessionSave.saveMeterData(Comman.METER_PICKUP_LOCATION, "", activity);
                                                errorDialogClass.showDialog(activity.getResources().getString(R.string.we_can_not_fatch_current_location),
                                                        getString(R.string.info_message));
                                            }
                                        }
                                        else
                                        {
                                            SessionSave.saveMeterData(Comman.METER_PICKUP_LOCATION, "", activity);
                                            errorDialogClass.showDialog(activity.getResources().getString(R.string.we_can_not_fatch_current_location),
                                                    getString(R.string.info_message));
                                        }
                                    }
                                }
                                else
                                {
                                    errorDialogClass.showDialog(activity.getResources().getString(R.string.you_are_already_on_your_trip),
                                            getString(R.string.info_message));
                                }
                            }
                            else
                            {
                                requestPermission();
                            }
                        }
                        else
                        {
                            //redirect user to settings page
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                            alertDialog.setTitle(activity.getResources().getString(R.string.new_gps_settings_title));
                            alertDialog.setMessage(activity.getResources().getString(R.string.please_set_your_location_mode_to_high_accuracy));
                            alertDialog.setPositiveButton(activity.getResources().getString(R.string.setting),
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                        }
                                    });

                            AlertDialog alertDialog1 = alertDialog.create();
                            Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
                            Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

                            if (Pbutton != null)
                            {
                                Pbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                            }
                            else
                            {
                                Log.e("Drawer_Activity","takePermision():- " + "Pbutton == null");
                            }

                            if (Pbutton != null)
                            {
                                Nbutton.setTextColor(Drawer_Activity.activity.getResources().getColor(R.color.colorBlack));
                            }
                            else
                            {
                                Log.e("Drawer_Activity","takePermision():- " + "Nbutton == null");

                            }

                            alertDialog.show();
                        }
                    }
                    catch (Settings.SettingNotFoundException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });

        tv_waitinCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String abc = tv_meter_digit.getText().toString();
                if (abc!=null && !abc.equalsIgnoreCase(""))
                {
                    double abc1 = Double.parseDouble(abc);
                    abc1 = abc1+0.000;
                    String ttt = String.format( "%.2f", abc1);
                    tv_meter_digit.setText(ttt+"");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tv_meter_digit.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0)
                {
                    String MinKm = "";
                    if (SessionSave.getMeterData(Comman.METER_MIN_KM, activity)!=null && !SessionSave.getMeterData(Comman.METER_MIN_KM, activity).equalsIgnoreCase(""))
                    {
                        MinKm = SessionSave.getMeterData(Comman.METER_MIN_KM, activity);

                    } else {
                        MinKm = 0+"";
                    }

                    MinKm = MinKm.trim().replace(",", "");

                    if (s.toString()!=null)
                    {
                        String textStr = s.toString().trim().replace(",", "");
                        if (Double.parseDouble(MinKm)>Double.parseDouble(textStr))
                        {
                            double  abc = 0.0;
                            if (SessionSave.getMeterData(Comman.METER_BASE_FARE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BASE_FARE, activity).equalsIgnoreCase(""))
                            {
                                abc = Double.parseDouble(SessionSave.getMeterData(Comman.METER_BASE_FARE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity).equalsIgnoreCase(""))
                            {
                                abc = abc + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity).equalsIgnoreCase(""))
                            {
                                abc = abc + Double.parseDouble(SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity));
                            }
                            String abcd = String.format( "%.2f", abc);
                            SessionSave.saveMeterData(Comman.METER_TOTAL_FARE_CALCULATE, abcd, activity);
                            tv_BaseFare.setText(abcd);
                        }
                        else
                        {
                            String textStr1 = s.toString().trim().replace(",", "");
                            double abc = Double.parseDouble(textStr1)- Double.parseDouble(MinKm);
                            double abc1 = 0.0;
                            if (SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc* Double.parseDouble(SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_BASE_FARE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BASE_FARE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BASE_FARE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity));
                            }
                            String abcd = String.format( "%.2f", abc1);
                            if (abcd!=null && !abcd.equalsIgnoreCase(""))
                            {
                                SessionSave.saveMeterData(Comman.METER_TOTAL_FARE_CALCULATE, abcd, activity);
                            }
                            tv_BaseFare.setText(abcd);
                        }
                    }
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMeter.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogMeter.getWindow().setAttributes(lp);

        GetAllWaitingMilisec();
        dialogMeter.show();
    }

    private void OpenDialog_AddPassengerMobNo()
    {
        final Dialog dialogPassMobNo = new Dialog(activity, R.style.PauseDialog);
        dialogPassMobNo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPassMobNo.setCancelable(false);
        dialogPassMobNo.setContentView(R.layout.dialog_add_passenger_mob_meter);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogPassMobNo.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogPassMobNo.getWindow().setAttributes(lp);

        dialogPassMobNo.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogPassMobNo.getWindow().setAttributes(lp);

        final LinearLayout dialog_ok_layout, main_layoutDialog;
        final EditText et_PassMobileNoMeter;
        ImageView dialog_close;

        et_PassMobileNoMeter = (EditText) dialogPassMobNo.findViewById(R.id.et_PassMobileNoMeter);
        dialog_ok_layout = (LinearLayout) dialogPassMobNo.findViewById(R.id.dialog_ok_layout);
        main_layoutDialog = (LinearLayout) dialogPassMobNo.findViewById(R.id.main_layoutDialog);
        dialog_close = (ImageView) dialogPassMobNo.findViewById(R.id.dialog_close);

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialogPassMobNo.dismiss();
                SessionSave.saveMeterData(Comman.METER_PASSENGER_MOBILE_NUMBER, "", activity);
                errorDialogClass.showDialog(getString(R.string.please_stay_connect), getString(R.string.info_message));
                startTripMeter();
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_PassMobileNoMeter.getText().toString().trim()))
                {
                    new SnackbarUtils(main_layoutDialog, getString(R.string.please_enter_passenger_mobile_no),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                }
                else if (et_PassMobileNoMeter.getText().toString().trim().length()!=10)
                {
                    new SnackbarUtils(main_layoutDialog, getString(R.string.please_enter_valid_mobile_no),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                }
                else
                {
                    dialogPassMobNo.dismiss();
                    SessionSave.saveMeterData(Comman.METER_PASSENGER_MOBILE_NUMBER, et_PassMobileNoMeter.getText().toString().trim(), activity);
                    errorDialogClass.showDialog(getString(R.string.please_stay_connect), getString(R.string.info_message));
                    startTripMeter();
                }
            }
        });

        dialogPassMobNo.show();
    }
    /*private void SendMeterDataToBackend(String DriverId, String PickupLocation, String DropoffLocation, String ModelId, String ModelName, String GrandTotal, String KM, String BaseFare
            , String WaitingTime, String WaitingCost, String NightCharge, final JSONObject estimateFareObj)*/

    private void SendMeterDataToBackend(String driverId, String PickupLocation, String DropoffLocation, String ModelId, final String modelName, final String baseFare, final String per_km_charge
            , final String kmTaken, final String tripFare, final String bookingFee, final String total, final String WaitingCost, String WaitingTime, String NightCharge)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_SEND_METER_TRIP_DATA;

        //// DriverId , PickupLocation , DropoffLocation , ModelId, ModelName , GrandTotal , KM , BaseFare, WaitingTime, WaitingCost, NightCharge
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_DRIVER_ID, driverId);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_PICK_UP_LOCATION, PickupLocation);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_DROP_OFF_LOACTION, DropoffLocation);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_MODEL_ID, ModelId);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_MODEL_NAME, modelName);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_GRAND_TOTAL, total);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_KM, kmTaken);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_BASE_FARE, baseFare);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_WAITING_TIME, WaitingTime);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_WAITING_COST, WaitingCost);
        params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_NIGHT_CHARGE, NightCharge);

        if (SessionSave.getMeterData(Comman.METER_PASSENGER_MOBILE_NUMBER, activity)!=null && !SessionSave.getMeterData(Comman.METER_PASSENGER_MOBILE_NUMBER, activity).equalsIgnoreCase(""))
        {
            params.put(WebServiceAPI.PARAM_SEND_METER_TRIP_DATA_MOBILE_NO, SessionSave.getMeterData(Comman.METER_PASSENGER_MOBILE_NUMBER, activity));
        }

        Log.e("CallSubmitDestination", "URL = " + url);
        Log.e("CallSubmitDestination", "PARAMS = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("CallSubmitDestination", "ResponseCode = " + responseCode);
                    Log.e("CallSubmitDestination", "Json = " + json);

                    if (json != null)
                    {
                        if (json.has("status") && json.getBoolean("status"))
                        {
                            Log.e("status", "true");
                            final Dialog dialog;
                            dialog = new Dialog(activity, R.style.DialogTheme);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(dialog.getWindow().getAttributes());
                            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                            dialog.getWindow().setAttributes(lp);
                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.dialog_meter_trip);

                            TextView tv_dialog_ok, tv_title, tv_carType, tvBaseFare, tv_perKmCharge, tv_totalKm, tv_tripfare, tv_bookingFee, tv_grandTotal
                                    , tv_waitingCost, tv_waitingTime;
                            ImageView dialog_close;

                            tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                            tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                            dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

                            tv_carType = (TextView) dialog.findViewById(R.id.tv_carType);
                            tvBaseFare = (TextView) dialog.findViewById(R.id.tvBaseFare);
                            tv_perKmCharge = (TextView) dialog.findViewById(R.id.tv_perKmCharge);
                            tv_totalKm = (TextView) dialog.findViewById(R.id.tv_totalKm);
                            tv_tripfare = (TextView) dialog.findViewById(R.id.tv_tripfare);
                            tv_bookingFee = (TextView) dialog.findViewById(R.id.tv_bookingFee);
                            tv_grandTotal = (TextView) dialog.findViewById(R.id.tv_grandTotal);
                            tv_waitingCost = (TextView) dialog.findViewById(R.id.tv_waitingCost);
                            tv_waitingTime = (TextView) dialog.findViewById(R.id.tv_waitingTime);

                            tv_title.setText("Meter Trip Fare");

                            dialog_close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    dialogMeter.dismiss();
                                }
                            });
                            tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    dialogMeter.dismiss();
                                }
                            });

                            if(modelName != null &&
                                    !modelName.equalsIgnoreCase("") &&
                                    !modelName.equalsIgnoreCase("null"))
                            {
                                tv_carType.setText(modelName);
                            }
                            else
                            {
                                tv_carType.setText("-");
                            }
                            if(baseFare != null &&
                                    !baseFare.equalsIgnoreCase("") &&
                                    !baseFare.equalsIgnoreCase("null"))
                            {
                                tvBaseFare.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                        baseFare);
                            }
                            else
                            {
                                tvBaseFare.setText("-");
                            }
                            if(per_km_charge != null &&
                                    !per_km_charge.equalsIgnoreCase("") &&
                                    !per_km_charge.equalsIgnoreCase("null"))
                            {
                                tv_perKmCharge.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                        per_km_charge);

                            }
                            else
                            {
                                tv_perKmCharge.setText("-");
                            }
                            if(kmTaken != null &&
                                    !kmTaken.equalsIgnoreCase("") &&
                                    !kmTaken.equalsIgnoreCase("null"))
                            {
                                tv_totalKm.setText(kmTaken + getResources().getString(R.string.km));
                            }
                            else
                            {
                                tv_totalKm.setText("-");
                            }
                            if(tripFare != null &&
                                    !tripFare.equalsIgnoreCase("") &&
                                    !tripFare.equalsIgnoreCase("null"))
                            {
                                tv_tripfare.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                        tripFare);
                            }
                            else
                            {
                                tv_tripfare.setText("-");
                            }
                            if(total != null &&
                                    !total.equalsIgnoreCase("") &&
                                    !total.equalsIgnoreCase("null"))
                            {
                                tv_grandTotal.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                        total);
                            }
                            else
                            {
                                tv_grandTotal.setText("-");
                            }
                            if(WaitingCost != null &&
                                    !WaitingCost.equalsIgnoreCase("") &&
                                    !WaitingCost.equalsIgnoreCase("null"))
                            {
                                tv_waitingCost.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                        WaitingCost);
                            }
                            else
                            {
                                tv_waitingCost.setText("-");
                            }
                            if(bookingFee != null &&
                                    !bookingFee.equalsIgnoreCase("") &&
                                    !bookingFee.equalsIgnoreCase("null"))
                            {
                                tv_bookingFee.setText(Drawer_Activity.activity.getResources().getString(R.string.currency) +" " +
                                        bookingFee);
                            }
                            else
                            {
                                tv_bookingFee.setText("-");
                            }

                            String totalMinInSecond = SessionSave.getMeterData(Comman.METER_TOTAL_MIN_CALCULATE, Drawer_Activity.activity);
                            if (totalMinInSecond!=null &&
                                    !totalMinInSecond.equalsIgnoreCase("") &&
                                    !totalMinInSecond.equalsIgnoreCase("null"))
                            {
                                long abc = (long)((Double.parseDouble(totalMinInSecond)));
                                long hour = (abc / 60)/60;
                                long minutes = abc / 60;
                                long seconds = abc % 60;
                                tv_waitingTime.setText(hour+":"+minutes+":"+seconds);
                            }
                            else
                            {
//                                tv_waitingTime.setText("00"+":"+"00"+":"+"00");
                                tv_waitingTime.setText("-");
                            }

                            stopTripMainTrip();
                            dialogClass.hideDialog();
                            dialog.show();
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e("CallSubmitDestination", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getResources().getString(R.string.error_message));
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void startTripMeter()
    {
        Log.e("MeterTrip","startTripMeter : ");
        SessionSave.saveUserSession(Comman.SUM_FOR_LAT_LONG_METER, "", Drawer_Activity.activity);
        stopChronometer();
        SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "1", activity);

        if (tv_waitingTime!=null)
        {
            SessionSave.saveMeterData(Comman.METER_SELECTED_CAR_MODEL_POSITION, spinner.getSelectedItemPosition()+"", activity);
            spinner.setEnabled(false);
            tv_startTripMeter.setVisibility(View.GONE);
            tv_endTripMeter.setVisibility(View.VISIBLE);
            tv_start_waitingTime.setVisibility(View.VISIBLE);
            tv_end_waitingTime.setVisibility(View.GONE);
        }
    }

    public static void stopTripMainTrip()
    {
        Log.e("MeterTrip","stopTripMainTrip : ");
        if (tv_waitingTime!=null)
        {
            spinner.setEnabled(true);
            tv_meter_digit.setText("");
            tv_BaseFare.setText("");
            tv_hireFee.setText("");
            tv_waitinCost.setText("");

            tv_startTripMeter.setVisibility(View.VISIBLE);
            tv_endTripMeter.setVisibility(View.GONE);
            tv_start_waitingTime.setVisibility(View.GONE);
            tv_end_waitingTime.setVisibility(View.GONE);
        }
        stopChronometer();
        SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "0", activity);
        selectedModelId = "";
        SessionSave.clearMeterData(activity);
    }

    private void stopTripMeter()
    {
        Log.e("MeterTrip","stopTripMeter : ");
        spinner.setEnabled(true);
        for (int i=0; i<ModelNameList.size(); i++)
        {
            String SelectedName = spinner.getSelectedItem().toString();
            Log.e("MeterTrip","SelectedName : "+SelectedName);
            if (SelectedName.equalsIgnoreCase(ModelNameList.get(i)))
            {
                selectedModelId = ModelIdList.get(i);
            }
        }

        Log.e("MeterTrip","selectedModelId : "+selectedModelId);

        if (Global.isNetworkconn(activity))
        {
            CallGetAmount();
        }
        else
        {
            errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
        }
    }

    private void CallGetAmount()
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_GET_FARE_FOR_METER;

        params.put(WebServiceAPI.PARAM_GET_FARE_FOR_METER_MODEL_ID, selectedModelId);
        params.put(WebServiceAPI.PARAM_GET_FARE_FOR_METER_ESTIMATE_KM, tv_meter_digit.getText().toString());
        if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity).equalsIgnoreCase(""))
        {
            params.put(WebServiceAPI.PARAM_GET_FARE_FOR_WAITING_MIN_COST, SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity));
        }
        else
        {
            params.put(WebServiceAPI.PARAM_GET_FARE_FOR_WAITING_MIN_COST, "0");
        }


        Log.e("url", "CallGetAmount = " + url);
        Log.e("param", "CallGetAmount = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "CallGetAmount = " + responseCode);
                    Log.e("Response", "CallGetAmount = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("estimate_fare"))
                                {
                                    String estimate_fare = json.getString("estimate_fare");
                                    if (estimate_fare!=null && !estimate_fare.equalsIgnoreCase(""))
                                    {
                                        JSONObject estimateFareObj = json.getJSONObject("estimate_fare");
                                        if (estimateFareObj!=null)
                                        {
                                            ModelName="";
                                            baseFare="";
                                            kmTaken="";
                                            per_km_charge="";
                                            tripFare="";
                                            bookingFee="";
                                            Total="";
                                            waitingCost="";

                                            if (estimateFareObj.has("name"))
                                            {
                                                ModelName = estimateFareObj.getString("name");
                                            }
                                            if (estimateFareObj.has("base_fare"))
                                            {
                                                baseFare = estimateFareObj.getString("base_fare");
                                            }
                                            if (estimateFareObj.has("per_km_charge"))
                                            {
                                                per_km_charge = estimateFareObj.getString("per_km_charge");
                                            }
                                            if (estimateFareObj.has("km"))
                                            {
                                                kmTaken = estimateFareObj.getString("km");
                                            }
                                            if (estimateFareObj.has("trip_fare"))
                                            {
                                                tripFare = estimateFareObj.getString("trip_fare");
                                            }
                                            if (estimateFareObj.has("booking_fee"))
                                            {
                                                bookingFee = estimateFareObj.getString("booking_fee");
                                            }
                                            if (estimateFareObj.has("total"))
                                            {
                                                Total = estimateFareObj.getString("total");
                                            }
                                            if (estimateFareObj.has("waiting_cost"))
                                            {
                                                waitingCost = estimateFareObj.getString("waiting_cost");
                                            }

                                            String DriverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                                            if (DriverId!=null && !DriverId.equalsIgnoreCase(""))
                                            {
                                                String totalMinInSecond = SessionSave.getMeterData(Comman.METER_TOTAL_MIN_CALCULATE, Drawer_Activity.activity);
                                                if (totalMinInSecond!=null && !totalMinInSecond.equalsIgnoreCase("")) {
                                                }
                                                else
                                                {
                                                    totalMinInSecond="0";
                                                }

                                                SendMeterDataToBackend(DriverId , SessionSave.getMeterData(Comman.METER_PICKUP_LOCATION, activity)
                                                        , SessionSave.getMeterData(Comman.METER_DROP_OFF_LOCATION, activity), selectedModelId
                                                        , ModelName, baseFare, per_km_charge, kmTaken, tripFare, bookingFee, Total, waitingCost, totalMinInSecond, "0");
                                            }
                                        }
                                        else
                                        {
                                            Log.e("status", "false");
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                            }
                                            else
                                            {
                                                errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                        else
                                        {
                                            errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("CallGetAmount", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void OnItemSelect_GetDetail(final String selectedModelId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_GET_MODEL_DETAIL;

        Log.e("url", "OnItemSelect_GetDetail = " + url);
        Log.e("param", "OnItemSelect_GetDetail = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "OnItemSelect_GetDetail = " + responseCode);
                    Log.e("Response", "OnItemSelect_GetDetail = " + json);

                    if (json != null)
                    {
                        Log.e("json", "not Null");
                        if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                        {
                            Log.e("Come","In Main Trip");
                            if (json.has("model_cat1"))
                            {
                                Log.e("json", "has model_cat1");
                                String model_cat1 = json.getString("model_cat1");
                                if (model_cat1!=null && !model_cat1.equalsIgnoreCase(""))
                                {
                                    JSONArray model_cat1Array = json.getJSONArray("model_cat1");
                                    if (model_cat1Array.length()>0)
                                    {
                                        for (int i=0; i<model_cat1Array.length(); i++)
                                        {
                                            JSONObject model_cat1OBJ = model_cat1Array.getJSONObject(i);
                                            if (model_cat1OBJ!=null)
                                            {
                                                if (model_cat1OBJ.has("Id"))
                                                {
                                                    String modelId = model_cat1OBJ.getString("Id");
                                                    if (modelId!=null && !modelId.equalsIgnoreCase("") && modelId.equalsIgnoreCase(selectedModelId))
                                                    {
                                                        if (model_cat1OBJ.has("BaseFare"))
                                                        {
                                                            if (model_cat1OBJ.getString("BaseFare")!=null && !model_cat1OBJ.getString("BaseFare").equalsIgnoreCase(""))
                                                            {
                                                                SessionSave.saveMeterData(Comman.METER_BASE_FARE, model_cat1OBJ.getString("BaseFare"), activity);
                                                                String abcd = String.format( "%.2f", Double.parseDouble(model_cat1OBJ.getString("BaseFare")));
                                                                tv_BaseFare.setText(abcd);
                                                            }
                                                        }
                                                        if (model_cat1OBJ.has("BaseFare"))
                                                        {
                                                            if (model_cat1OBJ.getString("BaseFare")!=null && !model_cat1OBJ.getString("BaseFare").equalsIgnoreCase(""))
                                                            {
                                                                SessionSave.saveMeterData(Comman.METER_HIRE_COST, model_cat1OBJ.getString("BaseFare"), activity);
                                                                tv_hireFee.setText(model_cat1OBJ.getString("BaseFare"));
                                                            }
                                                        }
                                                        if (model_cat1OBJ.has("MinKm"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_MIN_KM, model_cat1OBJ.getString("MinKm"), activity);
                                                        }
                                                        if (model_cat1OBJ.has("BookingFee"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_BOOKING_FEE, model_cat1OBJ.getString("BookingFee"), activity);
                                                        }
                                                        if (model_cat1OBJ.has("BelowPerKmCharge"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_BELOW_PER_KM_CHARGE, model_cat1OBJ.getString("BelowPerKmCharge"), activity);
                                                        }
                                                        if (model_cat1OBJ.has("WaitingTimePerMinuteCharge"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_WAITING_TIME_PER_MIN, model_cat1OBJ.getString("WaitingTimePerMinuteCharge"), activity);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("json", "no status found");
                                        dialogClass.hideDialog();
                                        /*if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                        else
                                        {
                                            errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                        }*/
                                    }
                                }
                                else
                                {
                                    Log.e("json", "no status found");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("Come","In Metereeee Trip");
                            if (json.has("meter_model"))
                            {
                                Log.e("json", "has meter_model");
                                String meter_model = json.getString("meter_model");
                                if (meter_model!=null && !meter_model.equalsIgnoreCase(""))
                                {
                                    JSONArray meter_modelArray = json.getJSONArray("meter_model");
                                    if (meter_modelArray.length()>0)
                                    {
                                        for (int i=0; i<meter_modelArray.length(); i++)
                                        {
                                            JSONObject meter_modelOBJ = meter_modelArray.getJSONObject(i);
                                            if (meter_modelOBJ!=null)
                                            {
                                                if (meter_modelOBJ.has("Id"))
                                                {
                                                    String modelId = meter_modelOBJ.getString("Id");
                                                    if (modelId!=null && !modelId.equalsIgnoreCase("") && modelId.equalsIgnoreCase(selectedModelId))
                                                    {
                                                        if (meter_modelOBJ.has("BaseFare"))
                                                        {
                                                            if (meter_modelOBJ.getString("BaseFare")!=null && !meter_modelOBJ.getString("BaseFare").equalsIgnoreCase(""))
                                                            {
                                                                SessionSave.saveMeterData(Comman.METER_BASE_FARE, meter_modelOBJ.getString("BaseFare"), activity);
                                                                String abcd = String.format( "%.2f", Double.parseDouble(meter_modelOBJ.getString("BaseFare")));
                                                                tv_BaseFare.setText(abcd);
                                                            }
                                                        }
                                                        if (meter_modelOBJ.has("BaseFare"))
                                                        {
                                                            if (meter_modelOBJ.getString("BaseFare")!=null && !meter_modelOBJ.getString("BaseFare").equalsIgnoreCase(""))
                                                            {
                                                                SessionSave.saveMeterData(Comman.METER_HIRE_COST, meter_modelOBJ.getString("BaseFare"), activity);
                                                                tv_hireFee.setText(meter_modelOBJ.getString("BaseFare"));
                                                            }
                                                        }
                                                        if (meter_modelOBJ.has("MinKm"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_MIN_KM, meter_modelOBJ.getString("MinKm"), activity);
                                                        }
                                                        if (meter_modelOBJ.has("BookingFee"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_BOOKING_FEE, meter_modelOBJ.getString("BookingFee"), activity);
                                                        }
                                                        if (meter_modelOBJ.has("BelowPerKmCharge"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_BELOW_PER_KM_CHARGE, meter_modelOBJ.getString("BelowPerKmCharge"), activity);
                                                        }
                                                        if (meter_modelOBJ.has("WaitingTimePerMinuteCharge"))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_WAITING_TIME_PER_MIN, meter_modelOBJ.getString("WaitingTimePerMinuteCharge"), activity);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        /*Log.e("json", "no status found");
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                        else
                                        {
                                            errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                        }*/
                                    }
                                }
                                else
                                {
                                    Log.e("json", "no status found");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                }
                            }
                        }

                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("CallGetAmount", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public static void GetBaseFareOffline(String distanceMain)
    {
        Log.e("MeterTrip ","GetBaseFareOffline");
        if (distanceMain!=null && !distanceMain.equalsIgnoreCase(""))
        {
            String MinKm = "";
            if (SessionSave.getMeterData(Comman.METER_MIN_KM, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_MIN_KM, Drawer_Activity.activity).equalsIgnoreCase(""))
            {
                MinKm = SessionSave.getMeterData(Comman.METER_MIN_KM, Drawer_Activity.activity);

            } else {
                MinKm = 0+"";
            }

            double BaseFareWithDist = 0.0;
            if (Double.parseDouble(MinKm)>Double.parseDouble(distanceMain))
            {
                double  abc = 0.0;
                if (SessionSave.getMeterData(Comman.METER_BASE_FARE, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_BASE_FARE, Drawer_Activity.activity).equalsIgnoreCase(""))
                {
                    abc = Double.parseDouble(SessionSave.getMeterData(Comman.METER_BASE_FARE, Drawer_Activity.activity));
                }
                if (SessionSave.getMeterData(Comman.METER_BOOKING_FEE, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_BOOKING_FEE, Drawer_Activity.activity).equalsIgnoreCase(""))
                {
                    abc = abc + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BOOKING_FEE, Drawer_Activity.activity));
                }
                if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, Drawer_Activity.activity).equalsIgnoreCase(""))
                {
                    abc = abc + Double.parseDouble(SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, Drawer_Activity.activity));
                }
                BaseFareWithDist =abc;
            }
            else
            {
                double abc = Double.parseDouble(distanceMain)- Double.parseDouble(MinKm);
                double abc1 = 0.0;
                if (SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, Drawer_Activity.activity).equalsIgnoreCase(""))
                {
                    abc1 = abc* Double.parseDouble(SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, Drawer_Activity.activity));
                }
                if (SessionSave.getMeterData(Comman.METER_BASE_FARE, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_BASE_FARE, Drawer_Activity.activity).equalsIgnoreCase(""))
                {
                    abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BASE_FARE, Drawer_Activity.activity));
                }
                if (SessionSave.getMeterData(Comman.METER_BOOKING_FEE, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_BOOKING_FEE, Drawer_Activity.activity).equalsIgnoreCase(""))
                {
                    abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BOOKING_FEE, Drawer_Activity.activity));
                }
                if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, Drawer_Activity.activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, Drawer_Activity.activity).equalsIgnoreCase(""))
                {
                    abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, Drawer_Activity.activity));
                }
                BaseFareWithDist = abc1;
            }
            String BaseFareWithDist2 = String.format( "%.2f", BaseFareWithDist);
            if (tv_meterFareOutSide!=null)
            {
                tv_meterFareOutSide.setText(BaseFareWithDist2);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        activity = Drawer_Activity.this;
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }

        if (PasscodeBackPress==1)
        {
            setTab_Home();
        }

        if (Create_Passcode_Activity.NfcFlag==1)
        {
            if(tabItemIndex==4)
            {
                Create_Passcode_Activity.NfcFlag=0;
            }
        }

        if (flagResumePass==1)
        {
            if (tabItemIndex==4)
            {
                if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                        && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
                {
                    Intent intentv = new Intent(activity, Create_Passcode_Activity.class);
                    startActivity(intentv);
                }
            }
        }
        flagResumePass=0;
        chronometerPersist.resumeState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (tabItemIndex==4)
        {
            flagResumePass=1;
        }

    }

    public static String commaFormat(String x) {
        if(x != null && !x.equalsIgnoreCase("") && x.length() > 3){
            String c =  new DecimalFormat("#,###,###.00").format(Double.parseDouble(x));
            c =  c.replace(",","/");
            c =  c.replace(".",",");
            return c.replace("/",".");
//            return c;
        }else {
            return x;
        }
    }
}
