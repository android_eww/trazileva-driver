package com.connectkargo.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Adapter.CreditCardList_Adapter;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Been.CreditCard_List_Been;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Wallet_Cards_Activity extends BaseActivity implements View.OnClickListener{

    public static Wallet_Cards_Activity activity;

    LinearLayout main_layout, ll_back, ll_toolbar_RightImage, ll_Sos, ll_addCard;
    TextView tv_toolbarTitle, tv_NoCardAvailable;
    ImageView iv_toolbar_RightImage;

    private RecyclerView recyclerView;
    private CreditCardList_Adapter adapter;
    public static List<CreditCard_List_Been> cardList = new ArrayList<>();

    private AQuery aQuery;
    DialogClass dialogClass;

    Intent intentExtra;
    public static String from="";

    public static int resumeFlag=1;

    ErrorDialogClass errorDialogClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_add_card);

        activity = Wallet_Cards_Activity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        intentExtra = getIntent();

        from="";
        if (intentExtra!=null)
        {
            from = intentExtra.getStringExtra("from");
        }
        else
        {
            from="";
        }
        resumeFlag=1;

        Log.e("from","from : "+from);
        /*if (from!=null && from.equalsIgnoreCase("drawer"))
        {
            resumeFlag=0;
        }
        else
        {
            resumeFlag=1;
        }*/

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_toolbar_RightImage = (LinearLayout) findViewById(R.id.ll_toolbar_RightImage);
        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_NoCardAvailable = (TextView) findViewById(R.id.tv_NoCardAvailable);

        iv_toolbar_RightImage = (ImageView) findViewById(R.id.iv_toolbar_RightImage);
        recyclerView = (RecyclerView) findViewById(R.id.rv_CreditCard);
        ll_addCard = (LinearLayout) findViewById(R.id.ll_addCard);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        ll_Sos.setOnClickListener(this);



        adapter = new CreditCardList_Adapter(activity, cardList, main_layout);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        tv_toolbarTitle.setText(getString(R.string.cards));
        iv_toolbar_RightImage.setVisibility(View.VISIBLE);
        iv_toolbar_RightImage.setImageResource(R.drawable.icon_plus_black);

        ll_back.setOnClickListener(this);
        ll_toolbar_RightImage.setOnClickListener(this);
        ll_addCard.setOnClickListener(this);


        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
        String cardCount = SessionSave.getUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, activity);

        if (cardCount!=null && !cardCount.equalsIgnoreCase(""))
        {
            if (Integer.parseInt(cardCount)==0)
            {
                OpenNextActivity();
                Set_NoCardLayout(1);
            }
            else
            {
                if (Drawer_Activity.callCardListMethod==0)
                {
                    if (driverId != null && !driverId.equalsIgnoreCase(""))
                    {
                        if (ConnectivityReceiver.isConnected())
                        {
                            GetCardList(driverId);
                        }
                        else
                        {
                            errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet), getResources().getString(R.string.internet_error_message));
                        }
                    }
                }
            }
        }
        else
        {
            if (driverId!=null && !driverId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    GetCardList(driverId);
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet), getResources().getString(R.string.internet_error_message));
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_toolbar_RightImage:

                Intent intent = new Intent(Wallet_Cards_Activity.this,Add_Card_In_List_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in,R.anim.left_out);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;

            case R.id.ll_addCard:

                Intent intent1 = new Intent(Wallet_Cards_Activity.this,Add_Card_In_List_Activity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.right_in,R.anim.left_out);

                break;
        }
    }

    public void Set_NoCardLayout(int flag)
    {
        if (flag==0)
        {
            tv_NoCardAvailable.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        else
        {
            tv_NoCardAvailable.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void OpenNextActivity()
    {
        resumeFlag=0;
        Intent i =  new Intent(activity, Add_Card_In_List_Activity.class);
        startActivity(i);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();

        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    private void GetCardList(String driverId)
    {
        cardList.clear();
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_DABITCARD_LIST + driverId;

        Log.e("url", "GetCardList = " + url);
        Log.e("param", "GetCardList = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "GetCardList = " + responseCode);
                    Log.e("Response", "GetCardList = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                if (json.has("cards"))
                                {
                                    String cards = json.getString("cards");
                                    if (cards!=null && !cards.equalsIgnoreCase(""))
                                    {
                                        JSONArray cardsArray = json.getJSONArray("cards");
                                        if (cardsArray!=null && cardsArray.length()>0)
                                        {
                                            for (int i=0; i<cardsArray.length(); i++)
                                            {
                                                String Id = "", CardNum="", CardNum2="", Type="", Alias="", Expiry="";

                                                JSONObject cardObj = cardsArray.getJSONObject(i);
                                                if (cardObj.has("Id"))
                                                {
                                                    Id = cardObj.getString("Id");
                                                }
                                                if (cardObj.has("CardNum"))
                                                {
                                                    CardNum = cardObj.getString("CardNum");
                                                }
                                                if (cardObj.has("CardNum2"))
                                                {
                                                    CardNum2 = cardObj.getString("CardNum2");
                                                }
                                                if (cardObj.has("Type"))
                                                {
                                                    Type = cardObj.getString("Type");
                                                }
                                                if (cardObj.has("Alias"))
                                                {
                                                    Alias = cardObj.getString("Alias");
                                                }
                                                if (cardObj.has("Expiry"))
                                                {
                                                    Expiry = cardObj.getString("Expiry");
                                                }
                                                Log.e("GetCardList","Id : "+Id+"\nCardNum : "+CardNum+"\nCardNum2 : "+CardNum2+"\nType : "+Type+"\nAlias : "+Alias);
                                                cardList.add(new CreditCard_List_Been(Id, Alias, CardNum2, Type, Expiry));
                                            }
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, cardList.size()+"", activity);
//                                            cardList.add(new CreditCard_List_Been("", "", "", "", ""));
                                            Log.e("position","cardList.size : "+cardList.size());
                                            if (cardList.size()>0)
                                            {
                                                Set_NoCardLayout(0);
                                                adapter.notifyDataSetChanged();
                                            }
                                            else
                                            {
                                                Set_NoCardLayout(1);
                                                OpenNextActivity();
                                            }
                                            Drawer_Activity.callCardListMethod=1;
                                        }
                                        else
                                        {
                                            Log.e("cardsArray", "null");
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                            dialogClass.hideDialog();
                                            Drawer_Activity.callCardListMethod=1;

                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"), getResources().getString(R.string.error_message));
                                            }
                                            Set_NoCardLayout(1);
                                            OpenNextActivity();
                                        }
                                    }
                                    else
                                    {
                                        Log.e("cards", "null");
                                        dialogClass.hideDialog();
                                        Drawer_Activity.callCardListMethod=1;
                                        SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getResources().getString(R.string.error_message));
                                        }
                                        Set_NoCardLayout(1);
                                        OpenNextActivity();
                                    }
                                }
                                else
                                {
                                    Log.e("cards", "noy found");
                                    dialogClass.hideDialog();
                                    Drawer_Activity.callCardListMethod=1;
                                    SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getResources().getString(R.string.error_message));
                                    }
                                    Set_NoCardLayout(1);
                                    OpenNextActivity();
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                Set_NoCardLayout(1);
                                SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            Set_NoCardLayout(1);
                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        Set_NoCardLayout(1);
                        SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getResources().getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("GetCardList", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    Set_NoCardLayout(1);
                    SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getResources().getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Wallet_Cards_Activity.from!=null && Wallet_Cards_Activity.from.equalsIgnoreCase("drawer"))
        {

        }
        else
        {
            if (resumeFlag==1)
            {
                if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                        && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
                {
                    resumeFlag = 0;
                    Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                    startActivity(intent);
                    if (Wallet_Balance_Activity.activity!=null)
                    {
                        Wallet_Balance_Activity.activity.finish();
                    }
                    if (Wallet_Balance_TopUp_Activity.activity!=null)
                    {
                        Wallet_Balance_TopUp_Activity.activity.finish();
                    }
                    if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                    {
                        Wallet_Balance_TransferToBank_Activity.activity.finish();
                    }
                    if (Wallet_Transfer_Activity.activity!=null)
                    {
                        Wallet_Transfer_Activity.activity.finish();
                    }
                    if (Wallet_Transfer_History_Activity.activity!=null)
                    {
                        Wallet_Transfer_History_Activity.activity.finish();
                    }
                    finish();
                }
            }
            else
            {
                resumeFlag=1;
            }
        }
    }
}