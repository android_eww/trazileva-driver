package com.connectkargo.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Fragment.Wallet_Transfer_ReceiveMoney_Fragment;
import com.connectkargo.driver.Fragment.Wallet_Transfer_SendMoney_Fragment;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet_Transfer_Activity extends BaseActivity implements View.OnClickListener
{
    String TAG = "walletTransferActivity";

    public static Wallet_Transfer_Activity activity;
    public Wallet_Transfer_SendMoney_Fragment fragment;

    public static LinearLayout main_layout;
    private LinearLayout ll_back,ll_send_money, ll_Sos;
    RelativeLayout rl_send_money, rl_receive_money, rl_history;

    FrameLayout frame_transfer;
    String clickName = "";
    String CLICK_SEND_MONEY="send_money", CLICK_RECEIVE_MONEY="receive_money";

    private TextView tv_toolbarTitle, tv_dollar, tv_send_money;
    private EditText et_send_money;

    public static String getQrCodeResult = "";

    private AQuery aQuery;
    private DialogClass dialogClass;
    int resumeFlag=1;

    private Animation animation;
    private Animation.AnimationListener animationListener;
    private int clickflag = 0;

    ErrorDialogClass errorDialogClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transfer);


        activity = Wallet_Transfer_Activity.this;

        aQuery = new AQuery(activity);
        fragment = new Wallet_Transfer_SendMoney_Fragment();
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        resumeFlag = 1;
        clickflag = 0;

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        tv_toolbarTitle = (TextView)findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(getResources().getText(R.string.transfer));

        ll_send_money = (LinearLayout) findViewById(R.id.ll_send_money);

        rl_send_money = (RelativeLayout) findViewById(R.id.rl_send_money);
        rl_receive_money = (RelativeLayout) findViewById(R.id.rl_receive_money);
        rl_history = (RelativeLayout) findViewById(R.id.rl_history);

        frame_transfer = (FrameLayout) findViewById(R.id.frame_transfer);
        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);

        tv_dollar = (TextView)findViewById(R.id.tv_dollar);
        tv_send_money = (TextView)findViewById(R.id.tv_send_money);
        et_send_money = (EditText)findViewById(R.id.et_send_money);


        ll_Sos.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        tv_send_money.setOnClickListener(this);

        rl_send_money.setOnClickListener(this);
        rl_receive_money.setOnClickListener(this);
        rl_history.setOnClickListener(this);

        et_send_money.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if(charSequence.length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_send_money.setText("");
                        tv_dollar.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        tv_dollar.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    tv_dollar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if (clickflag == 1)
                {
                    checkData();
                }
                else if (clickflag == 2)
                {
                    setSendMoney();

                }
                else if (clickflag == 3)
                {
                    setReceiveMoney();

                }
                else if (clickflag == 4)
                {
                    setHistory();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        };

        setSendMoney();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.tv_send_money:
                clickflag = 1;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                tv_send_money.startAnimation(animation);
                break;

            case R.id.rl_send_money:
                clickflag = 2;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                rl_send_money.startAnimation(animation);
                break;

            case R.id.rl_receive_money:
                clickflag = 3;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                rl_receive_money.startAnimation(animation);
                break;

            case R.id.rl_history:
                clickflag = 4;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                rl_history.startAnimation(animation);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void checkData() {

        if (et_send_money == null || TextUtils.isEmpty(et_send_money.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_send_amount),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else if (getQrCodeResult == null || getQrCodeResult.equalsIgnoreCase(""))
        {
            new SnackbarUtils(main_layout, getString(R.string.scan_bar_code_error_message),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        {
            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    sendMoney();
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),  getString(R.string.internet_error_message));
                }
            }
        }
    }

    public void sendMoney()
    {
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_SEND_MONEY;

        params.put(WebServiceAPI.SEND_MONEY_PARAM_QRCODE,getQrCodeResult);
        params.put(WebServiceAPI.SEND_MONEY_PARAM_SENDER_ID,SessionSave.getUserSession(Comman.USER_ID, activity));
        params.put(WebServiceAPI.SEND_MONEY_PARAM_AMOUNT,et_send_money.getText().toString());

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, params , JSONObject.class , new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if(json.getBoolean("status"))
                            {
                                Log.e(TAG,"Status = " + "true");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    String message = json.getString("message");
                                    errorDialogClass.showDialog(message, getString(R.string.success_message));
                                    refreshLayout();
                                }
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE,walletBalance,activity);
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = "  + "Null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "somthing_is_wrong");
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }

            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY,WebServiceAPI.HEADER_VALUE));



    }

    private void refreshLayout() {

        fragment.refreshScanLayout();
        tv_dollar.setVisibility(View.VISIBLE);
        et_send_money.setText("");
        dialogClass.hideDialog();
    }


    public void setSendMoney()
    {
        if (!clickName.equalsIgnoreCase(CLICK_SEND_MONEY))
        {
            ll_send_money.setVisibility(View.VISIBLE);

            Wallet_Transfer_SendMoney_Fragment fragment = new Wallet_Transfer_SendMoney_Fragment();
            FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
            transaction.replace(R.id.frame_transfer, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();
        }
        clickName = CLICK_SEND_MONEY;
    }


    public void setReceiveMoney()
    {
        if (!clickName.equalsIgnoreCase(CLICK_RECEIVE_MONEY))
        {

            ll_send_money.setVisibility(View.GONE);

            Wallet_Transfer_ReceiveMoney_Fragment fragment = new Wallet_Transfer_ReceiveMoney_Fragment();
            FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
            transaction.replace(R.id.frame_transfer, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();

        }
        clickName = CLICK_RECEIVE_MONEY;

    }

    public void setHistory()
    {
        resumeFlag=0;
        Intent intent = new Intent(activity, Wallet_Balance_TransferToBank_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }
                if (Wallet_Balance_TopUp_Activity.activity!=null)
                {
                    Wallet_Balance_TopUp_Activity.activity.finish();
                }
                if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                {
                    Wallet_Balance_TransferToBank_Activity.activity.finish();
                }
                if (Wallet_Cards_Activity.activity!=null)
                {
                    Wallet_Cards_Activity.activity.finish();
                }
                if (Wallet_Transfer_History_Activity.activity!=null)
                {
                    Wallet_Transfer_History_Activity.activity.finish();
                }
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }
}