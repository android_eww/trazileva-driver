package com.connectkargo.driver.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.GPSTracker;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Login_Activity extends BaseActivity implements View.OnClickListener{

    public static Login_Activity activity;

    TextView tv_signIn, tv_Registration, tv_login_forgotPass;
    EditText et_email, et_password;
    LinearLayout main_layout;

    Intent intent;

    private AQuery aQuery;
    DialogClass dialogClass;
    Animation animBounce;
    ErrorDialogClass errorDialogClass;
    GPSTracker gpsTracker;
    String Token;

    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = Login_Activity.this;

        aQuery = new AQuery(Login_Activity.this);
        dialogClass = new DialogClass(activity, 1);
        gpsTracker = new GPSTracker(activity);
        errorDialogClass = new ErrorDialogClass(activity);

        init();
    }

    private void init()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        tv_signIn = (TextView) findViewById(R.id.tv_signIn);
        tv_Registration = (TextView) findViewById(R.id.tv_Registration);
        tv_login_forgotPass = (TextView) findViewById(R.id.tv_login_forgotPass);

        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
//        et_email.setText("hello@excellentwebworld.com");
//        et_password.setText("12345678");

        tv_login_forgotPass.setOnClickListener(this);
        tv_signIn.setOnClickListener(this);
        tv_Registration.setOnClickListener(this);

        checkGPS();
    }


    public void checkGPS()
    {
        if (checkPermission())
        {
            if (gpsTracker.canGetLocation())
            {
                gpsTracker.getLocation();

                Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

                Log.e("latitude", "" + Constants.newgpsLatitude);
                Log.e("longitude", "" + Constants.newgpsLongitude);
            }
            else
            {
                gpsTracker.showSettingsAlert(activity);
            }
        }
        else
        {
            requestPermission();
        }

        retriveToken();
    }


    public boolean checkPermission()
    {
        int result = ContextCompat.checkSelfPermission(Login_Activity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }


    public void requestPermission()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        Log.e("call","onRequestPermissionsResult() = "+requestCode);
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if (gpsTracker.canGetLocation())
                    {
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    }
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.permission_denied_for_location),
                            activity.getString(R.string.location_));
                }
                break;
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.tv_login_forgotPass:
                animBounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
                tv_login_forgotPass.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        SessionSave.saveUserSession(Comman.USER_FORGOT_PASSWORD_OTP, "", activity);
                        SessionSave.saveUserSession(Comman.USER_FORGOT_PASSWORD_DRIVER_ID, "", activity);
                        intent = new Intent(activity, ForgotPassword_Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.tv_signIn:
                animBounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
                tv_signIn.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        DriverLogin();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;

            case R.id.tv_Registration:
                animBounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
                tv_Registration.startAnimation(animBounce);
                animBounce.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        intent = new Intent(activity, Registration_Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
        }
    }

    private void DriverLogin()
    {
        Token = SessionSave.getToken(Comman.DEVICE_TOKEN, activity);
        Log.e("Token","Token"+Token);
        if (Token!=null && !Token.equalsIgnoreCase(""))
        {
            if (TextUtils.isEmpty(et_email.getText().toString().trim())
                    && TextUtils.isEmpty(et_password.getText().toString().trim()))
            {
                new SnackbarUtils(main_layout, activity.getResources().getString(R.string.please_fill_all_details),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            }
            else if (TextUtils.isEmpty(et_email.getText().toString().trim()))
            {
                new SnackbarUtils(main_layout, getString(R.string.please_enter_email),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            }
            else if (!isEmailValid(et_email.getText().toString().trim()))
            {
                new SnackbarUtils(main_layout, activity.getResources().getString(R.string.please_enter_valid_email),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            }
            else if (TextUtils.isEmpty(et_password.getText().toString().trim()))
            {
                new SnackbarUtils(main_layout,getString(R.string.please_enter_password),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            }
//            else if(et_password.getText().toString().length() < 8)
//            {
//                new SnackbarUtils(main_layout, "Password must contain 8 charecter",
//                        ContextCompat.getColor(activity, R.color.snakbar_color),
//                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
//                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
//            }
            else
            {
                gpsTracker = new GPSTracker(activity);
                if (!gpsTracker.canGetLocation())
                {
                    gpsTracker.showSettingsAlert(activity);
                }
                else
                {
                    gpsTracker.getLocation();
                    Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                    Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

                    if (Global.isNetworkconn(activity))
                    {
                        UserSignIn();
                    }
                    else
                    {
                        errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),
                                getResources().getString(R.string.internet_error_message));
                    }
                }
            }
        }
        else
        {
            retriveToken();
        }

    }

    private void UserSignIn()
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_LOGIN;

        params.put(WebServiceAPI.LOGIN_PARAM_DEVICE_TYPE, Comman.DEVICE_TYPE);
        params.put(WebServiceAPI.LOGIN_PARAM_TOKEN, SessionSave.getToken(Comman.DEVICE_TOKEN, activity));
        params.put(WebServiceAPI.LOGIN_PARAM_USERNAME, et_email.getText().toString());
        params.put(WebServiceAPI.LOGIN_PARAM_PASSWORD, et_password.getText().toString());
        params.put(WebServiceAPI.LOGIN_PARAM_LATITUDE, Constants.newgpsLatitude);
        params.put(WebServiceAPI.LOGIN_PARAM_LONGITUDE, Constants.newgpsLongitude);

        Log.e("url", "UserSignIn = " + url);
        Log.e("param", "UserSignIn = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UserSignIn = " + responseCode);
                    Log.e("Response", "UserSignIn = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("driver"))
                                {
                                    JSONObject driverObject = json.getJSONObject("driver");
                                    if (driverObject!=null)
                                    {
                                        if (driverObject.has("profile"))
                                        {
                                            SessionSave.saveUserSession(Comman.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"0",activity);

                                            JSONObject driverProfile = driverObject.getJSONObject("profile");
                                            if (driverProfile!=null)
                                            {
                                                String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", QRCode="", Password="", Address="", SubUrb = ""
                                                        , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                        , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status="", DOB=""
                                                        , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="",
                                                        ProfileComplete="", CategoryId="", Balance="", ReferralAmount="", GovIdExpire = "",
                                                        FrontGovIdCard="", BackGovIdCard  ="", CriminalCertificate = "", AddressProof  = "";

                                                if (driverProfile.has("Id"))
                                                {
                                                    DriverId = driverProfile.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_ID, DriverId, activity);
                                                }
                                                if (driverProfile.has("CompanyId"))
                                                {
                                                    CompanyId = driverProfile.getString("CompanyId");
                                                    SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, activity);
                                                }
                                                if (driverProfile.has("DispatcherId"))
                                                {
                                                    DispatcherId = driverProfile.getString("DispatcherId");
                                                    SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, activity);
                                                }
                                                if (driverProfile.has("Email"))
                                                {
                                                    Email = driverProfile.getString("Email");
                                                    SessionSave.saveUserSession(Comman.USER_EMAIL, Email, activity);
                                                }
                                                if (driverProfile.has("Fullname"))
                                                {
                                                    FullName = driverProfile.getString("Fullname");
                                                    SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, activity);
                                                }

                                                if (driverProfile.has("DOB"))
                                                {
                                                    DOB = driverProfile.getString("DOB");
                                                    SessionSave.saveUserSession(Comman.USER_DATE_OF_BIRTH, DOB, activity);
                                                }

                                                if (driverProfile.has("MobileNo"))
                                                {
                                                    MobileNo = driverProfile.getString("MobileNo");
                                                    SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, activity);
                                                }
                                                if (driverProfile.has("Gender"))
                                                {
                                                    Gender = driverProfile.getString("Gender");
                                                    SessionSave.saveUserSession(Comman.USER_GENDER, Gender, activity);
                                                }
                                                if (driverProfile.has("Image"))
                                                {
                                                    Image = driverProfile.getString("Image");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, activity);
                                                }
                                                if (driverProfile.has("QRCode"))
                                                {
                                                    QRCode = driverProfile.getString("QRCode");
                                                    SessionSave.saveUserSession(Comman.USER_QR_CODE, QRCode, activity);
                                                }
                                                if (driverProfile.has("Password"))
                                                {
                                                    Password = driverProfile.getString("Password");
                                                    SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, activity);
                                                }
                                                if (driverProfile.has("Address"))
                                                {
                                                    Address = driverProfile.getString("Address");
                                                    SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, activity);
                                                }
                                                if (driverProfile.has("SubUrb"))
                                                {
                                                    SubUrb = driverProfile.getString("SubUrb");
                                                    SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, activity);
                                                }


                                                if (driverProfile.has("City"))
                                                {
                                                    City = driverProfile.getString("City");
                                                    SessionSave.saveUserSession(Comman.USER_CITY, City, activity);
                                                }
                                                if (driverProfile.has("State"))
                                                {
                                                    State = driverProfile.getString("State");
                                                    SessionSave.saveUserSession(Comman.USER_STATE, State, activity);
                                                }
                                                if (driverProfile.has("Country"))
                                                {
                                                    Country = driverProfile.getString("Country");
                                                    SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, activity);
                                                }
                                                if (driverProfile.has("ZipCode"))
                                                {
                                                    Zipcode = driverProfile.getString("ZipCode");
                                                    SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, activity);
                                                }
                                                if (driverProfile.has("ReferralCode"))
                                                {
                                                    ReferralCode = driverProfile.getString("ReferralCode");
                                                    SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, activity);
                                                }

                                                if (driverProfile.has("DriverLicenseFront"))
                                                {
                                                    DriverLicense = driverProfile.getString("DriverLicenseFront");
                                                    SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE_FRONT, DriverLicense, activity);
                                                }
                                                if (driverProfile.has("DriverLicenseBack"))
                                                {
                                                    SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE_BACK, driverProfile.getString("DriverLicenseBack"), activity);
                                                }
                                                if (driverProfile.has("AccreditationCertificate"))
                                                {
                                                    AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, activity);
                                                }
                                                if (driverProfile.has("DriverLicenseExpire"))
                                                {
                                                    DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                                    SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, activity);
                                                }
                                                if (driverProfile.has("AccreditationCertificateExpire"))
                                                {
                                                    AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, activity);
                                                }
                                                if (driverProfile.has("BankHolderName"))
                                                {
                                                    BankHolderName = driverProfile.getString("BankHolderName");
                                                    SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, activity);
                                                }
                                                if (driverProfile.has("BankName"))
                                                {
                                                    BankName = driverProfile.getString("BankName");
                                                    SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, activity);
                                                }



                                                if (driverProfile.has("BankAcNo"))
                                                {
                                                    BankAcNo = driverProfile.getString("BankAcNo");
                                                    SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, activity);
                                                }
                                                if (driverProfile.has("BSB"))
                                                {
                                                    BSB = driverProfile.getString("BSB");
                                                    SessionSave.saveUserSession(Comman.USER_BSB, BSB, activity);
                                                }
                                                if (driverProfile.has("Lat"))
                                                {
                                                    Lat = driverProfile.getString("Lat");
                                                    SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, activity);
                                                }
                                                if (driverProfile.has("Lng"))
                                                {
                                                    Lng = driverProfile.getString("Lng");
                                                    SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, activity);
                                                }
                                                if (driverProfile.has("Status"))
                                                {
                                                    Status = driverProfile.getString("Status");
                                                    SessionSave.saveUserSession(Comman.USER_STATE, Status, activity);
                                                }
                                                if (driverProfile.has("Availability"))
                                                {
                                                    Availability = driverProfile.getString("Availability");
                                                    SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, activity);
                                                }



                                                if (driverProfile.has("DriverDuty"))
                                                {
                                                    DriverDuty = driverProfile.getString("DriverDuty");
                                                    SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, activity);
                                                }
                                                if (driverProfile.has("ABN"))
                                                {
                                                    ABN = driverProfile.getString("ABN");
                                                    SessionSave.saveUserSession(Comman.USER_ABN, ABN, activity);
                                                }
                                                if (driverProfile.has("Description"))
                                                {
                                                    serviceDescription = driverProfile.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, activity);
                                                }
                                                if (driverProfile.has("DCNumber"))
                                                {
                                                    DCNumber = driverProfile.getString("DCNumber");
                                                    SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, activity);
                                                }
                                                if (driverProfile.has("ProfileComplete"))
                                                {
                                                    ProfileComplete = driverProfile.getString("ProfileComplete");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, activity);
                                                }

                                                if (driverProfile.has("CategoryId"))
                                                {
                                                    CategoryId = driverProfile.getString("CategoryId");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, activity);
                                                }
                                                if (driverProfile.has("Balance"))
                                                {
                                                    Balance = driverProfile.getString("Balance");
                                                    SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, Balance, activity);
                                                }
                                                if (driverProfile.has("ReferralAmount"))
                                                {
                                                    ReferralAmount = driverProfile.getString("ReferralAmount");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, ReferralAmount, activity);
                                                }
                                                if (driverProfile.has("GovIdExpire"))
                                                {
                                                    GovIdExpire = driverProfile.getString("GovIdExpire");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_EXPIRY, GovIdExpire, activity);
                                                }
                                                if (driverProfile.has("FrontGovIdCard"))
                                                {
                                                    FrontGovIdCard = driverProfile.getString("FrontGovIdCard");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_FRONT_ID, FrontGovIdCard, activity);
                                                }
                                                if (driverProfile.has("BackGovIdCard"))
                                                {
                                                    BackGovIdCard = driverProfile.getString("BackGovIdCard");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_GOVERNMENT_BACK_ID, BackGovIdCard, activity);
                                                }
                                                if (driverProfile.has("CriminalCertificate"))
                                                {
                                                    CriminalCertificate = driverProfile.getString("CriminalCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_CRIMINAL_CERTI, CriminalCertificate, activity);
                                                }
                                                if (driverProfile.has("AddressProof"))
                                                {
                                                    AddressProof = driverProfile.getString("AddressProof");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_ADDRESS_PROOF, AddressProof, activity);
                                                }

                                                if (driverProfile.has("DriverDadMobile"))
                                                {
                                                    SessionSave.saveUserSession(Comman.USER_DAD_NUMBER, driverProfile.getString("DriverDadMobile"),  activity);
                                                }
                                                if (driverProfile.has("DriverWifeMobile"))
                                                {
                                                    SessionSave.saveUserSession(Comman.USER_WIFE_NUMBER, driverProfile.getString("DriverWifeMobile"),  activity);
                                                }

                                                String Rate = "";

                                                if (driverProfile.has("rating"))
                                                {
                                                    JSONObject driverRating = driverProfile.getJSONObject("rating");

                                                    if (driverRating!=null)
                                                    {
                                                        if (driverRating.has("Rate"))
                                                        {
                                                            Rate = driverRating.getString("Rate");
                                                        }
                                                    }
                                                }
                                                SessionSave.saveUserSession(Comman.USER_DRIVER_RATE, Rate, activity);

                                                if (driverProfile.has("Vehicle"))
                                                {
                                                    JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");

                                                    if (driverVehicle!=null)
                                                    {
                                                        String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                                , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage,
                                                                VehicleLeftImage, VehicleRightImage, Description, CarCategoryId, VehicleModelName;

                                                        if (driverVehicle.has("Id"))
                                                        {
                                                            VehicleId = driverVehicle.getString("Id");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleModel"))
                                                        {
                                                            VehicleModel = driverVehicle.getString("VehicleModel");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                        }
                                                        if (driverVehicle.has("Company"))
                                                        {
                                                            CarCompany = driverVehicle.getString("Company");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                        }
                                                        if (driverVehicle.has("Color"))
                                                        {
                                                            CarColor = driverVehicle.getString("Color");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleRegistrationNo"))
                                                        {
                                                            VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                        }


                                                        if (driverVehicle.has("RegistrationCertificate"))
                                                        {
                                                            RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                            SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                        {
                                                            VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                        }
                                                        if (driverVehicle.has("RegistrationCertificateExpire"))
                                                        {
                                                            RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                            SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleImage"))
                                                        {
                                                            VehicleImage = driverVehicle.getString("VehicleImage");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleLeftImage"))
                                                        {
                                                            VehicleLeftImage = driverVehicle.getString("VehicleLeftImage");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_LEFT, VehicleLeftImage, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleRightImage"))
                                                        {
                                                            VehicleRightImage = driverVehicle.getString("VehicleRightImage");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE_RIGHT, VehicleRightImage, activity);
                                                        }

                                                        if (driverVehicle.has("Description"))
                                                        {
                                                            Description = driverVehicle.getString("Description");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleClass"))
                                                        {
                                                            VehicleModelName = driverVehicle.getString("VehicleClass");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                            Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                            if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Business Class"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Economy"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Taxi"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                            }
                                                            else if (VehicleModelName.contains("LUX-VAN"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Disability"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                            }
                                                            else
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                        }
                                                    }
                                                    Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, activity));

                                                    SessionSave.saveUserSession(Comman.LOGIN_FLAG_USER,"1",activity);
                                                    SessionSave.saveUserSession(Comman.CREATED_PASSCODE,"",activity);

                                                }

                                                dialogClass.hideDialog();
                                                intent = new Intent(activity, Drawer_Activity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                                finish();
                                            }
                                            else
                                            {
                                                dialogClass.hideDialog();
                                                if (json.has("message"))
                                                {
                                                    errorDialogClass.showDialog(json.getString("message"),
                                                            getResources().getString(R.string.error_message));
                                                }
                                                else
                                                {
                                                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                                                            getResources().getString(R.string.error_message));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"),
                                                        getResources().getString(R.string.error_message));
                                            }
                                            else
                                            {
                                                errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                                                        getResources().getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("DRIVER", "NULL");
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"),
                                                    getResources().getString(R.string.error_message));
                                        }
                                        else
                                        {
                                            errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                                                    getResources().getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("DRIVER", "NO");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));

                    }
                } catch (Exception e) {
                    Log.e("UserSignIn", "Exception : " + e.toString());
                    dialogClass.hideDialog();

                    errorDialogClass.showDialog(getString(R.string.something_is_wrong),"Error Message");
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }
}
