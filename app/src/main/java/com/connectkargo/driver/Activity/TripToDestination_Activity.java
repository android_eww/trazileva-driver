package com.connectkargo.driver.Activity;

import android.app.Dialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.GPSTracker;
import com.connectkargo.driver.R;
import com.connectkargo.driver.Util.Utility;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TripToDestination_Activity extends BaseActivity implements View.OnClickListener{

    public static TripToDestination_Activity activity;

    LinearLayout ll_back, ll_Sos, ll_dropLocation;
    TextView tv_toolbarTitle, tv_submit;
    public static AutoCompleteTextView tv_dropLocation;
    SwitchCompat switchView;

    GPSTracker gpsTracker;
    String DropOffLoc_Address="";
    String jsonurl = "";
    ArrayList<String> names, Place_id_type;
    paserdata parse;
    static JSONObject json;
    JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    String place_id;

    private AQuery aQuery;
    DialogClass dialogClass;
    ErrorDialogClass errorDialogClass;
    Dialog dialogOffTrip;

    Double Lat = 0.0, Lng = 0.0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_destination_location);

        activity = TripToDestination_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        Init();
    }

    private void Init()
    {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        ll_dropLocation = (LinearLayout) findViewById(R.id.ll_dropLocation);
        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(getString(R.string.trip_to_destination));

        tv_dropLocation = (AutoCompleteTextView) findViewById(R.id.tv_dropLocation);
        switchView = (SwitchCompat) findViewById(R.id.switchView);
        tv_submit = (TextView) findViewById(R.id.tv_submit);

        String CheckMark = SessionSave.getUserSession(Comman.DESTINATION_ADDRESS_FLAG, activity);
        if (CheckMark != null && !CheckMark.equalsIgnoreCase("") && CheckMark.equalsIgnoreCase("1"))
        {
            switchView.setChecked(true);
            if (SessionSave.getUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, activity)!=null && !SessionSave.getUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, activity).equalsIgnoreCase(""))
            {
                tv_submit.setVisibility(View.GONE);
                ll_dropLocation.setVisibility(View.VISIBLE);

                tv_dropLocation.setText(SessionSave.getUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, activity));
                tv_dropLocation.dismissDropDown();
                tv_dropLocation.setEnabled(false);
            }
            else
            {
                Log.e("switch_compat", "DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI null");
            }
        }
        else
        {
            ll_dropLocation.setVisibility(View.GONE);
            tv_submit.setVisibility(View.GONE);
            tv_dropLocation.setEnabled(true);

            SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FLAG, "0", activity);
            SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, "", activity);
            Log.e("switch_compat", "DESTINATION_ADDRESS_FLAG null");
        }

        switchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("switch_compat", switchView.isChecked() + "");
                if (switchView.isChecked())
                {
                    ll_dropLocation.setVisibility(View.VISIBLE);
                    tv_submit.setVisibility(View.VISIBLE);
                    tv_dropLocation.setEnabled(true);
                    tv_dropLocation.setText("");
                }
                else
                {
                    String CheckMark = SessionSave.getUserSession(Comman.DESTINATION_ADDRESS_FLAG, activity);
                    if (CheckMark != null && !CheckMark.equalsIgnoreCase("") && CheckMark.equalsIgnoreCase("1"))
                    {
                        OpendialogForCloseTrip();
                    }
                    else
                    {
                        ll_dropLocation.setVisibility(View.GONE);
                        tv_submit.setVisibility(View.GONE);
                        tv_dropLocation.setEnabled(true);
                        tv_dropLocation.setText("");
                    }
                }
            }
        });


        tv_dropLocation.addTextChangedListener(new TextWatcher() {
            String search_text[];

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                DropOffLoc_Address ="";

                search_text = tv_dropLocation.getText().toString().split(",");
                gpsTracker = new GPSTracker(activity);
                double lat=0,lng=0;
                if (gpsTracker.canGetLocation())
                {
                    lat = gpsTracker.getLatitude();
                    lng = gpsTracker.getLongitude();
                }

                jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20")+"&location="+ lat+","+ lng + "&radius=1000&sensor=true&key="+getString(R.string.api_key)+"&components=&language=en";
                Log.e("jsonurl","jsonurl : "+jsonurl);
                names = new ArrayList<String>();
                Place_id_type = new ArrayList<String>();
                if (parse != null) {
                    parse.cancel(true);
                    parse = null;
                }
                parse = new paserdata(tv_dropLocation);
                parse.execute();
                if (!Global.isNetworkconn(activity))
                {
                    errorDialogClass.HideErrorDialog();
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ll_back.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);
        tv_submit.setOnClickListener(this);
    }

    private void OpendialogForCloseTrip()
    {
        dialogOffTrip = new Dialog(activity, R.style.DialogTheme);
        dialogOffTrip.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogOffTrip.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogOffTrip.getWindow().setAttributes(lp);
        dialogOffTrip.setCancelable(false);
        dialogOffTrip.setContentView(R.layout.dialog_off_trip_to_desti);

        LinearLayout dialog_ok_layout = (LinearLayout) dialogOffTrip.findViewById(R.id.dialog_ok_layout);
        ImageView dialog_close = (ImageView) dialogOffTrip.findViewById(R.id.dialog_close);

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOffTrip.hide();
                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                if (driverId!=null && !driverId.equalsIgnoreCase(""))
                {
                    CallSubmitDestination(driverId, "0", "", Lat, Lng);
                }
            }
        });

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOffTrip.hide();
                String CheckMark = SessionSave.getUserSession(Comman.DESTINATION_ADDRESS_FLAG, activity);
                if (CheckMark != null && !CheckMark.equalsIgnoreCase("") && CheckMark.equalsIgnoreCase("1"))
                {
                    switchView.setChecked(true);
                }
                else
                {
                    switchView.setChecked(false);
                }
            }
        });

        dialogOffTrip.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;

            case R.id.tv_submit:
                if (Global.isNetworkconn(activity))
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        if (DropOffLoc_Address!=null && !TextUtils.isEmpty(DropOffLoc_Address.trim()))
                        {
                            CallSubmitDestination(driverId, "1", DropOffLoc_Address, Lat, Lng);
                        }
                        else
                        {
                            errorDialogClass.showDialog(getString(R.string.please_enter_dropoff_loc),getResources().getString(R.string.error_message));
                        }
                    }
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
                }
                break;
        }
    }

    private void CallSubmitDestination(String driverId, final String status1, final String dropOffLoc_Address, Double lat, Double lng)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_TRIP_TP_DESTINATION;

        params.put(WebServiceAPI.PARAM_TRIP_TO_DESTINATION_DRIVER_ID, driverId);
        params.put(WebServiceAPI.PARAM_TRIP_TO_DESTINATION_STATUS, status1);
        params.put(WebServiceAPI.PARAM_TRIP_TO_DESTINATION_LOCATION, dropOffLoc_Address);
        params.put(WebServiceAPI.PARAM_TRIP_TO_DESTINATION_LAT, lat);
        params.put(WebServiceAPI.PARAM_TRIP_TO_DESTINATION_LONG, lng);

        Log.e("CallSubmitDestination", "URL = " + url);
        Log.e("CallSubmitDestination", "PARAMS = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("CallSubmitDestination", "ResponseCode = " + responseCode);
                    Log.e("CallSubmitDestination", "Json = " + json);

                    if (json != null)
                    {
                        if (json.has("status") && json.getBoolean("status"))
                        {
                            Log.e("status", "true");
                            dialogClass.hideDialog();
                            if (status1.equalsIgnoreCase("0"))
                            {
                                ll_dropLocation.setVisibility(View.GONE);
                                tv_submit.setVisibility(View.GONE);
                                tv_dropLocation.setEnabled(true);

                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FLAG, "0", activity);
                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, "", activity);
                            }
                            else
                            {
                                ll_dropLocation.setVisibility(View.VISIBLE);
                                tv_submit.setVisibility(View.GONE);
                                tv_dropLocation.setEnabled(false);

                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FLAG, "1", activity);
                                SessionSave.saveUserSession(Comman.DESTINATION_ADDRESS_FOR_TRIP_TP_DESTI, dropOffLoc_Address, activity);
                            }
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.success_message));
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (status1.equalsIgnoreCase("0"))
                            {
                                switchView.setChecked(false);
                            }
                            else
                            {
                                switchView.setChecked(true);
                            }
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),getResources().getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        if (status1.equalsIgnoreCase("0"))
                        {
                            switchView.setChecked(false);
                        }
                        else
                        {
                            switchView.setChecked(true);
                        }
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong),getResources().getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e("CallSubmitDestination", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    if (status1.equalsIgnoreCase("0"))
                    {
                        switchView.setChecked(false);
                    }
                    else
                    {
                        switchView.setChecked(true);
                    }
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getResources().getString(R.string.error_message));
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public class paserdata extends AsyncTask<Void, Integer, Void> {

        AutoCompleteTextView autoCompleteTextView;

        public paserdata(AutoCompleteTextView autoCompleteTextView)
        {
            this.autoCompleteTextView = autoCompleteTextView;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                json = GetAddress(jsonurl.toString());
                if (json != null) {
                    names.clear();
                    Place_id_type.clear();
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String plc_id = c.getString("place_id");
                        Log.d("description", description);
                        names.add(description);
                        Place_id_type.add(plc_id);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("names", "" + names);
            try {
                ArrayAdapter<String> adp;
                adp = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, names) {
                    @Override
                    public View getView(final int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        final TextView text = (TextView) view.findViewById(android.R.id.text1);
                        text.setTextColor(getResources().getColor(R.color.mediumGray1));
                        text.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                if (Place_id_type.size() > 0) {
                                    place_id = Place_id_type.get(position);
                                    System.out.println("Place_id_type" + Place_id_type.get(position));
                                }

                                autoCompleteTextView.setText(text.getText().toString());
                                DropOffLoc_Address = text.getText().toString();
                                Log.e("Address", "DropOffLoc_Address : "+DropOffLoc_Address);

                                getLocationFromAddress(text.getText().toString(), autoCompleteTextView);
                                autoCompleteTextView.dismissDropDown();
                            }
                        });
                        return view;
                    }
                };
                autoCompleteTextView.setAdapter(adp);
                adp.notifyDataSetChanged();
            } catch (Exception e) {
                DropOffLoc_Address = "";
                // TODO: handle exception
            }
        }
    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetAddress(String Url) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }

    // Get the lat and lng from given address
    public void getLocationFromAddress(String strAddress, AutoCompleteTextView autoCompleteTextView) {
        Geocoder coder = new Geocoder(activity);
        List<Address> address;
        Address location = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (!address.isEmpty()) {
                location = address.get(0);
                location.getLatitude();
                location.getLongitude();
                Log.e("getLocationFromAddress","getLocationFromAddress getLatitude : "+location.getLatitude()+" longitude : "+location.getLongitude());
                Lat = location.getLatitude();
                Lng = location.getLongitude();
            }

            if (!address.isEmpty()) {
                Constants.newgpsLatitude_pickUp = location.getLatitude();
                Constants.newgpsLongitude_pickUp = location.getLongitude();
                if (autoCompleteTextView.getText().toString().trim().length() > 0) {
//                    dropdownlist_istouch = false;
                    Constants.address_pickUp = autoCompleteTextView.getText().toString();
                }
            } else {
                if (autoCompleteTextView.getText().toString().trim().length() > 0) {
                    Constants.address_pickUp = autoCompleteTextView.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key="+getResources().getString(R.string.api_key);

                    System.out.println("encode_url" + encode_url);
                    new GetGeoCoderAddress(encode_url, autoCompleteTextView).execute();
                }
            }
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
            Constants.newgpsLatitude_pickUp = 0.0;
            Constants.newgpsLongitude_pickUp = 0.0;
            Constants.address_pickUp = "";
            Lat = 0.0;
            Lng = 0.0;
        }
    }

    public class GetGeoCoderAddress extends AsyncTask<String, Void, Void> {
        private final String Urlcoreconfig;
        String lat, lng;
        private String jsonResult;
        AutoCompleteTextView autoCompleteTextView;

        public GetGeoCoderAddress(String url, AutoCompleteTextView autoCompleteTextView) {
            this.Urlcoreconfig = url;
            this.autoCompleteTextView= autoCompleteTextView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httppost = new HttpGet(Urlcoreconfig);
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("sureshhhhhhhhh " + jsonResult);
                JSONObject json = new JSONObject(jsonResult);
                System.out.println("ssssssssssss " + json);
                json = json.getJSONObject("result");
                json = json.getJSONObject("geometry");
                json = json.getJSONObject("location");
                lat = json.getString("lat");
                lng = json.getString("lng");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (lat!=null && !lat.equalsIgnoreCase(""))
            {
                Constants.newgpsLatitude_pickUp = Double.valueOf(lat);
                Constants.newgpsLongitude_pickUp = Double.valueOf(lng);
                Constants.address_pickUp = autoCompleteTextView.getText().toString();
                System.out.println("sureshhhhhhhhh " +Constants.address_pickUp + Constants.newgpsLatitude_pickUp + " , " + Constants.newgpsLongitude_pickUp);
            }
            else
            {
                Constants.newgpsLatitude_pickUp = 0.0;
                Constants.newgpsLongitude_pickUp = 0.0;
                Constants.address_pickUp = "";
            }
        }
    }

    public StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume()
    {
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
        super.onResume();
    }
}
