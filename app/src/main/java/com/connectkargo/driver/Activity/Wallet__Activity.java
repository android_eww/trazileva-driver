package com.connectkargo.driver.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet__Activity extends BaseActivity implements View.OnClickListener
{
    public String TAG = "Wallet__Activity";
    public static Wallet__Activity activity;

    private LinearLayout  ll_transfer, ll_balance, ll_cards, ll_Sos;
    private LinearLayout main_layout,ll_back;

    private Intent intent;
    String walleteBallence = "0";

    private TextView tv_toolbarTitle, tv_WalletBallence;

    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private AQuery aQuery;
    private DialogClass dialogClass;
    public static int flagResumePass=0;
    int resumeFlag=1;

    private Animation animation;
    private Animation.AnimationListener animationListener;
    private int clickflag = 0;

    ErrorDialogClass errorDialogClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        activity = Wallet__Activity.this;
        clickflag = 0;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        flagResumePass=0;
        resumeFlag=1;

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);
        takePermision(0);
        initUI();
    }

    public void takePermision(int flag)
    {
        if (ActivityCompat.checkSelfPermission(Wallet__Activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("call"," permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(Wallet__Activity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(Wallet__Activity.this);
                builder.setTitle(getResources().getString(R.string.need_camera_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_camera_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(Wallet__Activity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });

                AlertDialog alertDialog1 = builder.create();
                Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
                Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

                if (Pbutton != null)
                {
                    Pbutton.setTextColor(activity.getResources().getColor(R.color.colorBlack));
                }
                else
                {
                    Log.e(TAG,"takePermision():- " + "Pbutton == null");
                }

                if (Pbutton != null)
                {
                    Nbutton.setTextColor(activity.getResources().getColor(R.color.colorBlack));
                }
                else
                {
                    Log.e(TAG,"takePermision():- " + "Nbutton == null");

                }
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(Wallet__Activity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA,true);
            editor.commit();
        }
        else
        {
            if (flag==1)
            {
                flagResumePass=0;
                Intent intent = new Intent(Wallet__Activity.this, Wallet_Transfer_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        Log.e("call","onRequestPermissionResult call");
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                Log.e("call","onRequestPermissionResult call111");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("call","onRequestPermissionResult call 222");
                }
                else
                {
                    Log.e("call","onRequestPermissionsResult permission not granted");
                }
                break;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.e("call","onRequestPermissionResult call 333");
        if (sentToSettings)
        {
            if (ActivityCompat.checkSelfPermission(Wallet__Activity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    private void initUI()
    {

        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        tv_toolbarTitle = (TextView)findViewById(R.id.tv_toolbarTitle);

        tv_WalletBallence = (TextView) findViewById(R.id.tv_WalletBallence);

        ll_transfer = (LinearLayout) findViewById(R.id.ll_transfer);
        ll_balance = (LinearLayout) findViewById(R.id.ll_balance);
        ll_cards = (LinearLayout) findViewById(R.id.ll_cards);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);

        tv_toolbarTitle.setText(getResources().getText(R.string.wallet));

        ll_back.setOnClickListener(activity);

        ll_transfer.setOnClickListener(activity);
        ll_balance.setOnClickListener(activity);
        ll_cards.setOnClickListener(activity);
        ll_Sos.setOnClickListener(this);

        animationListener = new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if (clickflag == 1)
                {
                    intent = new Intent(Wallet__Activity.this, Wallet_Balance_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
                else if (clickflag == 2)
                {

                    takePermision(1);
                }
                else if (clickflag == 3)
                {
                    intent = new Intent(Wallet__Activity.this, Wallet_Cards_Activity.class);
                    intent.putExtra("from","");
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }


            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };


        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
        if (driverId!=null && !driverId.equalsIgnoreCase(""))
        {
            if (ConnectivityReceiver.isConnected())
            {
                GetHistory(driverId);
            }
            else
            {
                errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_balance:
                flagResumePass=0;
                resumeFlag=0;
                clickflag = 1;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_balance.startAnimation(animation);

                break;

            case R.id.ll_transfer:
                resumeFlag=0;
                clickflag = 2;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_transfer.startAnimation(animation);

                break;

            case R.id.ll_cards:
//                if (MainActivity.CardFlag==0)
//                {
//                    intent = new Intent(Wallet__Activity.this, Add_Card_In_List_Activity.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
//                }
//                else
//                {
                flagResumePass=0;
                resumeFlag=0;
                clickflag = 3;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                ll_cards.startAnimation(animation);

//                }
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private void GetHistory(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_TRANSACTION_HISTORY + driverId;

        Log.e("url", "GetCardList = " + url);
        Log.e("param", "GetCardList = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "GetCardList = " + responseCode);
                    Log.e("Response", "GetCardList = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, walletBalance, activity);
                                        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
                                        if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
                                        {
                                            String mm = getResources().getString(R.string.currency) + String.format("%.2f",Double.parseDouble(walleteBallence));
                                            tv_WalletBallence.setText(mm);
                                        }
                                        else
                                        {
                                            tv_WalletBallence.setVisibility(View.GONE);
                                        }
                                    }

                                }
                                else
                                {
                                    tv_WalletBallence.setVisibility(View.GONE);
                                    Log.e("history", "no history");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                tv_WalletBallence.setVisibility(View.GONE);
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            tv_WalletBallence.setVisibility(View.GONE);
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        tv_WalletBallence.setVisibility(View.GONE);
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    tv_WalletBallence.setVisibility(View.GONE);
                    Log.e("GetCardList", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }

        if (Drawer_Activity.PasscodeBackPress==1)
        {
            onBackPressed();
        }

        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
        if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
        {
            String mm = getResources().getString(R.string.currency)+ String.format("%.2f",Double.parseDouble(walleteBallence));
            tv_WalletBallence.setText(mm);
        }
        else
        {
            tv_WalletBallence.setVisibility(View.GONE);
        }

      /*  if (flagResumePass==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                Intent intentv = new Intent(activity, Create_Passcode_Activity.class);
                startActivity(intentv);
            }
        }
        */
        flagResumePass=0;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        flagResumePass=1;
    }
}
