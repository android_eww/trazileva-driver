package com.connectkargo.driver.Activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ShareRide_Activity extends BaseActivity implements View.OnClickListener{

    public static ShareRide_Activity activity;

    DialogClass dialogClass;
    AQuery aQuery;
    ErrorDialogClass errorDialogClass;
    LinearLayout main_layout, ll_back, ll_Sos;
    TextView  tv_toolbarTitle;
    SwitchCompat sw_passcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_rides);

        activity = ShareRide_Activity.this;

        aQuery = new AQuery(ShareRide_Activity.this);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        InitView();
    }

    private void InitView()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);

        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText("Share Rides");

        sw_passcode = (SwitchCompat) findViewById(R.id.sw_passcode);

        ll_back.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);


        Log.e("SHARE_RIDE_STATUS","SHARE_RIDE_STATUS : "+ SessionSave.getUserSession(Comman.SHARE_RIDE_STATUS, activity));
        String CheckMark = SessionSave.getUserSession(Comman.SHARE_RIDE_STATUS, activity);
        if (CheckMark != null && !CheckMark.equalsIgnoreCase("") && CheckMark.equalsIgnoreCase("1"))
        {
            sw_passcode.setChecked(true);
        }
        else
        {
            sw_passcode.setChecked(false);
        }

        sw_passcode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                Log.e("switch_compat", isChecked + "");
                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                if (driverId!=null && !driverId.equalsIgnoreCase(""))
                {
                    Change_ShareRide_Status(driverId);
                }
            }
        });
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    public void Change_ShareRide_Status(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CHANGE_SHARE_RIDE_STATUS + driverId;

        Log.e("Change_ShareRide_Status", "URL = " + url);
        Log.e("Change_ShareRide_Status", "Params = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("Change_ShareRide_Status", "responseCode = " + responseCode);
                    Log.e("Change_ShareRide_Status", "Response = " + json);

                    if (json != null)
                    {
                        Log.e("json", "Not Null");
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.success_message));
                                }
                                if (json.has("flag") && json.getString("flag").equalsIgnoreCase("1"))
                                {
                                    SessionSave.saveUserSession(Comman.SHARE_RIDE_STATUS, "1", activity);
                                }
                                else
                                {
                                    SessionSave.saveUserSession(Comman.SHARE_RIDE_STATUS, "0", activity);
                                }
                            }
                            else
                            {
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                        }
                    }
                    else
                    {
                        Log.e("json", "Null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("Change_ShareRide_Status", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }
}
