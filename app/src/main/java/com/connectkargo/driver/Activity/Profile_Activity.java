package com.connectkargo.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.R;


public class Profile_Activity extends BaseActivity implements View.OnClickListener{

    public String TAG = "Profile_Activity";
    Profile_Activity activity;
    Intent intent;
    CardView cv_prof_DriverProfile, cv_prof_carDetail, cv_prof_AccountDetail, cv_prof_DocumentDetail;
    LinearLayout main_layout, ll_back, ll_Sos;
    TextView tv_toolbarTitle;

    DialogClass dialogClass;

    private Animation animation;
    private Animation.AnimationListener animationListener;
    private int clickflag = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_driver);

        activity = Profile_Activity.this;
        clickflag = 0;

        initUI();
    }

    private void initUI()
    {
        Log.e(TAG,"IntiUI()");

        main_layout = findViewById(R.id.main_layout);
        ll_back = findViewById(R.id.ll_back);
        tv_toolbarTitle = findViewById(R.id.tv_toolbarTitle);

        tv_toolbarTitle.setText(getResources().getText(R.string.profile_update));

        ll_back.setOnClickListener(this);

        cv_prof_AccountDetail = findViewById(R.id.cv_prof_AccountDetail);
        cv_prof_DocumentDetail = findViewById(R.id.cv_prof_DocumentDetail);
        cv_prof_DriverProfile = findViewById(R.id.cv_prof_DriverProfile);
        cv_prof_carDetail = findViewById(R.id.cv_prof_carDetail);

        ll_Sos = findViewById(R.id.ll_Sos);

        cv_prof_AccountDetail.setOnClickListener(this);
        cv_prof_DocumentDetail.setOnClickListener(this);
        cv_prof_DriverProfile.setOnClickListener(this);
        cv_prof_carDetail.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);

        animationListener = new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if (clickflag == 1)
                {
                    Open_Prof_DriverProfile_Activity();
                }
                if (clickflag == 2)
                {
                    Open_Prof_CarDetail_Activity();
                }
                if (clickflag == 3)
                {
                    Open_Prof_AccountDetail_Activity();
                }
                if (clickflag == 4)
                {
                    Open_Prof_DocumentDetail_Activity();
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.cv_prof_DriverProfile:

                clickflag = 1;
                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                cv_prof_DriverProfile.startAnimation(animation);
                break;

            case R.id.cv_prof_carDetail:

                clickflag = 2;
                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                cv_prof_carDetail.startAnimation(animation);
                break;

            case R.id.cv_prof_AccountDetail:

                clickflag = 3;
                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                cv_prof_AccountDetail.startAnimation(animation);
                break;

            case R.id.cv_prof_DocumentDetail:
                clickflag = 4;
                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                cv_prof_DocumentDetail.startAnimation(animation);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void Open_Prof_AccountDetail_Activity()
    {
        intent = new Intent(activity, Profile_AccountDetail_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void Open_Prof_DocumentDetail_Activity()
    {
        intent = new Intent(activity, Profile_DocumentDetail_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void Open_Prof_DriverProfile_Activity()
    {
        intent = new Intent(activity, Profile_DriverProfile_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void Open_Prof_CarDetail_Activity()
    {
        intent = new Intent(activity, Profile_CarDetail_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }
}
