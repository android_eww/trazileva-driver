package com.connectkargo.driver.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SocketSingleObject;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;


import org.json.JSONObject;


public class Splash_Activity extends BaseActivity {

    String TAG = "Splash_Activity";

    Splash_Activity activity;

    public int SPLASH_TIME_OUT = 2000;
//    public int SPLASH_TIME_OUT = 200000;

    private AQuery aQuery;
    DialogClass dialogClass;
    ErrorDialogClass errorDialogClass;

    String message="", version="";
    String DialogType = "", COMP_UPDATE = "CompUpdate", OPTIONAL_UPDATE = "OptionalUpdate", UNDER_MAINTENANCE = "UnderMaintenance";
    String token;

    Handler handlerAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity = Splash_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        new Thread(new Runnable() {
            public void run() {
                try
                {
                    final InstanceID instanceID = InstanceID.getInstance(activity);
                    token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.e(TAG, "GCM Registration Token = " + SessionSave.getToken(Comman.DEVICE_TOKEN, activity));
                }
                catch (Exception e)
                {
                    Log.e(TAG, "GCM Registration Token Fail = ", e);
                }
            }
        }).start();

        try
        {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (version!=null && !version.equalsIgnoreCase(""))
        {
            if (ConnectivityReceiver.isConnected())
            {
                checkAppVersion(version);
            }
            else
            {
                errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
            }
        }

        retriveToken();
    }

    private void checkAppVersion(String version)
    {
        String url = WebServiceAPI.WEB_SERVICE_APP_SETTING +
                version +
                WebServiceAPI.APP_SETTING_APP_TYPE + "/" +
                SessionSave.getUserSession(Comman.USER_ID,activity);

        Log.e(TAG, "checkAppVersion URL = " + url);
        message = "";

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json!=null)
                    {
                        if (json.has("message"))
                        {
                            message= json.getString("message");
                        }
                        if(json.has("pdf_url")){
                            SessionSave.saveToken(Comman.INIT_PDF, json.getString("pdf_url"), Splash_Activity.this);
                        }

                        if (json.has("status"))
                        {
                            dialogClass.hideDialog();
                            if (json.getBoolean("status"))
                            {
                                if (json.has("driver"))
                                {
                                    JSONObject driverObject = json.getJSONObject("driver");
                                    if (driverObject!=null)
                                    {
                                        if (driverObject.has("profile"))
                                        {
                                            JSONObject driverProfile = driverObject.getJSONObject("profile");
                                            if (driverProfile!=null)
                                            {
                                                if (driverProfile.has("Vehicle"))
                                                {
                                                    JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");

                                                     if (driverVehicle!=null)
                                                    {
                                                        String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                                , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, CarCategoryId, VehicleModelName;

                                                        if (driverVehicle.has("Id"))
                                                        {
                                                            VehicleId = driverVehicle.getString("Id");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleModel"))
                                                        {
                                                            VehicleModel = driverVehicle.getString("VehicleModel");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                        }
                                                        if (driverVehicle.has("Company"))
                                                        {
                                                            CarCompany = driverVehicle.getString("Company");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                        }
                                                        if (driverVehicle.has("Color"))
                                                        {
                                                            CarColor = driverVehicle.getString("Color");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleRegistrationNo"))
                                                        {
                                                            VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                        }

                                                        if (driverVehicle.has("RegistrationCertificate"))
                                                        {
                                                            RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                            SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                        {
                                                            VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                        }
                                                        if (driverVehicle.has("RegistrationCertificateExpire"))
                                                        {
                                                            RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                            SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleImage"))
                                                        {
                                                            VehicleImage = driverVehicle.getString("VehicleImage");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                        }

                                                        if (driverVehicle.has("Description"))
                                                        {
                                                            Description = driverVehicle.getString("Description");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleClass"))
                                                        {
                                                            VehicleModelName = driverVehicle.getString("VehicleClass");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                            Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                            if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Business Class"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Economy"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Taxi"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                            }
                                                            else if (VehicleModelName.contains("LUX-VAN"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Disability"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                            }
                                                            else
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (json.has("update"))
                                {
                                    if (json.getBoolean("update"))
                                    {
                                        init();
                                    }
                                    else
                                    {
                                        DialogType = OPTIONAL_UPDATE;
                                        OpenDialogForNotice(DialogType, message);
                                    }
                                }
                                else
                                {
                                    init();
                                }
                            }
                            else
                            {
                                if (json.has("update"))
                                {
                                    if (json.getBoolean("update"))
                                    {
                                        DialogType = COMP_UPDATE;
                                        OpenDialogForNotice(DialogType, message);
                                    }
                                    else
                                    {
                                        DialogType = UNDER_MAINTENANCE;
                                        OpenDialogForNotice(DialogType, message);
                                    }
                                }
                                else
                                {
                                    if (json.has("maintenance"))
                                    {
                                        if (json.getBoolean("maintenance"))
                                        {
                                            DialogType = UNDER_MAINTENANCE;
                                            OpenDialogForNotice(DialogType, message);
                                        }
                                    }
                                    else
                                    {
                                        DialogType="";
                                        OpenDialogForNotice(DialogType, message);
                                    }
                                }
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            DialogType="";
                            OpenDialogForNotice(DialogType, message);
                        }

                    }
                    else
                    {
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "checkAppVersion Exception = "+e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }


    private void init()
    {
        handlerAnim = new Handler();
        handlerAnim.postDelayed(new Runnable(){
            @Override
            public void run(){
                String LoginFlag_User = SessionSave.getUserSession(Comman.LOGIN_FLAG_USER,activity);
                String Email_session = SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY,activity);
                Log.e(TAG,"LoginFlag_User = " + LoginFlag_User);

                if (LoginFlag_User!=null && !LoginFlag_User.equalsIgnoreCase("") && LoginFlag_User.equalsIgnoreCase("1"))
                {
                    Intent i = new Intent(Splash_Activity.this, Drawer_Activity.class);
                    startActivity(i);
                    finish();
                }
                else if (Email_session!=null && !Email_session.equalsIgnoreCase("") && Email_session.equalsIgnoreCase("1"))
                {
                    Intent i = new Intent(Splash_Activity.this, Registration_Activity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Intent i = new Intent(Splash_Activity.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }
                disconnectSocket();
            }
        }, SPLASH_TIME_OUT);
    }

    public void disconnectSocket()
    {
        Log.e(TAG,"disconnectSocket()");
        try
        {
            if (Comman.socket != null && Comman.socket.connected())
            {
                Comman.socket.disconnect();
                Comman.socket = null;
                SocketSingleObject.instance = null;
            }
            else
            {
                Log.e(TAG,"Socket already disconnected.");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"Socket connect exception = "+e.getMessage());
        }
    }


    private void OpenDialogForNotice(final String dialogType, String message)
    {
        final Dialog dialogNotice = new Dialog(activity, R.style.PauseDialog);
        dialogNotice.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNotice.setCancelable(false);
        dialogNotice.setContentView(R.layout.dialog_custom);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogNotice.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogNotice.getWindow().setAttributes(lp);

        dialogNotice.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogNotice.getWindow().setAttributes(lp);

        final TextView  tv_DialogMessage, tv_DetailUpdate;
        final ImageView iv_CDialogClose;
        final LinearLayout dialog_ok_layout;

        tv_DialogMessage = (TextView) dialogNotice.findViewById(R.id.tv_DialogMessage);
        tv_DetailUpdate = (TextView) dialogNotice.findViewById(R.id.tv_DetailUpdate);
        iv_CDialogClose = (ImageView) dialogNotice.findViewById(R.id.iv_CDialogClose);
        dialog_ok_layout = (LinearLayout) dialogNotice.findViewById(R.id.dialog_ok_layout);


        if (dialogType!=null && !dialogType.equalsIgnoreCase(""))
        {
            if (dialogType.equalsIgnoreCase(COMP_UPDATE))
            {
                tv_DialogMessage.setText(message);
                tv_DetailUpdate.setText(getString(R.string.update));
            }
            else if (dialogType.equalsIgnoreCase(OPTIONAL_UPDATE))
            {
                tv_DialogMessage.setText(message);
                tv_DetailUpdate.setText(getString(R.string.update));
            }
            else if (dialogType.equalsIgnoreCase(UNDER_MAINTENANCE))
            {
                tv_DialogMessage.setText(message);
                tv_DetailUpdate.setText(getString(R.string.ok));
            }
        }
        else
        {
            tv_DialogMessage.setText(getResources().getString(R.string.something_is_wrong));
            tv_DetailUpdate.setText(getString(R.string.ok));
        }

        tv_DetailUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNotice.dismiss();
                if (!dialogType.equalsIgnoreCase(UNDER_MAINTENANCE))
                {
                    OpenPlayStoreLink();
                }
                else
                {
                    finish();
                }
            }
        });

        dialog_ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNotice.dismiss();
                if (!dialogType.equalsIgnoreCase(UNDER_MAINTENANCE))
                {
                    OpenPlayStoreLink();
                }
                else
                {
                    finish();
                }
            }
        });

        iv_CDialogClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (dialogType.equalsIgnoreCase(OPTIONAL_UPDATE))
                {
                    dialogNotice.dismiss();
                    init();
                }
            }
        });
        dialogNotice.show();
    }

    private void OpenPlayStoreLink()
    {
        final String appPackageName = activity.getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }
}
