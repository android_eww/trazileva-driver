package com.connectkargo.driver.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Been.openBid.Datum;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.listner.BidStatus;
import com.connectkargo.driver.singleton.AcceptRejectBid;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class BidDetailActivity extends BaseActivity implements View.OnClickListener {

    LinearLayout ll_back,ll_toolbar_RightImage,ll_requestLayout,ll_boxType,ll_Notes;
    TextView tv_toolbarTitle,tv_Notes,tvBidID, tvAccept, tvReject, tvPickup, tvDropoff;
    EditText et_note, etBudget, etShipperName, etDateTime, etNote, etVehicleType;
    ImageView iv_toolbar_RightImage, ivParcelImg, ivVehicleImg;
    //ActivityBidDetailBinding binding;
    BidDetailActivity activity;

    String from="";
    Datum openBid;
    com.connectkargo.driver.Been.myBid.Datum myBid;

    ErrorDialogClass errorDialogClass;
    private DialogClass dialogClass;
    private AQuery aQuery;

    BidStatus bidStatus;
    private String bidId = "", passengerId="", noteFromPassenger="", passengerName="", status;
    private TextView tv_status;
    private ImageView iv_status;
    private LinearLayout ll_Status, mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid_detail);

        activity = BidDetailActivity.this;
        init();

    }

    private void init()
    {
        errorDialogClass = new ErrorDialogClass(this);
        aQuery = new AQuery(this);
        dialogClass = new DialogClass(this, 1);

        ll_back = findViewById(R.id.ll_back);
        tv_Notes = findViewById(R.id.tv_Notes);
        ll_Notes = findViewById(R.id.ll_Notes);
        et_note = findViewById(R.id.et_note);
        ll_back.setOnClickListener(this);
        tv_toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        //tv_toolbarTitle.setText(getString(R.string.bid_details));
        ll_toolbar_RightImage = findViewById(R.id.ll_toolbar_RightImage);
        iv_toolbar_RightImage = findViewById(R.id.iv_toolbar_RightImage);
        iv_toolbar_RightImage.setImageResource(R.drawable.ic_chat_ioc);
        iv_toolbar_RightImage.setOnClickListener(this);
        ll_toolbar_RightImage.setOnClickListener(this);
        ll_requestLayout = findViewById(R.id.ll_requestLayout);
        ll_boxType = findViewById(R.id.ll_boxType);
        tvBidID = findViewById(R.id.tvBidID);
        tv_status = findViewById(R.id.tv_status);
        iv_status = findViewById(R.id.iv_status);
        ll_Status = findViewById(R.id.ll_Status);

        tvAccept = findViewById(R.id.tv_accept);
        tvReject = findViewById(R.id.tv_reject);
        etBudget = findViewById(R.id.et_budget);
        mainLayout = findViewById(R.id.main_layout);
        tvPickup = findViewById(R.id.tv_pickup);
        tvDropoff = findViewById(R.id.tv_dropoff);
        etShipperName = findViewById(R.id.et_shipperName);
        etDateTime = findViewById(R.id.et_dateTime);
        etNote = findViewById(R.id.et_note);
        ivParcelImg = findViewById(R.id.iv_parcelImg);
        ivVehicleImg = findViewById(R.id.iv_vehicleImg);
        etVehicleType = findViewById(R.id.et_vehicleType);

        from = getIntent().getStringExtra("from");
        if(from.equalsIgnoreCase("mybid"))
        {
            myBid = (com.connectkargo.driver.Been.myBid.Datum) getIntent().getSerializableExtra("bidId");
            if (myBid!=null)
            {
                bidId = myBid.getBidId();
                passengerId = myBid.getPassengerId();
                noteFromPassenger = myBid.getNotes();
                passengerName = myBid.getShipperName();
                status = myBid.getStatus();
                setValue(myBid);
            }
        }
        else
        {
            openBid = (Datum) getIntent().getSerializableExtra("bidId");
            if (openBid!=null)
            {
                bidId = openBid.getId();
                passengerId = openBid.getPassengerId();
                noteFromPassenger = openBid.getNotes();
                passengerName = openBid.getShipperName();
                status = openBid.getStatus();
                setValue(openBid);
            }
        }

        tvBidID.setText("#"+bidId);

        if (noteFromPassenger!=null && !noteFromPassenger.trim().equalsIgnoreCase(""))
        {
            ll_Notes.setVisibility(View.VISIBLE);
            tv_Notes.setText(noteFromPassenger);
        }
        else
        {
            ll_Notes.setVisibility(View.GONE);
        }

        if (status!=null && status.equalsIgnoreCase("3"))
        {
            ll_Status.setVisibility(View.VISIBLE);
            tv_status.setText(getResources().getString(R.string.completed));
            iv_status.setImageResource(R.drawable.ic_trip_completed);
        }
        else
        {
            ll_Status.setVisibility(View.GONE);
        }

        if(from.equalsIgnoreCase("mybid"))
        {
            if (status!=null && status.equalsIgnoreCase("3"))
            {
                ll_toolbar_RightImage.setVisibility(View.GONE);
            }
            else
            {
                ll_toolbar_RightImage.setVisibility(View.VISIBLE);
            }
            ll_requestLayout.setVisibility(View.GONE);
            iv_toolbar_RightImage.setVisibility(View.VISIBLE);
        }
        else
        {
            ll_toolbar_RightImage.setVisibility(View.GONE);
            iv_toolbar_RightImage.setVisibility(View.GONE);
        }
        //callBidDetailApi();

        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAcceptApi();
            }
        });

        tvReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callRejectApi();
            }
        });
    }

    private void callAcceptApi()
    {
        if(etBudget.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(mainLayout, getResources().getString(R.string.msg_please_enter_budget),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            return;
        }
        else if(Double.parseDouble(etBudget.getText().toString().trim()) >  Double.parseDouble(openBid.getBudget()))
        {
            new SnackbarUtils(mainLayout,  getResources().getString(R.string.msg_please_enter_budget_valid_budget_amt),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            return;
        }
        else if (!Global.isNetworkconn(this))
        {
            errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
            return;
        }

        dialogClass.showDialog();

        String UserId = SessionSave.getUserSession(Comman.USER_ID, this);
        String url = WebServiceAPI.WEB_SERVICE_BID_ACCEPT;//+UserId
        Log.e("TAG", "callAcceptApi url:- " + url);



        HashMap<String,String> param = new HashMap<>();
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_BIDID,openBid.getId());
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_BUDGET, etBudget.getText().toString().trim());
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_DRIVERID,UserId);
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_PASSENGERID,openBid.getPassengerId());
        if(!et_note.getText().toString().trim().equalsIgnoreCase(""))
        {param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_NOTES,et_note.getText().toString().trim());}
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_STATUS,"1");

        Log.e("TAG", "callAcceptApi param:- " + param);

        aQuery.ajax(url, param, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {
                int responseCode = status.getCode();

                dialogClass.hideDialog();

                Log.e("TAG", "callAcceptApi responseCode:- " + responseCode);
                Log.e("TAG", "callAcceptApi Response:- " + json);
                if(responseCode == 200)
                {
                    try {
                        JSONObject jsonObject = new JSONObject(json.toString());
                        if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                        {
                            String message = "";

                            if (Locale.getDefault().getDisplayLanguage().trim().equalsIgnoreCase("English"))
                            {
                                message = json.getString("message");
                            }
                            else
                            {
                                message = json.getString("french_message");
                            }
                            errorDialogClass.showDialog(message, "", new ErrorDialogClass.SetOnClick() {
                                @Override
                                public void setOnClickListner() {
                                    onBackPressed();
                                }
                            });
                            ll_requestLayout.setVisibility(View.GONE);

                            bidStatus = AcceptRejectBid.getCallBack();
                            bidStatus.acceptBid(true);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }

            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void callRejectApi()
    {
        if (!Global.isNetworkconn(this))
        {
            errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
            return;
        }

        dialogClass.showDialog();

        String UserId = SessionSave.getUserSession(Comman.USER_ID, this);
        String url = WebServiceAPI.WEB_SERVICE_BID_ACCEPT;
        Log.e("TAG", "callRejectApi url:- " + url);

        HashMap<String,String> param = new HashMap<>();
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_BIDID,openBid.getId());
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_BUDGET,openBid.getBudget());
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_DRIVERID,UserId);
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_PASSENGERID,openBid.getPassengerId());
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_STATUS,"0");
        Log.e("TAG", "callRejectApi param :- " + param);

        aQuery.ajax(url, param, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {
                int responseCode = status.getCode();

                dialogClass.hideDialog();

                Log.e("TAG", "callRejectApi responseCode:- " + responseCode);
                Log.e("TAG", "callRejectApi Response:- " + json);
                if(responseCode == 200)
                {

                    try {
                        JSONObject jsonObject = new JSONObject(json.toString());
                        if(jsonObject.has("status") && json.getBoolean("status"))
                        {
                            String message = "";

                            if (Locale.getDefault().getDisplayLanguage().trim().equalsIgnoreCase("English"))
                            {
                                message = json.getString("message");
                            }
                            else
                            {
                                message = json.getString("french_message");
                            }
                            errorDialogClass.showDialog(message, "", new ErrorDialogClass.SetOnClick() {
                                @Override
                                public void setOnClickListner() {
                                    onBackPressed();
                                }
                            });
                            ll_requestLayout.setVisibility(View.GONE);

                            bidStatus = AcceptRejectBid.getCallBack();
                            bidStatus.acceptBid(false);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }

            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void setValue(com.connectkargo.driver.Been.myBid.Datum myBid)
    {
        etShipperName.setText(myBid.getShipperName());
        tvPickup.setText(myBid.getPickupLocation());
        tvDropoff.setText(myBid.getDropoffLocation());
        etBudget.setText(getResources().getString(R.string.currency)+" "+commaFormat(myBid.getDriverBudget()));

        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
            Date date = inputFormat.parse(myBid.getPickupDateTime());
            etDateTime.setText(outputFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(myBid.getDriverNotes()!=null && !myBid.getDriverNotes().equalsIgnoreCase(""))
        {
            etNote.setText(myBid.getDriverNotes());
        }

        if(myBid.getImage()!=null && !myBid.getImage().equalsIgnoreCase(""))
        {
            Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE+myBid.getImage()).into(ivParcelImg);
            ll_boxType.setVisibility(View.VISIBLE);
        }
        Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE+myBid.getModelImage()).into(ivVehicleImg);
        etVehicleType.setText(myBid.getName());

        etBudget.setFocusable(false);
        etBudget.setClickable(false);

        et_note.setFocusable(false);
        et_note.setClickable(false);
    }

    private void setValue(Datum openBid)
    {
        etShipperName.setText(openBid.getShipperName());
        tvPickup.setText(openBid.getPickupLocation());
        tvDropoff.setText(openBid.getDropoffLocation());
        etBudget.setHint(
                getResources().getString(R.string.lbl_maximum)+
                        " "+
                        getResources().getString(R.string.lbl_budget)+
                        " "+
                        getResources().getString(R.string.currency)+
                        commaFormat(openBid.getBudget())
        );

        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
            Date date = inputFormat.parse(openBid.getPickupDateTime());
            etDateTime.setText(outputFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(openBid.getImage()!=null && !openBid.getImage().equalsIgnoreCase(""))
        {
            Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE+openBid.getImage()).into(ivParcelImg);
            ll_boxType.setVisibility(View.VISIBLE);
        }
        Picasso.get().load(WebServiceAPI.BASE_URL_IMAGE+openBid.getModelImage()).into(ivVehicleImg);
        etVehicleType.setText(openBid.getName());
    }

    @Override
    public void onClick(View v)
    {
        if(v == ll_back)
        {
            onBackPressed();
        }
        else if(v == iv_toolbar_RightImage)
        {
            Intent intent = new Intent(BidDetailActivity.this, ChatDetailActivity.class);
            intent.putExtra("bidId",bidId);
            intent.putExtra("passengerId",passengerId);
            intent.putExtra("passengerName",passengerName);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
        else if(v == ll_toolbar_RightImage)
        {
            Intent intent = new Intent(BidDetailActivity.this, ChatDetailActivity.class);
            intent.putExtra("bidId",bidId);
            intent.putExtra("passengerId",passengerId);
            intent.putExtra("passengerName",passengerName);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    public static String commaFormat(String x) {
        if(x != null && !x.equalsIgnoreCase("") && x.length() > 3){
            String c =  new DecimalFormat("#,###,###.00").format(Double.parseDouble(x));
            c =  c.replace(",","/");
            c =  c.replace(".",",");
            return c.replace("/",".");
            //return c;
        }else {
            return x;
        }
    }

}