package com.connectkargo.driver.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet_Balance_TransferToBank_Activity extends BaseActivity implements View.OnClickListener
{
    public static Wallet_Balance_TransferToBank_Activity activity;

    LinearLayout ll_back, main_layout, ll_Sos;
    TextView tv_toolbarTitle, tv_currentBalance, tv_TransferToBank, tv_accountHolder_Name, tv_BankName, tv_BankAccountNo, tv_Bsb;
    EditText et_amountEnter;

    String walletBalance = "",ClickcardId="", holderName="", abn="", bankName="", bankAcBo="", bsb="";

    Intent intent;
    public static int flagResumeUse=0;

    private AQuery aQuery;
    DialogClass dialogClass;
    private Dialog dialog;

    private Animation animation;
    private Animation.AnimationListener animationListener;
    ErrorDialogClass errorDialogClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance_withdraw);

        activity = Wallet_Balance_TransferToBank_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        flagResumeUse=0;
        ClickcardId="";

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);

        tv_toolbarTitle = (TextView)findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(getResources().getText(R.string.transfer_to_bank));

        tv_currentBalance = (TextView) findViewById(R.id.tv_currentBalance);
        tv_TransferToBank = (TextView) findViewById(R.id.tv_TransferToBank);

        et_amountEnter = (EditText) findViewById(R.id.et_amountEnter);
        tv_accountHolder_Name = (TextView) findViewById(R.id.tv_accountHolder_Name);
        tv_BankName = (TextView) findViewById(R.id.tv_BankName);
        tv_BankAccountNo = (TextView) findViewById(R.id.tv_BankAccountNo);
        tv_Bsb = (TextView) findViewById(R.id.tv_Bsb);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        ll_Sos.setOnClickListener(this);

        ll_back.setOnClickListener(this);
        tv_TransferToBank.setOnClickListener(this);

        walletBalance = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE, activity);
        if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
        {
            tv_currentBalance.setText(activity.getResources().getString(R.string.currency) +walletBalance);

            if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
            {
                String mm = activity.getResources().getString(R.string.currency) +String.format("%.2f",Double.parseDouble(walletBalance));
                tv_currentBalance.setText(mm);
            }
            else
            {
                tv_currentBalance.setVisibility(View.GONE);
            }
        }
        else
        {
            tv_currentBalance.setText("--");
        }

        et_amountEnter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if(charSequence.length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_amountEnter.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holderName = SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, activity);
        abn = SessionSave.getUserSession(Comman.USER_ABN, activity);
        bankName = SessionSave.getUserSession(Comman.USER_BANK_NAME, activity);
        bankAcBo = SessionSave.getUserSession(Comman.USER_BANK_AC_NO, activity);
        bsb = SessionSave.getUserSession(Comman.USER_BSB, activity);

        if (holderName!=null && !holderName.equalsIgnoreCase(""))
        {
            tv_accountHolder_Name.setText(holderName);
        }
        else
        {
            tv_accountHolder_Name.setText("");
        }

        if (bankName!=null && !bankName.equalsIgnoreCase(""))
        {
            tv_BankName.setText(bankName);
        }
        else
        {
            tv_BankName.setText("");
        }

        if (bankAcBo!=null && !bankAcBo.equalsIgnoreCase(""))
        {
            tv_BankAccountNo.setText(bankAcBo);
        }
        else
        {
            tv_BankAccountNo.setText("");
        }

        if (bsb!=null && !bsb.equalsIgnoreCase(""))
        {
            tv_Bsb.setText(bsb);
        }
        else
        {
            tv_Bsb.setText("");
        }

        animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                checkData();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.tv_selectCard:
                intent = new Intent(activity, Wallet_Cards_Activity.class);
                intent.putExtra("from","TransferToBank");
                startActivity(intent);
                break;

            case R.id.tv_TransferToBank:

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                tv_TransferToBank.startAnimation(animation);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void checkData()
    {
        if (TextUtils.isEmpty(et_amountEnter.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_amount),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        {
            String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (driverId!=null && !driverId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    callTransferToBank(driverId);
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
            }
        }
    }

    private void callTransferToBank(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        String url = WebServiceAPI.WEB_SERVICE_TRANSFERT_TO_BANK;

        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_DRIVER_ID, driverId);
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_AMOUNT, et_amountEnter.getText().toString());
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_HOLDER_NAME, holderName);
//        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_ABN, abn );
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_BANK_NAME, bankName);
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_BSB, bsb);
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_ACCOUNT_NO, bankAcBo);

        Log.e("url", "callTransferToBank = " + url);
        Log.e("param", "callTransferToBank = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "callTransferToBank = " + responseCode);
                    Log.e("Response", "callTransferToBank = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");

                                if (json.has("wallet_balance"))
                                {
                                    String walletBalance = json.getString("wallet_balance");
                                    if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
                                    {
                                        Wallet_Balance_Activity.transferToBank = true;

                                        SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, walletBalance, activity);
                                        tv_currentBalance.setText(activity.getResources().getString(R.string.currency) + walletBalance);
                                    }
                                }
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    showPopup(json.getString("message"),"Success Message");
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        } else {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    } else {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e("callTransferToBank", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
        {
            Intent intent = new Intent(activity,Create_Passcode_Activity.class);
            startActivity(intent);

            if (Wallet_Balance_Activity.activity!=null)
            {
                Wallet_Balance_Activity.activity.finish();
            }
            if (Wallet_Balance_TopUp_Activity.activity!=null)
            {
                Wallet_Balance_TopUp_Activity.activity.finish();
            }
            if (Wallet_Cards_Activity.activity!=null)
            {
                Wallet_Cards_Activity.activity.finish();
            }
            if (Wallet_Transfer_Activity.activity!=null)
            {
                Wallet_Transfer_Activity.activity.finish();
            }
            if (Wallet_Transfer_History_Activity.activity!=null)
            {
                Wallet_Transfer_History_Activity.activity.finish();
            }
            finish();
        }
    }

    private void showPopup(String message,String title)
    {

        dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_information);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        TextView tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        tv_Title.setText(title);

        ll_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
//                        Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
//                        finish();
                        onBackPressed();
                    }
                },1000);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
//                        Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
//                        finish();
                        onBackPressed();
                    }
                },1000);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
//                        Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
//                        finish();
                        onBackPressed();
                    }
                },1000);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();

    }
}