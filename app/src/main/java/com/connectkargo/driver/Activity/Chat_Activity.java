package com.connectkargo.driver.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.connectkargo.driver.Adapter.BidChatAdapter;
import com.connectkargo.driver.Adapter.ChatAdapter;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Been.ChatBeen;
import com.connectkargo.driver.Been.ChatList_Been;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.Constants;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Chat_Activity extends BaseActivity implements View.OnClickListener {

    public String TAG = "Chat_Activity";
    public static Chat_Activity activity;
    public LinearLayout main_layout, ll_back, ll_Sos;
    public TextView tv_toolbarTitle;

    public ImageView iv_SendMessage;
    public EditText etChat;

    private RecyclerView recyclerView;
    public List<ChatList_Been> chatListBeens = new ArrayList<>();
    public ChatAdapter chatAdapter;

    public DialogClass dialogClass;
    public ErrorDialogClass errorDialogClass;
    public AQuery aQuery;

    public String BookingId = "", ReceiverId = "", BookingType = "", UserName = "", UserImage = "",receiverName="";
    public static boolean Chat_IsVisible = false;
    public Intent intent;
    private ChatReceiver chatReceiver;
    private String BidId = "0";
    private String passangerId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        activity = Chat_Activity.this;
        dialogClass = new DialogClass(activity,0);
        errorDialogClass = new ErrorDialogClass(activity);
        aQuery = new AQuery(activity);
        Chat_IsVisible = true;
        BookingId = "";
        ReceiverId = "";
        BookingType = "";
        UserName = "";
        UserImage = "";

        intent = getIntent();

        if (intent != null)
        {
            if (intent.hasExtra(Constants.INTENT_USER_NAME))
            {
                UserName = intent.getStringExtra(Constants.INTENT_USER_NAME);
            }
            if (intent.hasExtra(Constants.INTENT_BOOKING_ID))
            {
                BookingId = intent.getStringExtra(Constants.INTENT_BOOKING_ID);
            }
            if (intent.hasExtra(Constants.INTENT_RECEIVER_ID))
            {
                ReceiverId = intent.getStringExtra(Constants.INTENT_RECEIVER_ID);
            }
            if (intent.hasExtra(Constants.INTENT_BOOKING_TYPE))
            {
                BookingType = intent.getStringExtra(Constants.INTENT_BOOKING_TYPE);
            }

            if (intent.hasExtra(Constants.INTENT_RECEIVER_NAME))
            {
                receiverName = intent.getStringExtra(Constants.INTENT_RECEIVER_NAME);
            }
        }

        Log.e(TAG,"BookingId:- " + BookingId);
        Log.e(TAG,"BookingType:- " + BookingType);
        Log.e(TAG,"ReceiverId:- " + ReceiverId);
        Log.e(TAG,"UserName:- " + UserName);

        registerReceiver();
        init();
    }

    private void registerReceiver()
    {
        chatReceiver = new ChatReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.CHAT");
        registerReceiver(chatReceiver, filter);
    }

    private void init()
    {
        Log.e(TAG,"init()");

        BidId = SessionSave.getUserSession(Comman.PASSENGER_BIDID, activity);
        passangerId = SessionSave.getUserSession(Comman.PASSENGER_ID, activity);

        main_layout = findViewById(R.id.main_layout);
        ll_back = findViewById(R.id.ll_back);
        tv_toolbarTitle = findViewById(R.id.tv_toolbarTitle);

        if(!receiverName.equalsIgnoreCase("")){
            tv_toolbarTitle.setText(receiverName);
        }else {
            tv_toolbarTitle.setText(getResources().getText(R.string.activity_chat));
        }

        iv_SendMessage = findViewById(R.id.iv_SendMessage);
        etChat = findViewById(R.id.etChat);
        recyclerView = findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        chatAdapter = new ChatAdapter(activity, chatListBeens);
        recyclerView.setAdapter(chatAdapter);

        ll_back.setOnClickListener(this);
        iv_SendMessage.setOnClickListener(this);

        if (Global.isNetworkconn(activity))
        {
            if(BidId != null && !BidId.equalsIgnoreCase("") && !BidId.equalsIgnoreCase("0")) {
                callApi();
            } else {
                oldChatHistory(true);
            }
        }
        else
        {
            errorDialogClass.showDialog(activity.getResources().getString(R.string.not_connected_to_internet),
                    activity.getResources().getString(R.string.internet_error_message));
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_SendMessage:
                if (Global.isNetworkconn(activity))
                {
                    if (!TextUtils.isEmpty(etChat.getText().toString().trim()))
                    {
                        hideKeyboard();
                        iv_SendMessage.setEnabled(false);
                        callChatApi();
                    }
                }
                else
                {
                    etChat.setText("");
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet),
                            getString(R.string.internet_error_message));
                }
                break;
        }
    }

    private void oldChatHistory(boolean isShow)
    {
        if(isShow) {
            chatListBeens.clear();
            dialogClass.showDialog();
        }

        Log.e(TAG,"oldChatHistory()");

        String url = WebServiceAPI.WEB_SERVICE_CHAT_LIST + BookingId + "/" + BookingType;
        Log.e(TAG, "oldChatHistory URL = " + url);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {
                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "oldChatHistory responseCode= " + responseCode);
                        Log.e(TAG, "oldChatHistory Response= " + json);

                    if (json != null)
                    {
                        if (json.has("status") &&
                                json.getBoolean("status"))
                        {
                            Log.e(TAG, "true");
                            dialogClass.hideDialog();

                            if (json.has("chat_history"))
                            {
                                JSONArray messageArr = json.getJSONArray("chat_history");
                                if (messageArr != null &&
                                        messageArr.length() > 0)
                                {
                                    for (int i = 0; i < messageArr.length(); i++)
                                    {
                                        JSONObject messageObj = messageArr.getJSONObject(i);
                                        if (messageObj != null)
                                        {
                                            String Id = "", BookingId = "", SenderType = "", SenderId = "", ReceiverType = "",
                                                    ReceiverId = "", Message = "", CreatedDate = "", BookingType = "";

                                            if (messageObj.has("Id"))
                                            {
                                                Id = messageObj.getString("Id");
                                            }
                                            if (messageObj.has("BookingId"))
                                            {
                                                BookingId = messageObj.getString("BookingId");
                                            }
                                            if (messageObj.has("SenderType"))
                                            {
                                                SenderType = messageObj.getString("SenderType");
                                            }
                                            if (messageObj.has("SenderId"))
                                            {
                                                SenderId = messageObj.getString("SenderId");
                                            }
                                            if (messageObj.has("ReceiverType"))
                                            {
                                                ReceiverType = messageObj.getString("ReceiverType");
                                            }
                                            if (messageObj.has("ReceiverId"))
                                            {
                                                ReceiverId = messageObj.getString("ReceiverId");
                                            }
                                            if (messageObj.has("Message"))
                                            {
                                                Message = messageObj.getString("Message");
                                            }
                                            if (messageObj.has("CreatedDate"))
                                            {
                                                CreatedDate = messageObj.getString("CreatedDate");
                                            }
                                            if (messageObj.has("BookingType"))
                                            {
                                                BookingType = messageObj.getString("BookingType");
                                            }

                                            chatListBeens.add(new ChatList_Been(Id, BookingId, SenderType, SenderId, ReceiverType,
                                                    ReceiverId, Message, CreatedDate, BookingType));
                                        }
                                    }
                                    chatAdapter.notifyDataSetChanged();
                                    recyclerView.smoothScrollToPosition(chatListBeens.size() - 1);
                                    dialogClass.hideDialog();

                                    Log.e(TAG, "oldChatHistory()  chatListBeens.size():- " + chatListBeens.size());
                                }
                            }
                            else
                            {
                                Log.e(TAG, "oldChatHistory()  mesg blank");
                                dialogClass.hideDialog();
                            }
                        }
                        else
                        {
                            Log.e(TAG, "oldChatHistory()  no status found");
                            dialogClass.hideDialog();

                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"),
                                        getResources().getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e(TAG, "oldChatHistory()  json null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                                getResources().getString(R.string.error_message));

                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "oldChatHistory() Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getResources().getString(R.string.something_is_wrong),
                            getResources().getString(R.string.error_message));
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void callChatApi()
    {
        Log.e(TAG,"callChatApi()");

        String url = WebServiceAPI.WEB_SERVICE_CHAT;

        HashMap<String,String> param = new HashMap<>();
        param.put(WebServiceAPI.WEB_SERVICE_CHAT_PARAM_BOOKING_ID, BookingId);
        param.put(WebServiceAPI.WEB_SERVICE_CHAT_PARAM_BOOKING_TYPE, BookingType);
        param.put(WebServiceAPI.WEB_SERVICE_CHAT_PARAM_SENDER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
//        param.put(WebServiceAPI.WEB_PARAM_RECIVER_TYPE, "customer");
        param.put(WebServiceAPI.WEB_SERVICE_CHAT_PARAM_RECIEVER_ID, ReceiverId);
        param.put(WebServiceAPI.WEB_SERVICE_CHAT_PARAM_MESSAGE, etChat.getText().toString().trim());

        Log.e(TAG,"callSubmitApi() url:- " + url);
        Log.e(TAG,"callSubmitApi() param:- " + param);

        aQuery.ajax(url.trim(), param, JSONObject.class, new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {
                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "callSubmitApi() responseCode= " + responseCode);
                    Log.e(TAG, "callSubmitApi() Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status") &&
                                json.getBoolean("status"))
                        {
                            if (json.has("last_message"))
                            {
                                JSONObject jsonObject = json.getJSONObject("last_message");

                                if (jsonObject != null)
                                {
                                    String id = "", booking_id = "", sender_type = "", sender_id = "", receiver_type = "",
                                            receiver_id = "", message = "", created_at = "";

                                    if (jsonObject.has("Id"))
                                    {
                                        id = jsonObject.getString("Id");
                                    }

                                    if (jsonObject.has("BookingId"))
                                    {
                                        booking_id = jsonObject.getString("BookingId");
                                    }

                                    if (jsonObject.has("SenderType"))
                                    {
                                        sender_type = jsonObject.getString("SenderType");
                                    }

                                    if (jsonObject.has("SenderId"))
                                    {
                                        sender_id = jsonObject.getString("SenderId");
                                    }

                                    if (jsonObject.has("ReceiverType"))
                                    {
                                        receiver_type = jsonObject.getString("ReceiverType");
                                    }

                                    if (jsonObject.has("ReceiverId"))
                                    {
                                        receiver_id = jsonObject.getString("ReceiverId");
                                    }

                                    if (jsonObject.has("Message"))
                                    {
                                        message = jsonObject.getString("Message");
                                    }

                                    if (jsonObject.has("CreatedDate"))
                                    {
                                        created_at = jsonObject.getString("CreatedDate");
                                    }

                                    etChat.setText("");
                                    chatListBeens.add(new ChatList_Been(id, BookingId, sender_type, sender_id, receiver_type, ReceiverId, message, created_at, BookingType));
                                    chatAdapter.notifyDataSetChanged();
                                    recyclerView.smoothScrollToPosition(chatListBeens.size() - 1);
                                    Log.e(TAG,"callSubmitApi() jsonObjectChat not null");
                                }
                                else
                                {
                                    Log.e(TAG,"callSubmitApi() jsonObjectChat null");
                                }
                            }

                            Log.e(TAG,"callSubmitApi() status true");
                        }
                        else
                        {
                            Log.e(TAG,"callSubmitApi() status false");
                        }
                    }
                    else
                    {
                        Log.e(TAG,"callSubmitApi() json null");
                        Toast.makeText(activity,getString(R.string.please_try_agian_later),Toast.LENGTH_SHORT).show();
                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG,"callSubmitApi() Exception "+e.toString());
                    Toast.makeText(activity,getString(R.string.please_try_agian_later),Toast.LENGTH_SHORT).show();
                }
                finally
                {
                    Log.e(TAG,"callSubmitApi() finally");
                    dialogClass.hideDialog();
                    iv_SendMessage.setEnabled(true);
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    public void hideKeyboard()
    {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null)
        {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume()
    {
        Log.e(TAG,"onResume()");
        super.onResume();
        CabRideDriverApplication.setCurrentActivity(activity);
        Chat_IsVisible = true;
    }

    @Override
    protected void onPause()
    {
        Log.e(TAG,"onPause()");
        super.onPause();
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        hideKeyboard();
        overridePendingTransition(R.anim.left_in,R.anim.right_out);
    }

    @Override
    protected void onDestroy()
    {
        Log.e(TAG,"onDestroy()");
        super.onDestroy();
        Chat_IsVisible = false;

        if(chatReceiver != null){
            unregisterReceiver(chatReceiver);
        }
    }

    class ChatReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent != null)
            {
                String obj = intent.getStringExtra("CHAT");
                Log.e("TAG","obj = "+obj);
                if(obj != null && !obj.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(obj);
                        String id = "", booking_id = "", sender_type = "", sender_id = "", receiver_type = "",
                                receiver_id = "", message = "", created_at = "";

                        if (jsonObject.has("Id"))
                        {
                            id = jsonObject.getString("Id");
                        }

                        if (jsonObject.has("BookingId"))
                        {
                            booking_id = jsonObject.getString("BookingId");
                        }

                        if (jsonObject.has("SenderType"))
                        {
                            sender_type = jsonObject.getString("SenderType");
                        }

                        if (jsonObject.has("SenderId"))
                        {
                            sender_id = jsonObject.getString("SenderId");
                        }

                        if (jsonObject.has("ReceiverType"))
                        {
                            receiver_type = jsonObject.getString("ReceiverType");
                        }

                        if (jsonObject.has("ReceiverId"))
                        {
                            receiver_id = jsonObject.getString("ReceiverId");
                        }

                        if (jsonObject.has("Message"))
                        {
                            message = jsonObject.getString("Message");
                        }

                        if (jsonObject.has("CreatedDate"))
                        {
                            created_at = jsonObject.getString("CreatedDate");
                        }

                        chatListBeens.add(new ChatList_Been(id, BookingId, sender_type, sender_id, receiver_type, ReceiverId, message, created_at, BookingType));
                        chatAdapter.notifyDataSetChanged();
                        recyclerView.smoothScrollToPosition(chatListBeens.size() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private void callApi()
    {
        chatListBeens.clear();
        String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        params.put(WebServiceAPI.PARAM_SENDER, "Driver");
        params.put(WebServiceAPI.PARAM_RECEIVER, "Passenger");
        params.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_BIDID, BidId);
        params.put(WebServiceAPI.SEND_MONEY_PARAM_SENDER_ID, userId);
        params.put(WebServiceAPI.PARAM_RECEIVERID, passangerId);

        String url="";
        url = WebServiceAPI.WEB_BID_CHAT_HISTORY;

        Log.e("TAG", "getChatHistory() url:- " + url);
        Log.e("TAG", "getChatHistory() params:- " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();

                    Log.e("TAG", "getChatHistory() responseCode:- " + responseCode);
                    Log.e("TAG", "getChatHistory() Response:- " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String Fullname="",Image="";

                                if (json.has("driver"))
                                {
                                    JSONArray driver = json.getJSONArray("driver");

                                    if (driver!=null && driver.length()>0)
                                    {
                                        JSONObject driverObject = driver.getJSONObject(0);

                                        if (driverObject!=null)
                                        {
                                            if (driverObject.has("Fullname"))
                                            {
                                                Fullname = driverObject.getString("Fullname");
                                            }

                                            if (driverObject.has("Image"))
                                            {
                                                Image = WebServiceAPI.BASE_URL_IMAGE + "" + driverObject.getString("Image");
                                            }
                                        }
                                    }
                                }

                                if (Image!=null && !Image.trim().equals(""))
                                {
                                }

                                String ReceiverName="";
                                if (json.has("ReceiverName"))
                                {
                                    ReceiverName = json.getString("ReceiverName");
                                }
                                //passengerName = ReceiverName;
                                //tv_toolbarTitle.setText(passengerName);
                                if (json.has("data"))
                                {
                                    JSONArray jsonArray = json.getJSONArray("data");

                                    if (jsonArray!=null && jsonArray.length()>0)
                                    {
                                        for (int i=0; i<jsonArray.length(); i++)
                                        {
                                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                                            if (jsonObject!=null)
                                            {
                                                String Message="",Date="",Sender="",Receiver="";

                                                if (jsonObject.has("Message"))
                                                {
                                                    Message = jsonObject.getString("Message");
                                                }

                                                if (jsonObject.has("Date"))
                                                {
                                                    Date = jsonObject.getString("Date");
                                                }

                                                if (jsonObject.has("Sender"))
                                                {
                                                    Sender = jsonObject.getString("Sender");
                                                }

                                                if (jsonObject.has("Receiver"))
                                                {
                                                    Receiver = jsonObject.getString("Receiver");
                                                }

                                                //chatListBeens.add(new ChatBeen(Message,Date,Sender,Receiver));
                                                chatListBeens.add(new ChatList_Been("", BookingId, Sender, "", Receiver,
                                                        ReceiverId, Message, Date, BookingType));
                                                //chatListBeens.add(new ChatBeen(Message,Date,Sender,Receiver));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        chatAdapter = new ChatAdapter(activity, chatListBeens);
                                        recyclerView.setAdapter(chatAdapter);
                                        recyclerView.scrollToPosition(chatListBeens.size()-1);
                                        //scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                                    }
                                    //dialogClass.hideDialog();
                                }
                                //dialogClass.hideDialog();
                            }
                            else
                            {
                                //dialogClass.hideDialog();
                            }
                        }
                        else
                        {
                            //dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        //dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    Log.e("TAG","UpdateAccountDetail() Exception:- " + e.toString());
                    //dialogClass.hideDialog();
                } finally {
                    //dialogClass.hideDialog();
                    oldChatHistory(false);
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }


}
