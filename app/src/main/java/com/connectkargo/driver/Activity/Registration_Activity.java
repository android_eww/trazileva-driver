package com.connectkargo.driver.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Fragment.Register_Attachment_Fragment;
import com.connectkargo.driver.Fragment.Register_Bank_Fragment;
import com.connectkargo.driver.Fragment.Register_Email_Fragment;
import com.connectkargo.driver.Fragment.Register_ProfileDetail_Fragment;
import com.connectkargo.driver.Fragment.Register_VehicleDetail_Fragment;
import com.connectkargo.driver.R;


public class Registration_Activity extends BaseActivity implements View.OnClickListener
{
    public static Registration_Activity activity;

    public static LinearLayout ll_back;
    LinearLayout ll_1, ll_2, ll_3, ll_4, ll_5;
    ImageView iv_1, iv_2, iv_3, iv_4, iv_5;
    View view1_1, view1_2, view2_1, view2_2, view3_1, view3_2, view4_1, view4_2;

    public static String clickedType="";
    public static int ClickTabNumber = 0;
    public static Boolean clickEmail = false;

    FrameLayout frameRegister;
    String Email_session="", Otp_session="", profile_session="", bank_session="", vehicle_session="";

    public static int cameraPermissionLength = 0;
    private Register_ProfileDetail_Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        activity = Registration_Activity.this;

        clickedType="";
        ClickTabNumber = 0;
        clickEmail = false;
        cameraPermissionLength = 0;

        initUI();
    }

    private void initUI()
    {

        frameRegister = findViewById(R.id.frame_Register);
        ll_back = findViewById(R.id.ll_back);

        ll_1 = findViewById(R.id.ll_1);
        ll_2 = findViewById(R.id.ll_2);
        ll_3 = (LinearLayout) findViewById(R.id.ll_3);
        ll_4 = (LinearLayout) findViewById(R.id.ll_4);
        ll_5 = (LinearLayout) findViewById(R.id.ll_5);

        iv_1 = (ImageView) findViewById(R.id.iv_1);
        iv_2 = (ImageView) findViewById(R.id.iv_2);
        iv_3 = (ImageView) findViewById(R.id.iv_3);
        iv_4 = (ImageView) findViewById(R.id.iv_4);
        iv_5 = (ImageView) findViewById(R.id.iv_5);

        view1_1= (View) findViewById(R.id.view1_1);
        view1_2= (View) findViewById(R.id.view1_2);
        view2_1= (View) findViewById(R.id.view2_1);
        view2_2= (View) findViewById(R.id.view2_2);
        view3_1= (View) findViewById(R.id.view3_1);
        view3_2= (View) findViewById(R.id.view3_2);
        view4_1= (View) findViewById(R.id.view4_1);
        view4_2= (View) findViewById(R.id.view4_2);

        ll_1.setOnClickListener(this);
        ll_2.setOnClickListener(this);
        ll_3.setOnClickListener(this);
        ll_4.setOnClickListener(this);
        ll_5.setOnClickListener(this);
        ll_back.setOnClickListener(this);

        Email_session = SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY,activity);
        Otp_session = SessionSave.getUserSession(Comman.REGISTER_OTP_ACTIVITY,activity);
        profile_session = SessionSave.getUserSession(Comman.REGISTER_PROFILE_ACTIVITY,activity);
        bank_session = SessionSave.getUserSession(Comman.REGISTER_BANK_ACTIVITY,activity);
        vehicle_session = SessionSave.getUserSession(Comman.REGISTER_VEHICLE_ACTIVITY,activity);

        if (Email_session!=null && !Email_session.equalsIgnoreCase("") &&
                Otp_session!=null && !Otp_session.equalsIgnoreCase(""))
        {
//            if (profile_session!=null && !profile_session.equalsIgnoreCase("") && profile_session.equalsIgnoreCase("1"))
//            {
//                if (vehicle_session!=null && !vehicle_session.equalsIgnoreCase("") && vehicle_session.equalsIgnoreCase("1"))
//                {
//                    setTabAttachment();
//                }
//                else
//                {
//                    setTabCarInfo1();
//                }
//            }
//            else
//            {
//                setTabAddProfileDetails();
//            }
            setTabAttachment();
        }
        else
        {
            setTabEmail();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

//            case R.id.ll_1:
//                if(ClickTabNumber != 0)
//                {
//                    Email_session = SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY,activity);
//                    if (Email_session!=null && !Email_session.equalsIgnoreCase("") && Email_session.equalsIgnoreCase("1"))
//                    {
//                        clickEmail = true;
//                        setTabEmail();
//                    }
//                }
//                break;
//
//            case R.id.ll_2:
//                if (ClickTabNumber != 1)
//                {
//                    Email_session = SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY,activity);
//                    if (Email_session!=null && !Email_session.equalsIgnoreCase("") && Email_session.equalsIgnoreCase("1"))
//                    {
//                        setTabAddProfileDetails();
//                    }
//                }
//                break;
//
//            case R.id.ll_3:
//                if (ClickTabNumber != 2)
//                {
//                    profile_session = SessionSave.getUserSession(Comman.REGISTER_PROFILE_ACTIVITY,activity);
//                    if (profile_session!=null && !profile_session.equalsIgnoreCase("") && profile_session.equalsIgnoreCase("1"))
//                    {
//                        setTabBank();
//                    }
//                }
//                break;
//
//            case R.id.ll_4:
//                if (ClickTabNumber != 3)
//                {
//                    bank_session = SessionSave.getUserSession(Comman.REGISTER_BANK_ACTIVITY,activity);
//                    if (bank_session!=null && !bank_session.equalsIgnoreCase("") && bank_session.equalsIgnoreCase("1"))
//                    {
//                        setTabCarInfo1();
//                    }
//                }
//                break;
//
//            case R.id.ll_5:
//                if (ClickTabNumber != 4)
//                {
//                    vehicle_session = SessionSave.getUserSession(Comman.REGISTER_VEHICLE_ACTIVITY,activity);
//                    if (vehicle_session!=null && !vehicle_session.equalsIgnoreCase("") && vehicle_session.equalsIgnoreCase("1"))
//                    {
//                        setTabAttachment();
//                    }
//                }
//                break;
//
//            case R.id.tv_signOut:
//                SessionSave.clearUserSession(activity);
//                Intent intent = new Intent(getApplicationContext(), Login_Activity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();
//                break;

        }
    }

    public void setTabEmail()
    {
        ll_back.setVisibility(View.VISIBLE);

        Register_Email_Fragment fragment = new Register_Email_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 0;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_mail_yellow));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_profile_white));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_bank_white));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.truck_unselect/*ic_car_white*/));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_attach_white));

        view1_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view1_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view2_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view2_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view3_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view3_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view4_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view4_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
    }

    public void setTabAddProfileDetails()
    {
        ll_back.setVisibility(View.VISIBLE);

        fragment = new Register_ProfileDetail_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 1;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_mail_yellow));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_profile_yellow));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_bank_white));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.truck_unselect/*ic_car_white*/));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_attach_white));

        view1_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view1_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view2_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view2_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view3_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view3_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view4_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view4_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
    }

    public void setTabBank()
    {
        ll_back.setVisibility(View.VISIBLE);

        Register_Bank_Fragment fragment = new Register_Bank_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 2;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_mail_yellow));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_profile_yellow));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_bank_yellow));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.truck_unselect/*ic_car_white*/));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_attach_white));

        view1_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view1_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view2_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view2_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view3_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view3_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view4_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view4_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
    }

    public void setTabCarInfo1()
    {
        ll_back.setVisibility(View.VISIBLE);

        Register_VehicleDetail_Fragment fragment = new Register_VehicleDetail_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

//        ClickTabNumber = 3;
        ClickTabNumber = 2;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_mail_yellow));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_profile_yellow));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_bank_yellow));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.truck_select/*ic_car_yellow*/));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_attach_white));

        view1_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view1_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view2_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view2_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view3_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view3_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view4_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
        view4_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.lightGray));
    }

    public void setTabAttachment()
    {
        ll_back.setVisibility(View.VISIBLE);

        Register_Attachment_Fragment fragment = new Register_Attachment_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 3;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_mail_yellow));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_profile_yellow));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_bank_yellow));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.truck_select/*ic_car_yellow*/));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_attach_yellow));

        view1_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view1_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view2_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view2_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view3_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view3_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view4_1.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
        view4_2.setBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
    }

    @Override
    public void onBackPressed()
    {

        if (ClickTabNumber == 0)
        {
            super.onBackPressed();

            SessionSave.clearUserSession(activity);
            Intent intent = new Intent(Registration_Activity.activity, Login_Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.left_in, R.anim.right_out);

            if (Registration_Activity.activity!=null)
            {
                Registration_Activity.activity.finish();
            }
        }
//        else if (ClickTabNumber == 1)
//        {
//            SessionSave.saveUserSession(Comman.REGISTER_OTP_ACTIVITY,"",activity);
//            setTabEmail();
//        }
//        else if (ClickTabNumber == 2)
//        {
//            setTabAddProfileDetails();
//            SessionSave.saveUserSession(Comman.REGISTER_PROFILE_ACTIVITY,"",activity);
//        }
//        else if (ClickTabNumber == 3)
//        {
//            setTabBank();
//            SessionSave.saveUserSession(Comman.REGISTER_BANK_ACTIVITY,"",activity);
//        }
        else if (ClickTabNumber == 3)
        {
//            setTabCarInfo1();
//            SessionSave.saveUserSession(Comman.REGISTER_VEHICLE_ACTIVITY,"",activity);
            setTabEmail();
            SessionSave.saveUserSession(Comman.REGISTER_OTP_ACTIVITY,"",activity);
        }

//        SessionSave.clearUserSession(activity);
//        finish();
//        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void LogoutButton_Display(int i)
    {
        /*if (i==1)
        {
            tv_signOut.setVisibility(View.VISIBLE);
            ll_back.setVisibility(View.GONE);
        }
        else
        {
            tv_signOut.setVisibility(View.GONE);
            ll_back.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("TAG","requestCode = "+requestCode);
        switch (requestCode)
        {
            case 5:
                Log.e("TAG","grantResults.length = "+grantResults.length);

                if(cameraPermissionLength == 2)
                {
                    boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if(cameraPermission && readExternalFile)
                    {
                        if (fragment != null)
                        {
                            fragment.ShowPictureDialog();
                        }
                    }
                }
                else if(cameraPermissionLength == 1)
                {
                    int a = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
                    int b = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if(a == PackageManager.PERMISSION_GRANTED && b == PackageManager.PERMISSION_GRANTED)
                    {
                        if (fragment != null)
                        {
                            fragment.ShowPictureDialog();
                        }
                    }
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(activity.getResources().getString(R.string.permission_denied));
                    builder.setMessage(activity.getResources().getString(R.string.please_allow_permission_other_allow_from_setting));
                    builder.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alertDialog1 = builder.create();
                    Button Pbutton = alertDialog1.getButton(DialogInterface.BUTTON_POSITIVE);
                    Button Nbutton = alertDialog1.getButton(DialogInterface.BUTTON_NEUTRAL);

                    if (Pbutton != null)
                    {
                        Pbutton.setTextColor(Registration_Activity.activity.getResources().getColor(R.color.colorBlack));
                    }
                    else
                    {
                        Log.e("Map Fragment","takePermision():- " + "Pbutton == null");
                    }

                    if (Pbutton != null)
                    {
                        Nbutton.setTextColor(Registration_Activity.activity.getResources().getColor(R.color.colorBlack));
                    }
                    else
                    {
                        Log.e("Map Fragment","takePermision():- " + "Nbutton == null");

                    }

                    builder.show();
                }
                break;
        }
    }
}
