package com.connectkargo.driver.Activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Profile_AccountDetail_Activity extends BaseActivity implements View.OnClickListener{

    Profile_AccountDetail_Activity activity;

    LinearLayout main_layout, ll_back, ll_toolbar_RightImage, ll_Sos, ll_save;
    EditText et_acountHolder_name, et_bank_name, et_bsb, et_bankAccount;
    TextView tv_toolbarTitle;
    ImageView iv_toolbar_RightImage;

    DialogClass dialogClass;
    AQuery aQuery;
    String userId;
    ErrorDialogClass errorDialogClass;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_accountdetail);

        activity = Profile_AccountDetail_Activity.this;

        aQuery = new AQuery(activity);
        errorDialogClass = new ErrorDialogClass(activity);

        initUI();
    }

    private void initUI() {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_toolbar_RightImage = (LinearLayout) findViewById(R.id.ll_toolbar_RightImage);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);

        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(activity.getResources().getString(R.string.account));

        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        iv_toolbar_RightImage = (ImageView) findViewById(R.id.iv_toolbar_RightImage);
        iv_toolbar_RightImage.setVisibility(View.GONE);
        iv_toolbar_RightImage.setImageResource(R.drawable.icon_save_white);

        et_acountHolder_name = (EditText) findViewById(R.id.et_acountHolder_name);
        et_bank_name = (EditText) findViewById(R.id.et_bank_name);
        et_bsb = (EditText) findViewById(R.id.et_bsb);
        et_bankAccount = (EditText) findViewById(R.id.et_bankAccount);

        /*et_acountHolder_name.setEnabled(false);
        et_bank_name.setEnabled(false);
        et_bsb.setEnabled(false);
        et_bankAccount.setEnabled(false);
        ll_toolbar_RightImage.setVisibility(View.GONE);*/

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);

        if (SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,activity).equalsIgnoreCase(""))
        {
            et_acountHolder_name.setText(SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_BANK_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_BANK_NAME,activity).equalsIgnoreCase(""))
        {
            et_bank_name.setText(SessionSave.getUserSession(Comman.USER_BANK_NAME,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_BSB,activity)!= null && !SessionSave.getUserSession(Comman.USER_BSB,activity).equalsIgnoreCase(""))
        {
            et_bsb.setText(SessionSave.getUserSession(Comman.USER_BSB,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_BANK_AC_NO,activity)!= null && !SessionSave.getUserSession(Comman.USER_BANK_AC_NO,activity).equalsIgnoreCase(""))
        {
            et_bankAccount.setText(SessionSave.getUserSession(Comman.USER_BANK_AC_NO,activity));
        }


        ll_back.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_save:
                CheckData();
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void CheckData()
    {
        if (TextUtils.isEmpty(et_acountHolder_name.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_company_holder_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_bank_name.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_bsb.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_branch),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_bankAccount.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_no),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (et_bankAccount.getText().toString().trim().length() < 9 )
        {
            new SnackbarUtils(main_layout, activity.getResources().getString(R.string.please_enter_valid_account_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        {
            userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    UpdateAccountDetail(userId);
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
            }
        }
    }

    private void UpdateAccountDetail(String userId)
    {

        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_UPDATE_BANK_INFO ;

        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_DRIVER_ID, userId);
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_ACCOUNT_HOLDER_NAME, et_acountHolder_name.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_BANK_NAME, et_bank_name.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_BANK_AC_NO, et_bankAccount.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_BSB, et_bsb.getText().toString());

        Log.e("url", "UpdateCarDetail = " + url);
        Log.e("param", "UpdateCarDetail = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateCarDetail = " + responseCode);
                    Log.e("Response", "UpdateCarDetail = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String BankHolderName, BankName="", BankAcNo="", BSB="";

                                        if (driverProfile.has("BankHolderName"))
                                        {
                                            BankHolderName = driverProfile.getString("BankHolderName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, activity);
                                        }
                                        if (driverProfile.has("BankName"))
                                        {
                                            BankName = driverProfile.getString("BankName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, activity);
                                        }

                                        if (driverProfile.has("BankAcNo"))
                                        {
                                            BankAcNo = driverProfile.getString("BankAcNo");
                                            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, activity);
                                        }
                                        if (driverProfile.has("BSB"))
                                        {
                                            BSB = driverProfile.getString("BSB");
                                            SessionSave.saveUserSession(Comman.USER_BSB, BSB, activity);
                                        }
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            SuccessDialog(json.getString("message"), getString(R.string.success_message));
//                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.success_message));
                                        }
                                        else
                                        {
                                            SuccessDialog(getString(R.string.account_update_succuessfully), getString(R.string.success_message));
//                                            errorDialogClass.showDialog(getString(R.string.profile_updated_successfully), getString(R.string.success_message));
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                }

                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("UpdateCarDetail", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void SuccessDialog(String message, String title)
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok, tv_Message, tv_Title;
        LinearLayout ll_Ok;
        ImageView iv_close;

        tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);
            }
        });

        if (message!=null && !message.equalsIgnoreCase(""))
        {
            tv_Message.setText(message);
        }
        else
        {
            tv_Message.setVisibility(View.GONE);
        }

        if (title!=null && !title.equalsIgnoreCase(""))
        {
            tv_Title.setText(title);
        }
        else
        {
            tv_Title.setVisibility(View.GONE);
        }


        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }
}