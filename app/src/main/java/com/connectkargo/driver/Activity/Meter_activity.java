package com.connectkargo.driver.Activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.connectkargo.driver.R;


public class Meter_activity extends BaseActivity {

   /* Meter_activity activity;

    LinearLayout ll_back, ll_Sos;
    TextView tv_toolbarTitle, tv_startTripMeter, tv_endTripMeter, tv_start_waitingTime, tv_end_waitingTime, tv_BaseFare, tv_hireFee;
    public static TextView tv_meter_digit, tv_waitinCost, tv_waitingTime;
    CardView cv_meter_digit;

    ErrorDialogClass errorDialogClass;

    Spinner spinner;
    List<String> ModelNameList = new ArrayList<String>();
    List<String> ModelIdList = new ArrayList<String>();
    ArrayAdapter<String> spinnerArrayAdapter;

    String selectedModelId = "";
    DialogClass dialogClass;
    AQuery aQuery;

    String meterActiveFlag;

    private static final int PERMISSION_REQUEST_CODE = 1;

    Calendar calendar;
    SimpleDateFormat dateFormat;*/



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meter);

       /* activity = Meter_activity.this;
        errorDialogClass = new ErrorDialogClass(activity);
        dialogClass = new DialogClass(activity, 1);
        aQuery = new AQuery(activity);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

        selectedModelId = "";

        Init();*/
    }


  /*  private void Init() {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText("Private Meter");

        tv_meter_digit = (TextView) findViewById(R.id.tv_meter_digit);
        tv_startTripMeter = (TextView) findViewById(R.id.tv_startTripMeter);
        tv_endTripMeter = (TextView) findViewById(R.id.tv_endTripMeter);
        tv_start_waitingTime = (TextView) findViewById(R.id.tv_start_waitingTime);
        tv_end_waitingTime = (TextView) findViewById(R.id.tv_end_waitingTime);
        tv_BaseFare = (TextView) findViewById(R.id.tv_BaseFare);
        tv_hireFee = (TextView) findViewById(R.id.tv_hireFee);
        tv_waitingTime = (TextView) findViewById(R.id.tv_waitingTime);
        tv_waitinCost = (TextView) findViewById(R.id.tv_waitinCost);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        cv_meter_digit = (CardView) findViewById(R.id.cv_meter_digit);
        cv_meter_digit.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setEnabled(true);

        ModelNameList.clear();
        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity).equalsIgnoreCase(""))
        {
            ModelNameList = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL_NAME,activity).split("\\s*,\\s*"));
        }
        Log.e("ModelNameList","ModelNameList : "+ModelNameList.size());
        spinnerArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item,ModelNameList);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);

        String spinnerSelectedItemPosition = SessionSave.getMeterData(Comman.METER_SELECTED_CAR_MODEL_POSITION, activity);
        if (spinnerSelectedItemPosition!=null && !spinnerSelectedItemPosition.equalsIgnoreCase(""))
        {
            spinner.setSelection(Integer.parseInt(SessionSave.getMeterData(Comman.METER_SELECTED_CAR_MODEL_POSITION, activity)));
        }
        else
        {
            spinner.setSelection(0);
        }

        ModelIdList.clear();
        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).equalsIgnoreCase(""))
        {
            ModelIdList = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).split("\\s*,\\s*"));
        }
        Log.e("ModelIdList","ModelIdList : "+ModelIdList.size());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedModelId = ModelIdList.get(position);
                OnItemSelect_GetDetail(selectedModelId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity);
        if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
        {
            if (meterActiveFlag.equalsIgnoreCase("1"))
            {
                call_startTrip();
            }
            else if (meterActiveFlag.equalsIgnoreCase("2"))
            {
                call_PauseTrip();
            }
            else if (meterActiveFlag.equalsIgnoreCase("3"))
            {
                call_PauseStopTrip();
            }
        }
        else
        {
            tv_meter_digit.setText("0.0");
            cv_meter_digit.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
            tv_startTripMeter.setVisibility(View.VISIBLE);
            tv_endTripMeter.setVisibility(View.GONE);
            tv_start_waitingTime.setVisibility(View.GONE);
            tv_end_waitingTime.setVisibility(View.GONE);
        }

        String hireCost = SessionSave.getMeterData(Comman.METER_HIRE_COST, activity);
        if (hireCost!=null && !hireCost.equalsIgnoreCase(""))
        {
            tv_hireFee.setText(hireCost);
        }


        String waitingCost = SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity);
        if (waitingCost!=null && !waitingCost.equalsIgnoreCase(""))
        {
            tv_waitinCost.setText(waitingCost);
        }

        tv_waitinCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String abc = tv_meter_digit.getText().toString();
                if (abc!=null && !abc.equalsIgnoreCase(""))
                {
                    double abc1 = Double.parseDouble(abc);
                    abc1 = abc1+0.000;
                    String ttt = String.format( "%.2f", abc1);
                    tv_meter_digit.setText(ttt+"");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tv_meter_digit.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0)
                {
                    String MinKm = "";
                    if (SessionSave.getMeterData(Comman.METER_MIN_KM, activity)!=null && !SessionSave.getMeterData(Comman.METER_MIN_KM, activity).equalsIgnoreCase(""))
                    {
                        MinKm = SessionSave.getMeterData(Comman.METER_MIN_KM, activity);
                    }
                    else
                    {
                        MinKm = 0+"";
                    }
                    if (MinKm!=null && !MinKm.equalsIgnoreCase(""))
                    {
                        if (Double.parseDouble(MinKm)>Double.parseDouble(s.toString()))
                        {
                            double  abc = 0.0;
                            if (SessionSave.getMeterData(Comman.METER_BASE_FARE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BASE_FARE, activity).equalsIgnoreCase(""))
                            {
                                abc = Double.parseDouble(SessionSave.getMeterData(Comman.METER_BASE_FARE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity).equalsIgnoreCase(""))
                            {
                                abc = abc + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity).equalsIgnoreCase(""))
                            {
                                abc = abc + Double.parseDouble(SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity));
                            }
                            String abcd = String.format( "%.2f", abc);
                            SessionSave.saveMeterData(Comman.METER_TOTAL_FARE_CALCULATE, abcd, activity);
                            tv_BaseFare.setText(abcd);
                        }
                        else
                        {
                            double abc = Double.parseDouble(s.toString())- Double.parseDouble(MinKm);
                            double abc1 = 0.0;
                            if (SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc* Double.parseDouble(SessionSave.getMeterData(Comman.METER_BELOW_PER_KM_CHARGE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_BASE_FARE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BASE_FARE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BASE_FARE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity)!=null && !SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_BOOKING_FEE, activity));
                            }
                            if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity).equalsIgnoreCase(""))
                            {
                                abc1 = abc1 + Double.parseDouble(SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity));
                            }
                            String abcd = String.format( "%.2f", abc1);
                            if (abcd!=null && !abcd.equalsIgnoreCase(""))
                            {
                                SessionSave.saveMeterData(Comman.METER_TOTAL_FARE_CALCULATE, abcd, activity);
                            }
                            tv_BaseFare.setText(abcd);
                        }
                    }
                }
            }
        });


        ll_back.setOnClickListener(this);
        tv_startTripMeter.setOnClickListener(this);
        tv_endTripMeter.setOnClickListener(this);
        tv_start_waitingTime.setOnClickListener(this);
        tv_end_waitingTime.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.tv_startTripMeter:
                SessionSave.saveUserSession(Comman.SUM_FOR_LAT_LONG_METER, "", Drawer_Activity.activity);
                Log.e("kkkkkk","tv_startTripMeter : "+ SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, Drawer_Activity.activity));
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                }
                else
                {
                    try {
                        int locationMode = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
                        int LOCATION_MODE_HIGH_ACCURACY = 3;
                        if (locationMode == LOCATION_MODE_HIGH_ACCURACY)
                        {
                            //request location updates
                            if (checkPermission())
                            {
                                String AcceptFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, activity);
                                if (AcceptFlag!=null && !AcceptFlag.equalsIgnoreCase("") && !AcceptFlag.equalsIgnoreCase("1"))
                                {
                                    String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                                    if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && DutyStatus.equalsIgnoreCase("1"))
                                    {
                                        errorDialogClass.showDialog("First, you have to be offline!", getString(R.string.info_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getString(R.string.please_stay_connect), getString(R.string.info_message));
                                       *//* LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver, new IntentFilter(LOACTION_ACTION));
                                        if (AppUtils.hasM() && !(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                                            askPermissions();
                                        } else {
                                            startLocationService();
                                            call_startTrip();
                                        }*//*
                                        call_startTrip();
                                    }
                                }
                                else
                                {
                                    errorDialogClass.showDialog("You are already on Your trip, first finished your trip", getString(R.string.info_message));
                                }
                            }
                            else
                            {
                                requestPermission();
                            }
                        }
                        else
                        {
                            //redirect user to settings page
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                            alertDialog.setTitle("GPS is settings");
                            alertDialog.setMessage("Please set your Location Mode to High accuracy.");
                            alertDialog.setPositiveButton("Settings",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                        }
                                    });
                            alertDialog.show();
                        }
                    } catch (Settings.SettingNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.tv_endTripMeter:
//                LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver);
                call_endTrip();
                break;

            case R.id.tv_start_waitingTime:
//                cmTimer.setBase(SystemClock.elapsedRealtime());
                call_PauseTrip();
                break;

            case R.id.tv_end_waitingTime:
                call_PauseStopTrip();
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void call_startTrip()
    {
        SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "1", activity);
        SessionSave.saveMeterData(Comman.METER_SELECTED_CAR_MODEL_POSITION, spinner.getSelectedItemPosition()+"", activity);

        spinner.setEnabled(false);
        cv_meter_digit.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));

        String Str_distSum = SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, Drawer_Activity.activity);
        Log.e("kkkkkk","tv_startTripMeter22222222222222 : "+ SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, Drawer_Activity.activity));
        if (Str_distSum!=null && !Str_distSum.equalsIgnoreCase(""))
        {
            String ttt = String.format( "%.2f",  Double.parseDouble(Str_distSum));
            tv_meter_digit.setText(ttt);
        }
        else
        {
            tv_meter_digit.setText("0.0");
        }

        tv_startTripMeter.setVisibility(View.GONE);
        tv_endTripMeter.setVisibility(View.VISIBLE);
        tv_start_waitingTime.setVisibility(View.VISIBLE);
        tv_end_waitingTime.setVisibility(View.GONE);
    }

    private void call_PauseTrip()
    {
       *//* SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "2", activity);

        spinner.setEnabled(false);
        cv_meter_digit.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.colorHintGray));

        String Str_distSum = SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, Drawer_Activity.activity);
        if (Str_distSum!=null && !Str_distSum.equalsIgnoreCase(""))
        {

            String ttt = String.format( "%.2f", Double.parseDouble(Str_distSum));
            tv_meter_digit.setText(ttt);
        }
        else
        {
            tv_meter_digit.setText("0.0");
        }

        tv_startTripMeter.setVisibility(View.GONE);
        tv_endTripMeter.setVisibility(View.VISIBLE);
        tv_start_waitingTime.setVisibility(View.GONE);
        tv_end_waitingTime.setVisibility(View.VISIBLE);

        Map_Fragment.WaitingTimeStart();*//*
    }

    private void call_PauseStopTrip()
    {
        *//*SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "3", activity);

        spinner.setEnabled(false);
        cv_meter_digit.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));

        String Str_distSum = SessionSave.getUserSession(Comman.SUM_FOR_LAT_LONG_METER, Drawer_Activity.activity);
        if (Str_distSum!=null && !Str_distSum.equalsIgnoreCase(""))
        {
            String ttt = String.format( "%.2f",  Double.parseDouble(Str_distSum));
            tv_meter_digit.setText(ttt);
        }
        else
        {
            tv_meter_digit.setText("0.0");
        }

        tv_startTripMeter.setVisibility(View.GONE);
        tv_endTripMeter.setVisibility(View.VISIBLE);
        tv_start_waitingTime.setVisibility(View.VISIBLE);
        tv_end_waitingTime.setVisibility(View.GONE);

        Map_Fragment.WaitingTimePause();*//*
    }

    private void call_endTrip()
    {
        spinner.setEnabled(true);
        for (int i=0; i<ModelNameList.size(); i++)
        {
            String SelectedName = spinner.getSelectedItem().toString();
            Log.e("SelectedName","SelectedName : "+SelectedName);
            if (SelectedName.equalsIgnoreCase(ModelNameList.get(i)))
            {
                selectedModelId = ModelIdList.get(i);
            }
        }

        Log.e("SelectedName","selectedModelId : "+selectedModelId);

        if (Global.isNetworkconn(activity))
        {
            CallGetAmount();
        }
        else
        {
            errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
        }
    }

    *//*private void WaitingTimeStart()
    {
        String abc = SessionSave.getMeterData(Comman.METER_PAUSE_TIME, activity);
        Log.e("abc","abcabcabc : "+abc);
        if (abc!=null && !abc.equalsIgnoreCase(""))
        {
            long time = SystemClock.elapsedRealtime() + Long.parseLong(abc);
            if (SessionSave.getMeterData(Comman.METER_LAST_TIME_IN_MILISEC, activity)!=null && !SessionSave.getMeterData(Comman.METER_LAST_TIME_IN_MILISEC, activity).equalsIgnoreCase(""))
            {
                String startDate = SessionSave.getMeterData(Comman.METER_LAST_TIME_IN_MILISEC, activity);
                String endDate = dateFormat.format(calendar.getTime());

                Log.e("printDifference","time taken : endDate "+ endDate);
                long difference = printDifference(startDate, endDate);

                time = time - difference;
            }
            cmTimer.setBase(time);
        }
        cmTimer.start();
    }

    private void WaitingTimePause()
    {
        SessionSave.saveMeterData(Comman.METER_LAST_TIME_IN_MILISEC, "", activity);
        String abc = SessionSave.getMeterData(Comman.METER_PAUSE_TIME, activity);
        Log.e("abc","abcabcabc : "+abc);
        if (abc!=null && !abc.equalsIgnoreCase(""))
        {
            long time = SystemClock.elapsedRealtime() + Long.parseLong(abc);
            cmTimer.setBase(time);
        }
        cmTimer.stop();
        resume = true;
    }

    private void WaitingTimeClear()
    {
        cmTimer.stop();
        cmTimer.setText("00:00");
        resume = false;
    }*//*

    public boolean checkPermission()
    {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestPermission()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("call", "onRequestPermissionsResult() 11 = " + requestCode);
      *//*  if (null != mBothPermissionRequest) {
            mBothPermissionRequest.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }*//*
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                String AcceptFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, activity);
                if (AcceptFlag!=null && !AcceptFlag.equalsIgnoreCase("") && !AcceptFlag.equalsIgnoreCase("1"))
                {
                    String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                    if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && DutyStatus.equalsIgnoreCase("1"))
                    {
                        errorDialogClass.showDialog("First, you have to be offline!", getString(R.string.info_message));
                    }
                    else
                    {
                        errorDialogClass.showDialog(getString(R.string.please_stay_connect), getString(R.string.info_message));
                        call_startTrip();
                    }
                }
                else
                {
                    errorDialogClass.showDialog("You are already on Your trip, first finished your trip", getString(R.string.info_message));
                }
                break;
        }
    }

    private void OnItemSelect_GetDetail(final String selectedModelId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_GET_MODEL_DETAIL;

        Log.e("url", "OnItemSelect_GetDetail = " + url);
        Log.e("param", "OnItemSelect_GetDetail = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "OnItemSelect_GetDetail = " + responseCode);
                    Log.e("Response", "OnItemSelect_GetDetail = " + json);

                    if (json != null)
                    {
                        Log.e("json", "not Null");
                        if (json.has("model_cat1"))
                        {
                            Log.e("json", "has model_cat1");
                            String model_cat1 = json.getString("model_cat1");
                            if (model_cat1!=null && !model_cat1.equalsIgnoreCase(""))
                            {
                                JSONArray model_cat1Array = json.getJSONArray("model_cat1");
                                if (model_cat1Array.length()>0)
                                {
                                    for (int i=0; i<model_cat1Array.length(); i++)
                                    {
                                        JSONObject model_cat1OBJ = model_cat1Array.getJSONObject(i);
                                        if (model_cat1OBJ!=null)
                                        {
                                            if (model_cat1OBJ.has("Id"))
                                            {
                                                String modelId = model_cat1OBJ.getString("Id");
                                                if (modelId!=null && !modelId.equalsIgnoreCase("") && modelId.equalsIgnoreCase(selectedModelId))
                                                {
                                                    if (model_cat1OBJ.has("BaseFare"))
                                                    {
                                                        String meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity);
                                                        if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
                                                        {
                                                            String totalFareCalculate =  SessionSave.getMeterData(Comman.METER_TOTAL_FARE_CALCULATE, activity);
                                                            if (totalFareCalculate!=null && !totalFareCalculate.equalsIgnoreCase(""))
                                                            {
                                                                String abcd = String.format( "%.2f", Double.parseDouble(totalFareCalculate));
                                                                tv_BaseFare.setText(abcd);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (model_cat1OBJ.getString("BaseFare")!=null && !model_cat1OBJ.getString("BaseFare").equalsIgnoreCase(""))
                                                            {
                                                                SessionSave.saveMeterData(Comman.METER_BASE_FARE, model_cat1OBJ.getString("BaseFare"), activity);
                                                                String abcd = String.format( "%.2f", Double.parseDouble(model_cat1OBJ.getString("BaseFare")));
                                                                tv_BaseFare.setText(abcd);
                                                            }
                                                        }
                                                    }
                                                    if (model_cat1OBJ.has("BaseFare"))
                                                    {
                                                        if (model_cat1OBJ.getString("BaseFare")!=null && !model_cat1OBJ.getString("BaseFare").equalsIgnoreCase(""))
                                                        {
                                                            SessionSave.saveMeterData(Comman.METER_HIRE_COST, model_cat1OBJ.getString("BaseFare"), activity);
                                                            tv_hireFee.setText(model_cat1OBJ.getString("BaseFare"));
                                                        }
                                                    }
                                                    if (model_cat1OBJ.has("MinKm"))
                                                    {
                                                        SessionSave.saveMeterData(Comman.METER_MIN_KM, model_cat1OBJ.getString("MinKm"), activity);
                                                    }
                                                    if (model_cat1OBJ.has("BookingFee"))
                                                    {
                                                        SessionSave.saveMeterData(Comman.METER_BOOKING_FEE, model_cat1OBJ.getString("BookingFee"), activity);
                                                    }
                                                    if (model_cat1OBJ.has("BelowPerKmCharge"))
                                                    {
                                                        SessionSave.saveMeterData(Comman.METER_BELOW_PER_KM_CHARGE, model_cat1OBJ.getString("BelowPerKmCharge"), activity);
                                                    }
                                                    if (model_cat1OBJ.has("WaitingTimePerMinuteCharge"))
                                                    {
                                                        SessionSave.saveMeterData(Comman.METER_WAITING_TIME_PER_MIN, model_cat1OBJ.getString("WaitingTimePerMinuteCharge"), activity);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("json", "no status found");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                }
                            }
                            dialogClass.hideDialog();
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("CallGetAmount", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void CallGetAmount()
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_GET_FARE_FOR_METER;

        params.put(WebServiceAPI.PARAM_GET_FARE_FOR_METER_MODEL_ID, selectedModelId);
        params.put(WebServiceAPI.PARAM_GET_FARE_FOR_METER_ESTIMATE_KM, tv_meter_digit.getText().toString());
        if (SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity)!=null && !SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity).equalsIgnoreCase(""))
        {
            params.put(WebServiceAPI.PARAM_GET_FARE_FOR_WAITING_MIN_COST, SessionSave.getMeterData(Comman.METER_TOTAL_WAITING_COST_CALCULATE, activity));
        }
        else
        {
            params.put(WebServiceAPI.PARAM_GET_FARE_FOR_WAITING_MIN_COST, "0");
        }


        Log.e("url", "CallGetAmount = " + url);
        Log.e("param", "CallGetAmount = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "CallGetAmount = " + responseCode);
                    Log.e("Response", "CallGetAmount = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("estimate_fare"))
                                {
                                    String estimate_fare = json.getString("estimate_fare");
                                    if (estimate_fare!=null && !estimate_fare.equalsIgnoreCase(""))
                                    {
                                        JSONObject estimateFareObj = json.getJSONObject("estimate_fare");
                                        if (estimateFareObj!=null)
                                        {
                                            final Dialog dialog;
                                            dialog = new Dialog(activity, R.style.DialogTheme);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                            lp.copyFrom(dialog.getWindow().getAttributes());
                                            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                                            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                                            dialog.getWindow().setAttributes(lp);
                                            dialog.setCancelable(false);
                                            dialog.setContentView(R.layout.dialog_meter_trip);

                                            TextView tv_dialog_ok, tv_title, tv_carType, tvBaseFare, tv_perKmCharge, tv_totalKm, tv_tripfare, tv_bookingFee, tv_grandTotal, tv_waitingCost;
                                            ImageView dialog_close;

                                            tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                                            tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                                            dialog_close = (ImageView) dialog.findViewById(R.id.dialog_close);

                                            tv_carType = (TextView) dialog.findViewById(R.id.tv_carType);
                                            tvBaseFare = (TextView) dialog.findViewById(R.id.tvBaseFare);
                                            tv_perKmCharge = (TextView) dialog.findViewById(R.id.tv_perKmCharge);
                                            tv_totalKm = (TextView) dialog.findViewById(R.id.tv_totalKm);
                                            tv_tripfare = (TextView) dialog.findViewById(R.id.tv_tripfare);
                                            tv_bookingFee = (TextView) dialog.findViewById(R.id.tv_bookingFee);
                                            tv_grandTotal = (TextView) dialog.findViewById(R.id.tv_grandTotal);
                                            tv_waitingCost = (TextView) dialog.findViewById(R.id.tv_waitingCost);

                                            tv_title.setText("Meter Trip Fare");

                                            dialog_close.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    if (spinner.getSelectedItemPosition()==0)
                                                    {
                                                        Log.e("hhhhhhhhhhhhhhh","11111111111111111");
                                                        selectedModelId = ModelIdList.get(0);
                                                        OnItemSelect_GetDetail(selectedModelId);
                                                    }
                                                    else
                                                    {
                                                        Log.e("hhhhhhhhhhhhhhh","222222222222222222");
                                                        spinner.setSelection(0);
                                                    }

                                                }
                                            });
                                            tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    if (spinner.getSelectedItemPosition()==0)
                                                    {
                                                        Log.e("hhhhhhhhhhhhhhh","11111111111111111");
                                                        selectedModelId = ModelIdList.get(0);
                                                        OnItemSelect_GetDetail(selectedModelId);
                                                    }
                                                    else
                                                    {
                                                        Log.e("hhhhhhhhhhhhhhh","222222222222222222");
                                                        spinner.setSelection(0);
                                                    }
                                                }
                                            });

                                            tv_meter_digit.setText("");
                                            tv_BaseFare.setText("");
                                            tv_hireFee.setText("");
                                            tv_waitinCost.setText("");
//                                            WaitingTimeClear();

                                            cv_meter_digit.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.appColor));
                                            SessionSave.saveMeterData(Comman.METER_ACTIVE_FLAG, "0", activity);
                                            tv_startTripMeter.setVisibility(View.VISIBLE);
                                            tv_endTripMeter.setVisibility(View.GONE);
                                            tv_start_waitingTime.setVisibility(View.GONE);
                                            tv_end_waitingTime.setVisibility(View.GONE);
                                            selectedModelId = "";
                                            SessionSave.clearMeterData(activity);



                                            String ModelName="", baseFare="", kmTaken="", per_km_charge="", tripFare="", bookingFee="", Total="", waitingCost="";
                                            if (estimateFareObj.has("name"))
                                            {
                                                ModelName = estimateFareObj.getString("name");
                                                tv_carType.setText(ModelName);
                                            }
                                            if (estimateFareObj.has("base_fare"))
                                            {
                                                baseFare = estimateFareObj.getString("base_fare");
                                                tvBaseFare.setText(baseFare);
                                            }
                                            if (estimateFareObj.has("per_km_charge"))
                                            {
                                                per_km_charge = estimateFareObj.getString("per_km_charge");
                                                tv_perKmCharge.setText(per_km_charge);
                                            }
                                            if (estimateFareObj.has("km"))
                                            {
                                                kmTaken = estimateFareObj.getString("km");
                                                tv_totalKm.setText(kmTaken);
                                            }
                                            if (estimateFareObj.has("trip_fare"))
                                            {
                                                tripFare = estimateFareObj.getString("trip_fare");
                                                tv_tripfare.setText(tripFare);
                                            }
                                            if (estimateFareObj.has("booking_fee"))
                                            {
                                                bookingFee = estimateFareObj.getString("booking_fee");
                                                tv_bookingFee.setText(bookingFee);
                                            }
                                            if (estimateFareObj.has("total"))
                                            {
                                                Total = estimateFareObj.getString("total");
                                                tv_grandTotal.setText(Total);
                                            }
                                            if (estimateFareObj.has("waiting_cost"))
                                            {
                                                waitingCost = estimateFareObj.getString("waiting_cost");
                                                Log.e("waitingCost","waitingCost : "+waitingCost);
                                                tv_waitingCost.setText(waitingCost);
                                            }

                                            dialogClass.hideDialog();
                                            dialog.show();
                                        }
                                        else
                                        {
                                            Log.e("status", "false");
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                            }
                                            else
                                            {
                                                errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                        else
                                        {
                                            errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                    else
                                    {
                                        errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                            else
                            {
                                errorDialogClass.showDialog(getResources().getString(R.string.please_try_agian_later), getString(R.string.error_message));
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                }
                catch (Exception e)
                {
                    Log.e("CallGetAmount", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }


    @Override
    public void onBackPressed()
    {
        *//*meterActiveFlag = SessionSave.getMeterData(Comman.METER_ACTIVE_FLAG, activity);
        if (meterActiveFlag!=null && !meterActiveFlag.equalsIgnoreCase("") && !meterActiveFlag.equalsIgnoreCase("0"))
        {
            errorDialogClass.showDialog("You are already on Your trip, first finished your trip", getString(R.string.info_message));
        }
        else
        {*//*
            super.onBackPressed();
            Drawer_Activity.flagResumePass=0;
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
       *//* }*//*

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity != null) {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }
*/
    /*@Override
    protected void onDestroy()
    {
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        SessionSave.saveMeterData(Comman.METER_LAST_TIME_IN_MILISEC, dateFormat.format(calendar.getTime()), activity);
        try {
            Log.e("printDifference","time taken : startDate "+  dateFormat.parse(dateFormat.format(calendar.getTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }*/
}
