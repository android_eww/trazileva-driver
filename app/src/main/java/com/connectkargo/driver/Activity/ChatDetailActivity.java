package com.connectkargo.driver.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.connectkargo.driver.Adapter.BidChatAdapter;
import com.connectkargo.driver.Adapter.ChatAdapter;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Been.ChatBeen;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.SocketSingleObject;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChatDetailActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_back,llSend,mainLayout;
    private TextView tv_toolbarTitle,tvBidID;
    private EditText etMessage;
    public static ChatDetailActivity activity;
    private ArrayList<ChatBeen> list = new ArrayList<>();
    private BidChatAdapter chatAdapter;
    private RecyclerView recycleview;
    private AQuery aQuery;
    private DialogClass dialogClass;
    private String bidId="";
    private String passengerId="";
    private String passengerName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_detail);

        activity = ChatDetailActivity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("bidId")!=null)
            {
                bidId = this.getIntent().getStringExtra("bidId");
            }

            if (this.getIntent().getStringExtra("passengerId")!=null)
            {
                passengerId = this.getIntent().getStringExtra("passengerId");
            }

            if (this.getIntent().getStringExtra("passengerName")!=null)
            {
                passengerName = this.getIntent().getStringExtra("passengerName");
            }
        }

        init();

    }

    @Override
    protected void onPause() {
        super.onPause();
        activity = null;
    }

    private void init()
    {
        ll_back = findViewById(R.id.ll_back);
        tv_toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        llSend = findViewById(R.id.llSend);
        etMessage = findViewById(R.id.etMessage);
        mainLayout = findViewById(R.id.mainLayout);
        recycleview = findViewById(R.id.recycleview);
        tvBidID = findViewById(R.id.tvBidID);
        tv_toolbarTitle.setText(passengerName);
        tvBidID.setText("#"+bidId);
        llSend.setOnClickListener(this);
        ll_back.setOnClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        recycleview.setLayoutManager(layoutManager);
        recycleview.setHasFixedSize(true);

        //connectSocket();
        callApi();
    }

    /*Params : Sender: Driver
    Receiver: Passenger
    BidId:1
    SenderId:6(DriverId)
    ReceiverId:3(PassengerId)*/

    private void callApi()
    {
        list.clear();
        String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        params.put(WebServiceAPI.PARAM_SENDER, "Driver");
        params.put(WebServiceAPI.PARAM_RECEIVER, "Passenger");
        params.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_BIDID, bidId);
        params.put(WebServiceAPI.SEND_MONEY_PARAM_SENDER_ID, userId);
        params.put(WebServiceAPI.PARAM_RECEIVERID, passengerId);

        String url="";
        url = WebServiceAPI.WEB_BID_CHAT_HISTORY;

        Log.e("TAG", "getChatHistory() url:- " + url);
        Log.e("TAG", "getChatHistory() params:- " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();

                    Log.e("TAG", "getChatHistory() responseCode:- " + responseCode);
                    Log.e("TAG", "getChatHistory() Response:- " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String Fullname="",Image="";

                                if (json.has("driver"))
                                {
                                    JSONArray driver = json.getJSONArray("driver");

                                    if (driver!=null && driver.length()>0)
                                    {
                                        JSONObject driverObject = driver.getJSONObject(0);

                                        if (driverObject!=null)
                                        {
                                            if (driverObject.has("Fullname"))
                                            {
                                                Fullname = driverObject.getString("Fullname");
                                            }

                                            if (driverObject.has("Image"))
                                            {
                                                Image = WebServiceAPI.BASE_URL_IMAGE + "" + driverObject.getString("Image");
                                            }
                                        }
                                    }
                                }

                                if (Image!=null && !Image.trim().equals(""))
                                {
                                }

                                String ReceiverName="";
                                if (json.has("ReceiverName"))
                                {
                                    ReceiverName = json.getString("ReceiverName");
                                }
                                passengerName = ReceiverName;
                                tv_toolbarTitle.setText(passengerName);
                                if (json.has("data"))
                                {
                                    JSONArray jsonArray = json.getJSONArray("data");

                                    if (jsonArray!=null && jsonArray.length()>0)
                                    {
                                        for (int i=0; i<jsonArray.length(); i++)
                                        {
                                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                                            if (jsonObject!=null)
                                            {
                                                String Message="",Date="",Sender="",Receiver="";

                                                if (jsonObject.has("Message"))
                                                {
                                                    Message = jsonObject.getString("Message");
                                                }

                                                if (jsonObject.has("Date"))
                                                {
                                                    Date = jsonObject.getString("Date");
                                                }

                                                if (jsonObject.has("Sender"))
                                                {
                                                    Sender = jsonObject.getString("Sender");
                                                }

                                                if (jsonObject.has("Receiver"))
                                                {
                                                    Receiver = jsonObject.getString("Receiver");
                                                }

                                                list.add(new ChatBeen(Message,Date,Sender,Receiver));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                        chatAdapter = new BidChatAdapter(activity, list);
                                        recycleview.setAdapter(chatAdapter);
                                        recycleview.scrollToPosition(list.size()-1);
                                        //scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                                    }
                                    dialogClass.hideDialog();
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                dialogClass.hideDialog();
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    Log.e("TAG","UpdateAccountDetail() Exception:- " + e.toString());
                    dialogClass.hideDialog();
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    private void connectSocket()
    {
        Comman.socketChat = SocketSingleObject.get(activity).getSocket();
        if (Comman.socketChat!=null)
        {
            if (!Comman.socketChat.connected())
            {
                Comman.socketChat.on("receive_message", onReceiveMessage);
                Comman.socketChat.on(Socket.EVENT_CONNECT, onConnect);
                Comman.socketChat.on(Socket.EVENT_DISCONNECT, onDisconnect);
                Comman.socketChat.connect();
            }
            else
            {
                Comman.socketChat.on("receive_message", onReceiveMessage);
            }
        }
    }

    private void disconnectSocket()
    {
        if (Comman.socketChat!=null)
        {
            if (Comman.socketChat.connected())
            {
                Comman.socketChat.off("receive_message", onReceiveMessage);
                Comman.socketChat.off(Socket.EVENT_CONNECT, onConnect);
                Comman.socketChat.off(Socket.EVENT_DISCONNECT, onDisconnect);
                Comman.socketChat.disconnect();
                Comman.socketChat = null;
            }
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    Log.e("socketChat", "connected");
                    try
                    {
                        if (Comman.socketChat!=null)
                        {
                            if (Comman.socketChat.connected())
                            {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("DriverId",SessionSave.getUserSession(Comman.USER_ID,activity));
                                Comman.socketChat.emit("driver_connect",jsonObject);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("call","exception = "+e.getMessage());
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("socketChat", "diconnected");
                }
            });
        }
    };

    @Override
    public void onClick(View v)
    {
        if(v == ll_back)
        {
            onBackPressed();
        }
        else if(v == llSend)
        {
            CheckData();
        }
    }

    private void CheckData()
    {
        Log.e("TAG","CheckData()");

        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);

        if (TextUtils.isEmpty(etMessage.getText().toString()))
        {

        }
        else
        {
            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (Global.isNetworkconn(activity))
                {
                    //sendMessage();
                    sendBidChat(etMessage.getText().toString(),
                            "Driver",
                            "Passenger",
                            bidId,
                            userId,
                            passengerId);
                }
                else
                {
                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),getResources().getString(R.string.internet_error_message));
                }
            }
        }
    }

    public void sendMessage()
    {
        if (Comman.socketChat != null)
        {
            try
            {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("BidId",bidId);
                jsonObject.put("Sender","Driver");
                jsonObject.put("Message",etMessage.getText().toString().trim());
                jsonObject.put("SenderId", SessionSave.getUserSession(Comman.USER_ID,activity));

                Log.e("call","json object = "+jsonObject.toString());

                Comman.socketChat.emit("send_message",jsonObject);
                list.add(new ChatBeen(etMessage.getText().toString().trim(), CabRideDriverApplication.getCurrentDateTime(),"Driver","Passenger"));
                etMessage.setText("");
                if (list!=null && list.size()>0)
                {
                    chatAdapter = new BidChatAdapter(activity, list);
                    recycleview.setAdapter(chatAdapter);
                    recycleview.scrollToPosition(list.size()-1);
                }
            }
            catch (Exception e)
            {
                Log.e("call","Exception in making json = "+e.getMessage());
            }
        }
        else
        {
            new SnackbarUtils(mainLayout,getString(R.string.no_internet_connection),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
    }

    @Override
    protected void onDestroy()
    {
        disconnectSocket();
        super.onDestroy();
    }

    private Emitter.Listener onReceiveMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try
                    {
                        if (args[0]!=null && args[0].toString().length()>0)
                        {
                            JSONObject jsonObject = new JSONObject(args[0].toString());

                            Log.e("call","onReceiveMessage()");

                            if (jsonObject!=null)
                            {
                                String Message="",Date="",Sender="",Receiver="";

                                if (jsonObject.has("Message"))
                                {
                                    Message = jsonObject.getString("Message");
                                }

                                if (jsonObject.has("Date"))
                                {
                                    Date = jsonObject.getString("Date");
                                }

                                if (jsonObject.has("Sender"))
                                {
                                    Sender = jsonObject.getString("Sender");
                                }

                                if (jsonObject.has("Receiver"))
                                {
                                    Receiver = jsonObject.getString("Receiver");
                                }

                                list.add(new ChatBeen(Message,Date,Sender,Receiver));

                                if (list!=null && list.size()>0)
                                {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            chatAdapter = new BidChatAdapter(activity,list);
                                            recycleview.setAdapter(chatAdapter);
                                            recycleview.scrollToPosition(list.size()-1);
                                        }
                                    });
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("call","Exception e = "+e.getMessage());
                    }
                }
            });
        }
    };

    public void appendMessage(JSONObject jsonObject)
    {
        try
        {
            Log.e("call","onReceiveMessage()");

            if (jsonObject!=null)
            {
                String Message="",Date="",Sender="",Receiver="";

                if (jsonObject.has("Message"))
                {
                    Message = jsonObject.getString("Message");
                }

                if (jsonObject.has("Date"))
                {
                    Date = jsonObject.getString("Date");
                }

                if (jsonObject.has("Sender"))
                {
                    Sender = jsonObject.getString("Sender");
                }

                if (jsonObject.has("Receiver"))
                {
                    Receiver = jsonObject.getString("Receiver");
                }

                list.add(new ChatBeen(Message,Date,Sender,Receiver));

                if (list!=null && list.size()>0)
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            chatAdapter = new BidChatAdapter(activity,list);
                            recycleview.setAdapter(chatAdapter);
                            recycleview.scrollToPosition(list.size()-1);
                        }
                    });
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","Exception e = "+e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        disconnectSocket();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
            Log.e("onCreate2","CabRideDriverApplication.currentActivity().getLocalClassName() = "+CabRideDriverApplication.currentActivity().getClass().getSimpleName());
        }
    }


    /*Message:THIS IS TEST
    Sender:Driver
    Receiver:Passenger
    BidId:1
    SenderId:6(DriverId)
    ReceiverId:3(PassengerId)*/

    private void sendBidChat(String message, String sender, String receiver, String bidid, String senderid, String receiverId) {

        String URL = WebServiceAPI.WEB_BID_CHAT;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebServiceAPI.WEB_SERVICE_CHAT_PARAM_MESSAGE, message);
        param.put(WebServiceAPI.PARAM_SENDER, sender);
        param.put(WebServiceAPI.PARAM_RECEIVER, receiver);
        param.put(WebServiceAPI.WEB_SERVICE_BID_ACCEPT_BIDID, bidid);
        param.put(WebServiceAPI.SEND_MONEY_PARAM_SENDER_ID, senderid);
        param.put(WebServiceAPI.WEB_SERVICE_CHAT_PARAM_RECIEVER_ID, receiverId);

        Log.e(TAG, "sendBidChat() : url : "+URL);
        Log.e(TAG, "sendBidChat() : param : "+param);
        aQuery.ajax(URL, param, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);

                try {

                    if(object != null) {

                        Log.e(TAG, "sendBidChat() : RESPONSE : "+object);

                        if(object.has("status")) {

                            if(object.getBoolean("status")) {

                                list.add(new ChatBeen(etMessage.getText().toString().trim(), CabRideDriverApplication.getCurrentDateTime(), "Driver", "Passenger"));
                                etMessage.setText("");
                                if (list != null && list.size() > 0) {
                                    chatAdapter = new BidChatAdapter(activity, list);
                                    recycleview.setAdapter(chatAdapter);
                                    recycleview.scrollToPosition(list.size() - 1);
                                }
                            } else {
                                Log.e(TAG, "sendBidChat() : status false");
                            }

                        } else {
                            Log.e(TAG, "sendBidChat() : status not found");
                        }

                    } else {
                        Log.e(TAG, "sendBidChat() : object null");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "sendBidChat() : ERROR : "+e.getMessage());
                }

            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));

    }

    public void addChatList(final String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.add(new ChatBeen(message, CabRideDriverApplication.getCurrentDateTime(), "Passenger", "Driver"));
                //et_Message.setText("");
                if (list != null && list.size() > 0) {
                    chatAdapter = new BidChatAdapter(activity, list);
                    recycleview.setAdapter(chatAdapter);
                    recycleview.scrollToPosition(list.size() - 1);
                }
            }
        });

    }

}