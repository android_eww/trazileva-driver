package com.connectkargo.driver.Activity;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class BaseActivity extends AppCompatActivity {

    public String TAG = "BaseActivity";
    public BaseActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = BaseActivity.this;
        updateResources(BaseActivity.this, "pt");
    }

    public Context updateResources(Context context, String language)
    {
        Log.e(TAG,"updateResources()");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17)
        {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        else
        {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

    public void retriveToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task)
                    {
                        if (!task.isSuccessful())
                        {
                            Log.e(TAG, "getInstanceId failed:- ", task.getException());
                            return;
                        }
                        String token = task.getResult().getToken();
                        SessionSave.saveToken(Comman.DEVICE_TOKEN, token, activity);
                        Log.e(TAG, "onComplete() token:- " + SessionSave.getToken(Comman.DEVICE_TOKEN, activity));
                    }
                });
    }
}
