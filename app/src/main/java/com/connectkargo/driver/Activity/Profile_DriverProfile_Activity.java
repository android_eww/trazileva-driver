package com.connectkargo.driver.Activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import de.hdodenhof.circleimageview.CircleImageView;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Profile_DriverProfile_Activity extends BaseActivity implements View.OnClickListener{

    Profile_DriverProfile_Activity activity;
    LinearLayout ll_toolbar_RightImage, ll_back, ll_Sos, ll_save,llDadNumber,llWifeNumber;
    ImageView  iv_toolbar_RightImage;
    CircleImageView iv_profilePic;
    ProgressBar progressBar;
    RelativeLayout rl_profile_pic;
    LinearLayout main_layout;
    EditText et_full_name_profile, et_residential_address_profile, et_post_code_profile, et_MobNo_profile,et_driver_wife_mobileNo,et_driver_dad_mobileNo;
    TextView tv_email, tv_toolbarTitle, tv_full_name_Dob;
    AppCompatRadioButton checkBox_male, checkBox_Female;

    String MALE="Male", FEMALE ="Female", chackBox_Selected ="";

    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private static final int MY_REQUEST_CODE_CAMERA = 101;
    private static final int MY_REQUEST_CODE_STORAGE = 100;
    private int GALLERY = 1, CAMERA = 0, REQUEST_CAMERA_SUMSUNG = 5;
    byte[] Profile_ByteImage=null;
    public Transformation mTransformation;

    private AQuery aQuery;
    DialogClass dialogClass;
    Bitmap BitmapLLL;

    Uri mImageUri;
    public static Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    ErrorDialogClass errorDialogClass;
    public static Bitmap bitmapSumsung;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_profile);

        activity = Profile_DriverProfile_Activity.this;
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        mImageUri=null;
        bitmapImage=null;
        picturePath=null;

        initUI();
    }

    private void initUI()
    {
        aQuery = new AQuery(activity);
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        ll_toolbar_RightImage = (LinearLayout) findViewById(R.id.ll_toolbar_RightImage);
        iv_toolbar_RightImage = (ImageView) findViewById(R.id.iv_toolbar_RightImage);
        iv_toolbar_RightImage.setVisibility(View.GONE);
        iv_toolbar_RightImage.setImageResource(R.drawable.icon_save_white);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);

        llWifeNumber = findViewById(R.id.llWifeNumber);
        llDadNumber = findViewById(R.id.llDadNumber);

        tv_email = (TextView)findViewById(R.id.tv_email);
        tv_full_name_Dob = (TextView)findViewById(R.id.tv_full_name_Dob);
        tv_toolbarTitle = (TextView)findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText(activity.getResources().getString(R.string.profile_update));

        et_full_name_profile = (EditText) findViewById(R.id.et_full_name_profile);
        et_residential_address_profile = (EditText) findViewById(R.id.et_residential_address_profile);
        et_post_code_profile = (EditText) findViewById(R.id.et_post_code_profile);
        et_MobNo_profile = (EditText) findViewById(R.id.et_MobNo_profile);
        et_driver_dad_mobileNo = findViewById(R.id.et_driver_dad_mobileNo);
        et_driver_wife_mobileNo = findViewById(R.id.et_driver_wife_mobileNo);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        iv_profilePic = findViewById(R.id.iv_profilePic);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);

        rl_profile_pic = (RelativeLayout) findViewById(R.id.rl_profile_pic);

        checkBox_male = (AppCompatRadioButton) findViewById(R.id.checkBox_male);
        checkBox_male.setChecked(true);
        chackBox_Selected = MALE;
        checkBox_Female = (AppCompatRadioButton) findViewById(R.id.checkBox_Female);

        checkBox_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              selectMale();
            }
        });
        checkBox_Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectFemale();
            }
        });

        rl_profile_pic.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        tv_full_name_Dob.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);

        /*rl_profile_pic.setEnabled(false);
        tv_full_name_Dob.setEnabled(false);
        et_full_name_profile.setEnabled(false);
        et_residential_address_profile.setEnabled(false);
        et_post_code_profile.setEnabled(false);
        et_MobNo_profile.setEnabled(false);*/

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.colorThemeYellow))
                .borderWidthDp(2)
                .oval(true)
                .build();

        String UserImage = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);

        if(UserImage!=null &&
                !UserImage.equalsIgnoreCase("") &&
                !UserImage.equalsIgnoreCase("null"))
        {
            Log.e("Profile_DriverProfil","USER_IMAGE");

            progressBar.setVisibility(View.GONE);
            Picasso.get()
                    .load(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,activity))
//                    .fit()
//                    .transform(mTransformation)
                    .into(iv_profilePic);

//            Picasso.get()
//                    .load(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,activity))
//                    .into(new Target() {
//                        @Override
//                        public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from) {
//                            BitmapLLL = bitmap;
//                        }
//
//                        @Override
//                        public void onPrepareLoad(Drawable placeHolderDrawable) {}
//
//                        @Override
//                        public void onBitmapFailed(Drawable errorDrawable) {}
//                    });
        }
        else
        {
            iv_profilePic.setImageResource(R.drawable.cab_dummy_profile);
            progressBar.setVisibility(View.GONE);
        }

        Log.e("USER_GENDER", "USER_GENDER USER_GENDER : "+SessionSave.getUserSession(Comman.USER_GENDER,activity));
        if (SessionSave.getUserSession(Comman.USER_GENDER,activity)!= null && !SessionSave.getUserSession(Comman.USER_GENDER,activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.USER_GENDER,activity).equals(FEMALE))
            {
                selectFemale();
            }
            else
            {
                selectMale();
            }
        }

        if (SessionSave.getUserSession(Comman.USER_EMAIL,activity)!= null && !SessionSave.getUserSession(Comman.USER_EMAIL,activity).equalsIgnoreCase(""))
        {
            tv_email.setText(SessionSave.getUserSession(Comman.USER_EMAIL,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,activity)!= null && !SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,activity).equalsIgnoreCase(""))
        {
            et_MobNo_profile.setText("+244 " + SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_DAD_NUMBER,activity)!= null && !SessionSave.getUserSession(Comman.USER_DAD_NUMBER,activity).equalsIgnoreCase(""))
        {
            et_driver_dad_mobileNo.setText(SessionSave.getUserSession(Comman.USER_DAD_NUMBER,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_WIFE_NUMBER,activity)!= null && !SessionSave.getUserSession(Comman.USER_WIFE_NUMBER,activity).equalsIgnoreCase(""))
        {
            et_driver_wife_mobileNo.setText(SessionSave.getUserSession(Comman.USER_WIFE_NUMBER,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_FULL_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_FULL_NAME,activity).equalsIgnoreCase(""))
        {
            et_full_name_profile.setText(SessionSave.getUserSession(Comman.USER_FULL_NAME,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,activity)!= null && !SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,activity).equalsIgnoreCase(""))
        {
            et_residential_address_profile.setText(SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_POST_CODE,activity)!= null && !SessionSave.getUserSession(Comman.USER_POST_CODE,activity).equalsIgnoreCase(""))
        {
            et_post_code_profile.setText(SessionSave.getUserSession(Comman.USER_POST_CODE,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_DATE_OF_BIRTH,activity)!= null && !SessionSave.getUserSession(Comman.USER_DATE_OF_BIRTH,activity).equalsIgnoreCase(""))
        {
            tv_full_name_Dob.setText(SessionSave.getUserSession(Comman.USER_DATE_OF_BIRTH,activity));
        }
    }

    public void selectMale()
    {
        chackBox_Selected = MALE;
        checkBox_male.setChecked(true);
        checkBox_Female.setChecked(false);
    }
    public void selectFemale()
    {
        chackBox_Selected = FEMALE;
        checkBox_male.setChecked(false);
        checkBox_Female.setChecked(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.rl_profile_pic:
                ShowPictureDialog();
                break;

            case R.id.ll_save:
                saveProfileDetail();
                break;

            case R.id.tv_full_name_Dob:
                OpenPopopFor_datePicker(tv_full_name_Dob);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void saveProfileDetail()
    {
        if (TextUtils.isEmpty(et_full_name_profile.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_full_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_MobNo_profile.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_mobile_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(tv_full_name_Dob.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_dob),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_residential_address_profile.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_residential_address),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        /*else  if (TextUtils.isEmpty(et_post_code_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_post_code),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }*/
        else
        {
            String driverId =  SessionSave.getUserSession(Comman.USER_ID, activity);
            if (driverId!=null && !driverId.equalsIgnoreCase(""))
            {
                if (Global.isNetworkconn(activity))
                {
                    UpdateDriverProfile(driverId);
                }
                else
                {
                    errorDialogClass.showDialog(getString(R.string.not_connected_to_internet),  getString(R.string.internet_error_message));
                }
            }
        }
    }

    private void UpdateDriverProfile(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_UPDATE_DRIVER_BASIC_INFO ;

        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_DIVER_ID, driverId);
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_FULL_NAME, et_full_name_profile.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_MOB_NO, et_MobNo_profile.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_DOB, tv_full_name_Dob.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_GENDER, chackBox_Selected);
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_ADDRESS, et_residential_address_profile.getText().toString());

        if (!TextUtils.isEmpty(et_post_code_profile.getText().toString()))
        {
            params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_ZIPCODE, et_post_code_profile.getText().toString());
        }

        if (Profile_ByteImage!=null)
        {
            params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_DRIVER_IMAGE, Profile_ByteImage);
        }


        Log.e("url", "UpdateDriverProfile = " + url);
        Log.e("param", "UpdateDriverProfile = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateDriverProfile = " + responseCode);
                    Log.e("Response", "UpdateDriverProfile = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String DriverId="", Email="", FullName="", MobileNo="", Gender="", Image="", Address="", Zipcode="",  DOB="";

                                        if (driverProfile.has("Id"))
                                        {
                                            DriverId = driverProfile.getString("Id");
                                            SessionSave.saveUserSession(Comman.USER_ID, DriverId, activity);
                                        }
                                        if (driverProfile.has("Email"))
                                        {
                                            Email = driverProfile.getString("Email");
                                            SessionSave.saveUserSession(Comman.USER_EMAIL, Email, activity);
                                        }
                                        if (driverProfile.has("Fullname"))
                                        {
                                            FullName = driverProfile.getString("Fullname");
                                            SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, activity);
                                        }
                                        if (driverProfile.has("MobileNo"))
                                        {
                                            MobileNo = driverProfile.getString("MobileNo");
                                            SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, activity);
                                        }
                                        if (driverProfile.has("Gender"))
                                        {
                                            Gender = driverProfile.getString("Gender");
                                            Log.e("USER_GENDER","Gender : "+Gender);
                                            SessionSave.saveUserSession(Comman.USER_GENDER, Gender, activity);
                                        }
                                        if (driverProfile.has("DOB"))
                                        {
                                            DOB = driverProfile.getString("DOB");
                                            SessionSave.saveUserSession(Comman.USER_DATE_OF_BIRTH, DOB, activity);
                                        }
                                        if (driverProfile.has("Image"))
                                        {
                                            Image = driverProfile.getString("Image");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, activity);
                                        }
                                        if (driverProfile.has("Address"))
                                        {
                                            Address = driverProfile.getString("Address");
                                            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, activity);
                                        }
                                        if (driverProfile.has("ZipCode"))
                                        {
                                            Zipcode = driverProfile.getString("ZipCode");
                                            SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, activity);
                                        }

                                        String Rate = "";

                                        if (driverProfile.has("rating"))
                                        {
                                            JSONObject driverRating = driverProfile.getJSONObject("rating");

                                            if (driverRating!=null)
                                            {
                                                if (driverRating.has("Rate"))
                                                {
                                                    Rate = driverRating.getString("Rate");
                                                }
                                            }
                                        }
                                        SessionSave.saveUserSession(Comman.USER_DRIVER_RATE, Rate, activity);

                                        if (json.has("message"))
                                        {
                                            SuccessDialog(json.getString("message"), getString(R.string.success_message));
//                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.success_message));
                                        }
                                        else
                                        {
                                            SuccessDialog(getString(R.string.profile_updated_successfully), getString(R.string.success_message));
//                                            errorDialogClass.showDialog(getString(R.string.profile_updated_successfully), getString(R.string.success_message));
                                        }

                                        dialogClass.hideDialog();
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                        }
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));

                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }
                } catch (Exception e) {
                    Log.e("UpdateDriverProfile", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }

    private void ShowPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE_CAMERA);
        }
        else if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE_STORAGE);
        }
        else
        {
            final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_photo_choose);

            TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
            TextView tv_camera = (TextView) dialog.findViewById(R.id.dialog_tv_camera);
            TextView tv_gallery = (TextView) dialog.findViewById(R.id.dialog_tv_gallery);
            LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
            ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

            ll_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            tv_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            iv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    dialog.dismiss();
                }
            });

            tv_gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            choosePhotoFromGallary();
                        }
                    },800);
                }
            });

            tv_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            takePhotoFromCamera();
                        }
                    },800);

                }
            });

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            //lp.width = Comman.DEVICE_WIDTH;
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);

            dialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        bitmapSumsung=null;
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }
        else
        {
            userChoosenTask = chooseCamera;
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "NewPicture");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
            startActivityForResult(intent, CAMERA);
        }
    }

    public String getDeviceName()
    {
        String manufacturer = Build.MANUFACTURER;
        return manufacturer;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission CAMERA permission granted");
                    ShowPictureDialog();
                }
                break;

            case MY_REQUEST_CODE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission GALLERY permission granted");
                    ShowPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                if (data!=null)
                {
                    onSelectFromGalleryResult(data);
                }
            }
            else if (requestCode == CAMERA)
            {
                Log.e("hahahahahahahaha","11111111111111111111");
                onCaptureImageResult(data);
            }

            else
            {
                Profile_ByteImage=null;
            }
        }
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try
        {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (bitmapImage!=null)
        {
            Picasso.get()
                    .load(data.getData())
                    .fit()
                    .transform(mTransformation)
                    .into(iv_profilePic);
            Profile_ByteImage = ConvertToByteArray(bitmapImage);
        }
        else
        {
            Profile_ByteImage=null;
            new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
    }

    private void onCaptureImageResult(Intent data)
    {
        if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
        {
            Log.e("hahahahahahahaha", "getDeviceName() : "+getDeviceName());
            bitmapSumsung = (Bitmap) data.getExtras().get("data");
            iv_profilePic.setImageBitmap(bitmapSumsung);
            Profile_ByteImage = ConvertToByteArray(bitmapSumsung);
        }
        else
        {
            Log.e("hahahahahahahaha", "222222222222 : "+imageUri);
            if (imageUri!=null)
            {
                try
                {
                    Log.e("hahahahahahahaha", "33333333333333 : "+imageUri);
                    Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    Log.e("hahahahahahahaha","bmp height = "+thumbnail.getHeight());
                    Log.e("hahahahahahahaha","bmp width = "+thumbnail.getWidth());
                    thumbnail = getResizedBitmap(thumbnail,400);
                    bitmapImage=thumbnail;

                    if (thumbnail!=null)
                    {
                        Picasso.get()
                                .load(imageUri)
                                .fit()
                                .transform(mTransformation)
                                .into(iv_profilePic);
                        Profile_ByteImage = ConvertToByteArray(thumbnail);

                    }
                    else
                    {
                        iv_profilePic.setImageResource(R.drawable.cab_dummy_profile);
                        Profile_ByteImage=null;
                        new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("hahahahahahahaha", "44444444444444 : "+imageUri);
                    iv_profilePic.setImageResource(R.drawable.cab_dummy_profile);
                    SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",activity);
                    Profile_ByteImage=null;
                    Log.e("call","exception = "+e.getMessage());
                }
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public void OpenPopopFor_datePicker(final TextView textView)
    {
        Calendar newCalendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        fromDatePickerDialog = new DatePickerDialog(activity, R.style.DialogThemeReal, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        fromDatePickerDialog.setSpinnersShown

        fromDatePickerDialog.setTitle(activity.getResources().getString(R.string.select_date_of_birth));
        fromDatePickerDialog.show();
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.e("yyyyyy","yyyyyyyyyyy: ");
                textView.setText("");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
        if (bitmapSumsung != null)
        {
            Log.e("Call OnResume:- ", "Camera Click True");
            if (getDeviceName() != null && getDeviceName().toLowerCase().contains("samsung"))
            {
                Log.e("Call OnResume:- ", "Company Name" + getDeviceName() );
                Uri imageUri1 = getImageUri(activity, bitmapSumsung);
                Picasso.get()
                        .load(imageUri1)
                        .fit()
                        .transform(mTransformation)
                        .into(iv_profilePic);
                Profile_ByteImage = ConvertToByteArray(bitmapSumsung);
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "PickNGo", null);
        return Uri.parse(path);
    }

    private void SuccessDialog(String message, String title)
    {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_common_error);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.getWindow().setAttributes(lp);

        TextView tv_Ok, tv_Message, tv_Title;
        LinearLayout ll_Ok;
        ImageView iv_close;

        tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                Handler handler = new Handler();
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onBackPressed();
                    }
                };
                handler.postDelayed(runnable,1000);
            }
        });

        if (message!=null && !message.equalsIgnoreCase(""))
        {
            tv_Message.setText(message);
        }
        else
        {
            tv_Message.setVisibility(View.GONE);
        }

        if (title!=null && !title.equalsIgnoreCase(""))
        {
            tv_Title.setText(title);
        }
        else
        {
            tv_Title.setVisibility(View.GONE);
        }


        dialog.show();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }
}