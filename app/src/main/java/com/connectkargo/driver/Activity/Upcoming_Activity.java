package com.connectkargo.driver.Activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.R;


public class Upcoming_Activity extends BaseActivity implements View.OnClickListener{

    public static Upcoming_Activity activity;

    LinearLayout ll_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming);

        activity = Upcoming_Activity.this;

        initUI();
    }

    private void initUI() {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_back.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }
}
