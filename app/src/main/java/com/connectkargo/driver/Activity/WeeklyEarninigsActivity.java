package com.connectkargo.driver.Activity;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Fragment.DispatchEarningFragment;
import com.connectkargo.driver.Fragment.RidesEarningFragment;
import com.connectkargo.driver.Fragment.TotalEarningFragment;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.connectkargo.driver.Been.Weekly_Earning_Been;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.DialogClass;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WeeklyEarninigsActivity extends BaseActivity {

    public static WeeklyEarninigsActivity activity;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    LinearLayout ll_back, main_layout;
    ImageView iv_back;
    TextView tv_title;

    private AQuery aQuery;
    DialogClass dialogClass;

    public static List<Weekly_Earning_Been> list_total = new ArrayList<>();
    public static List<Weekly_Earning_Been> list_ride = new ArrayList<>();
    public static List<Weekly_Earning_Been> list_tickpay = new ArrayList<>();
    public static List<Weekly_Earning_Been> list_dispatch = new ArrayList<>();

    String SUN="Sun", MON="Mon", TUE="Tue", WED="Wed", THU="Thu", FRI="Fri", SAT="Sat";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_earninigs);

        activity = WeeklyEarninigsActivity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);


        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_layout_back);
        iv_back = (ImageView)findViewById(R.id.iv_back);
        tv_title = (TextView)findViewById(R.id.tv_title);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.pager);

        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        tv_title.setText(getResources().getText(R.string.weekly_earning));

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
        if (driverId!=null && !driverId.equalsIgnoreCase(""))
        {
            if (ConnectivityReceiver.isConnected())
            {
                GetWeekly_Earning(driverId);
            }
            else
            {
                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet),"No internet connection");
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new TotalEarningFragment(), "Total");
        adapter.addFrag(new RidesEarningFragment(), "Rides");
//        adapter.addFrag(new TiCKPAYEarningFragment(), "TiCKPAY");
        adapter.addFrag(new DispatchEarningFragment(), "Dispatch");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void GetWeekly_Earning(String driverId)
    {
        list_total.clear();
        list_ride.clear();
        list_tickpay.clear();
        list_dispatch.clear();

        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_WEEKLY_EARNING + driverId;

        Log.e("url", "GetWeekly_Earning = " + url);
        Log.e("param", "GetWeekly_Earning = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "GetWeekly_Earning = " + responseCode);
                    Log.e("Response", "GetWeekly_Earning = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String start_date="", end_date="";
                                Log.e("status", "true");

                                if (json.has("start_date"))
                                {
                                    start_date= json.getString("start_date");
                                    SessionSave.saveUserSession(Comman.USER_PREFERENCE_WEEKLY_EARNING_START_DATE, start_date, activity);
                                }
                                if (json.has("end_date"))
                                {
                                    end_date= json.getString("end_date");
                                    SessionSave.saveUserSession(Comman.USER_PREFERENCE_WEEKLY_EARNING_END_DATE, end_date, activity);
                                }
                                if (json.has("earning"))
                                {
                                    String earning = json.getString("earning");
                                    if (earning!=null && !earning.equalsIgnoreCase(""))
                                    {
                                        JSONObject earningObj = json.getJSONObject("earning");
                                        if (earningObj!=null)
                                        {
                                            if (earningObj.has("rides"))
                                            {
                                                String rides= earningObj.getString("rides");
                                                if (rides!=null && !rides.equalsIgnoreCase(""))
                                                {
                                                    JSONObject Object = earningObj.getJSONObject("rides");
                                                    if (Object!=null)
                                                    {
                                                        Calendar c = Calendar.getInstance();
                                                        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                                        if (Calendar.MONDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_ride.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_ride.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_ride.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_ride.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_ride.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_ride.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_ride.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(MON, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.TUESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_ride.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_ride.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_ride.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_ride.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_ride.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_ride.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_ride.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.WEDNESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_ride.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_ride.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_ride.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_ride.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_ride.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_ride.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_ride.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.THURSDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_ride.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_ride.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_ride.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_ride.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_ride.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_ride.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_ride.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(THU, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.FRIDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_ride.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_ride.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_ride.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_ride.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_ride.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_ride.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_ride.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SATURDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_ride.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_ride.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                Log.e("Tue","Tue : "+Tue);
                                                                list_ride.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_ride.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_ride.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_ride.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_ride.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SUNDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_ride.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_ride.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_ride.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_ride.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_ride.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_ride.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_ride.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_ride.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (earningObj.has("tickpay"))
                                            {
                                                String tickpay= earningObj.getString("tickpay");
                                                if (tickpay!=null && !tickpay.equalsIgnoreCase(""))
                                                {
                                                    JSONObject Object = earningObj.getJSONObject("tickpay");
                                                    if (Object!=null)
                                                    {
                                                        Calendar c = Calendar.getInstance();
                                                        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                                        if (Calendar.MONDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.TUESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.WEDNESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.THURSDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.FRIDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SATURDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SUNDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_tickpay.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (earningObj.has("dispatch"))
                                            {

                                                String dispatch= earningObj.getString("dispatch");
                                                if (dispatch!=null && !dispatch.equalsIgnoreCase(""))
                                                {
                                                    JSONObject Object = earningObj.getJSONObject("dispatch");
                                                    if (Object!=null)
                                                    {
                                                        Calendar c = Calendar.getInstance();
                                                        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                                        if (Calendar.MONDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.TUESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.WEDNESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.THURSDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.FRIDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SATURDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SUNDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_dispatch.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (earningObj.has("total"))
                                            {
                                                String total= earningObj.getString("total");
                                                if (total!=null && !total.equalsIgnoreCase(""))
                                                {
                                                    JSONObject Object = earningObj.getJSONObject("total");
                                                    if (Object!=null)
                                                    {
                                                        Calendar c = Calendar.getInstance();
                                                        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                                        if (Calendar.MONDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_total.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_total.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_total.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_total.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_total.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_total.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_total.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(MON, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.TUESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_total.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_total.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_total.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_total.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_total.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_total.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_total.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.WEDNESDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_total.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_total.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_total.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_total.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_total.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_total.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_total.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                        }
                                                        else if (Calendar.THURSDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_total.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_total.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_total.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_total.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_total.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_total.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_total.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(THU, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.FRIDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_total.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_total.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_total.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_total.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_total.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_total.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_total.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SATURDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_total.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_total.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                Log.e("Tue","Tue111111111 : "+Tue);
                                                                list_total.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_total.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_total.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_total.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_total.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }
                                                        }
                                                        else if (Calendar.SUNDAY == dayOfWeek)
                                                        {
                                                            String Sat="", Sun="", Mon="", Tue="", Wed="", Thu="", Fri="";

                                                            if (Object.has("Mon"))
                                                            {
                                                                Mon = Object.getString("Mon");
                                                                list_total.add(new Weekly_Earning_Been(MON, Mon));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(MON, "0"));
                                                            }

                                                            if (Object.has("Tue"))
                                                            {
                                                                Tue = Object.getString("Tue");
                                                                list_total.add(new Weekly_Earning_Been(TUE, Tue));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(TUE, "0"));
                                                            }

                                                            if (Object.has("Wed"))
                                                            {
                                                                Wed = Object.getString("Wed");
                                                                list_total.add(new Weekly_Earning_Been(WED, Wed));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(WED, "0"));
                                                            }

                                                            if (Object.has("Thu"))
                                                            {
                                                                Thu = Object.getString("Thu");
                                                                list_total.add(new Weekly_Earning_Been(THU, Thu));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(THU, "0"));
                                                            }

                                                            if (Object.has("Fri"))
                                                            {
                                                                Fri = Object.getString("Fri");
                                                                list_total.add(new Weekly_Earning_Been(FRI, Fri));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(FRI, "0"));
                                                            }

                                                            if (Object.has("Sat"))
                                                            {
                                                                Sat = Object.getString("Sat");
                                                                list_total.add(new Weekly_Earning_Been(SAT, Sat));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SAT, "0"));
                                                            }

                                                            if (Object.has("Sun"))
                                                            {
                                                                Sun = Object.getString("Sun");
                                                                list_total.add(new Weekly_Earning_Been(SUN, Sun));
                                                            } else {
                                                                list_total.add(new Weekly_Earning_Been(SUN, "0"));
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Log.e("status", "false");
                                            dialogClass.hideDialog();
                                            if (json.has("message"))
                                            {
                                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                                errorDialogClass.showDialog(json.getString("message"), "Error Message");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        if (json.has("message"))
                                        {
                                            ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                            errorDialogClass.showDialog(json.getString("message"), "Error Message");
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                        errorDialogClass.showDialog(json.getString("message"), "Error Message");
                                    }
                                }

                                dialogClass.hideDialog();
                                setupViewPager(viewPager);
                                tabLayout.setupWithViewPager(viewPager);
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                    errorDialogClass.showDialog(json.getString("message"), "Error Message");
                                }
                                setupViewPager(viewPager);
                                tabLayout.setupWithViewPager(viewPager);
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                                errorDialogClass.showDialog(json.getString("message"), "Error Message");
                            }
                            setupViewPager(viewPager);
                            tabLayout.setupWithViewPager(viewPager);
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();

                        ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), "Error Message");

                        setupViewPager(viewPager);
                        tabLayout.setupWithViewPager(viewPager);

                    }
                }
                catch (Exception e)
                {
                    Log.e("GetWeekly_Earning", "Exception : " + e.toString());
                    dialogClass.hideDialog();

                    ErrorDialogClass errorDialogClass = new ErrorDialogClass(activity);
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), "Error Message");
                }
            }
        }.header(WebServiceAPI.HEADER_KEY, WebServiceAPI.HEADER_VALUE));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
    }
}
