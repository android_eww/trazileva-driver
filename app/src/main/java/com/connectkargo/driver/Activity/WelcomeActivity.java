package com.connectkargo.driver.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import me.relex.circleindicator.CircleIndicator;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectkargo.driver.Adapter.WelcomeAdapter;
import com.connectkargo.driver.R;

public class WelcomeActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = "WelcomeActivity";
    public WelcomeActivity activity;

    public TextView tvSkip;
    public ImageView ivNumber;

    private CircleIndicator indicator;
    public ViewPager vpIntro;
    public WelcomeAdapter welcomeAdapter;
    public int[] layouts;
    public int vpCurrentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        activity = WelcomeActivity.this;
        vpCurrentPosition = 0;

        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        tvSkip = findViewById(R.id.tvSkip);
        indicator = findViewById(R.id.indicator);
        vpIntro = findViewById(R.id.vpIntro);
        ivNumber = findViewById(R.id.ivNumber);

        layouts = new int[]{
                R.layout.intro_layout_1,
                R.layout.intro_layout_2,
                R.layout.intro_layout_3};

        welcomeAdapter = new WelcomeAdapter(activity, layouts);
        vpIntro.setAdapter(welcomeAdapter);
        indicator.setViewPager(vpIntro);

        vpIntro.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position)
            {
                if (position == 0)
                {
                    ivNumber.setImageResource(R.mipmap.number_one);
                }
                else if (position == 1)
                {
                    ivNumber.setImageResource(R.mipmap.number_two);
                }
                else
                {
                    ivNumber.setImageResource(R.mipmap.number_three);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

        tvSkip.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.tvSkip:
                callLogin();
                break;
        }
    }

    private void callLogin()
    {
        Log.e(TAG,"callLogin()");

        Intent intent = new Intent(WelcomeActivity.this, Login_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }
}
