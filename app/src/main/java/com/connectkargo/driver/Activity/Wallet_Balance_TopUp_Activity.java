package com.connectkargo.driver.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Comman.WebServiceAPI;
import com.connectkargo.driver.View.DialogClass;
import com.connectkargo.driver.View.SnackbarUtils;
import com.connectkargo.driver.Adapter.CreditCardList_Adapter;
import com.connectkargo.driver.Others.ConnectivityReceiver;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.R;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet_Balance_TopUp_Activity extends BaseActivity implements View.OnClickListener
{
    public static Wallet_Balance_TopUp_Activity activity;

    LinearLayout ll_back, main_layout, select_card_layout, ll_Sos;
    TextView tv_title, tv_selectCard, tv_add_funds, tv_currentBalance;

    Intent intent;
    public static int flagResumeUse=0;
    String ClickcardId="";
    EditText et_topUpAmount;

    private AQuery aQuery;
    private DialogClass dialogClass;
    private Dialog dialog;

    int resumeFlag=1;

    private Animation animation;
    private Animation.AnimationListener animationListener;
    private int clickflag = 0;
    String walletBalance="";

    ErrorDialogClass errorDialogClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance_addfunds);

        activity = Wallet_Balance_TopUp_Activity.this;

        flagResumeUse=0;
        ClickcardId="";
        resumeFlag=1;
        clickflag = 0;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        errorDialogClass = new ErrorDialogClass(activity);

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        tv_title = (TextView)findViewById(R.id.tv_toolbarTitle);

        select_card_layout = (LinearLayout) findViewById(R.id.select_card_layout);
        tv_selectCard = (TextView) findViewById(R.id.tv_selectCard);
        tv_add_funds = (TextView) findViewById(R.id.tv_add_funds);
        tv_currentBalance = (TextView) findViewById(R.id.tv_currentBalance);
        et_topUpAmount = (EditText) findViewById(R.id.et_topUpAmount);

        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);
        ll_Sos.setOnClickListener(this);

        tv_title.setText(getResources().getText(R.string.top_up));


        walletBalance = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE, activity);
        if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
        {
            tv_currentBalance.setText(activity.getResources().getString(R.string.currency) + walletBalance);

            if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
            {
                String mm = activity.getResources().getString(R.string.currency) +String.format("%.2f",Double.parseDouble(walletBalance));
                tv_currentBalance.setText(mm);
            }
            else
            {
                tv_currentBalance.setVisibility(View.GONE);
            }
        }
        else
        {
            tv_currentBalance.setText("--");
        }

        et_topUpAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if(charSequence.length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_topUpAmount.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ll_back.setOnClickListener(this);
        tv_selectCard.setOnClickListener(this);
        select_card_layout.setOnClickListener(this);
        tv_add_funds.setOnClickListener(this);

        animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if (clickflag == 1)
                {

                    intent = new Intent(activity, Wallet_Cards_Activity.class);
                    intent.putExtra("from","TopUp");
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
                else if (clickflag == 2)
                {
                    checkData();
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.tv_selectCard:
                resumeFlag=0;
                clickflag = 1;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                select_card_layout.startAnimation(animation);
                break;

            case R.id.select_card_layout:
                resumeFlag=0;
                clickflag = 1;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                select_card_layout.startAnimation(animation);
                break;

            case R.id.tv_add_funds:
                clickflag = 2;

                animation = AnimationUtils.loadAnimation(activity,R.anim.bounce);
                animation.setAnimationListener(animationListener);
                tv_add_funds.startAnimation(animation);
                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;
        }
    }

    private void checkData()
    {

        if (tv_selectCard.getText().toString().equalsIgnoreCase("Select Card"))
        {
            new SnackbarUtils(main_layout, "Please select card",
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_topUpAmount.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, "Please enter amount",
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
        else
        {
            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    sendMoney(userId);
                }
                else
                {
                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                }
            }
        }
    }

    public void sendMoney(String userId)
    {
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_ADD_MONEY;

        params.put(WebServiceAPI.ADD_MONEY_PARAM_DRIVER_ID,userId);
        params.put(WebServiceAPI.ADD_MONEY_PARAM_AMOUNT,et_topUpAmount.getText().toString());
        params.put(WebServiceAPI.ADD_MONEY_PARAM_CARD_ID,CreditCardList_Adapter.ClickcardId);

        Log.e("sendMoney", "URL = " + url);
        Log.e("sendMoney", "PARAMS = " + params);

        aQuery.ajax(url, params , JSONObject.class , new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("sendMoney", "responseCode = " + responseCode);
                    Log.e("sendMoney", "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if(json.getBoolean("status"))
                            {
                                Log.e("sendMoney","Status = " + "true");
                                dialogClass.hideDialog();

                                if(json.has("walletBalance"))
                                {
                                    walletBalance = json.getString("walletBalance");
                                    Log.e("walletBalance","walletBalance = " + walletBalance);
                                    SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, walletBalance, activity);

                                }

                                if (json.has("message"))
                                {
                                    Wallet_Balance_Activity.transferToBank = true;
                                    showPopup(json.getString("message"), getString(R.string.success_message));
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                errorDialogClass.showDialog(json.getString("message"), getString(R.string.error_message));
                            }

                        }

                    }
                    else
                    {
                        Log.e("sendMoney", "getMessage = "  + "Null");
                        dialogClass.hideDialog();
                        errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));
                    }

                }
                catch (Exception e)
                {
                    Log.e("sendMoney", "Exception = " + e.getMessage() + "somthing_is_wrong");
                    dialogClass.hideDialog();
                    errorDialogClass.showDialog(getString(R.string.something_is_wrong), getString(R.string.error_message));

                }

            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY,WebServiceAPI.HEADER_VALUE));
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }
        if (flagResumeUse==1)
        {
            if (CreditCardList_Adapter.ClickecardNumber!=null && !CreditCardList_Adapter.ClickecardNumber.equalsIgnoreCase(""))
            {
                tv_selectCard.setText(CreditCardList_Adapter.ClickecardNumber);
                ClickcardId = CreditCardList_Adapter.ClickcardId;
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }
                if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                {
                    Wallet_Balance_TransferToBank_Activity.activity.finish();
                }
                if (Wallet_Cards_Activity.activity!=null)
                {
                    Wallet_Cards_Activity.activity.finish();
                }
                if (Wallet_Transfer_Activity.activity!=null)
                {
                    Wallet_Transfer_Activity.activity.finish();
                }
                if (Wallet_Transfer_History_Activity.activity!=null)
                {
                    Wallet_Transfer_History_Activity.activity.finish();
                }
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    private void showPopup(String message,String title)
    {

        dialog = new Dialog(activity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_information);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.dialog_ok_textview);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.dialog_message);
        TextView tv_Title = (TextView) dialog.findViewById(R.id.dialog_title);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.dialog_ok_layout);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.dialog_close);

        tv_Message.setText(message);
        tv_Title.setText(title);

        ll_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        finish();
                    }
                },400);
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        finish();
                    }
                },400);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        finish();
                    }
                },400);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        walletBalance = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE, activity);
        if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
        {
            tv_currentBalance.setText(activity.getResources().getString(R.string.currency) + walletBalance);

            if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
            {
                String mm = activity.getResources().getString(R.string.currency) + String.format("%.2f",Double.parseDouble(walletBalance));
                tv_currentBalance.setText(mm);
            }
            else
            {
                tv_currentBalance.setVisibility(View.GONE);
            }
        }
        else
        {
            tv_currentBalance.setText("--");
        }


        dialog.show();

    }
}