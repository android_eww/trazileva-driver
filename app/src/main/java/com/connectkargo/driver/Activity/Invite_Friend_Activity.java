package com.connectkargo.driver.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connectkargo.driver.Application.CabRideDriverApplication;
import com.connectkargo.driver.Comman.CallSos;
import com.connectkargo.driver.Comman.Comman;
import com.connectkargo.driver.Comman.SessionSave;
import com.connectkargo.driver.Others.ErrorDialogClass;
import com.connectkargo.driver.Others.Global;
import com.connectkargo.driver.R;
import com.connectkargo.driver.View.SnackbarUtils;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class Invite_Friend_Activity extends BaseActivity implements View.OnClickListener{

    public static Invite_Friend_Activity activity;

    LinearLayout ll_back, main_layout, ll_Sos;

    CircleImageView iv_prof_inviteDrive;

    RelativeLayout ll_faceBook;
    //    LinearLayout ll_twitter, ll_email, ll_whatsApp, ll_SMS;
    TextView tv_ReferralAmount;
    TextView tv_ReferralCode, tv_toolbarTitle, tv_more_option;;

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;

    String ReferralCode, ReferralAmount, profileImage="";
    Transformation mTransformation;
    ProgressBar progBar_Image;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100, RESULT_PICK_CONTACT = 800;;
    private ListView lstNames;
    Spanned message ;
    private Animation animation;
    ErrorDialogClass errorDialogClass;

    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_driver);

        activity = Invite_Friend_Activity.this;
        errorDialogClass = new ErrorDialogClass(activity);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.appColor))
                .borderWidthDp(2)
                .oval(true)
                .build();

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        tv_toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        tv_toolbarTitle.setText("Invite Drivers");

        progBar_Image = (ProgressBar) findViewById(R.id.progressBar);

        ll_faceBook = (RelativeLayout) findViewById(R.id.ll_faceBook);
//        ll_twitter = (LinearLayout) findViewById(R.id.ll_twitter);
//        ll_email = (LinearLayout) findViewById(R.id.ll_email);
//        ll_whatsApp = (LinearLayout) findViewById(R.id.ll_whatsApp);
//        ll_SMS = (LinearLayout) findViewById(R.id.ll_SMS);

        tv_ReferralAmount = (TextView) findViewById(R.id.tv_ReferralAmount);
        tv_ReferralCode = (TextView) findViewById(R.id.tv_ReferralCode);
        tv_more_option = (TextView) findViewById(R.id.tv_more_option);

        iv_prof_inviteDrive = (CircleImageView) findViewById(R.id.iv_prof_inviteDrive);
        ll_Sos = (LinearLayout) findViewById(R.id.ll_Sos);

        ReferralCode = SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity);
        ReferralAmount = SessionSave.getUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, activity);
        profileImage = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);

        if (ReferralCode!=null && !ReferralCode.equalsIgnoreCase(""))
        {
            tv_ReferralCode.setText(ReferralCode);
        }

        if (ReferralAmount!=null &&
                !ReferralAmount.equalsIgnoreCase("") &&
                !ReferralAmount.equalsIgnoreCase("null"))
        {
//            ReferralAmount = String.format("%.2f", ReferralAmount);
            Log.e("ReferralAmount","ReferralAmount : "+ReferralAmount);

            tv_ReferralAmount.setText(getResources().getString(R.string.currency) +
                    String.format("%.2f",Double.parseDouble(ReferralAmount)));
        }
        else
        {
            tv_ReferralAmount.setText(getResources().getString(R.string.currency) + "0.00");
        }

        if (profileImage != null && !profileImage.equalsIgnoreCase(""))
        {
            progBar_Image.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(profileImage)
                    .into(iv_prof_inviteDrive, new Callback() {

                        @Override
                        public void onSuccess()
                        {
                            progBar_Image.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e)
                        {
                            iv_prof_inviteDrive.setImageResource(R.drawable.cab_dummy_profile);
                            progBar_Image.setVisibility(View.GONE);
                        }
                    });
        }
        else
        {
            iv_prof_inviteDrive.setImageResource(R.drawable.cab_dummy_profile);
        }

        ll_back.setOnClickListener(this);
        ll_faceBook.setOnClickListener(this);
        tv_more_option.setOnClickListener(activity);
//        ll_twitter.setOnClickListener(this);
//        ll_email.setOnClickListener(this);
//        ll_whatsApp.setOnClickListener(this);
//        ll_SMS.setOnClickListener(this);
        ll_Sos.setOnClickListener(this);

       /* message = Html.fromHtml("<p>"+SessionSave.getUserSession(Comman.USER_FULL_NAME, activity)+" has invited you to become a <b>PickNGo</b> Driver.</p>"
                +"<a href=\"https://goo.gl/y4xSuP\">click here https://goo.gl/y4xSuP</a>"
                + "<p>Your invite code is : "+SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity)+"</p>"
                +"<a href=\"www..net\">www..net</a><br/>"
                +"<a href=\"www.facebook.com/.net\">www.facebook.com/.net</a>");*/

        message = Html.fromHtml("<p>"+SessionSave.getUserSession(Comman.USER_FULL_NAME, activity)+" has invited you to become a <b>Cab Ride</b> Driver.</p>"
                +"<a href=\"https://play.google.com/store/apps/details?id=com.Apurple.cabridefull\">click here https://play.google.com/store/apps/details?id=com.Apurple.cabridefull</a>"
                + "<p>Your invite code is : "+SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity)+"</p>"
                +"<a href=\"https://www.apurple.co/\">https://www.apurple.co/</a>");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_faceBook:
                Open_FaceBook();
                break;

//            case R.id.ll_twitter:
//                Open_Twitter();
//                break;
//
//            case R.id.ll_email:
//                Open_Email();
//                break;
//
//            case R.id.ll_whatsApp:
//                Open_Whatsapp();
//                break;
//
//            case R.id.ll_SMS:
//                OpenSMS();
//                break;

            case R.id.ll_Sos:
                new CallSos(activity);
                break;

            case R.id.tv_more_option:
                animation = AnimationUtils.loadAnimation(this,R.anim.bounce);
                animation.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation)
                    {
                        Log.e("call","onAnimationStart");

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        Log.e("call","onAnimationEnd");
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if (Global.isNetworkconn(activity))
                                {
                                    call_more_option();
                                }
                                else
                                {
                                    errorDialogClass.showDialog(getResources().getString(R.string.not_connected_to_internet), getString(R.string.internet_error_message));
                                }
                            }
                        },100);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation)
                    {
                        Log.e("call","onAnimationRepeat");
                    }
                });
                tv_more_option.startAnimation(animation);
                break;


        }
    }

    private void call_more_option()
    {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        startActivity(Intent.createChooser(shareIntent, "Share Via"));
    }


    private void Open_FaceBook()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.facebook.katana");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        whatsappIntent.putExtra(Intent.EXTRA_TEXT   , message.toString());
        try
        {
            activity.startActivity(whatsappIntent);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            new SnackbarUtils(main_layout, getResources().getString(R.string.facebook_is_not_install),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
    }

    private void Open_Twitter()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.twitter.android");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        try
        {
            activity.startActivity(whatsappIntent);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            new SnackbarUtils(main_layout, getResources().getString(R.string.twitter_is_not_install),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
    }

    private void Open_Email()
    {
        try
        {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, "developer.eww@gmail.com");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Cab Ride Driver");
            intent.putExtra(Intent.EXTRA_TEXT   , message.toString());
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        catch (android.content.ActivityNotFoundException e)
        {
            Log.e("kkkkkkkk","kkkkkkkkkkkk "+e.toString());
            new SnackbarUtils(main_layout, getResources().getString(R.string.email_send_error),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
    }

    private void Open_Whatsapp()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        try
        {
            activity.startActivity(whatsappIntent);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            new SnackbarUtils(main_layout, getResources().getString(R.string.whats_app_is_not_install),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
        }
    }

    private void OpenSMS()
    {
        Log.e("Send SMS", "etsrhftmfymuytmym");
        showContacts();
    }
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        }
        else
        {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
        }
    }


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("MainActivity", "onActivityResult");
        if (resultCode == RESULT_OK) {
            Log.e("MainActivity", "RESULT_OK");
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case RESULT_PICK_CONTACT:

                    Log.e("MainActivity", "contactPicked");
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }
    /**
     * Query the Uri and read contact details. Handle the picked contact data.
     * @param data
     */
    private void contactPicked(Intent data)
    {
        Cursor cursor = null;
        try {
            String phoneNo = "" ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
//            textView1.setText(name);
//            textView2.setText(phoneNo);
            Log.e("contactPicked","name : "+name);
            Log.e("contactPicked","phoneNo : "+phoneNo);


            if (phoneNo!=null && !phoneNo.equalsIgnoreCase(""))
            {
                OpenSMS(phoneNo);
               /* try
                {
                    Log.e("message.toString()","message.toString() : "+message.toString());
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNo, null, message.toString(), null, null);
                    new SnackbarUtils(main_layout,"message sent",
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                }
                catch (Exception e)
                {
                    new SnackbarUtils(main_layout, getResources().getString(R.string.sms_failed),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
                    Log.e("kkkkkkkk","kkkkkkkkkkkk "+e.toString());
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OpenSMS(String phoneNo) {

        final String phone = phoneNo.trim().replace(" ","");

        String message = SessionSave.getUserSession(Comman.USER_FULL_NAME, activity)+" has invited you to become a Cab Ride Driver."
            /*    + "\nclick here https://goo.gl/y4xSuP" +
                "\nYour invite code is: " + SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity) */;

        Log.e("call","phone number = "+phone);
        Log.e("call","message = "+message);
        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
        } else {

            final SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (final String msg : messages) {

                final PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                final PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);

                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);
                    }
                }, 1000);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            CabRideDriverApplication.setCurrentActivity(activity);
        }

        sentStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Unknown Error";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Sent Successfully !!";
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        s = "Generic Failure Error";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        s = "Error : No Service Available";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        s = "Error : Null PDU";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        s = "Error : Radio is off";
                        break;
                    default:
                        break;
                }
                new SnackbarUtils(main_layout, s+"",
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            }
        };

        deliveredStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Message Not Delivered";
                switch(getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Delivered Successfully";
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                new SnackbarUtils(main_layout, s+"",
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color), activity).snackieBar().show();
            }
        };
        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(sentStatusReceiver);
        unregisterReceiver(deliveredStatusReceiver);
    }
}
